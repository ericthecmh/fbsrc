package main;

import gui.DreamRangeGuildGUI;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.util.HashMap;

import org.osbot.rs07.antiban.AntiBan.BehaviorType;
import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.Entity;
import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.api.ui.Message;
import org.osbot.rs07.api.ui.Skill;
import org.osbot.rs07.api.util.GraphicUtilities;
import org.osbot.rs07.input.mouse.EntityDestination;
import org.osbot.rs07.input.mouse.MouseDestination;
import org.osbot.rs07.input.mouse.RectangleDestination;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.script.ScriptManifest;
import org.osbot.rs07.utility.Area;

import Library.CameraSetting;
import Library.Shop;
import Library.ShopItem;

@ScriptManifest(author = "Ericthecmh & Eliot", info = "Best RangeGuild Script on OSBot!", logo = "", name = "DreamRangeGuild v0.0.6", version = 0.06)
public class DreamRangeGuild extends Script {

	// CONSTANTS
	public enum State {
		SHOOT, START_GAME, EXCHANGE
	}

	// paint stuff
	private final Color black = new Color(0, 0, 0, 165);
	private final Color green = new Color(51, 255, 51);
	private final Font paintFont = new Font("Arial", 0, 11);
	private final BasicStroke stroke = new BasicStroke(1.0F);
	private final Color green_outline = new Color(51, 255, 51, 125);

	// anti ban stuff
	private long AFK = 0;
	int time = 1;
	
	
	long runTime;
	long startTime;
	private int rangeExp;
	private String itemToBuy;
	private int startTickets = 0;
	private HashMap<Position, CameraSetting> cameraValues = new HashMap<Position, CameraSetting>();

	// current state of the script
	protected State state;
	public static boolean canStart = false;
	public static String reward;
	public static String activity;
	private Area gameArea = new Area(2667, 3413, 2672, 3423);

	private Shop shop;

	Position cP;

	@Override
	public void onStart() throws InterruptedException {
		DreamRangeGuildGUI gui = new DreamRangeGuildGUI();
		gui.setVisible(true);
		while (gui.isVisible()) {
			sleep(100);
		}
		startTime = System.currentTimeMillis();
		rangeExp = skills.getExperience(Skill.RANGED);
		shop = new Shop(this, "Ticket Merchant");
		if (activity.equals("Exchange Tickets")) {
			state = State.EXCHANGE;
		} else {
			state = State.SHOOT;
		}
		if (inventory.contains("Archery ticket"))
			startTickets = (int) inventory.getAmount("Archery ticket");
		cP = myPosition();
		setCamera();
		antiBan.unregisterBehavior(BehaviorType.CAMERA_SHIFT);
		antiBan.unregisterBehavior(BehaviorType.OTHER);
		antiBan.unregisterBehavior(BehaviorType.RANDOM_MOUSE_MOVEMENT);
		antiBan.unregisterBehavior(BehaviorType.SLIGHT_MOUSE_MOVEMENT);
		cameraValues.put(new Position(2672, 3421, 0),
				new CameraSetting(347, 22));
		cameraValues.put(new Position(2672, 3420, 0),
				new CameraSetting(353, 33));
		cameraValues.put(new Position(2673, 3420, 0), new CameraSetting(1, 38));
		cameraValues.put(new Position(2673, 3419, 0), new CameraSetting(1, 31));
		cameraValues.put(new Position(2673, 3418, 0), new CameraSetting(7, 31));
		cameraValues
				.put(new Position(2675, 3418, 0), new CameraSetting(19, 36));
		cameraValues
				.put(new Position(2674, 3417, 0), new CameraSetting(10, 26));
		cameraValues.put(new Position(2672, 3419, 0),
				new CameraSetting(358, 28));
		cameraValues.put(new Position(2671, 3420, 0),
				new CameraSetting(350, 28));
		cameraValues.put(new Position(2671, 3419, 0),
				new CameraSetting(351, 27));
		cameraValues.put(new Position(2671, 3419, 0),
				new CameraSetting(351, 27));
		cameraValues.put(new Position(2673, 3417, 0), new CameraSetting(6, 25));
		cameraValues.put(new Position(2673, 3416, 0), new CameraSetting(9, 26));
		cameraValues.put(new Position(2672, 3417, 0), new CameraSetting(3, 25));
		cameraValues.put(new Position(2671, 3418, 0),
				new CameraSetting(356, 26));
		cameraValues.put(new Position(2670, 3418, 0),
				new CameraSetting(350, 27));
		cameraValues.put(new Position(2670, 3416, 0),
				new CameraSetting(353, 22));
		cameraValues.put(new Position(2671, 3417, 0),
				new CameraSetting(357, 24));
		cameraValues.put(new Position(2670, 3419, 0),
				new CameraSetting(347, 24));
		cameraValues.put(new Position(2670, 3417, 0),
				new CameraSetting(352, 23));
		cameraValues.put(new Position(2670, 3420, 0),
				new CameraSetting(341, 24));
		cameraValues.put(new Position(2669, 3420, 0),
				new CameraSetting(341, 24));
		
		//antiban randomization
		generateAFK();
		canStart = true;
	}

	private void generateAFK() {
		AFK = AFK + random((int)(3600000/7), 3600000);
		time++;
		
	}

	@Override
	public int onLoop() throws InterruptedException {
		// log("position: " + myPosition().toString() + " yaw: " +
		// camera.getYawAngle() + " pitch: " +
		// camera.getPitchAngle() + " angle: " + camera.getX() + " " +
		// camera.getY() + " " + camera.getZ());

		while (myPlayer().isUnderAttack() || myPosition().getZ() > 0)
			evade();
		switch (state) {
		case SHOOT:
			if (cP != myPosition()) {
				setCamera();
				cP = myPosition();
			}
			shootTarget();
			antiBan();
			break;
		case START_GAME:
			startGame();
			break;

		case EXCHANGE:
			exchange(reward);
			break;
		}

		return random(160, 200);
	}

	private void evade() {
		Entity ladder = objects.closest("Tower ladder");
		if (ladder != null && ladder.interact("Climb-up")) {
			try {
				sleep(random(5000,10000));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (myPosition().getZ() > 0) {
			ladder = objects.closest("Tower ladder");
			if (ladder != null && ladder.interact("Climb-down")) {
				try {
					sleep(random(3000,5500));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		
	}

	private void antiBan() {
		if (runTime > AFK) {
			mouse.moveOutsideScreen();
			 try {
				sleep(random(10000,30000));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 generateAFK();
		}
		
	}

	private void exchange(String name) throws InterruptedException {
		log("" + shop.isOpen() + " " + shop.validate() + " "
				+ (shop.getItemByName("Rune arrow") == null));
		if (shop.isOpen()) {
			RectangleDestination ad = null;
			switch (name) {
			case "Rune arrow":
				ad = new RectangleDestination(this.bot, 177, 69, 20, 20);
				break;
			case "Barb bolttips":
				ad = new RectangleDestination(this.bot, 82, 72, 20, 20);
				break;
			case "Studded body":
				ad = new RectangleDestination(this.bot, 132, 70, 20, 20);
				break;
			case "Coif":
				ad = new RectangleDestination(this.bot, 224, 69, 20, 20);
				break;
			case "Green d'hide body":
				ad = new RectangleDestination(this.bot, 274, 69, 20, 20);
				break;
			case "Adamant javelin":
				ad = new RectangleDestination(this.bot, 316, 69, 20, 20);
				break;

			}
			ShopItem.doMenu("Buy", ad, this);
			sleep(random(300, 500));
		} else {
			shop.tryOpen();
			sleep(1000);
		}

	}

	private void setCamera() {
		CameraSetting cv = cameraValues.get(myPosition());
		if (cv != null) {
			camera.moveYaw(cv.getYaw());
			camera.movePitch(cv.getPitch());
		} else if (!gameArea.contains(myPosition())) {
			localWalker.walk(2671, 3418);
		}

	}

	private void startGame() throws InterruptedException {
		if (shouldLogOut())
			stop();
		if (interfaces.getChild(242, 2) != null
				&& (interfaces.getChild(242, 2).getMessage()
						.contains("score") || interfaces.getChild(242,
						2) != null
						&& interfaces.getChild(242, 2).getMessage()
								.contains("Sorry"))) {
			NPC judge = npcs.closest("Competition Judge");
			MouseDestination judgeD = new EntityDestination(bot, judge);
			if (isVisibleOnMainScreen(judgeD.getBoundingBox())) {
				mouse.move(judgeD);
				sleep(500);
				if (mouse.getEntitiesOnCursor().size() == 1 &&
						mouse.getEntitiesOnCursor().get(0).getName().equals("Competition Judge"))
					mouse.click(judgeD);
				else
					judge.interact("Talk-to");
				sleep(800);
			}
			else
				judge.interact("Talk-to");
			sleep(800);
		}
		NPC judge = npcs.closest("Competition Judge");
		if (judge != null && interfaces.get(241).getChild(3) == null
				&& interfaces.get(64).getChild(3) == null
				&& interfaces.get(230).getChild(1) == null
				&& interfaces.get(242).getChild(4) == null) {
			MouseDestination judgeD = new EntityDestination(bot, judge);
			if (isVisibleOnMainScreen(judgeD.getBoundingBox())) {
				mouse.move(judgeD);
				sleep(500);
				if (mouse.getEntitiesOnCursor().size() == 1 &&
						mouse.getEntitiesOnCursor().get(0).getName().equals("Competition Judge"))
					mouse.click(judgeD);
				else
					judge.interact("Talk-to");
				sleep(800);
			}
			else
				judge.interact("Talk-to");
			sleep(800);
		}
		if (interfaces.get(241).getChild(3) != null) {
			dialogues.clickContinue();
			sleep(500);
			state = State.SHOOT;
			return;
		} else if (interfaces.get(230).getChild(1) != null) {
			interfaces.getChild(230, 1).interact("Continue");
			sleep(random(100, 200));
		} else if (interfaces.get(241).getChild(3) == null) {
			dialogues.clickContinue();
		}
		sleep(random(100, 200));
	}

	private boolean shouldLogOut() {
		if (!inventory.contains("Coins"))
			if (tabs.logoutTab.logOut())
				return true;
		return false;

	}

	public void shootTarget() {
		if (gameArea.contains(myPosition())) {
			if (interfaces.get(241).getChild(2) != null
					&& interfaces.get(241).getChild(2).getMessage()
							.contains("10 bronze")) {
				inventory.interact("Wield", "Bronze arrow");
				try {
					sleep(random(400, 500));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				Entity target = objects.closest("Target");
				if (target != null) {
					MouseDestination md = new EntityDestination(this.bot,
							target);
					if (//isVisibleOnMainScreen(md.getBoundingBox()) 
					       md.isVisible() || interfaces.get(325).getChild(89) != null) {
						mouse.move(md);	
						if (menu.getMenu() != null && menu.getMenu().get(0).action.contains("Fire-at"))
							mouse.click(md);
						else if (interfaces.get(325).getChild(89) == null) {
							mouse.move(getRandomDest(md));
						}
					}
					else {
						camera.toEntity(target);
					}
				}
				if (interfaces.get(325).getChild(89) != null) {
					MouseDestination id = new RectangleDestination(this.bot,
							480, 36, 13, 13);
					mouse.click(id);
				}
				if (interfaces.getChild(242, 2) != null
						&& (interfaces.getChild(242, 2).getMessage()
								.contains("score") || interfaces.getChild(242,
								2) != null
								&& interfaces.getChild(242, 2).getMessage()
										.contains("Sorry"))) {
					state = State.START_GAME;
				}
			}
		} else {
			localWalker.walk(gameArea.getRandomPosition(0));
		}
	}

	public static void setReward(String reward1) {
		reward = reward1;
	}

	public static void setActivity(String activity1) {
		activity = activity1;
	}
	
	//return a random rectangle destination based on a entity destination
	public RectangleDestination getRandomDest(MouseDestination ed){
		RectangleDestination rd = null;
		Rectangle r = ed.getBoundingBox();
		Rectangle r1 = null;
		r1 = new Rectangle((int)r.getMinX() + random(0,(int)r.getMaxX()), (int)r.getMinY() + random(0,(int)r.getMaxY()), 2, 2);
		rd = new RectangleDestination(bot, r1);
		return rd;
	 }
	

	/**
	 * Draws the paint
	 */
	@Override
	public void onPaint(Graphics2D gr) {
		if (canStart) {
			// paint
			int tickets = 0;
			if (inventory.contains("Archery ticket")) {
				tickets = (int) (inventory.getAmount("Archery ticket") - startTickets);
			}
			int totalExp = skills.getExperience(Skill.RANGED) - rangeExp;
			runTime = System.currentTimeMillis() - startTime;
			gr.setColor(black);
			gr.fillRoundRect(7, 307, 503, 30, 6, 6);
			gr.setColor(green_outline);
			gr.drawRoundRect(7, 307, 503, 30, 6, 6);
			// text
			gr.setStroke(this.stroke);
			gr.setColor(green);
			gr.setFont(paintFont);
			gr.drawString("Runtime: " + format(runTime), 14, 318);
			if (state != null)
				gr.drawString("State: " + state.toString(), 14, 332);

			gr.drawString("Total EXP: " + totalExp, 182, 318);
			gr.drawString("Tickets: " + tickets, 342, 318);

			gr.setRenderingHints(new RenderingHints(
					RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_ON));

			gr.setColor(green);
			gr.setStroke(this.stroke);
			gr.setFont(paintFont);
			gr.drawString("EXP/hour: "
					+ (int) (totalExp * (3600000.0D / runTime)), 182, 332);

			gr.drawString("Tickets/hr: "
					+ (int) (tickets * (3600000.0D / runTime)), 342, 332);
		}

	}

	String format(final long time) {
		final StringBuilder t = new StringBuilder();
		final long total_secs = time / 1000;
		final long total_mins = total_secs / 60;
		final long total_hrs = total_mins / 60;
		final int secs = (int) total_secs % 60;
		final int mins = (int) total_mins % 60;
		final int hrs = (int) total_hrs;
		if (hrs < 10)
			t.append("0");
		t.append(hrs + ":");
		if (mins < 10)
			t.append("0");
		t.append(mins + ":");
		if (secs < 10) {
			t.append("0");
			t.append(secs);
		} else
			t.append(secs);
		return t.toString();
	}

	public void onMessage(Message m) throws InterruptedException {
		if (m.getMessage().contains("those bronze arrows"))
			if (inventory.contains("Bronze arrow")) {
				inventory.interact("Wield", "Bronze arrow");
				sleep(random(400, 500));
			}
	}
	
	public static boolean isVisibleOnMainScreen(Rectangle rectangle) {
        if (rectangle == null) {
            return false;
        }
        Rectangle intersection = rectangle.intersection(GraphicUtilities.MAIN_SCREEN_CLIP);
        return isVisibleOnMainScreen(intersection.getLocation());
    }

    public static boolean isVisibleOnMainScreen(java.awt.geom.Area area) {
        if (area == null || area.isEmpty())
            return false;
        java.awt.geom.Area partialArea = (java.awt.geom.Area) area.clone();
        partialArea.intersect(new java.awt.geom.Area(GraphicUtilities.MAIN_SCREEN_CLIP));
        Rectangle fullRect = partialArea.getBounds();
        return isVisibleOnMainScreen(fullRect.getLocation());
    }
    public static boolean isVisibleOnMainScreen(int x, int y) {
        return GraphicUtilities.MAIN_SCREEN_CLIP.contains(x, y);
    }

    public static boolean isVisibleOnMainScreen(Point p) {
        return GraphicUtilities.MAIN_SCREEN_CLIP.contains(p);
    }

}
