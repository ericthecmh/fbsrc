package gui;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JButton;

import main.DreamRangeGuild;

public class DreamRangeGuildGUI extends JFrame {
	public DreamRangeGuildGUI() {
		final JFrame guiFrame = new JFrame("DreamRangeGuild");
		guiFrame.setSize(221, 136);
		guiFrame.getContentPane().setLayout(null);

		JLabel lblActivity = new JLabel("Activity");
		lblActivity.setBounds(10, 11, 46, 14);
		guiFrame.getContentPane().add(lblActivity);

		final JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.setBounds(66, 8, 125, 20);
		guiFrame.getContentPane().add(comboBox);

		// add options
		comboBox.addItem("Play Game");
		comboBox.addItem("Exchange Tickets");

		JLabel lblReward = new JLabel("Reward");
		lblReward.setBounds(10, 39, 46, 14);
		guiFrame.getContentPane().add(lblReward);

		final JComboBox<String> comboBox_1 = new JComboBox<String>();
		comboBox_1.setBounds(66, 36, 125, 20);
		guiFrame.getContentPane().add(comboBox_1);

		// add options
		comboBox_1.addItem("Rune arrow");
		comboBox_1.addItem("Barb bolttips");
		comboBox_1.addItem("Studded body");
		comboBox_1.addItem("Coif");
		comboBox_1.addItem("Green d'hide body");
		comboBox_1.addItem("Adamant javelin");

		JButton btnStart = new JButton("Start");
		btnStart.setBounds(10, 74, 186, 23);

		btnStart.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				DreamRangeGuild.setActivity(comboBox.getSelectedItem()
						.toString());
				DreamRangeGuild.setReward(comboBox_1.getSelectedItem()
						.toString());
				guiFrame.setVisible(false);
				dispose();

			}

		});

		guiFrame.getContentPane().add(btnStart);
		guiFrame.setVisible(true);
	}

}
