package Library;

public class CameraSetting {

	int yaw;
	int pitch;

	public CameraSetting(int yaw, int pitch) {
		this.yaw = yaw;
		this.pitch = pitch;
	}

	public int getYaw() {
		return yaw;
	}

	public int getPitch() {
		return pitch;
	}
}
