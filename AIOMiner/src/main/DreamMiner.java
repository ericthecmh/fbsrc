package main;

import gui.AIOMinerGUI;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.StringTokenizer;

import library.HashPosition;
import library.Library;
import library.LootItem;
import library.PriceFetcher;
import library.WorldHopping;
import miners.AlkharidMiner;
import miners.CraftingGuildMiner;
import miners.HeroesGuildRuniteRocksMiner;
import miners.LumbridgeSwampMiner;
import miners.Miner;
import miners.MiningGuildMiner;
import miners.PowerMiner;
import miners.RimmingtonMiner;
import miners.VarrockEastMiner;
import miners.VarrockEssenceMiner;
import miners.VarrockWestMiner;
import miners.WildernessRuniteRocksMiner;
import miners.YanilleMiner;

import org.osbot.BotApplication;
import org.osbot.rs07.antiban.AntiBan.BehaviorType;
import org.osbot.rs07.api.Client.LoginState;
import org.osbot.rs07.api.filter.Filter;
import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.Entity;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.Message;
import org.osbot.rs07.api.ui.Skill;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.script.ScriptManifest;

import randomstracker.RandomTracker;
import chat.ChatWindow;
import chat.FriendMessage;

/**
 * 
 * Class loads the GUI and then calls the methods in the appropriate class
 * 
 * @author Ericthecmh
 * 
 */

@ScriptManifest(author = "Ericthecmh & Eliot", info = "Best AIO Miner ever!\nMade by Ericthecmh and Eliot\nVersion 0.2.0", logo = "", name = "DreamMiner v0.2.0", version = 0.20)
public class DreamMiner extends Script {

	public final static boolean USE_CUSTOM_INTERACT = false;

	public static double antibanRate = 0.005;

	private Script miner;
	private boolean stopScript;
	private boolean canStart;
	private int oldCoalCount = 0;
	private int oldMithrilCount = 0;
	private int oldAdamantiteCount = 0;
	private int oldRuniteCount = 0;
	private int oldCopperCount = 0;
	private int oldTinCount = 0;
	private int oldClayCount = 0;
	private int oldIronCount = 0;
	private int oldGraniteCount = 0;
	private int oldSilverCount = 0;
	private int oldGoldCount = 0;
	private int coalMined = 0;
	private int mithrilMined = 0;
	private int adamantiteMined = 0;
	private int runiteMined = 0;
	private int copperMined = 0;
	private int tinMined = 0;
	private int clayMined = 0;
	private int ironMined = 0;
	private int graniteMined = 0;
	private int silverMined = 0;
	private int goldMined = 0;
	private long oresMined;
	private int coalPrice;
	private int mithrilPrice;
	private int adamantitePrice;
	private int runitePrice;
	private int copperPrice;
	private int tinPrice;
	private int clayPrice;
	private int ironPrice;
	private int granitePrice;
	private int silverPrice;
	private int goldPrice;
	String username = "Ericthecmh";
	private ChatWindow chatWindow;
	public final WorldHopping worldHopping = new WorldHopping(this);

	// DO NOT TOUCH THIS
	public final static String ip3 = "179";

	public static final int SMOKING_ROCK = 100;

	public ArrayList<LootItem> lootList = new ArrayList<LootItem>();
	public ArrayList<String> stringList = new ArrayList<String>();
	// paint stuff
	private final Color black = new Color(0, 0, 0, 165);
	private final Color green = new Color(51, 255, 51);
	private final Font paintFont = new Font("Arial", 0, 11);
	private final BasicStroke stroke = new BasicStroke(1.0F);
	private final Color green_outline = new Color(51, 255, 51, 125);
	boolean guiFinished = false;
	long startTime;
	private boolean window2 = false;
	int miningExperience = 0;
	private AIOMinerGUI gui;

	private boolean shouldAFK = false;
	private long lastAFKTime;
	private int AFKTimeDelay;

	public class BotMouseListener implements MouseListener {
		@Override
		public void mouseClicked(MouseEvent e) {
			if (e.getPoint().x >= 353 && e.getPoint().x <= 402
					&& e.getPoint().y >= 319 && e.getPoint().y <= 335
					&& !window2) {
				window2 = true;
			}
			if (e.getPoint().x >= 407 && e.getPoint().x <= 455
					&& e.getPoint().y >= 457 && e.getPoint().y <= 472
					&& window2) {
				window2 = false;
			}
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub

		}

	}

	@Override
	public void onStart() {
		log("Welcome to DreamMiner!");
		log("Brought to you by the great scripters Eliot and Ericthecmh");
		log("Fetching prices...");
		new Thread(new Runnable() {
			public void run() {
				try {
					coalPrice = PriceFetcher.getPrice("Coal");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					mithrilPrice = PriceFetcher.getPrice("Mithril ore");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					adamantitePrice = PriceFetcher.getPrice("Adamantite ore");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					runitePrice = PriceFetcher.getPrice("Runite ore");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					copperPrice = PriceFetcher.getPrice("Copper ore");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					tinPrice = PriceFetcher.getPrice("Tin ore");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					clayPrice = PriceFetcher.getPrice("Clay");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					ironPrice = PriceFetcher.getPrice("Iron ore");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					granitePrice = PriceFetcher.getPrice("Granite");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					silverPrice = PriceFetcher.getPrice("Silver ore");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					goldPrice = PriceFetcher.getPrice("Gold ore");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).start();
		try {
			sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		antiBan.unregisterBehavior(BehaviorType.CAMERA_SHIFT);
		antiBan.unregisterBehavior(BehaviorType.OTHER);
		antiBan.unregisterBehavior(BehaviorType.RANDOM_MOUSE_MOVEMENT);
		antiBan.unregisterBehavior(BehaviorType.SLIGHT_MOUSE_MOVEMENT);

		// exp tracker
		MouseListener bml = new BotMouseListener();
		bot.addMouseListener(bml);
		miningExperience = skills.getExperience(Skill.MINING);

		// gui
		gui = new AIOMinerGUI();
		gui.setVisible(true);
		HashSet<HashPosition> set = new HashSet<HashPosition>();
		HashSet<HashPosition> set2 = new HashSet<HashPosition>();
		while (gui.isVisible()) {
			// Powermining
			try {
				if (gui.locationList.getSelectedIndex() == gui.locationList
						.getModel().getSize() - 2) {
					if (gui.powermineSelect.isSelected()) {
						List<Entity> entities = mouse.getEntitiesOnCursor();
						for (Entity e : entities) {
							if (e != null && e.getName().equals("Rocks")) {
								HashPosition hp = new HashPosition(
										e.getPosition());
								if (set.contains(hp)) {
									set.remove(hp);
									for (int i = 0; i < gui.powermineList
											.getModel().getSize(); i++) {
										if (gui.powermineLocations.get(i)
												.equals(hp)) {
											gui.powermineLocations.remove(i);
											break;
										}
									}
								} else {
									set.add(hp);
									gui.powermineLocations.addElement(hp);
								}
							}
						}
					}
					Thread.sleep(500);
				} else {
					gui.powermineLocations.clear();
					set.clear();
				}
				if (gui.mineModel.size() == 1
						&& ((String) gui.mineModel.get(0)).equals("Custom")) {
					if (gui.customSelect.isSelected()) {
						List<Entity> entities = mouse.getEntitiesOnCursor();
						for (Entity e : entities) {
							if (e != null && e.getName().equals("Rocks")) {
								HashPosition hp = new HashPosition(
										e.getPosition());
								if (set2.contains(hp)) {
									set2.remove(hp);
									for (int i = 0; i < gui.customList
											.getModel().getSize(); i++) {
										if (gui.customLocations.get(i).equals(
												hp)) {
											gui.customLocations.remove(i);
											break;
										}
									}
								} else {
									set2.add(hp);
									gui.customLocations.addElement(hp);
								}
							}
						}
					}
					Thread.sleep(500);
				} else {
					gui.customLocations.clear();
					set2.clear();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		guiFinished = true;
		startTime = System.currentTimeMillis();
		if (gui.locationList.getSelectedIndex() == -1) {
			log("PLEASE SELECT A LOCATION");
			stopScript = true;
			canStart = true;
		} else {
			if (gui.mineModel.getSize() == 0
					&& gui.locationList.getSelectedIndex() != gui.locationList
							.getModel().getSize() - 2) {
				log("PLEASE HAVE AT LEAST ONE TYPE OF ROCK IN MINE LIST");
				stopScript = true;
				canStart = true;
			}
			String[] rocks = new String[gui.mineModel.getSize()];
			for (int i = 0; i < rocks.length; i++) {
				rocks[i] = (String) gui.mineModel.get(i);
				stringList.add(rocks[i]);
			}
			new Thread(new Runnable() {
				public void run() {
					for (String s : stringList) {
						try {
							lootList.add(new LootItem(s, PriceFetcher
									.getPrice(s)));
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}).start();
			Position[] customPositions = null;
			if (gui.mineModel.getSize() == 1
					&& ((String) gui.mineModel.get(0)).equals("Custom")) {
				customPositions = new Position[gui.customLocations.size()];
				for (int i = 0; i < gui.customLocations.size(); i++) {
					HashPosition hp = (HashPosition) gui.customLocations.get(i);
					customPositions[i] = new Position(hp.x, hp.y, hp.z);
				}
			}
			switch (gui.locationList.getSelectedIndex()) {
			case 0:
				miner = new LumbridgeSwampMiner(this, rocks,
						gui.prioritize.isSelected(), customPositions,
						gui.enableAFK.isSelected());
				try {
					miner.onStart();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case 1:
				boolean bankGreater = gui.wildernessRuniteRocksBankGreater
						.isSelected();
				int bankGreaterAmount = -1;
				try {
					bankGreaterAmount = Integer
							.parseInt(gui.wildernessRuniteRocksBankGreaterAmount
									.getText());
				} catch (Exception e) {
					if (bankGreater) {
						log("ERROR PARSING BANK WHEN GREATER THAN OR EQUAL TO NUMBER");
						stopScript = true;
						canStart = true;
						break;
					}
				}
				boolean bankWhenNone = gui.wildernessRuniteRocksBankWhenNoMore
						.isSelected();
				boolean searchHop = gui.wildernessRuniteRocksHopWhenNoMore
						.isSelected();
				boolean hopPlayer = gui.wildernessRuniteRocksHopPlayer
						.isSelected();
				boolean eatAtBank = gui.wildernessRuniteRocksEatAtBank
						.isSelected();
				String foodName = gui.wildernessRuniteRocksFoodName.getText();
				int eatUntil = -1;
				try {
					eatUntil = Integer
							.parseInt(gui.wildernessRuniteRocksEatUntil
									.getText());
				} catch (Exception e) {
					if (eatAtBank) {
						log("ERROR PARSING EAT AT BANK NUMERIC DATA");
						stopScript = true;
						canStart = true;
						break;
					}
				}
				miner = new WildernessRuniteRocksMiner(this, rocks,
						gui.prioritize.isSelected(), bankGreater,
						bankGreaterAmount, bankWhenNone, searchHop, hopPlayer,
						eatAtBank, foodName, eatUntil, customPositions,
						gui.enableAFK.isSelected());
				try {
					miner.onStart();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case 2:
				miner = new VarrockEastMiner(this, rocks,
						gui.prioritize.isSelected(), customPositions,
						gui.enableAFK.isSelected());
				try {
					miner.onStart();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case 3:
				miner = new AlkharidMiner(this, rocks,
						gui.prioritize.isSelected(), customPositions,
						gui.enableAFK.isSelected());
				try {
					miner.onStart();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case 4:
				miner = new YanilleMiner(this, rocks,
						gui.prioritize.isSelected(), customPositions,
						gui.enableAFK.isSelected());
				try {
					miner.onStart();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case 5:
				miner = new MiningGuildMiner(this, rocks,
						gui.prioritize.isSelected(), customPositions,
						gui.enableAFK.isSelected());
				try {
					miner.onStart();
				} catch (InterruptedException e) {

				}
				break;
			case 6:
				miner = new CraftingGuildMiner(this, rocks,
						gui.prioritize.isSelected(),
						gui.craftingGuildFaladorTeleport.isSelected(),
						customPositions, gui.enableAFK.isSelected());
				try {
					miner.onStart();
				} catch (InterruptedException e) {

				}
				break;
			case 7:
				miner = new VarrockWestMiner(this, rocks,
						gui.prioritize.isSelected(), customPositions,
						gui.enableAFK.isSelected());
				try {
					miner.onStart();
				} catch (InterruptedException e) {

				}
				break;
			case 8:
				miner = new RimmingtonMiner(this, rocks,
						gui.prioritize.isSelected(), customPositions,
						gui.enableAFK.isSelected());
				try {
					miner.onStart();
				} catch (InterruptedException e) {

				}
				break;
			case 9:
				miner = new VarrockEssenceMiner(this, rocks,
						gui.prioritize.isSelected(), customPositions,
						gui.enableAFK.isSelected());
				try {
					miner.onStart();
				} catch (InterruptedException e) {

				}
				break;
			case 10:
				miner = new HeroesGuildRuniteRocksMiner(this, rocks,
						gui.prioritize.isSelected(), customPositions,
						gui.enableAFK.isSelected());
				try {
					miner.onStart();
				} catch (InterruptedException e) {

				}
				break;
			case 11:
				try {
					Position[] powerminePositions = new Position[set.size()];
					int index = 0;
					for (HashPosition p : set) {
						powerminePositions[index++] = new Position(p.x, p.y,
								p.z);
					}
					miner = new PowerMiner(this, "Powermine",
							powerminePositions, gui.enableAFK.isSelected(),
							gui.useM1D1.isSelected());
					try {
						miner.onStart();
					} catch (InterruptedException e) {

					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			default:
				log("ERROR, UNABLE TO RECOGNIZE LOCATION");
				stopScript = true;
				canStart = true;
				break;
			}
		}
		StringTokenizer st = new StringTokenizer(gui.rockIDs.getText(), ",");
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			token = token.trim();
			int id = -1;
			try {
				id = Integer.parseInt(token);
			} catch (Exception e) {

			}
			if (id != -1) {
				((Miner) miner).realImmutableIDs.add(id);
			}
		}
		oldCoalCount = (int) inventory.getAmount("Coal");
		oldMithrilCount = (int) inventory.getAmount("Mithril ore");
		oldAdamantiteCount = (int) inventory.getAmount("Adamantite ore");
		oldRuniteCount = (int) inventory.getAmount("Runite ore");
		oldCopperCount = (int) inventory.getAmount("Copper ore");
		oldTinCount = (int) inventory.getAmount("Tin ore");
		oldClayCount = (int) inventory.getAmount("Clay ore");
		oldIronCount = (int) inventory.getAmount("Iron ore");
		oldGraniteCount = (int) inventory.getAmount("Granite");
		oldSilverCount = (int) inventory.getAmount("Silver ore");
		oldGoldCount = (int) inventory.getAmount("Gold ore");
		if (gui.chatWindow.isSelected()) {
			chatWindow = new ChatWindow();
			chatWindow.setVisible(true);
		}
		if (gui.enableAFK.isSelected()) {
			lastAFKTime = System.currentTimeMillis();
			AFKTimeDelay = MethodProvider.random(5 * 60 * 1000, 20 * 60 * 1000);
			shouldAFK = true;
			log("AFK Enabled");
			log("First AFK: " + AFKTimeDelay / 1000 + " seconds from now");
		}
		new Thread(new ServerUpdater()).start();
		new Thread(new RandomTracker(this)).start();
		startTime = System.currentTimeMillis();
		canStart = true;
	}

	private boolean doneLoading;

	public int onLoop() {
		if (camera.getPitchAngle() < 57) {
			camera.movePitch(65);
		}
		if (canStart) {
			if (stopScript) {
				stop();
			}
			if (client.getLoginState() == LoginState.LOGGED_IN
					&& worldHopping.isInBotWorld()) {
				worldHopping.switchWorlds();
			}
			if (shouldAFK
					&& System.currentTimeMillis() - lastAFKTime > AFKTimeDelay
					&& myPlayer().getAnimation() == -1) {
				lastAFKTime = System.currentTimeMillis();
				AFKTimeDelay = MethodProvider.random(5 * 60 * 1000,
						20 * 60 * 1000);
				final int time = MethodProvider.random(5000, 20000);
				log("Going AFK for " + time / 1000 + " seconds");
				mouse.moveOutsideScreen();
				doneLoading = false;
				new Thread(new Runnable() {
					public void run() {
						try {
							sleep(time);
							doneLoading = true;
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}).start();
				while (!doneLoading) {
					RS2Object rock = objects.closest(new Filter<RS2Object>() {

						@Override
						public boolean match(RS2Object arg0) {
							return arg0.getName().equals("Rocks")
									&& arg0.getHeight() > 100;
						}

					});
					if (rock != null) {
						break;
					}
				}
				log("Resuming");
				log("Next AFK: " + (AFKTimeDelay - time) / 1000
						+ " seconds from now");
			}
			if (chatWindow != null) {
				String playerMessage = chatWindow.getNextPlayerMessage();
				if (playerMessage != null) {
					this.keyboard.typeString(playerMessage);
				}
				String clanMessage = chatWindow.getNextClanMessage();
				if (clanMessage != null) {
					this.keyboard.typeString("/" + clanMessage);
				}
				FriendMessage fm = chatWindow.getNextFriendMessage();
				if (fm != null) {
					log("Send message");
					Library.sendFriendMessage(fm.username, fm.message, this);
				}
			}
			if (miner == null) {
				log("Something went wrong starting the script.");
				stop();
			} else {
				int coalCount = (int) inventory.getAmount("Coal");
				int mithrilCount = (int) inventory.getAmount("Mithril ore");
				int adamantiteCount = (int) inventory
						.getAmount("Adamantite ore");
				int runiteCount = (int) inventory.getAmount("Runite ore");
				int copperCount = (int) inventory.getAmount("Copper ore");
				int tinCount = (int) inventory.getAmount("Tin ore");
				int clayCount = (int) inventory.getAmount("Clay ore");
				int ironCount = (int) inventory.getAmount("Iron ore");
				int graniteCount = (int) inventory.getAmount("Granite");
				int silverCount = (int) inventory.getAmount("Silver ore");
				int goldCount = (int) inventory.getAmount("Gold ore");
				if (coalCount > oldCoalCount)
					coalMined++;
				if (mithrilCount > oldMithrilCount)
					mithrilMined++;
				if (adamantiteCount > oldAdamantiteCount)
					adamantiteMined++;
				if (runiteCount > oldRuniteCount)
					runiteMined++;
				if (copperCount > oldCopperCount)
					copperMined++;
				if (tinCount > oldTinCount)
					tinMined++;
				if (clayCount > oldClayCount)
					clayMined++;
				if (ironCount > oldIronCount)
					ironMined++;
				if (graniteCount > oldGraniteCount)
					graniteMined++;
				if (silverCount > oldSilverCount)
					silverMined++;
				if (goldCount > oldGoldCount)
					goldMined++;
				oldCoalCount = coalCount;
				oldMithrilCount = mithrilCount;
				oldAdamantiteCount = adamantiteCount;
				oldRuniteCount = runiteCount;
				oldCopperCount = copperCount;
				oldTinCount = tinCount;
				oldClayCount = clayCount;
				oldIronCount = ironCount;
				oldGraniteCount = graniteCount;
				oldSilverCount = silverCount;
				oldGoldCount = goldCount;
				oresMined = coalMined + mithrilMined + adamantiteMined
						+ runiteMined + copperMined + tinMined + clayMined
						+ ironMined + graniteMined + silverMined + goldMined;
				try {
					return miner.onLoop();
				} catch (InterruptedException e) {
				}
			}
		}
		return 0;
	}

	public void onExit() {
		if (miner != null) {
			try {
				miner.onExit();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		stopScript = true;
	}

	/**
	 * Draws the paint
	 */
	@Override
	public void onPaint(Graphics2D g) {
		if (!guiFinished) {
			try {
				if (gui != null
						&& gui.locationList.getSelectedIndex() == gui.locationList
								.getModel().getSize() - 2) {
					for (int i = 0; i < gui.powermineLocations.getSize(); i++) {
						HashPosition hp = (HashPosition) gui.powermineLocations
								.get(i);
						Position p = new Position(hp.x, hp.y, hp.z);
						Polygon poly = p.getPolygon(bot);
						if (poly != null) {
							g.setColor(new Color(0.0f, 1.0f, 0.0f, 0.2f));
							g.drawPolygon(poly);
						}
					}
				}
			} catch (Exception e) {
			}
			try {
				if (gui != null && gui.mineModel.size() == 1
						&& ((String) gui.mineModel.get(0)).equals("Custom")) {
					for (int i = 0; i < gui.customLocations.getSize(); i++) {
						HashPosition hp = (HashPosition) gui.customLocations
								.get(i);
						Position p = new Position(hp.x, hp.y, hp.z);
						Polygon poly = p.getPolygon(bot);
						if (poly != null) {
							g.setColor(new Color(0.0f, 1.0f, 0.0f, 0.2f));
							g.drawPolygon(poly);
						}
					}
				}
			} catch (Exception e) {

			}
			return;
		}
		// paint
		Graphics2D gr = g;
		int totalGP = 0;
		for (LootItem item : lootList) {
			if (item.avgPrice > 0) {
				totalGP += item.avgPrice * item.lootedAmount;
			}
		}
		int totalExp = skills.getExperience(Skill.MINING) - miningExperience;
		long runTime = System.currentTimeMillis() - startTime;
		gr.setColor(black);
		gr.fillRoundRect(7, 307, 503, 30, 6, 6);
		gr.setColor(green_outline);
		gr.drawRoundRect(7, 307, 503, 30, 6, 6);
		// text
		gr.setStroke(this.stroke);
		gr.setColor(green);
		gr.setFont(paintFont);
		gr.drawString("Runtime: " + format(runTime), 14, 318);
		if (miner != null && ((Miner) miner).getState() != null)
			gr.drawString("State: " + ((Miner) miner).getState().toString(),
					14, 332);
		gr.drawString("Total EXP: " + totalExp, 182, 318);
		gr.drawString("Total GP: " + totalGP, 362, 318);
		gr.setRenderingHints(new RenderingHints(
				RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON));

		gr.setColor(green);
		gr.setStroke(this.stroke);
		gr.setFont(paintFont);
		gr.drawString("EXP/hour: " + (int) (totalExp * (3600000.0D / runTime)),
				182, 332);

		if (window2) {
			gr.setColor(black);
			gr.fillRoundRect(355, 339, 155, 135, 6, 6);
			gr.setColor(green_outline);
			gr.drawRoundRect(355, 339, 155, 135, 6, 6);
			gr.setColor(green);
			gr.setStroke(this.stroke);
			gr.setFont(paintFont);
			gr.drawString("GP/hour: "
					+ (int) (totalGP * (3600000.0D / runTime)), 362, 332);
			int y = 355;
			for (LootItem item : lootList) {
				if (item.avgPrice > 0) {
					gr.drawString(item.name + " [GP]: " + item.lootedAmount
							+ " [" + item.avgPrice * item.lootedAmount + "]",
							362, y);
					y += 20;
				}
			}
			gr.drawString("Collapse", 412, 471);
		} else {
			gr.setColor(green);
			gr.setStroke(this.stroke);
			gr.setFont(paintFont);
			gr.drawString("Expand", 362, 332);
		}
	}

	String format(final long time) {
		final StringBuilder t = new StringBuilder();
		final long total_secs = time / 1000;
		final long total_mins = total_secs / 60;
		final long total_hrs = total_mins / 60;
		final int secs = (int) total_secs % 60;
		final int mins = (int) total_mins % 60;
		final int hrs = (int) total_hrs;
		if (hrs < 10)
			t.append("0");
		t.append(hrs + ":");
		if (mins < 10)
			t.append("0");
		t.append(mins + ":");
		if (secs < 10) {
			t.append("0");
			t.append(secs);
		} else
			t.append(secs);
		return t.toString();
	}

	public void onMessage(Message m) {
		try {
			if (chatWindow != null)
				try {
					chatWindow.addMessage(m.getMessage(), m.getTypeId(),
							m.getUsername());
				} catch (Exception e) {

				}
		} catch (Exception e) {

		}
		if (miner != null) {
			// loot tracking
			try {
				for (LootItem li : lootList) {
					int split = li.name.indexOf(' ');
					String firstWord = li.name;
					if (split != -1)
						firstWord = li.name.substring(0, li.name.indexOf(' '));
					if (m.getMessage().contains(firstWord.toLowerCase())) {
						li.lootedAmount++;
					}
				}
			} catch (Exception e) {
			}
			try {
				miner.onMessage(m);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	class ServerUpdater implements Runnable {

		long oldrun = 0;
		int oldxp = skills.getExperience(Skill.MINING);
		long oldoresmined = 0;
		long oldgp = 0;

		public void run() {
			while (!stopScript) {
				if (canStart) {
					long run = (long) ((System.currentTimeMillis() - startTime) / 1000);
					int currentExp = skills.getExperience(Skill.MINING);
					// System.out.println(currentExp);
					String prepString = "" + System.nanoTime();
					prepString = prepString.substring(prepString.length() - 6);
					String runtime = Library.toHex(Library.encrypt(prepString
							+ (run - oldrun), "DSSOSBOT"));
					prepString = "" + System.nanoTime();
					prepString = prepString.substring(prepString.length() - 6);
					String expgained = Library.toHex(Library.encrypt(prepString
							+ (currentExp - oldxp), "DSSOSBOT"));
					prepString = "" + System.nanoTime();
					prepString = prepString.substring(prepString.length() - 6);
					String oresmined = Library.toHex(Library.encrypt(prepString
							+ (oresMined - oldoresmined), "DSSOSBOT"));
					prepString = "" + System.nanoTime();
					prepString = prepString.substring(prepString.length() - 6);
					long totalGP = 0;
					for (LootItem item : lootList) {
						if (item.avgPrice > 0) {
							totalGP += item.avgPrice * item.lootedAmount;
						}
					}
					String gpearned = Library.toHex(Library.encrypt(prepString
							+ (totalGP - oldgp), "DSSOSBOT"));
					// System.out.println(totalGP);
					oldrun = run;
					oldxp = currentExp;
					oldoresmined = oresMined;
					oldgp = totalGP;
					String valid = Library.toHex(Library.encrypt("VALID",
							expgained));
					try {
						URL url = new URL(
								"http://dreamscripts.org/updatesig.php?Script=DreamMiner&Username="
										+ BotApplication.getInstance()
												.getOSAccount().username
										+ "&Runtime=" + runtime + "&OresMined="
										+ oresmined + "&Expgained=" + expgained
										+ "&GPEarned=" + gpearned + "&Valid="
										+ valid);
						BufferedReader br = new BufferedReader(
								new InputStreamReader(url.openStream()));
						// System.out.println(br.readLine());
					} catch (Exception e) {
					}
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}

}
