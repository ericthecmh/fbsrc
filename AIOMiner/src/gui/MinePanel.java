package gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 * 
 * @author Ericthecmh
 */
public class MinePanel extends JPanel {

	private final String[] urls = new String[] {
			"http://dreamscripts.org/images/LumbridgeSwampMine.jpg",
			"http://dreamscripts.org/images/WildernessRuniteMine.jpg",
			"http://dreamscripts.org/images/VarrockEast.jpg",
			"http://dreamscripts.org/images/Alkharid.jpg",
			"http://dreamscripts.org/images/Yanille.jpg",
			"http://dreamscripts.org/images/MiningGuild.jpg",
			"http://dreamscripts.org/images/CraftingGuild.jpg",
			"http://dreamscripts.org/images/VarrockWestMine.jpg",
			"http://dreamscripts.org/images/Rimmington.jpg"};

	private BufferedImage bi;
	private int selectionI = -1;
	private int highlight = 0;
	private boolean doneLoading;

	public MinePanel() {
		doneLoading = false;
		Thread t = new Thread(new Runnable() {
			public void run() {
				try {
					bi = ImageIO
							.read(new URL(
									"http://dreamscripts.org/images/Nothing.jpg"));
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					doneLoading = true;
				}
			}
		});
		t.start();
		for (int wait = 0; wait < 20 && !doneLoading; wait++) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException ex) {
				Logger.getLogger(MinePanel.class.getName()).log(Level.SEVERE,
						null, ex);
			}
		}
		if (!doneLoading) {
			if (t.isAlive()) {
				t.interrupt();
			}
			bi = null;
		}
	}

	public void setSelection(int i) {
		selectionI = i;
		doneLoading = false;
		Thread t = new Thread(new Runnable() {
			public void run() {
				try {
					bi = ImageIO.read(new URL(urls[selectionI]));
				} catch (Exception e) {

				} finally {
					doneLoading = true;
				}
			}
		});
		t.start();
		for (int wait = 0; wait < 20 && !doneLoading; wait++) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException ex) {
				Logger.getLogger(MinePanel.class.getName()).log(Level.SEVERE,
						null, ex);
			}
		}
		if (!doneLoading) {
			if (t.isAlive()) {
				t.interrupt();
			}
			bi = null;
		}
		this.repaint();
	}

	public void setHighlight(int h) {
		highlight = h;
		this.repaint();
	}

	public void paintComponent(Graphics g) {
		if (bi == null) {
			return;
		}
		g.drawImage(bi, 0, 0, this);
		switch (selectionI) {
		case 0:
			if ((highlight & 1) != 0) { // Coal
				g.setColor(new Color(1.0f, 0.0f, 0.0f, 0.2f));
				g.fillRect(182, 115, 25, 25);
				g.fillRect(152, 141, 25, 25);
				g.fillRect(187, 163, 25, 25);
				g.fillRect(135, 193, 25, 25);
				g.fillRect(160, 193, 25, 25);
				g.fillRect(191, 214, 25, 25);
				g.fillRect(166, 247, 25, 25);
				g.setColor(Color.RED);
				g.drawRect(182, 115, 25, 25);
				g.drawRect(152, 141, 25, 25);
				g.drawRect(187, 163, 25, 25);
				g.drawRect(135, 193, 25, 25);
				g.drawRect(160, 193, 25, 25);
				g.drawRect(191, 214, 25, 25);
				g.drawRect(166, 247, 25, 25);
			}
			if ((highlight & 2) != 0) { // Mithril
				g.setColor(new Color(0.0f, 0.0f, 1.0f, 0.2f));
				g.fillRect(178, 90, 25, 25);
				g.fillRect(208, 61, 25, 25);
				g.fillRect(174, 33, 25, 25);
				g.fillRect(117, 36, 25, 25);
				g.fillRect(56, 5, 25, 25);
				g.setColor(Color.BLUE);
				g.drawRect(178, 90, 25, 25);
				g.drawRect(208, 61, 25, 25);
				g.drawRect(174, 33, 25, 25);
				g.drawRect(117, 36, 25, 25);
				g.drawRect(56, 5, 25, 25);
			}
			if ((highlight & 4) != 0) { // Adamant
				g.setColor(new Color(0.0f, 1.0f, 0.0f, 0.2f));
				g.fillRect(93, 66, 25, 25);
				g.fillRect(61, 88, 25, 25);
				g.setColor(Color.GREEN);
				g.drawRect(93, 66, 25, 25);
				g.drawRect(61, 88, 25, 25);
			}
			break;
		case 1:
			if ((highlight & 8) != 0) { // Rune
				g.setColor(new Color(1.0f, 0.0f, 0.0f, 0.2f));
				g.fillRect(106, 107, 50, 50);
				g.fillRect(107, 161, 48, 46);
				g.setColor(Color.RED);
				g.drawRect(106, 107, 50, 50);
				g.drawRect(107, 161, 48, 46);
			}
			break;
		case 2:
			if ((highlight & 64) != 0) { // Iron
				g.setColor(new Color(1.0f, 0.0f, 0.0f, 0.2f));
				g.fillRect(152, 0, 25, 25);
				g.fillRect(93, 26, 25, 25);
				g.fillRect(118, 26, 25, 25);
				g.fillRect(94, 55, 25, 25);
				g.setColor(Color.RED);
				g.drawRect(152, 0, 25, 25);
				g.drawRect(93, 26, 25, 25);
				g.drawRect(118, 26, 25, 25);
				g.drawRect(94, 55, 25, 25);
			}
			if ((highlight & 16) != 0) { // Copper
				g.setColor(new Color(0.0f, 0.0f, 1.0f, 0.2f));
				g.fillRect(30, 29, 25, 25);
				g.fillRect(33, 59, 25, 25);
				g.fillRect(144, 133, 25, 25);
				g.fillRect(118, 158, 25, 25);
				g.fillRect(192, 183, 25, 25);
				g.fillRect(218, 214, 25, 25);
				g.fillRect(173, 249, 25, 25);
				g.fillRect(142, 253, 25, 25);
				g.fillRect(94, 259, 25, 25);
				g.setColor(Color.BLUE);
				g.drawRect(30, 29, 25, 25);
				g.drawRect(33, 59, 25, 25);
				g.drawRect(144, 133, 25, 25);
				g.drawRect(118, 158, 25, 25);
				g.drawRect(192, 183, 25, 25);
				g.drawRect(218, 214, 25, 25);
				g.drawRect(173, 249, 25, 25);
				g.drawRect(142, 253, 25, 25);
				g.drawRect(94, 259, 25, 25);
			}
			if ((highlight & 32) != 0) { // Tin
				g.setColor(new Color(0.0f, 1.0f, 0.0f, 0.2f));
				g.fillRect(181, 45, 25, 25);
				g.fillRect(158, 99, 25, 25);
				g.fillRect(24, 137, 25, 25);
				g.fillRect(22, 167, 25, 25);
				g.fillRect(0, 197, 25, 25);
				g.fillRect(0, 233, 25, 25);
				g.setColor(Color.GREEN);
				g.drawRect(181, 45, 25, 25);
				g.drawRect(158, 99, 25, 25);
				g.drawRect(24, 137, 25, 25);
				g.drawRect(22, 163, 25, 25);
				g.drawRect(0, 197, 25, 25);
				g.drawRect(0, 233, 25, 25);
			}
		case 4:
			if ((highlight & 64) != 0) { // IRON
				g.setColor(new Color(1.0f, 0.0f, 0.0f, 0.2f));
				g.fillRect(14, 39, 68, 35);
				g.fillRect(121, 117, 40, 35);
				g.fillRect(165, 129, 44, 32);
				g.setColor(Color.RED);
				g.drawRect(14, 39, 68, 35);
				g.drawRect(121, 117, 40, 35);
				g.drawRect(165, 129, 40, 32);
			}
		case 6:
			if ((highlight & 128) != 0) {
				g.setColor(new Color(0.0f, 0.0f, 1.0f, 0.2f));
				g.fillRect(143, 161, 20, 20);
				g.fillRect(143, 181, 20, 20);
				g.fillRect(153, 200, 20, 20);
				g.fillRect(176, 201, 20, 20);
				g.fillRect(186, 201, 20, 20);
				g.fillRect(197, 162, 20, 20);
				g.fillRect(197, 173, 20, 20);
				g.setColor(Color.BLUE);
				g.drawRect(143, 161, 20, 20);
				g.drawRect(143, 181, 20, 20);
				g.drawRect(153, 200, 20, 20);
				g.drawRect(176, 201, 20, 20);
				g.drawRect(186, 201, 20, 20);
				g.drawRect(197, 162, 20, 20);
				g.drawRect(197, 173, 20, 20);
			}
			if ((highlight & 256) != 0) {
				g.setColor(new Color(1.0f, 0.0f, 0.0f, 0.2f));
				g.fillRect(183, 52, 20, 20);
				g.fillRect(196, 66, 20, 20);
				g.fillRect(153, 66, 20, 20);
				g.fillRect(174, 84, 20, 20);
				g.fillRect(176, 114, 20, 20);
				g.fillRect(175, 114, 20, 20);
				g.fillRect(154, 96, 20, 20);
				g.setColor(Color.RED);
				g.drawRect(183, 52, 20, 20);
				g.drawRect(196, 66, 20, 20);
				g.drawRect(153, 66, 20, 20);
				g.drawRect(174, 84, 20, 20);
				g.drawRect(176, 114, 20, 20);
				g.drawRect(175, 114, 20, 20);
				g.drawRect(154, 96, 20, 20);
			}
			if ((highlight & 512) != 0) {
				g.setColor(new Color(0.0f, 1.0f, 0.0f, 0.2f));
				g.fillRect(143, 112, 20, 20);
				g.fillRect(132, 125, 20, 20);
				g.fillRect(165, 130, 20, 20);
				g.fillRect(195, 123, 20, 20);
				g.fillRect(186, 135, 20, 20);
				g.fillRect(197, 143, 20, 20);
				g.setColor(Color.GREEN);
				g.drawRect(143, 112, 20, 20);
				g.drawRect(132, 125, 20, 20);
				g.drawRect(165, 130, 20, 20);
				g.drawRect(195, 123, 20, 20);
				g.drawRect(186, 135, 20, 20);
				g.drawRect(197, 143, 20, 20);
			}
			break;
		default:
			break;
		}
	}
}
