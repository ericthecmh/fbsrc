package miners;

import java.awt.Point;

import library.Area;
import library.Conditional;
import library.Library;
import library.PickaxeFilter;
import main.DreamMiner;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.map.PositionPolygon;
import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.Message;
import org.osbot.rs07.input.mouse.MiniMapTileDestination;
import org.osbot.rs07.script.Script;

/**
 * 
 * Mines in the Lumbridge Swamp (the one with coal, mithril, and adamant)
 * 
 * @author Ericthecmh, Eliot
 * 
 */

public class VarrockWestMiner extends Miner {

	/************************************ CONSTANTS ************************************/

	// Mining area
	private final Area miningArea = new Area(3183, 3379, 3172, 3365);
	private final Area bankArea = new Area(3185, 3432, 3180, 3447);

	// Array of positions of some tin rocks (used to determine IDs)
	private final static Position[] tinPositions = new Position[] {
			new Position(3173, 3365, 0), new Position(3173, 3366, 0),
			new Position(3181, 3375, 0), new Position(3183, 3376, 0),
			new Position(3181, 3377, 0), new Position(3181, 3376, 0),
			new Position(3172, 3366, 0), new Position(3176, 3369, 0) };

	// Array of positions of some clay rocks (used to determine IDs)
	private final static Position[] clayPositions = new Position[] {
			new Position(3179, 3371, 0), new Position(3180, 3372, 0),
			new Position(3183, 3377, 0) };

	// Array of positions of some silver rocks (used to determine IDs)
	private final static Position[] silverPositions = new Position[] {
			new Position(3177, 3366, 0), new Position(3176, 3365, 0),
			new Position(3177, 3370, 0) };

	// Array of positions of some iron rocks (used to determine IDs)
	private final static Position[] ironPositions = new Position[] {
			new Position(3175, 3366, 0), new Position(3175, 3368, 0),
			new Position(3181, 3373, 0) };

	// Path to bank
	private final PositionPolygon pathToMine = new PositionPolygon(new int[][] {
			{ 3185, 3437 }, { 3177, 3429 }, { 3170, 3423 }, { 3171, 3415 },
			{ 3171, 3407 }, { 3171, 3399 }, { 3174, 3391 }, { 3177, 3384 },
			{ 3179, 3380 }, { 3182, 3374 }, });

	/**
	 * Constructor
	 * 
	 * @param rocks
	 *            the list of rock names to be mined
	 * @param prioritize
	 *            whether or not to prioritize rocks by the order in rocks array
	 * @param s
	 *            reference to the actual script being run
	 * @param customPositions
	 * @param b
	 */
	public VarrockWestMiner(Script s, String[] rocks, boolean prioritize,
			Position[] customPositions, boolean b) {
		super(s, rocks, prioritize, null, tinPositions, ironPositions,
				silverPositions, null, null, null, null, null, null,
				clayPositions, customPositions, b);
	}

	/**
	 * onStart
	 * 
	 * Calls the super's onStart and sets up whatever necessary for this
	 * particular miner
	 * 
	 */
	@Override
	public void onStart() {
		super.onStart();
		if (miningArea.isInAreaInclusive(script.myPosition()))
			state = State.MINE;
		else if (bankArea.isInAreaInclusive(script.myPosition()))
			state = State.BANK;
		else if (script.inventory.getEmptySlots() < 14)
			state = State.WALK_TO_BANK;
		else
			state = State.WALK_TO_MINE;
		canStart = true;
	}

	/**
	 * 
	 * onLoop
	 * 
	 * Worker for the script
	 * 
	 */
	public int onLoop() {
		try {
			super.onLoop();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (canStart) {
			switch (state) {
			case MINE:
				Object[] keys = incorrectIDs.keySet().toArray();
				for (Object i : keys) {
					long elapsed = System.currentTimeMillis()
							- incorrectIDs.get(i).addedTime;
					if (elapsed > incorrectIDs.get(i).expireTime)
						incorrectIDs.remove(i);
				}
				if (script.inventory.isFull()) {
					state = State.WALK_TO_BANK;
					break;
				}
				// If you're not mining/idle/rock you're mining is dead, then
				// mine a new rock
				if ((rockPosition == null
						|| Library.getRockIDAt(rockPosition, script) != rockID
						|| System.currentTimeMillis() - lastActionTime > 2000 || Library.getFacingRock(script) != Library.getRockAt(
						rockPosition, script))
						&& !script.myPlayer().isMoving()) {
					// System.out.println("Getting new rock...");
					RS2Object nearestRock = Library.getRockForIDs(
							rocksPositions, miningArea, prioritize, script,
							incorrectIDs);
					if (nearestRock != null) {
						// System.out.println("New rock position: (" +
						// nearestRock.getX() + "," + nearestRock.getY() + ")");
						rockID = nearestRock.getId();
						rockPosition = nearestRock.getPosition();
						for (int i = 0; i < 5
								&& !Library.interactRock(nearestRock, script); i++) {
							try {
								script.sleep(random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						lastActionTime = System.currentTimeMillis();
					} else {

					}
				} else {
				}
				break;
			case WALK_TO_BANK:
				if (bankArea.isInAreaInclusive(script.myPosition())) {
					keys = incorrectIDs.keySet().toArray();
					for (Object i : keys) {
						incorrectIDs.get(i).value--;
						if (incorrectIDs.get(i).value == 0) {
							incorrectIDs.remove(i);
						} else {
							long elapsed = System.currentTimeMillis()
									- incorrectIDs.get(i).addedTime;
							if (elapsed > incorrectIDs.get(i).expireTime)
								incorrectIDs.remove(i);
						}
					}
					state = State.BANK;
					break;
				}
				walk(true);
				break;
			case BANK:
				if (script.inventory.getEmptySlots() > 20) {
					if (!pickaxeInInventory
							|| script.inventory.contains(pickaxeID)) {
						state = State.WALK_TO_MINE;
						break;
					}
				}
				if (!script.bank.isOpen()) {
					RS2Object bankBooth = Library.closestObject("Bank booth",
							"Bank", script);
					if (bankBooth != null) {
						if (Library.distanceTo(bankBooth.getPosition(), script) > 3
								&& !bankArea.isInAreaInclusive(script
										.myPosition())) {
							final MiniMapTileDestination td = new MiniMapTileDestination(
									script.bot, bankBooth.getPosition());
							// script.mouse.move(td);
							// if (Library.waitFor(new Conditional() {
							//
							// @Override
							// public boolean isSatisfied(Script script) {
							// return td.getBoundingBox().contains(
							// script.mouse.getPosition());
							// }
							//
							// }, 3000, 100, script)) {
							// script.mouse.click(false);
							// }
							script.mouse.click(td);
							Library.waitFor(new Conditional() {

								@Override
								public boolean isSatisfied(Script script) {
									return script.myPlayer().isMoving();
								}
							}, 1000, 100, script);
							Library.waitFor(new Conditional() {

								@Override
								public boolean isSatisfied(Script script) {
									return !script.myPlayer().isMoving();
								}
							}, 5000, 100, script);
							Library.waitFor(new Conditional() {

								@Override
								public boolean isSatisfied(Script script) {
									return script.bank.isOpen();
								}
							}, 1000, 100, script);
							return 0;
						}
						if (DreamMiner.USE_CUSTOM_INTERACT)
							Library.interact(bankBooth, "Bank", script);
						else
							bankBooth.interact("Bank");
					}
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return script.myPlayer().isMoving();
						}
					}, 1000, 100, script);
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return !script.myPlayer().isMoving();
						}
					}, 5000, 100, script);
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return script.bank.isOpen();
						}
					}, 1000, 100, script);
				} else {
					script.bank.depositAll(new PickaxeFilter<Item>());
				}
				break;
			case WALK_TO_MINE:
				if (miningArea.isInAreaInclusive(script.myPosition())) {
					state = State.MINE;
					break;
				}
				walk(false);
				break;
			}
		}
		return 100;
	}

	// Code stolen from Laz <3
	private int walk(boolean reverse) {
		int pi = pathToMine.getClosestIndex(script.myPosition());
		if (pi == -1) {
			return -1;
		}

		if (!reverse && miningArea.isInAreaInclusive(script.myPosition())) {
			return -1;
		} else if (reverse && bankArea.isInAreaInclusive(script.myPosition())) {
			return -1;
		}

		int c = pathToMine.getCount();
		int wi = reverse ? 0 : c - 1;

		if (wi == pi) {
			return -1;
		}

		if (!script.settings.isRunning()
				&& script.settings.getRunEnergy() > random(30, 50)) {
			script.settings.setRunning(true);
			return gRandom(400, 200);
		}

		while (wi >= 0 && wi < c && (reverse ? wi <= pi : wi >= pi)) {
			Point point = pathToMine.get(wi);
			if (script.localWalker.walk(point.x, point.y)) {
				try {
					script.sleep(random(500, 800));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return 0;
			}
			wi += reverse ? 1 : -1;
		}

		return -1;
	}

	public void onMessage(Message m) throws InterruptedException {
		super.onMessage(m);
	}

	public void onExit() {
		stopScript = true;
	}
}
