package miners;

import java.awt.Point;

import library.Area;
import library.Conditional;
import library.Library;
import library.PickaxeFilter;
import main.DreamMiner;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.map.PositionPolygon;
import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.Message;
import org.osbot.rs07.input.mouse.MiniMapTileDestination;
import org.osbot.rs07.script.Script;

/**
 * 
 * Mines in the Varrock Essence mine
 * 
 * @author Ericthecmh, Eliot
 * 
 */

public class VarrockEssenceMiner extends Miner {

	/************************************ CONSTANTS ************************************/

	// Mining area (actually the rune shop
	private final Area miningArea = new Area(3252, 3399, 3254, 3404);
	private final Area miningArea2 = new Area(3255, 3401, 3255, 3401);
	private final Area bankArea = new Area(3257, 3419, 3250, 3423);

	// Path to bank
	private final PositionPolygon pathToMine = new PositionPolygon(new int[][] {
			{ 3253, 3421 }, { 3256, 3428 }, { 3262, 3424 }, { 3262, 3416 },
			{ 3258, 3410 }, { 3257, 3404 }, { 3253, 3398 }, { 3177, 3384 },
			{ 3179, 3380 }, { 3182, 3374 }, });

	/**
	 * Constructor
	 * 
	 * @param rocks
	 *            the list of rock names to be mined
	 * @param prioritize
	 *            whether or not to prioritize rocks by the order in rocks array
	 * @param s
	 *            reference to the actual script being run
	 * @param customPositions
	 * @param b
	 */
	public VarrockEssenceMiner(Script s, String[] rocks, boolean prioritize,
			Position[] customPositions, boolean b) {
		super(s, rocks, prioritize, null, null, null, null, null, null, null,
				null, null, null, null, null, b);
	}

	/**
	 * onStart
	 * 
	 * Calls the super's onStart and sets up whatever necessary for this
	 * particular miner
	 * 
	 */
	@Override
	public void onStart() {
		super.onStart();
		if (script.objects.closest("Rune Essence") != null)
			state = State.MINE;
		else if (bankArea.isInAreaInclusive(script.myPosition()))
			state = State.BANK;
		else if (script.inventory.getEmptySlots() < 14)
			state = State.WALK_TO_BANK;
		else
			state = State.WALK_TO_MINE;
		canStart = true;
	}

	/**
	 * 
	 * onLoop
	 * 
	 * Worker for the script
	 * 
	 */
	public int onLoop() {
		try {
			super.onLoop();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (canStart) {
			switch (state) {
			case MINE:
				Object[] keys = incorrectIDs.keySet().toArray();
				for (Object i : keys) {
					long elapsed = System.currentTimeMillis()
							- incorrectIDs.get(i).addedTime;
					if (elapsed > incorrectIDs.get(i).expireTime)
						incorrectIDs.remove(i);
				}
				script.dialogues.clickContinue();
				if (script.inventory.isFull()) {
					state = State.WALK_TO_BANK;
					break;
				}
				if (script.myPlayer().getAnimation() == -1) {
					RS2Object ess = script.objects.closest("Rune Essence");
					if (ess != null) {
						ess.interact("Mine");
						try {
							script.sleep(script.random(400, 700));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				break;
			case WALK_TO_BANK:
				if (script.objects.closest("Rune Essence") != null) {
					RS2Object portal = null;
					double distance = 2000000000;
					for (RS2Object o : script.objects.getAll()) {
						if (o != null
								&& o.getName().contains("Portal")
								&& Library.distanceTo(o.getPosition(), script) < distance) {
							distance = Library.distanceTo(o.getPosition(),
									script);
							portal = o;
						}
					}
					if (portal != null) {
						portal.interact(portal.getDefinition().getActions()[0]);
						try {
							script.sleep(script.random(400, 700));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {
						NPC npcportal = null;
						distance = 2000000000;
						for (NPC o : script.npcs.getAll()) {
							if (o != null
									&& o.getName().contains("Portal")
									&& Library.distanceTo(o.getPosition(),
											script) < distance) {
								distance = Library.distanceTo(o.getPosition(),
										script);
								npcportal = o;
							}
						}
						if (npcportal != null) {
							npcportal.interact(npcportal.getDefinition()
									.getActions()[0]);
							try {
								script.sleep(script.random(400, 700));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				} else {
					if (miningArea.isInAreaInclusive(script.myPosition())
							|| miningArea2.isInAreaInclusive(script
									.myPosition())) {
						RS2Object closedDoor = null;
						for (RS2Object o : script.objects.getAll()) {
							if (o != null && o.getName().equals("Door")
									&& o.getX() == 3253 && o.getY() == 3398) {
								closedDoor = o;
								break;
							}
						}
						if (closedDoor != null) {
							// Closed door found, open it
							closedDoor.interact("Open");
							try {
								script.sleep(script.random(400, 700));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							break;
						} else {
							walk(true);
						}
					} else {
						if (bankArea.isInAreaInclusive(script.myPosition())) {
							keys = incorrectIDs.keySet().toArray();
							for (Object i : keys) {
								incorrectIDs.get(i).value--;
								if (incorrectIDs.get(i).value == 0) {
									incorrectIDs.remove(i);
								} else {
									long elapsed = System.currentTimeMillis()
											- incorrectIDs.get(i).addedTime;
									if (elapsed > incorrectIDs.get(i).expireTime)
										incorrectIDs.remove(i);
								}
							}
							state = State.BANK;
							break;
						} else {
							walk(true);
						}
					}
				}
				break;
			case BANK:
				if (script.inventory.getEmptySlots() > 20) {
					if (!pickaxeInInventory
							|| script.inventory.contains(pickaxeID)) {
						state = State.WALK_TO_MINE;
						break;
					}
				}
				if (!script.bank.isOpen()) {
					RS2Object bankBooth = Library.closestObject("Bank booth",
							"Bank", script);
					if (bankBooth != null) {
						if (Library.distanceTo(bankBooth.getPosition(), script) > 3 && !bankArea.isInAreaInclusive(script.myPosition())) {
							final MiniMapTileDestination td = new MiniMapTileDestination(
									script.bot, bankBooth.getPosition());
							// script.mouse.move(td);
							// if (Library.waitFor(new Conditional() {
							//
							// @Override
							// public boolean isSatisfied(Script script) {
							// return td.getBoundingBox().contains(
							// script.mouse.getPosition());
							// }
							//
							// }, 3000, 100, script)) {
							// script.mouse.click(false);
							// }
							script.mouse.click(td);
							Library.waitFor(new Conditional() {

								@Override
								public boolean isSatisfied(Script script) {
									return script.myPlayer().isMoving();
								}
							}, 1000, 100, script);
							Library.waitFor(new Conditional() {

								@Override
								public boolean isSatisfied(Script script) {
									return !script.myPlayer().isMoving();
								}
							}, 5000, 100, script);
							Library.waitFor(new Conditional() {

								@Override
								public boolean isSatisfied(Script script) {
									return script.bank.isOpen();
								}
							}, 1000, 100, script);
							return 0;
						}
						if (DreamMiner.USE_CUSTOM_INTERACT)
							Library.interact(bankBooth, "Bank", script);
						else
							bankBooth.interact("Bank");
					}
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return script.myPlayer().isMoving();
						}
					}, 1000, 100, script);
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return !script.myPlayer().isMoving();
						}
					}, 5000, 100, script);
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return script.bank.isOpen();
						}
					}, 1000, 100, script);
				} else {
					script.bank.depositAll(new PickaxeFilter<Item>());
				}
				break;
			case WALK_TO_MINE:
				if (script.objects.closest("Rune Essence") != null) {
					state = State.MINE;
					break;
				}
				if (miningArea.isInAreaInclusive(script.myPosition())
						|| miningArea2.isInAreaInclusive(script.myPosition())) {
					NPC aubury = script.npcs.closest("Aubury");
					if (aubury != null) {
						if (aubury.interact("Teleport")) {
							try {
								script.sleep(script.random(2000, 3000));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else {
							try {
								script.sleep(script.random(400, 700));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						break;
					}
				} else {
					RS2Object closedDoor = null;
					for (RS2Object o : script.objects.getAll()) {
						if (o != null && o.getName().equals("Door")
								&& o.getX() == 3253 && o.getY() == 3398) {
							closedDoor = o;
							break;
						}
					}
					if (closedDoor != null) {
						// Closed door found, open it
						closedDoor.interact("Open");
						try {
							script.sleep(script.random(400, 700));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						break;
					}
					if (Library.distanceTo(new Position(3253, 3401, 0), script) < 6) {
						// Close by and no door
						MiniMapTileDestination td = new MiniMapTileDestination(
								script.bot, new Position(3253, 3401, 0));
						script.mouse.click(td);
						try {
							script.sleep(script.random(400, 700));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {
						walk(false);
					}
				}
				break;
			}
		}
		return 100;
	}

	// Code stolen from Laz <3
	private int walk(boolean reverse) {
		int pi = pathToMine.getClosestIndex(script.myPosition());
		if (pi == -1) {
			return -1;
		}

		if (!reverse && miningArea.isInAreaInclusive(script.myPosition())) {
			return -1;
		} else if (reverse && bankArea.isInAreaInclusive(script.myPosition())) {
			return -1;
		}

		int c = pathToMine.getCount();
		int wi = reverse ? 0 : c - 1;

		if (wi == pi) {
			return -1;
		}

		if (!script.settings.isRunning()
				&& script.settings.getRunEnergy() > random(30, 50)) {
			script.settings.setRunning(true);
			return gRandom(400, 200);
		}

		while (wi >= 0 && wi < c && (reverse ? wi <= pi : wi >= pi)) {
			Point point = pathToMine.get(wi);
			if (script.localWalker.walk(point.x, point.y)) {
				try {
					script.sleep(random(500, 800));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return 0;
			}
			wi += reverse ? 1 : -1;
		}

		return -1;
	}

	public void onMessage(Message m) throws InterruptedException {
		super.onMessage(m);
	}

	public void onExit() {
		stopScript = true;
	}
}