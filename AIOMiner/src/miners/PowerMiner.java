package miners;

import library.Area;
import library.Conditional;
import library.Library;
import miners.Miner.State;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.Message;
import org.osbot.rs07.api.ui.Tab;
import org.osbot.rs07.script.Script;

/**
 * 
 * Mines in the Lumbridge Swamp (the one with coal, mithril, and adamant)
 * 
 * @author Ericthecmh, Eliot
 * 
 */

public class PowerMiner extends Miner {

	/************************************ CONSTANTS ************************************/

	// Positions to powermine
	private Position[] powerminePositions;

	// pick
	private int[] doNotDropsInts = Library.pickIDs;

	// Area anywhere in the world
	private Area miningArea = new Area(0, 0, 100000, 100000);

	private boolean useM1D1;

	/**
	 * Constructor
	 * 
	 * @param string
	 *            the list of rock names to be mined
	 * @param set
	 *            Set of positions to mine
	 * @param s
	 *            reference to the actual script being run
	 * @param b
	 */
	public PowerMiner(Script s, String rocks, Position[] powerminePositions,
			boolean b, boolean useM1D1) {
		super(s, new String[] { rocks }, false, null, null, null, null, null,
				null, null, null, null, null, null, powerminePositions, b);
		this.powerminePositions = powerminePositions;
		this.useM1D1 = useM1D1;
	}

	/**
	 * onStart
	 * 
	 * Calls the super's onStart and sets up whatever necessary for this
	 * particular miner
	 * 
	 */
	@Override
	public void onStart() {
		super.onStart();
		state = State.MINE;
		canStart = true;
	}

	/**
	 * 
	 * onLoop
	 * 
	 * Worker for the script
	 * 
	 */
	public int onLoop() {
		try {
			super.onLoop();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (canStart) {
			switch (state) {
			case MINE:
				if (script.inventory.isFull()
						|| (script.inventory.getEmptySlots() != (pickaxeInInventory ? 27
								: 28) && useM1D1)) {
					state = State.DROP;
					break;
				}
				Object[] keys = incorrectIDs.keySet().toArray();
				for (Object i : keys) {
					long elapsed = System.currentTimeMillis()
							- incorrectIDs.get(i).addedTime;
					if (elapsed > incorrectIDs.get(i).expireTime)
						incorrectIDs.remove(i);
				}
				// If you're not mining/idle/rock you're mining is dead, then
				// mine a new rock
				if ((rockPosition == null
						|| Library.getRockIDAt(rockPosition, script) != rockID
						|| System.currentTimeMillis() - lastActionTime > 2000 || Library
						.getFacingRock(script) != Library.getRockAt(
						rockPosition, script))
						&& !script.myPlayer().isMoving()) {
					// System.out.println(Library
					// .getRockIDAt(rockPosition, script) + " " + rockID);
					// System.out.println(lastActionTime);
					// System.out.println("Getting new rock...");
					RS2Object nearestRock = Library.getRockForIDs(
							rocksPositions, miningArea, prioritize, script,
							incorrectIDs);
					if (nearestRock != null) {
						// System.out.println("New rock position: (" +
						// nearestRock.getX() + "," + nearestRock.getY() + ")");
						rockID = nearestRock.getId();
						rockPosition = nearestRock.getPosition();
						for (int i = 0; i < 5
								&& !Library.interactRock(nearestRock, script); i++) {
							try {
								script.sleep(random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						//script.log("DONE");
					} else {
						drop();
					}
				} else {
				}
				break;
			case DROP:
				while (script.inventory.getEmptySlots() != (pickaxeInInventory ? 27
						: 28)) {
					drop();
					try {
						script.sleep(random(400, 600));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				state = State.MINE;
				break;
			}
		}
		return 100;
	}

	private void drop() {
		// for (Item i : script.inventory.getItems()) {
		// if (i != null && i.getId() != pickaxeID) {
		// final int amount = script.inventory.getEmptySlots();
		// i.interact("Drop");
		// Library.waitFor(new Conditional() {
		//
		// @Override
		// public boolean isSatisfied(Script script) {
		// return script.inventory.getEmptySlots() != amount;
		// }
		//
		// }, 1000, 50, script);
		// break;
		// }
		// }
		try {
			Library.dropAllExcept(doNotDropsInts, script);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void onMessage(Message m) throws InterruptedException {
		super.onMessage(m);
	}

	public void onExit() {
		stopScript = true;
	}
}
