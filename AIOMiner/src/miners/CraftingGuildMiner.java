package miners;

import java.awt.Point;

import library.Area;
import library.Conditional;
import library.Library;
import library.PickaxeFilter;
import main.DreamMiner;



import org.osbot.rs07.api.Bank;
import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.map.PositionPolygon;
import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.Message;
import org.osbot.rs07.input.mouse.MiniMapTileDestination;
import org.osbot.rs07.script.Script;

/**
 * 
 * Mines in the Lumbridge Swamp (the one with coal, mithril, and adamant)
 * 
 * @author Ericthecmh, Eliot
 * 
 */

public class CraftingGuildMiner extends Miner {

	/************************************ CONSTANTS ************************************/

	// Mining area
	private final Area miningArea = new Area(2943, 3291, 2937, 3276);
	private final Area bankArea = new Area(3009, 3358, 3018, 3355);

	// Array of positions of some silver rocks (used to determine IDs)
	private final static Position[] silverPositions = new Position[] {
			new Position(2943, 3290, 0), new Position(2942, 3291, 0),
			new Position(2939, 3290, 0), new Position(2941, 3288, 0),
			new Position(2939, 3287, 0), new Position(2941, 3285, 0) };

	// Array of positions of some gold rocks (used to determine IDs)
	private final static Position[] goldPositions = new Position[] {
			new Position(2938, 3280, 0), new Position(2938, 3278, 0),
			new Position(2939, 3276, 0), new Position(2941, 3276, 0),
			new Position(2942, 3276, 0), new Position(2943, 3280, 0),
			new Position(2943, 3279, 0) };

	// Array of positions of some clay rocks (used to determine IDs)
	private final static Position[] clayPositions = new Position[] {
			new Position(2943, 3284, 0), new Position(2942, 3283, 0),
			new Position(2943, 3282, 0), new Position(2940, 3283, 0),
			new Position(2938, 3285, 0), new Position(2937, 3284, 0) };

	// Path to bank (from guild)
	private final PositionPolygon pathToBank = new PositionPolygon(new int[][] {
			{ 2933, 3290 }, { 2933, 3298 }, { 2938, 3303 }, { 2944, 3305 },
			{ 2951, 3305 }, { 2958, 3306 }, { 2965, 3306 }, { 2973, 3306 },
			{ 2980, 3306 }, { 2988, 3308 }, { 2995, 3312 }, { 3001, 3316 },
			{ 3006, 3322 }, { 3007, 3329 }, { 3007, 3337 }, { 3007, 3345 },
			{ 3007, 3353 }, { 3013, 3357 } });
	// Path to bank (from Falador center)
	private final PositionPolygon pathToBank1 = new PositionPolygon(
			new int[][] { { 2969, 3379 }, { 2979, 3377 }, { 2985, 3373 },
					{ 2993, 3369 }, { 2999, 3365 }, { 3006, 3361 },
					{ 3013, 3356 } });
	private final Position bankPosition = new Position(3013, 3357, 0);

	// Path to the guild
	private final PositionPolygon pathToGuild = new PositionPolygon(
			new int[][] { { 3013, 3356 }, { 3006, 3355 }, { 3007, 3348 },
					{ 3007, 3341 }, { 3007, 3334 }, { 3007, 3327 },
					{ 3005, 3320 }, { 3000, 3316 }, { 2995, 3313 },
					{ 2991, 3309 }, { 2986, 3304 }, { 2980, 3301 },
					{ 2974, 3297 }, { 2969, 3299 }, { 2962, 3299 },
					{ 2955, 3298 }, { 2948, 3296 }, { 2942, 3296 },
					{ 2936, 3294 }, { 2933, 3289 } });

	// Whether to use falador teletab to go to the bank
	private final boolean useTeletab;

	// Positions to test whether player is inside the mining guild
	private final Position locationTest = new Position(2933, 3290, 0);
	private final Position locationTest1 = new Position(2940, 3282, 0);

	/**
	 * Constructor
	 * 
	 * @param rocks
	 *            the list of rock names to be mined
	 * @param prioritize
	 *            whether or not to prioritize rocks by the order in rocks array
	 * @param s
	 *            reference to the actual script being run
	 * @param customPositions
	 * @param b
	 */
	public CraftingGuildMiner(Script s, String[] rocks, boolean prioritize,
			boolean useTeletab, Position[] customPositions, boolean b) {
		super(s, rocks, prioritize, null, null, null, silverPositions, null,
				goldPositions, null, null, null, null, clayPositions,
				customPositions, b);
		this.useTeletab = useTeletab;
	}

	/**
	 * onStart
	 * 
	 * Calls the super's onStart and sets up whatever necessary for this
	 * particular miner
	 * 
	 */
	@Override
	public void onStart() {
		super.onStart();
		if (miningArea.isInAreaInclusive(script.myPosition()))
			state = State.MINE;
		else if (bankArea.isInAreaInclusive(script.myPosition()))
			state = State.BANK;
		else if (script.inventory.getEmptySlots() < 14)
			state = State.WALK_TO_BANK;
		else
			state = State.WALK_TO_MINE;
		canStart = true;
	}

	/**
	 * 
	 * onLoop
	 * 
	 * Worker for the script
	 * 
	 */
	public int onLoop() {
		try {
			super.onLoop();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (canStart) {
			switch (state) {
			case MINE:
				Object[] keys = incorrectIDs.keySet().toArray();
				for (Object i : keys) {
					long elapsed = System.currentTimeMillis()
							- incorrectIDs.get(i).addedTime;
					if (elapsed > incorrectIDs.get(i).expireTime)
						incorrectIDs.remove(i);
				}
				if (script.inventory.isFull()) {
					state = State.WALK_TO_BANK;
					break;
				}
				// If you're not mining/idle/rock you're mining is dead, then
				// mine a new rock
				if ((rockPosition == null
						|| Library.getRockIDAt(rockPosition, script) != rockID || System
						.currentTimeMillis() - lastActionTime > 2000 || Library.getFacingRock(script) != Library.getRockAt(rockPosition, script))
						&& !script.myPlayer().isMoving()) {
					// System.out.println("Getting new rock...");
					RS2Object nearestRock = Library.getRockForIDs(
							rocksPositions, miningArea, prioritize, script,
							incorrectIDs);
					if (nearestRock != null) {
						// System.out.println("New rock position: (" +
						// nearestRock.getX() + "," + nearestRock.getY() + ")");
						rockID = nearestRock.getId();
						rockPosition = nearestRock.getPosition();
						for (int i = 0; i < 5
								&& !Library.interactRock(nearestRock, script); i++) {
							try {
								script.sleep(random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						lastActionTime = System.currentTimeMillis();
					} else {

					}
				} else {
				}
				break;
			case WALK_TO_BANK:
				if (useTeletab) {
					if (!script.map.canReach(locationTest)
							&& script.myPosition().getY() < 3293
							&& script.inventory.contains("Falador teleport")) {
						script.inventory.interact("Break", "Falador teleport");
						try {
							script.sleep(random(500, 600));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {
						if (Library.distanceTo(bankPosition, script) < 6) {
							if (bankArea.isInAreaInclusive(script.myPosition())
									&& !script.myPlayer().isMoving()) {
								keys = incorrectIDs.keySet().toArray();
								for (Object i : keys) {
									incorrectIDs.get(i).value--;
									if (incorrectIDs.get(i).value == 0) {
										incorrectIDs.remove(i);
									} else {
										long elapsed = System
												.currentTimeMillis()
												- incorrectIDs.get(i).addedTime;
										if (elapsed > incorrectIDs.get(i).expireTime)
											incorrectIDs.remove(i);
									}
								}
								state = State.BANK;
								break;
							} else {
								MiniMapTileDestination md = new MiniMapTileDestination(
										script.bot, bankPosition);
								script.mouse.click(md);
								try {
									script.sleep(random(500, 700));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						} else {
							Library.walkPath(pathToBank1, bankPosition, script);
						}
					}
				} else {
					if (!script.map.canReach(locationTest)
							&& script.myPosition().getY() < 3293) {
						RS2Object guildDoor = script.objects
								.closest("Guild Door");
						if (guildDoor != null) {
							if (DreamMiner.USE_CUSTOM_INTERACT)
								Library.interact(guildDoor, "Open", script);
							else
								guildDoor.interact("Open");
							Library.waitFor(new Conditional() {

								@Override
								public boolean isSatisfied(Script script) {
									return script.myPlayer().isMoving();
								}

							}, 2000, 100, script);
							if (Library.waitFor(new Conditional() {

								@Override
								public boolean isSatisfied(Script script) {
									return !script.myPlayer().isMoving();
								}

							}, 10000, 200, script)) {
								try {
									script.sleep(random(2000, 4000));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
					} else {
						if (Library.distanceTo(bankPosition, script) < 6) {
							if (bankArea.isInAreaInclusive(script.myPosition())
									&& !script.myPlayer().isMoving()) {
								keys = incorrectIDs.keySet().toArray();
								for (Object i : keys) {
									incorrectIDs.get(i).value--;
									if (incorrectIDs.get(i).value == 0) {
										incorrectIDs.remove(i);
									} else {
										long elapsed = System
												.currentTimeMillis()
												- incorrectIDs.get(i).addedTime;
										if (elapsed > incorrectIDs.get(i).expireTime)
											incorrectIDs.remove(i);
									}
								}
								state = State.BANK;
								break;
							} else {
								MiniMapTileDestination md = new MiniMapTileDestination(
										script.bot, bankPosition);
								script.mouse.click(md);
								try {
									script.sleep(random(500, 700));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						} else {
							Library.walkPath(pathToBank, bankPosition, script);
						}
					}
				}
				break;
			case BANK:
				if (script.inventory.getEmptySlots() > 20) {
					if (!pickaxeInInventory
							|| script.inventory.contains(pickaxeID)) {
						if (!useTeletab
								|| script.inventory
										.contains("Falador teleport")) {
							state = State.WALK_TO_MINE;
							break;
						}
					}
				}
				if (!script.bank.isOpen()) {
					RS2Object bankBooth = Library.closestObject("Bank booth",
							"Bank", script);
					if (bankBooth != null) {
						if (Library.distanceTo(bankBooth.getPosition(), script) > 3
								&& !bankArea.isInAreaInclusive(script
										.myPosition())) {
							final MiniMapTileDestination td = new MiniMapTileDestination(
									script.bot, bankBooth.getPosition());
							// script.mouse.move(td);
							// if (Library.waitFor(new Conditional() {
							//
							// @Override
							// public boolean isSatisfied(Script script) {
							// return td.getBoundingBox().contains(
							// script.mouse.getPosition());
							// }
							//
							// }, 3000, 100, script)) {
							// script.mouse.click(false);
							// }
							script.mouse.click(td);
							Library.waitFor(new Conditional() {

								@Override
								public boolean isSatisfied(Script script) {
									return script.myPlayer().isMoving();
								}
							}, 1000, 100, script);
							Library.waitFor(new Conditional() {

								@Override
								public boolean isSatisfied(Script script) {
									return !script.myPlayer().isMoving();
								}
							}, 5000, 100, script);
							Library.waitFor(new Conditional() {

								@Override
								public boolean isSatisfied(Script script) {
									return script.bank.isOpen();
								}
							}, 1000, 100, script);
							return 0;
						}
						if (DreamMiner.USE_CUSTOM_INTERACT)
							Library.interact(bankBooth, "Bank", script);
						else
							bankBooth.interact("Bank");
					}
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return script.myPlayer().isMoving();
						}
					}, 1000, 100, script);
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return !script.myPlayer().isMoving();
						}
					}, 5000, 100, script);
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return script.bank.isOpen();
						}
					}, 1000, 100, script);
				} else {
					script.bank.depositAll(new PickaxeFilter<Item>());
					while (useTeletab
							&& !script.inventory.contains("Falador teleport")) {
						script.bank.withdraw("Falador teleport",
								Bank.WITHDRAW_1);
						try {
							script.sleep(random(700, 1200));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				break;
			case WALK_TO_MINE:
				if (script.map.canReach(locationTest1)) {
					state = State.MINE;
					break;
				}
				if (Library.distanceTo(locationTest, script) < 6) {
					RS2Object guildDoor = script.objects.closest("Guild Door");
					if (guildDoor != null) {
						if (DreamMiner.USE_CUSTOM_INTERACT)
							Library.interact(guildDoor, "Open", script);
						else
							guildDoor.interact("Open");
						Library.waitFor(new Conditional() {

							@Override
							public boolean isSatisfied(Script script) {
								return script.myPlayer().isMoving();
							}

						}, 2000, 100, script);
						if (Library.waitFor(new Conditional() {

							@Override
							public boolean isSatisfied(Script script) {
								return !script.myPlayer().isMoving();
							}

						}, 10000, 200, script)) {
							try {
								script.sleep(random(2000, 4000));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				} else {
					Library.walkPath(pathToGuild, locationTest, script);
				}
				break;
			}
		}
		return 100;
	}

	// Code stolen from Laz <3
	private int walk(boolean reverse) {
		int pi = pathToBank.getClosestIndex(script.myPosition());
		if (pi == -1) {
			return -1;
		}

		if (!reverse && bankArea.isInAreaInclusive(script.myPosition())) {
			return -1;
		} else if (reverse && miningArea.isInAreaInclusive(script.myPosition())) {
			return -1;
		}

		int c = pathToBank.getCount();
		int wi = reverse ? 0 : c - 1;

		if (wi == pi) {
			return -1;
		}

		if (!script.settings.isRunning()
				&& script.settings.getRunEnergy() > random(30, 50)) {
			script.settings.setRunning(true);
			return gRandom(400, 200);
		}

		while (wi >= 0 && wi < c && (reverse ? wi <= pi : wi >= pi)) {
			Point point = pathToBank.get(wi);
			if (script.localWalker.walk(point.x, point.y)) {
				try {
					script.sleep(random(500, 800));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return 0;
			}
			wi += reverse ? 1 : -1;
		}

		return -1;
	}

	public void onMessage(Message m) throws InterruptedException {
		super.onMessage(m);
	}

	public void onExit() {
		stopScript = true;
	}
}