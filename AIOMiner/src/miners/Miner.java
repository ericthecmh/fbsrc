package miners;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import library.AntibanAction;
import library.Library;
import library.TimePair;
import main.DreamMiner;



import org.osbot.rs07.api.Inventory;
import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.GroundItem;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.EquipmentSlot;
import org.osbot.rs07.api.ui.Message;
import org.osbot.rs07.api.ui.Tab;
import org.osbot.rs07.input.mouse.MainScreenTileDestination;
import org.osbot.rs07.input.mouse.RectangleDestination;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;

/**
 * Miner script extended by other miners. Contains some library code to be run
 * when mining
 * 
 * @author Ericthecmh
 * 
 */
public class Miner extends Script {

	private long lastUpdate;

	// Enum to track state
	public enum State {
		BANK, WALK_TO_MINE, WALK_TO_BANK, MINE, DROP;
	}

	// Reference to script
	protected final Script script;
	private final AntibanAction antiban;

	// Array of positions of some copper rocks (used to determine IDs). Null if
	// not mining copper
	protected final Position[] copperPositions;
	// Array of positions of some tin rocks (used to determine IDs). Null if
	// not mining tin
	protected final Position[] tinPositions;
	// Array of positions of some iron rocks (used to determine IDs). Null if
	// not mining iron
	protected final Position[] ironPositions;
	// Array of positions of some silver rocks (used to determine IDs). Null if
	// not mining silver
	protected final Position[] silverPositions;
	// Array of positions of some coal rocks (used to determine IDs). Null if
	// not mining coal
	protected final Position[] coalPositions;
	// Array of positions of some gold rocks (used to determine IDs). Null if
	// not mining gold
	protected final Position[] goldPositions;
	// Array of positions of some granite (used to determine IDs). Null if
	// not mining granite
	protected final Position[] granitePositions;
	// Array of positions of some mithril rocks (used to determine IDs). Null if
	// not mining mithril
	protected final Position[] mithrilPositions;
	// Array of positions of some adamantite rocks (used to determine IDs). Null
	// if
	// not mining adamantite
	protected final Position[] adamantitePositions;
	// Array of positions of some runite rocks (used to determine IDs). Null if
	// not mining rune
	protected final Position[] runitePositions;
	// Array of positions of some clay rocks (used to determine IDs). Null if
	// not mining clay
	protected final Position[] clayPositions;
	// Array of positions of the rocks to powermine. Will mine all rocks
	// matching these IDs
	protected final Position[] customPositions;

	// Array of rocks to mine (names, eg. Copper, Coal, etc)
	protected final String[] rocks;

	// Whether to prioritize certain rocks (in the order in the rockIDs matrix)
	protected final boolean prioritize;

	// Whether the user starts with a pickaxe in inventory
	protected final boolean pickaxeInInventory;

	// Pickaxe ID
	protected final int pickaxeID;

	// rockIDs[i] is an array of IDs for rock i. rockIDs is in order passed into
	// constructor
	protected Position[][] rocksPositions;
	// Records the incorrect IDs
	protected HashMap<Integer, TimePair> incorrectIDs;

	// The rock that is currently being mined
	protected int rockID;
	protected Position rockPosition;

	protected boolean canStart;
	protected boolean stopScript;
	protected State state;

	public HashSet<Integer> immutableIDs = new HashSet<Integer>();
	public HashSet<Integer> realImmutableIDs = new HashSet<Integer>();
	protected HashMap<Integer, Integer> correctCounts = new HashMap<>();
	protected HashMap<Integer, Integer> incorrectCounts = new HashMap<>();

	public Miner(Script s, String[] rocks, boolean prioritize,
			Position[] copper, Position[] tin, Position[] iron,
			Position[] silver, Position[] coal, Position[] gold,
			Position[] granite, Position[] mithril, Position[] adamantite,
			Position[] runite, Position[] clay, boolean b) {
		this(s, rocks, prioritize, copper, tin, iron, silver, coal, gold,
				granite, mithril, adamantite, runite, clay, null, b);
	}

	public Miner(Script s, String[] rocks, boolean prioritize,
			Position[] copper, Position[] tin, Position[] iron,
			Position[] silver, Position[] coal, Position[] gold,
			Position[] granite, Position[] mithril, Position[] adamantite,
			Position[] runite, Position[] clay, Position[] custom, boolean b) {
		copperPositions = copper;
		tinPositions = tin;
		ironPositions = iron;
		silverPositions = silver;
		coalPositions = coal;
		goldPositions = gold;
		granitePositions = granite;
		mithrilPositions = mithril;
		adamantitePositions = adamantite;
		runitePositions = runite;
		clayPositions = clay;
		customPositions = custom;
		this.script = s;
		this.rocks = rocks;
		// Initialize and predict rock IDs
		makePrediction();
		this.prioritize = prioritize;
		int pickID = -1;
		// Determine the pickaxe ID being used, and whether it's in inventory
		boolean pickInInvent = false;
		for (int i = Library.pickIDs.length - 1; i >= 0; i--) {
			if ((script.equipment.getItemInSlot(EquipmentSlot.WEAPON.slot) != null)
					&& (script.equipment.getItemInSlot(
							EquipmentSlot.WEAPON.slot).getId() == Library.pickIDs[i])) {
				pickID = Library.pickIDs[i];
				pickInInvent = false;
				break;
			}
			if (script.inventory.contains(Library.pickIDs[i])) {
				pickID = Library.pickIDs[i];
				pickInInvent = true;
				break;
			}
		}
		pickaxeInInventory = pickInInvent;
		pickaxeID = pickID;
		antiban = new AntibanAction(script, b);
		if (pickID == -1) {
			script.log("Unable to find your pickaxe.");
			stopScript = true;
			canStart = true;
			return;
		} else {
			script.log("Pickaxe ID: " + pickID);
		}
		incorrectIDs = new HashMap<Integer, TimePair>();
		lastUpdate = System.currentTimeMillis();
	}

	/**
	 * 
	 * makePrediction
	 * 
	 * Predicts the IDs for the rocks that will be mined
	 * 
	 */
	private void makePrediction() {
		// Initialize and predict rock IDs
		rocksPositions = new Position[rocks.length][];
		int rocksIDsI = 0;
		for (String rock : rocks) {
			if (rock.contains("Coal")) { // Coal
				rocksPositions[rocksIDsI] = new Position[coalPositions.length];
				for (int i = 0; i < coalPositions.length; i++) {
					rocksPositions[rocksIDsI][i] = coalPositions[i];
					// script.log(rocksPositions[rocksIDsI][i] + "");
				}
			} else if (rock.contains("Mithril")) { // Mithril
				rocksPositions[rocksIDsI] = new Position[mithrilPositions.length];
				for (int i = 0; i < mithrilPositions.length; i++) {
					rocksPositions[rocksIDsI][i] = mithrilPositions[i];
					// script.log(rocksPositions[rocksIDsI][i] + "");
				}
			} else if (rock.contains("Adam")) { // Adamantite
				rocksPositions[rocksIDsI] = new Position[adamantitePositions.length];
				for (int i = 0; i < adamantitePositions.length; i++) {
					rocksPositions[rocksIDsI][i] = adamantitePositions[i];
					// script.log(rocksPositions[rocksIDsI][i] + "");
				}
			} else if (rock.contains("Run")) {
				rocksPositions[rocksIDsI] = new Position[runitePositions.length];
				for (int i = 0; i < runitePositions.length; i++) {
					rocksPositions[rocksIDsI][i] = runitePositions[i];
					// script.log(rocksPositions[rocksIDsI][i] + "");
				}

			} else if (rock.contains("Copper")) {
				rocksPositions[rocksIDsI] = new Position[copperPositions.length];
				for (int i = 0; i < copperPositions.length; i++) {
					rocksPositions[rocksIDsI][i] = copperPositions[i];
					// script.log(rocksPositions[rocksIDsI][i] + "");
				}
			} else if (rock.contains("Tin")) {
				rocksPositions[rocksIDsI] = new Position[tinPositions.length];
				for (int i = 0; i < tinPositions.length; i++) {
					rocksPositions[rocksIDsI][i] = tinPositions[i];
					// script.log(rocksPositions[rocksIDsI][i] + "");
				}
			} else if (rock.contains("Iron")) {
				rocksPositions[rocksIDsI] = new Position[ironPositions.length];
				for (int i = 0; i < ironPositions.length; i++) {
					rocksPositions[rocksIDsI][i] = ironPositions[i];
					// script.log(rocksPositions[rocksIDsI][i] + "");
				}
			} else if (rock.contains("Silver")) {
				rocksPositions[rocksIDsI] = new Position[silverPositions.length];
				for (int i = 0; i < silverPositions.length; i++) {
					rocksPositions[rocksIDsI][i] = silverPositions[i];
					// script.log(rocksPositions[rocksIDsI][i] + "");
				}
			} else if (rock.contains("Gold")) {
				rocksPositions[rocksIDsI] = new Position[goldPositions.length];
				for (int i = 0; i < goldPositions.length; i++) {
					rocksPositions[rocksIDsI][i] = goldPositions[i];
					// script.log(rocksPositions[rocksIDsI][i] + "");
				}
			} else if (rock.contains("Clay")) {
				rocksPositions[rocksIDsI] = new Position[clayPositions.length];
				for (int i = 0; i < clayPositions.length; i++) {
					rocksPositions[rocksIDsI][i] = clayPositions[i];
					// script.log(rocksPositions[rocksIDsI][i] + "");
				}
			} else if (rock.contains("Power") || rock.contains("Custom")) {
				rocksPositions[rocksIDsI] = new Position[customPositions.length];
				for (int i = 0; i < customPositions.length; i++) {
					rocksPositions[rocksIDsI][i] = customPositions[i];
					// script.log(rocksPositions[rocksIDsI][i] + "");
				}
			}
			rocksIDsI++;
		}
	}

	// Keep track of player's last action time
	protected long lastActionTime;

	public void onStart() {
		new Thread(new Runnable() {
			public void run() {
				while (!stopScript) {
					try {
						if (script.myPlayer().isMoving()
								|| script.myPlayer().getAnimation() != -1)
							lastActionTime = System.currentTimeMillis();
					} catch (Exception e) {

					}
				}
			}
		}).start();
	}

	// return the state
	public State getState() {
		return state;
	}

	@Override
	public int onLoop() throws InterruptedException {
		// System.out.println("OnLoop");
		if (canStart) {
			RS2Object rock = Library.getRockAt(rockPosition, script);
			if (rock != null && rock.getHeight() > DreamMiner.SMOKING_ROCK
					&& script.myPlayer().getAnimation() != -1) {
				ArrayList<Position> positions = new ArrayList<Position>();
				for (int i = -3; i < 4; i++) {
					for (int j = -3; j < 4; j++) {
						Position p = new Position(script.myPosition().getX()
								+ i, script.myPosition().getY() + j, script
								.myPosition().getZ());
						if (Library.distanceTo(p, script) > 2.5) {
							positions.add(p);
						}
					}
				}
				MainScreenTileDestination md = new MainScreenTileDestination(
						script.bot, positions.get(MethodProvider
								.random(positions.size())));
				script.mouse.click(md);
				script.sleep(1000);
				return 10;
			}
			while (!pickaxeInInventory
					&& script.inventory.contains(pickaxeID)
					&& !script.equipment.isWearingItem(EquipmentSlot.WEAPON,
							pickaxeID)) {
				script.inventory.interact("Wield", pickaxeID);
				script.sleep(random(2000, 4000));
			}
			if (script.equipment.isWearingItem(EquipmentSlot.WEAPON,
					"Pickaxe handle")
					|| script.inventory.contains("Pickaxe handle")) {
				// JOptionPane.showMessageDialog(null, "Pickaxe head");
				script.log("Finding pickaxe head");
				while (script.inventory.getEmptySlots() < 3) {
					script.log("Dropping stuff for space");
					script.inventory.dropForNameThatContains("ore");
					script.inventory.dropForNameThatContains("Coal");
				}
				script.log("Picking up head");
				List<GroundItem> items = script.groundItems.getAll();
				for (GroundItem item : items) {
					if ((item.getName().contains("pick"))
							&& (item.getName().contains("head"))) {
						try {
							for (int i = 0; i < 10 && !(item.interact("Take")); i++) {
								script.sleep(random(500, 600));
							}
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				if (!script.inventory.contains("Pickaxe handle")) {
					script.log("Unequipping handle");
					try {
						for (int i = 0; i < 10
								&& !script.equipment
										.unequip(EquipmentSlot.WEAPON); i++) {
							script.sleep(random(500, 600));
						}
						script.sleep(random(1000, 2000));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				Inventory inv = script.inventory;
				try {
					for (int i = 0; i < 10 && !script.tabs.open(Tab.INVENTORY); i++)
						script.sleep(random(500, 600));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				script.log("Remaking pickaxe");
				try {
					for (int j = 0; !inv.interact("Use", 466) && j < 10; j++) {
						script.sleep(random(500, 600));
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					for (int j = 0; j < 10
							&& !inv.interactWithNameThatContains("Use", "head"); j++) {
						script.sleep(random(500, 600));
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				script.sleep(random(2000, 4000));
			}
			if (stopScript)
				script.stop();
			script.dialogues.clickContinue();
			if (script.inventory.isItemSelected()) {
				// script.log(script.inventory.getSelectedItemName());
				RectangleDestination rd = new RectangleDestination(script.bot,
						630, 173, 25, 27);
				script.mouse.click(rd);
			}

			try {
				switch (state) {
				case MINE:
					if (script.myPlayer().getAnimation() != -1
							&& Math.random() < DreamMiner.antibanRate) {
						// script.log("Antiban");
						antiban.execute(script, rockID, rockPosition);
					}
					if (rockID > 0
							&& (rockPosition == null
									|| Library
											.getRockIDAt(rockPosition, script) != rockID
									|| System.currentTimeMillis()
											- lastActionTime > 2000 || Library
									.getFacingRock(script) != Library
									.getRockAt(rockPosition, script))
							&& !script.myPlayer().isMoving()) {
						if (correctCounts.containsKey(rockID)) {
							int num = correctCounts.get(rockID);
							num++;
							correctCounts.put(rockID, num);
						} else {
							correctCounts.put(rockID, 1);
						}
						for (int i : correctCounts.keySet()) {
							if (immutableIDs.contains(i))
								continue;
							int correctCount, incorrectCount;
							if (!incorrectCounts.containsKey(i))
								incorrectCount = 0;
							else
								incorrectCount = incorrectCounts.get(i);
							correctCount = correctCounts.get(i);
							if (correctCount > incorrectCount * 5
									&& correctCount >= 5) {
								immutableIDs.add(i);
							}
						}
						for (int i : incorrectCounts.keySet()) {
							if (!immutableIDs.contains(i))
								continue;
							int correctCount, incorrectCount;
							if (!correctCounts.containsKey(i)) {
								correctCount = 0;
							} else {
								correctCount = correctCounts.get(i);
							}
							incorrectCount = incorrectCounts.get(i);
							if (incorrectCount > correctCount + 5) {
								immutableIDs.remove(i);
							}
						}
					}
					break;
				case BANK:
					rockID = 0;
					rockPosition = null;
					break;
				case WALK_TO_BANK:
					RS2Object bankBooth = script.objects.closest("Bank booth");
					if (bankBooth != null
							&& Library.distanceTo(bankBooth.getPosition(),
									script) < 12) {
						Object[] keys = incorrectIDs.keySet().toArray();
						for (Object i : keys) {
							incorrectIDs.get(i).value--;
							if (incorrectIDs.get(i).value == 0) {
								incorrectIDs.remove(i);
							} else {
								long elapsed = System.currentTimeMillis()
										- incorrectIDs.get(i).addedTime;
								if (elapsed > incorrectIDs.get(i).expireTime)
									incorrectIDs.remove(i);
							}
						}
						state = State.BANK;
						break;
					}
				}
			} catch (Exception e) {

			}
		}
		return 0;
	}

	public void onMessage(Message m) throws InterruptedException {
		if (m.getMessage().contains("no ore currently")
				&& System.currentTimeMillis() - lastUpdate > 2000) {
			int test = Library.getRockIDAt(rockPosition, script);
			// script.log("Test ID: " + test + "; Rock ID: " + rockID);
			if (test == rockID && !immutableIDs.contains(rockID) && !realImmutableIDs.contains(rockID)) {
				script.log("Dead ID detected: " + rockID);
				if (incorrectCounts.containsKey(rockID)) {
					int num = incorrectCounts.get(rockID);
					num++;
					incorrectCounts.put(rockID, num);
				} else {
					incorrectCounts.put(rockID, 1);
				}
				incorrectIDs.put(rockID,
						new TimePair(100, System.currentTimeMillis(),
								MethodProvider.random(4, 8) * 60 * 1000));
				rockID = -1;
				rockPosition = null;
			}
			lastUpdate = System.currentTimeMillis();
		}
	}
}