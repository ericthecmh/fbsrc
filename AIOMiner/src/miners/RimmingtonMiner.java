package miners;

import java.awt.Point;
import java.util.ArrayList;

import library.Area;
import library.BestMatch;
import library.Conditional;
import library.Library;
import main.DreamMiner;

import org.osbot.rs07.api.Bank;
import org.osbot.rs07.api.filter.Filter;
import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.map.PositionPolygon;
import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.Message;
import org.osbot.rs07.script.Script;

/**
 * 
 * Mines in the Varrock East
 * 
 * @author Ericthecmh, Erik
 * 
 */

public class RimmingtonMiner extends Miner {

	/************************************ CONSTANTS ************************************/

	// Mining area
	private final Area miningArea = new Area(2962, 3225, 2992, 3252);
	private final Area bankArea = new Area(3043, 3234, 3048, 3237);

	// Array of positions of some copper rocks (used to determine IDs)
	private final static Position[] copperPositions = new Position[] {
			new Position(2979, 3248, 0), new Position(2978, 3248, 0),
			new Position(2977, 3247, 0), new Position(2976, 3247, 0),
			new Position(2977, 3245, 0) };

	// Array of positions of some tin rocks (used to determine IDs)
	private final static Position[] tinPositions = new Position[] {
			new Position(2986, 3235, 0), new Position(2984, 3237, 0) };

	// Array of positions of some iron rocks (used to determine IDs)
	private final static Position[] ironPositions = new Position[] {
			new Position(2969, 3242, 0), new Position(2969, 3240, 0),
			new Position(2968, 3239, 0), new Position(2971, 3237, 0),
			new Position(2981, 3233, 0), new Position(2982, 3234, 0) };

	// Array of positions of some gold rocks (used to determine IDs)
	private final static Position[] goldPositions = new Position[] {
			new Position(2977, 3233, 0), new Position(2975, 3234, 0) };

	// Array of positions of some clay rocks (used to determine IDs)
	private final static Position[] clayPositions = new Position[] {
			new Position(2986, 3239, 0), new Position(2987, 3240, 0) };

	// Path to bank
	private final PositionPolygon pathToBank = new PositionPolygon(new int[][] {
			{ 2977, 3242 }, { 2978, 3252 }, { 2985, 3252 }, { 2994, 3248 },
			{ 3004, 3244 }, { 3016, 3242 }, { 3027, 3240 }, { 3037, 3237 },
			{ 3045, 3235 } });

	/**
	 * Constructor
	 * 
	 * @param rocks
	 *            the list of rock names to be mined
	 * @param prioritize
	 *            whether or not to prioritize rocks by the order in rocks array
	 * @param s
	 *            reference to the actual script being run
	 * @param customPositions
	 * @param b
	 */
	public RimmingtonMiner(Script s, String[] rocks, boolean prioritize,
			Position[] customPositions, boolean b) {
		super(s, rocks, prioritize, copperPositions, tinPositions,
				ironPositions, null, null, goldPositions, null, null, null,
				null, clayPositions, customPositions, b);
	}

	/**
	 * onStart
	 * 
	 * Calls the super's onStart and sets up whatever necessary for this
	 * particular miner
	 * 
	 */
	@Override
	public void onStart() {
		super.onStart();
		if (miningArea.isInAreaInclusive(script.myPosition()))
			state = State.MINE;
		else if (bankArea.isInAreaInclusive(script.myPosition()))
			state = State.BANK;
		else if (script.inventory.getEmptySlots() < 14)
			state = State.WALK_TO_BANK;
		else
			state = State.WALK_TO_MINE;
		canStart = true;
	}

	/**
	 * 
	 * onLoop
	 * 
	 * Worker for the script
	 * 
	 * @throws InterruptedException
	 * 
	 */
	public int onLoop() throws InterruptedException {
		try {
			super.onLoop();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (canStart) {
			switch (state) {
			case MINE:
				Object[] keys = incorrectIDs.keySet().toArray();
				for (Object i : keys) {
					long elapsed = System.currentTimeMillis()
							- incorrectIDs.get(i).addedTime;
					if (elapsed > incorrectIDs.get(i).expireTime)
						incorrectIDs.remove(i);
				}
				if (script.inventory.isFull()) {
					state = State.WALK_TO_BANK;
					break;
				}
				// If you're not mining/idle/rock you're mining is dead, then
				// mine a new rock
				if ((rockPosition == null
						|| Library.getRockIDAt(rockPosition, script) != rockID
						|| System.currentTimeMillis() - lastActionTime > 2000 || Library.getFacingRock(script) != Library.getRockAt(
						rockPosition, script))
						&& !script.myPlayer().isMoving()) {
					// System.out.println("Getting new rock...");
					RS2Object nearestRock = Library.getRockForIDs(
							rocksPositions, miningArea, prioritize, script,
							incorrectIDs);
					if (nearestRock != null) {
						// System.out.println("New rock position: (" +
						// nearestRock.getX() + "," + nearestRock.getY() + ")");
						rockID = nearestRock.getId();
						rockPosition = nearestRock.getPosition();
						for (int i = 0; i < 5
								&& !Library.interactRock(nearestRock, script); i++) {
							try {
								script.sleep(random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						lastActionTime = System.currentTimeMillis();
					} else {

					}
				} else {
				}
				break;
			case WALK_TO_BANK:
				if (bankArea.isInAreaInclusive(script.myPosition())) {
					keys = incorrectIDs.keySet().toArray();
					for (Object i : keys) {
						incorrectIDs.get(i).value--;
						if (incorrectIDs.get(i).value == 0) {
							incorrectIDs.remove(i);
						} else {
							long elapsed = System.currentTimeMillis()
									- incorrectIDs.get(i).addedTime;
							if (elapsed > incorrectIDs.get(i).expireTime)
								incorrectIDs.remove(i);
						}
					}
					state = State.BANK;
					break;
				}
				walk(false);
				break;
			case BANK:
				if (script.inventory.getEmptySlots() > 20) {
					if (!pickaxeInInventory
							|| script.inventory.contains(pickaxeID)) {
						state = State.WALK_TO_MINE;
						break;
					}
				}
				if (!script.bank.isOpen()) {
					RS2Object bankBooth = Library.closestObject(
							"Bank deposit box", "Deposit", script);
					if (bankBooth != null) {
						if (DreamMiner.USE_CUSTOM_INTERACT)
							Library.interact(bankBooth, "Deposit", script);
						else
							bankBooth.interact("Deposit");
					}
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return script.myPlayer().isMoving();
						}
					}, 1000, 100, script);
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return !script.myPlayer().isMoving();
						}
					}, 5000, 100, script);
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return script.bank.isOpen();
						}
					}, 1000, 100, script);
				} else {
					if (pickaxeInInventory) {
						script.bank.depositAll(new Filter<Item>() {

							@Override
							public boolean match(Item item) {
								return item != null
										&& item.getId() != pickaxeID;
							}

						});
					} else {
						script.bank.depositAll();
					}
					while (pickaxeInInventory
							&& !script.inventory.contains(pickaxeID)) {
						script.bank.withdraw(pickaxeID, Bank.WITHDRAW_1);
						try {
							script.sleep(random(700, 1200));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				library.DepositBox b = new library.DepositBox(script);
				if (b.boxIsOpen()) {
					Item[] is = b.getItems();
					ArrayList<Item> items = new ArrayList<Item>();
					ArrayList<Integer> slots = new ArrayList<Integer>();
					do {
						is = b.getItems();
						for (Item i : is) {
							if (i != null && !items.contains(i)
									&& !i.getName().contains("pickaxe")) {
								slots.add(b.getItemSlot(i));
								items.add(i);
							}
						}
						for (int i : slots) {
							b.interact(b.getRectangle(i), "Deposit All");
							script.sleep(200);
						}
						script.sleep(1000);
						if (script.inventory
								.onlyContains(new BestMatch(script)))
							break;
					} while (!script.inventory.isEmpty());
				}
				break;
			case WALK_TO_MINE:
				if (miningArea.isInAreaInclusive(script.myPosition())) {
					state = State.MINE;
					break;
				}
				walk(true);
				break;
			}
		}
		return 100;
	}

	// Code stolen from Laz <3
	private int walk(boolean reverse) {
		int pi = pathToBank.getClosestIndex(script.myPosition());
		if (pi == -1) {
			return -1;
		}

		if (!reverse && bankArea.isInAreaInclusive(script.myPosition())) {
			return -1;
		} else if (reverse && miningArea.isInAreaInclusive(script.myPosition())) {
			return -1;
		}

		int c = pathToBank.getCount();
		int wi = reverse ? 0 : c - 1;

		if (wi == pi) {
			return -1;
		}

		if (!script.settings.isRunning()
				&& script.settings.getRunEnergy() > random(30, 50)) {
			script.settings.setRunning(true);
			return gRandom(400, 200);
		}

		while (wi >= 0 && wi < c && (reverse ? wi <= pi : wi >= pi)) {
			Point point = pathToBank.get(wi);
			if (script.localWalker.walk(point.x, point.y)) {
				try {
					script.sleep(random(500, 800));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return 0;
			}
			wi += reverse ? 1 : -1;
		}

		return -1;
	}

	public void onMessage(Message m) throws InterruptedException {
		super.onMessage(m);
	}

	public void onExit() {
		stopScript = true;
	}
}