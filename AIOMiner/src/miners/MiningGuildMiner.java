package miners;

import java.awt.Point;
import java.util.Set;

import library.Area;
import library.Conditional;
import library.Library;
import library.PickaxeFilter;
import main.DreamMiner;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.map.PositionPolygon;
import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.Message;
import org.osbot.rs07.input.mouse.MiniMapTileDestination;
import org.osbot.rs07.script.Script;

/**
 * 
 * Mines in the Lumbridge Swamp (the one with coal, mithril, and adamant)
 * 
 * @author Ericthecmh, Eliot
 * 
 */

public class MiningGuildMiner extends Miner {

	/************************************ CONSTANTS ************************************/

	// Mining area
	private final Area miningArea = new Area(3016, 9756, 3055, 9732);
	private final Area bankArea = new Area(3009, 3358, 3018, 3355);

	// Array of positions of some coal rocks (used to determine IDs)
	private final static Position[] coalPositions = new Position[] {
			new Position(3028, 9739, 0), new Position(3028, 9737, 0),
			new Position(3029, 9734, 0), new Position(3031, 9739, 0),
			new Position(3032, 9741, 0), new Position(3032, 9737, 0),
			new Position(3034, 9739, 0), new Position(3035, 9742, 0),
			new Position(3037, 9739, 0), new Position(3035, 9734, 0),
			new Position(3037, 9735, 0), new Position(3032, 9735, 0),
			new Position(3037, 9733, 0), new Position(3039, 9733, 0),
			new Position(3040, 9732, 0), new Position(3039, 9738, 0),
			new Position(3039, 9741, 0), new Position(3042, 9739, 0),
			new Position(3038, 9748, 0), new Position(3045, 9744, 0),
			new Position(3043, 9742, 0), new Position(3044, 9753, 0),
			new Position(3048, 9754, 0), new Position(3048, 9747, 0),
			new Position(3048, 9743, 0), new Position(3045, 9740, 0),
			new Position(3044, 9737, 0), new Position(3047, 9737, 0),
			new Position(3049, 9740, 0), new Position(3046, 9735, 0),
			new Position(3042, 9734, 0), new Position(3049, 9734, 0),
			new Position(3049, 9735, 0), new Position(3051, 9735, 0),
			new Position(3054, 9739, 0), new Position(3053, 9742, 0),
			new Position(3054, 9746, 0) };

	// Array of positions of some mithril rocks (used to determine IDs)
	private final static Position[] mithrilPositions = new Position[] {
			new Position(3046, 9733, 0), new Position(3047, 9733, 0),
			new Position(3050, 9738, 0), new Position(3053, 9737, 0),
			new Position(3052, 9739, 0) };

	// Path to ladder (from mine)
	private final PositionPolygon pathToLadder = new PositionPolygon(
			new int[][] { { 3052, 9744 }, { 3047, 9743 }, { 3043, 9741 },
					{ 3038, 9740 }, { 3033, 9738 }, { 3029, 9738 },
					{ 3026, 9738 }, { 3021, 9739 } });
	private final Position ladderPosition = new Position(3021, 9739, 0);

	// Path to bank (from ladder)
	private final PositionPolygon pathToBank = new PositionPolygon(new int[][] {
			{ 3021, 3339 }, { 3024, 3337 }, { 3028, 3336 }, { 3031, 3341 },
			{ 3030, 3346 }, { 3026, 3350 }, { 3023, 3353 }, { 3023, 3358 },
			{ 3018, 3361 }, { 3013, 3357 } });
	private final Position bankPosition = new Position(3013, 3357, 0);

	// Path to ladder (from bank)
	private final PositionPolygon pathToLadder1 = new PositionPolygon(
			new int[][] { { 3013, 3357 }, { 3018, 3360 }, { 3023, 3357 },
					{ 3024, 3352 }, { 3031, 3343 }, { 3021, 3339 } });
	private final Position ladderPosition1 = new Position(3021, 3339, 0);

	/**
	 * Constructor
	 * 
	 * @param rocks
	 *            the list of rock names to be mined
	 * @param prioritize
	 *            whether or not to prioritize rocks by the order in rocks array
	 * @param s
	 *            reference to the actual script being run
	 * @param customPositions
	 * @param b
	 */
	public MiningGuildMiner(Script s, String[] rocks, boolean prioritize,
			Position[] customPositions, boolean b) {
		super(s, rocks, prioritize, null, null, null, null, coalPositions,
				null, null, mithrilPositions, null, null, null,
				customPositions, b);
	}

	/**
	 * onStart
	 * 
	 * Calls the super's onStart and sets up whatever necessary for this
	 * particular miner
	 * 
	 */
	@Override
	public void onStart() {
		super.onStart();
		if (miningArea.isInAreaInclusive(script.myPosition()))
			state = State.MINE;
		else if (bankArea.isInAreaInclusive(script.myPosition()))
			state = State.BANK;
		else if (script.inventory.getEmptySlots() < 14)
			state = State.WALK_TO_BANK;
		else
			state = State.WALK_TO_MINE;
		canStart = true;
	}

	/**
	 * 
	 * onLoop
	 * 
	 * Worker for the script
	 * 
	 */
	public int onLoop() {
		try {
			super.onLoop();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (canStart) {
			switch (state) {
			case MINE:
				Object[] keys = incorrectIDs.keySet().toArray();
				for (Object i : keys) {
					long elapsed = System.currentTimeMillis()
							- incorrectIDs.get(i).addedTime;
					if (elapsed > incorrectIDs.get(i).expireTime)
						incorrectIDs.remove(i);
				}
				if (script.inventory.isFull()) {
					state = State.WALK_TO_BANK;
					break;
				}
				// If you're not mining/idle/rock you're mining is dead, then
				// mine a new rock
				if ((rockPosition == null
						|| Library.getRockIDAt(rockPosition, script) != rockID
						|| System.currentTimeMillis() - lastActionTime > 2000 || Library.getFacingRock(script) != Library.getRockAt(
						rockPosition, script))
						&& !script.myPlayer().isMoving()) {
					// System.out.println("Getting new rock...");
					RS2Object nearestRock = Library.getRockForIDs(
							rocksPositions, miningArea, prioritize, script,
							incorrectIDs);
					if (nearestRock != null) {
						// System.out.println("New rock position: (" +
						// nearestRock.getX() + "," + nearestRock.getY() + ")");
						rockID = nearestRock.getId();
						rockPosition = nearestRock.getPosition();
						for (int i = 0; i < 5
								&& !Library.interactRock(nearestRock, script); i++) {
							try {
								script.sleep(random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						lastActionTime = System.currentTimeMillis();
					} else {

					}
				} else {
				}
				break;
			case WALK_TO_BANK:
				System.out.println("1");
				if (bankArea.isInAreaInclusive(script.myPosition())) {
					System.out.println("2");
					keys = incorrectIDs.keySet().toArray();
					for (Object i : keys) {
						incorrectIDs.get(i).value--;
						if (incorrectIDs.get(i).value == 0) {
							incorrectIDs.remove(i);
						} else {
							long elapsed = System.currentTimeMillis()
									- incorrectIDs.get(i).addedTime;
							if (elapsed > incorrectIDs.get(i).expireTime)
								incorrectIDs.remove(i);
						}
					}
					state = State.BANK;
					break;
				} else {
					System.out.println("3");
					if (miningArea.isInAreaInclusive(script.myPosition())) {
						System.out.println("4");
						if (Library.distanceTo(ladderPosition, script) > 6) {
							Library.walkPath(pathToLadder, ladderPosition,
									script);
						} else {
							RS2Object ladder = script.objects.closest("Ladder");
							if (ladder != null) {
								if (DreamMiner.USE_CUSTOM_INTERACT)
									Library.interact(ladder, "Climb-up", script);
								else
									ladder.interact("Climb-up");
								try {
									script.sleep(random(500, 700));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
					} else {
						System.out.println("5");
						if (Library.distanceTo(bankPosition, script) < 6) {
							if (bankArea.isInAreaInclusive(script.myPosition())
									&& !script.myPlayer().isMoving()) {
								System.out.println("6");
								keys = incorrectIDs.keySet().toArray();
								for (Object i : keys) {
									incorrectIDs.get(i).value--;
									if (incorrectIDs.get(i).value == 0) {
										incorrectIDs.remove(i);
									} else {
										long elapsed = System
												.currentTimeMillis()
												- incorrectIDs.get(i).addedTime;
										if (elapsed > incorrectIDs.get(i).expireTime)
											incorrectIDs.remove(i);
									}
								}
								state = State.BANK;
								break;
							} else {
								System.out.println("7");
								MiniMapTileDestination md = new MiniMapTileDestination(
										script.bot, bankPosition);
								script.mouse.click(md);
								try {
									script.sleep(random(500, 700));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						} else {
							System.out.println("8");
							Library.walkPath(pathToBank, bankPosition, script);
						}
					}
				}
				break;
			case BANK:
				if (!bankArea.isInAreaInclusive(script.myPosition())) {
					state = State.WALK_TO_BANK;
					break;
				}

				if (script.inventory.getEmptySlots() > 20) {
					if (!pickaxeInInventory
							|| script.inventory.contains(pickaxeID)) {
						state = State.WALK_TO_MINE;
						break;
					}
				}
				if (!script.bank.isOpen()) {
					RS2Object bankBooth = Library.closestObject("Bank booth",
							"Bank", script);
					if (bankBooth != null) {
						if (DreamMiner.USE_CUSTOM_INTERACT)
							Library.interact(bankBooth, "Bank", script);
						else
							bankBooth.interact("Bank");
					}
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return script.myPlayer().isMoving();
						}
					}, 1000, 100, script);
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return !script.myPlayer().isMoving();
						}
					}, 5000, 100, script);
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return script.bank.isOpen();
						}
					}, 1000, 100, script);
				} else {
					script.bank.depositAll(new PickaxeFilter<Item>());
				}
				break;
			case WALK_TO_MINE:
				if (miningArea.isInAreaInclusive(script.myPosition())) {
					state = State.MINE;
					break;
				} else {
					if (!miningArea.isInAreaInclusive(script.myPosition())) {
						if (Library.distanceTo(ladderPosition1, script) > 6) {
							Library.walkPath(pathToLadder1, ladderPosition1,
									script);
						} else {
							RS2Object ladder = script.objects.closest("Ladder");
							if (ladder != null) {
								if (DreamMiner.USE_CUSTOM_INTERACT)
									Library.interact(ladder, "Climb-down",
											script);
								else
									ladder.interact("Climb-down");
								try {
									script.sleep(random(500, 700));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
					}
				}
				break;
			}
		}
		return 100;
	}

	// Code stolen from Laz <3
	private int walk(boolean reverse) {
		int pi = pathToBank.getClosestIndex(script.myPosition());
		if (pi == -1) {
			return -1;
		}

		if (!reverse && bankArea.isInAreaInclusive(script.myPosition())) {
			return -1;
		} else if (reverse && miningArea.isInAreaInclusive(script.myPosition())) {
			return -1;
		}

		int c = pathToBank.getCount();
		int wi = reverse ? 0 : c - 1;

		if (wi == pi) {
			return -1;
		}

		if (!script.settings.isRunning()
				&& script.settings.getRunEnergy() > random(30, 50)) {
			script.settings.setRunning(true);
			return gRandom(400, 200);
		}

		while (wi >= 0 && wi < c && (reverse ? wi <= pi : wi >= pi)) {
			Point point = pathToBank.get(wi);
			if (script.localWalker.walk(point.x, point.y)) {
				try {
					script.sleep(random(500, 800));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return 0;
			}
			wi += reverse ? 1 : -1;
		}

		return -1;
	}

	public void onMessage(Message m) throws InterruptedException {
		super.onMessage(m);
	}

	public void onExit() {
		stopScript = true;
	}
}
