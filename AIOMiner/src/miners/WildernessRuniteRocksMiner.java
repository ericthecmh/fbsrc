package miners;

import library.Area;
import library.Conditional;
import library.Library;
import library.PickaxeFilter;
import main.DreamMiner;

import org.osbot.rs07.api.Bank;
import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.map.PositionPolygon;
import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.Message;
import org.osbot.rs07.api.ui.Skill;
import org.osbot.rs07.input.mouse.MiniMapTileDestination;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;

/**
 * 
 * Mines Runite Rocks in the Wilderness (right north of Lava Maze)
 * 
 * @author Ericthecmh
 * 
 */

public class WildernessRuniteRocksMiner extends Miner {

	/************************************ CONSTANTS ************************************/

	// Mining area
	private final Area miningArea = new Area(3055, 3889, 3064, 3880);
	private final Area bankArea = new Area(3097, 3240, 3088, 3246);

	// Array of positions of the runite rocks (used to determine IDs)
	private final static Position[] runitePositions = new Position[] {
			new Position(3060, 3884, 0), new Position(3059, 3885, 0) };

	// Path to gate (from rocks to gate)
	private final PositionPolygon pathToGate = new PositionPolygon(new int[][] {
			{ 3059, 3884 }, { 3053, 3889 }, { 3044, 3891 }, { 3028, 3896 },
			{ 3021, 3897 }, { 3013, 3898 }, { 3005, 3899 }, { 2996, 3900 },
			{ 2989, 3900 }, { 2981, 3900 }, { 2972, 3901 }, { 2964, 3902 },
			{ 2955, 3902 }, { 2949, 3903 } });
	private final Position gatePosition = new Position(2947, 3904, 0);
	// Path from gate to the bank
	private final PositionPolygon pathToBank = new PositionPolygon(new int[][] {
			{ 2948, 3904 }, { 2952, 3910 }, { 2952, 3917 }, { 2955, 3924 },
			{ 2965, 3927 }, { 2963, 3920 }, { 2972, 3915 }, { 2981, 3915 },
			{ 2988, 3912 }, { 2996, 3911 }, { 3004, 3910 }, { 3011, 3913 },
			{ 3018, 3919 }, { 3025, 3923 }, { 3033, 3928 }, { 3040, 3932 },
			{ 3047, 3936 }, { 3053, 3936 }, { 3063, 3939 }, { 3071, 3942 },
			{ 3076, 3950 }, { 3084, 3956 }, { 3091, 3959 }, { 3097, 3957 } });
	private final Position bankPosition = new Position(3097, 3957, 0);

	// Path to gate (from bank to gate)
	private final PositionPolygon pathToGate1 = new PositionPolygon(
			new int[][] { { 3097, 3957 }, { 3091, 3959 }, { 3084, 3956 },
					{ 3076, 3950 }, { 3071, 3942 }, { 3063, 3939 },
					{ 3053, 3936 }, { 3047, 3936 }, { 3040, 3932 },
					{ 3033, 3928 }, { 3025, 3923 }, { 3018, 3919 },
					{ 3011, 3913 }, { 3004, 3910 }, { 2996, 3911 },
					{ 2988, 3912 }, { 2981, 3915 }, { 2972, 3915 },
					{ 2963, 3920 }, { 2965, 3927 }, { 2955, 3924 },
					{ 2952, 3917 }, { 2952, 3910 }, { 2948, 3904 } });

	// Path from gate to mine
	private final PositionPolygon pathToMine = new PositionPolygon(new int[][] {
			{ 2949, 3903 }, { 2955, 3902 }, { 2964, 3902 }, { 2972, 3901 },
			{ 2981, 3900 }, { 2989, 3900 }, { 2996, 3900 }, { 3005, 3899 },
			{ 3013, 3898 }, { 3021, 3897 }, { 3028, 3896 }, { 3044, 3891 },
			{ 3053, 3889 }, { 3059, 3884 } });
	private final Position minePosition = new Position(3059, 3884, 0);

	// Outer most area of the bank building thing
	private final Area area1 = new Area(3096, 3958, 3095, 3956);
	private final Position web1Position = new Position(3095, 3957, 0);
	// Area between the two webs
	private final Area area2 = new Area(3094, 3958, 3093, 3956);
	private final Position web2Position = new Position(3092, 3957, 0);
	// Three tile area right after the second web
	private final Area area3_1 = new Area(3092, 3956, 3092, 3958);
	private final Area area3_2 = new Area(3090, 3958, 3091, 3954);
	private final Position leverPosition = new Position(3090, 3956, 0);

	// Position player is in after teleporting to bank
	private final Position bankTeleportPosition = new Position(2539, 4712, 0);

	// Whether or not the script should bank when there are at least x ores in
	// bank
	private final boolean bankGreater;
	// The number to bank at
	private final int bankGreaterAmount;

	// Whether to bank when there are no more runite rocks to mine
	// NOTE: This is mutually exclusive with searchHop
	private final boolean bankWhenNone;

	// Whether to hop worlds in search for a place to mine
	private final boolean searchHop;

	// Whether to hop if a player is nearby
	private final boolean hopPlayer;

	// Whether to eat at the bank
	private final boolean eatAtBank;
	private final String foodName;
	private final int eatUntil;

	/**
	 * Constructor
	 * 
	 * @param rocks
	 *            the list of rock names to be mined
	 * @param prioritize
	 *            whether or not to prioritize rocks by the order in rocks array
	 * @param s
	 *            reference to the actual script being run
	 * @param customPositions
	 * @param b
	 */
	public WildernessRuniteRocksMiner(Script s, String[] rocks,
			boolean prioritize, boolean bankGreater, int bankGreaterAmount,
			boolean bankWhenNone, boolean searchHop, boolean hopPlayer,
			boolean eatAtBank, String foodName, int eatUntil,
			Position[] customPositions, boolean b) {
		super(s, rocks, prioritize, null, null, null, null, null, null, null,
				null, null, runitePositions, null, customPositions, b);
		this.bankGreater = bankGreater;
		this.bankGreaterAmount = bankGreaterAmount;
		this.bankWhenNone = bankWhenNone;
		this.searchHop = searchHop;
		this.hopPlayer = hopPlayer;
		this.eatAtBank = eatAtBank;
		this.foodName = foodName;
		this.eatUntil = eatUntil;
	}

	/**
	 * onStart
	 * 
	 * Calls the super's onStart and sets up whatever necessary for this
	 * particular miner
	 * 
	 */
	@Override
	public void onStart() {
		super.onStart();
		if (miningArea.isInAreaInclusive(script.myPosition()))
			state = State.MINE;
		else if (bankArea.isInAreaInclusive(script.myPosition()))
			state = State.BANK;
		else if (script.inventory.getEmptySlots() < 14)
			state = State.WALK_TO_BANK;
		else
			state = State.WALK_TO_MINE;
		canStart = true;
	}

	/**
	 * 
	 * onLoop
	 * 
	 * Worker for the script
	 * 
	 */
	public int onLoop() {
		script.log(state.toString());
		try {
			super.onLoop();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (canStart) {
			switch (state) {
			case MINE:
				Object[] keys = incorrectIDs.keySet().toArray();
				for (Object i : keys) {
					long elapsed = System.currentTimeMillis()
							- incorrectIDs.get(i).addedTime;
					if (elapsed > incorrectIDs.get(i).expireTime)
						incorrectIDs.remove(i);
				}
				if (Library.playerNearBy(script) && hopPlayer) {
					script.log("PLAYER");
					((DreamMiner) script).worldHopping.switchWorlds();
					break;
				}
				if (script.inventory.isFull()
						|| (bankGreater && script.inventory
								.getAmount("Runite ore") >= bankGreaterAmount)
						|| (!searchHop && bankWhenNone)) {
					state = State.WALK_TO_BANK;
					break;
				}
				script.log("Runite ore: "
						+ script.inventory.getAmount("Runite ore"));
				// If you're not mining/idle/rock you're mining is dead, then
				// mine a new rock
				if ((rockPosition == null
						|| Library.getRockIDAt(rockPosition, script) != rockID
						|| System.currentTimeMillis() - lastActionTime > 2000 || Library
						.getFacingRock(script) != Library.getRockAt(
						rockPosition, script))
						&& !script.myPlayer().isMoving()) {
					// System.out.println("Getting new rock...");
					RS2Object nearestRock = Library.getRockForIDs(
							rocksPositions, miningArea, prioritize, script,
							incorrectIDs);
					if (nearestRock != null) {
						// System.out.println("New rock position: (" +
						// nearestRock.getX() + "," + nearestRock.getY() + ")");
						rockID = nearestRock.getId();
						rockPosition = nearestRock.getPosition();
						for (int i = 0; i < 5
								&& !Library.interactRock(nearestRock, script); i++) {
							try {
								script.sleep(random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						lastActionTime = System.currentTimeMillis();
					} else {
						if (searchHop) {
							((DreamMiner) script).worldHopping.switchWorlds();
						}
					}
				} else {
				}
				break;
			case WALK_TO_BANK:
				if (bankArea.isInAreaInclusive(script.myPosition())) {
					keys = incorrectIDs.keySet().toArray();
					for (Object i : keys) {
						incorrectIDs.get(i).value--;
						if (incorrectIDs.get(i).value == 0) {
							incorrectIDs.remove(i);
						} else {
							long elapsed = System.currentTimeMillis()
									- incorrectIDs.get(i).addedTime;
							if (elapsed > incorrectIDs.get(i).expireTime)
								incorrectIDs.remove(i);
						}
					}
					state = State.BANK;
					break;
				}
				// South of the border
				if (script.myPosition().getY() <= 3903) {
					RS2Object gate = script.objects.closest("Gate");
					if (gate != null
							&& (gate.getY() == 3903 || gate.getY() == 3904)) {
						// Found the right gate
						if (gate.getY() == 3904) {
							// If the gate is closed
							if (DreamMiner.USE_CUSTOM_INTERACT)
								Library.interact(gate, "Open", script);
							else
								gate.interact("Open");
							try {
								script.sleep(MethodProvider.random(450, 650));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else {
							// The gate is open
							script.localWalker
									.walk(new Position(2950, 3913, 0));
							try {
								script.sleep(MethodProvider.random(450, 650));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					} else {
						Library.walkPath(pathToGate, gatePosition, script);
					}
				} else {
					if (Library.distanceTo(bankPosition, script) > 10) {
						Library.walkPath(pathToBank, bankPosition, script);
					} else {
						// We are at the bank building thing
						if ((area3_1.isInAreaInclusive(script.myPosition()) || area3_2
								.isInAreaInclusive(script.myPosition()))) {
							if (!script.myPlayer().isMoving()) {
								// We are at the lever, interact with lever
								RS2Object lever = script.objects
										.closest("Lever");
								if (lever != null
										&& (DreamMiner.USE_CUSTOM_INTERACT ? Library
												.interact(lever, "Pull", script)
												: lever.interact("Pull"))) {
									try {
										script.sleep(MethodProvider.random(
												1000, 2000));
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									if (script.myPlayer().getAnimation() == 714
											|| script.myPlayer().getAnimation() == 2140) {
										try {
											script.sleep(MethodProvider.random(
													3000, 4000));
										} catch (InterruptedException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										keys = incorrectIDs.keySet().toArray();
										for (Object i : keys) {
											incorrectIDs.get(i).value--;
											if (incorrectIDs.get(i).value == 0) {
												incorrectIDs.remove(i);
											} else {
												long elapsed = System
														.currentTimeMillis()
														- incorrectIDs.get(i).addedTime;
												if (elapsed > incorrectIDs
														.get(i).expireTime)
													incorrectIDs.remove(i);
											}
										}
										state = State.BANK;
									} else if (Library.distanceTo(
											bankTeleportPosition, script) < 2) {
										keys = incorrectIDs.keySet().toArray();
										for (Object i : keys) {
											incorrectIDs.get(i).value--;
											if (incorrectIDs.get(i).value == 0) {
												incorrectIDs.remove(i);
											} else {
												long elapsed = System
														.currentTimeMillis()
														- incorrectIDs.get(i).addedTime;
												if (elapsed > incorrectIDs
														.get(i).expireTime)
													incorrectIDs.remove(i);
											}
										}
										state = State.BANK;
									}
								}
							}
						} else if (area2.isInAreaInclusive(script.myPosition())) {
							// Slash the second web if it's there, walk into the
							// room otherwise
							RS2Object web = Library.getObjectAt(web2Position,
									"Web", script);
							if (web != null) {
								if (DreamMiner.USE_CUSTOM_INTERACT)
									Library.interact(web, "Slash", script);
								else
									web.interact("Slash");
								try {
									script.sleep(MethodProvider
											.random(500, 700));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							} else {
								leverPosition.interact(script.bot, "Walk here");
							}
						} else if (area1.isInAreaInclusive(script.myPosition())) {
							// Slash the second web if it's there, walk into the
							// room otherwise
							RS2Object web = Library.getObjectAt(web1Position,
									"Web", script);
							if (web != null) {
								if (DreamMiner.USE_CUSTOM_INTERACT)
									Library.interact(web, "Slash", script);
								else
									web.interact("Slash");
								try {
									script.sleep(MethodProvider
											.random(500, 700));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							} else {
								web2Position.interact(script.bot, "Walk here");
							}
						} else {
							if (!script.myPlayer().isMoving()) {
								MiniMapTileDestination md = new MiniMapTileDestination(
										script.bot, web1Position);
								script.mouse.click(md);
								try {
									script.sleep(MethodProvider
											.random(500, 700));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
					}
				}
				break;
			case BANK:
				if (script.inventory.contains(foodName)) {
					// Inventory contains food. Eat it
					while (script.skills.getDynamic(Skill.HITPOINTS) < eatUntil) {
						while (script.bank.isOpen()) {
							script.bank.close();
							try {
								script.sleep(MethodProvider.random(500, 700));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						script.inventory.interact("Eat", foodName);
					}
				}
				if (script.inventory.getEmptySlots() > 20) {
					if (!pickaxeInInventory
							|| script.inventory.contains(pickaxeID)) {
						if (!eatAtBank
								|| script.skills.getDynamic(Skill.HITPOINTS) >= eatUntil) {
							if (script.inventory.contains(946) /* a knife */) {
								state = State.WALK_TO_MINE;
								break;
							}
						}
					}
				}
				if (!script.bank.isOpen()) {
					NPC gundai = script.npcs.closest("Gundai");
					if (gundai != null) {
						if (DreamMiner.USE_CUSTOM_INTERACT)
							Library.interact(gundai, "Bank", script);
						else
							gundai.interact("Bank");
					}
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return script.myPlayer().isMoving();
						}
					}, 1000, 100, script);
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return !script.myPlayer().isMoving();
						}
					}, 5000, 100, script);
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return script.bank.isOpen();
						}
					}, 1000, 100, script);
				} else {
					script.bank.depositAll(new PickaxeFilter<Item>());
					while (!script.inventory.contains(946)) {
						script.bank.withdraw(946, Bank.WITHDRAW_1);
						try {
							script.sleep(random(700, 1200));
						} catch (Exception e) {

						}
					}
					if (eatAtBank
							&& script.skills.getDynamic(Skill.HITPOINTS) < eatUntil) {
						script.bank.withdraw(foodName, Bank.WITHDRAW_ALL);
						try {
							script.sleep(random(700, 1200));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				break;
			case WALK_TO_MINE:
				if (Library.distanceTo(bankTeleportPosition, script) < 20) {
					// In the bank area
					RS2Object lever = script.objects.closest("Lever");
					if (lever != null) {
						if (DreamMiner.USE_CUSTOM_INTERACT)
							Library.interact(lever, "Pull", script);
						else
							lever.interact("Pull");
						try {
							script.sleep(random(500, 700));
						} catch (Exception e) {

						}
					}
				} else {
					if (script.myPosition().getY() <= 3903) {
						if (miningArea.isInAreaInclusive(script.myPosition())) {
							state = State.MINE;
						} else {
							Library.walkPath(pathToMine, minePosition, script);
						}
					} else {
						RS2Object gate = script.objects.closest("Gate");
						if (gate != null
								&& (gate.getY() == 3903 || gate.getY() == 3904)) {
							// Found the right gate
							if (gate.getY() == 3904) {
								// If the gate is closed
								if (DreamMiner.USE_CUSTOM_INTERACT)
									Library.interact(gate, "Open", script);
								else
									gate.interact("Open");
								try {
									script.sleep(MethodProvider
											.random(450, 650));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							} else {
								// The gate is open
								script.localWalker.walk(new Position(2950,
										3895, 0));
								try {
									script.sleep(MethodProvider
											.random(450, 650));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						} else {
							if (area3_1.isInAreaInclusive(script.myPosition())
									|| area3_2.isInAreaInclusive(script
											.myPosition())) {
								// Slash the second web if it's there, walk into
								// the
								// room otherwise
								RS2Object web = Library.getObjectAt(
										web2Position, "Web", script);
								if (web != null) {
									if (DreamMiner.USE_CUSTOM_INTERACT)
										Library.interact(web, "Slash", script);
									else
										web.interact("Slash");
									try {
										script.sleep(MethodProvider.random(500,
												700));
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								} else {
									web1Position.interact(script.bot,
											"Walk here");
								}
							} else if (area2.isInAreaInclusive(script
									.myPosition())) {
								// Slash the second web if it's there, walk into
								// the
								// room otherwise
								RS2Object web = Library.getObjectAt(
										web1Position, "Web", script);
								if (web != null) {
									if (DreamMiner.USE_CUSTOM_INTERACT)
										Library.interact(web, "Slash", script);
									else
										web.interact("Slash");
									try {
										script.sleep(MethodProvider.random(500,
												700));
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								} else {
									Library.walkPath(pathToGate1, gatePosition,
											script);
								}
							} else {
								Library.walkPath(pathToGate1, gatePosition,
										script);
							}
						}
					}
				}
				break;
			}
		}
		return 100;
	}

	public void onMessage(Message m) throws InterruptedException {
		super.onMessage(m);
	}

	public void onExit() {
		stopScript = true;
	}
}