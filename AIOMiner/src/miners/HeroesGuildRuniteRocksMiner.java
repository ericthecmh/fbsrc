package miners;

import library.Area;
import library.Conditional;
import library.Library;
import library.PickaxeFilter;
import main.DreamMiner;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.map.PositionPolygon;
import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.Message;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;

/**
 * 
 * Mines Runite Rocks in the Wilderness (right north of Lava Maze)
 * 
 * @author Ericthecmh
 * 
 */

public class HeroesGuildRuniteRocksMiner extends Miner {

	/************************************ CONSTANTS ************************************/

	// Mining area
	private final Area miningArea = new Area(2943, 9887, 2934, 9881);
	private final Area bankArea = new Area(3097, 3240, 3088, 3246);

	// Array of positions of the runite rocks (used to determine IDs)
	private final static Position[] runitePositions = new Position[] {
			new Position(2941, 9884, 0), new Position(2937, 9882, 0) };

	// Path to gate (from rocks to gate)
	private final PositionPolygon pathMineToLadder = new PositionPolygon(
			new int[][] { { 2937, 9883 }, { 2935, 9891 }, { 2936, 9897 },
					{ 2937, 9904 }, { 2932, 9910 }, { 2923, 9911 },
					{ 2915, 9912 }, { 2907, 9913 }, { 2900, 9911 },
					{ 2893, 9907 } });
	private final PositionPolygon pathToMine = new PositionPolygon(new int[][] {
			{ 2893, 9907 }, { 2900, 9911 }, { 2907, 9913 }, { 2915, 9912 },
			{ 2923, 9911 }, { 2932, 9910 }, { 2937, 9904 }, { 2936, 9897 },
			{ 2935, 9891 }, { 2937, 9883 } });
	private final PositionPolygon pathGuildToBar = new PositionPolygon(
			new int[][] { { 2902, 3510 }, { 2905, 3516 }, { 2908, 3522 },
					{ 2904, 3529 }, { 2903, 3536 }, { 2903, 3543 },
					{ 2907, 3544 } });
	private final PositionPolygon pathToGuild = new PositionPolygon(
			new int[][] { { 2907, 3544 }, { 2903, 3543 }, { 2903, 3536 },
					{ 2904, 3529 }, { 2908, 3522 }, { 2905, 3516 },
					{ 2902, 3510 } });
	private final Position barDoorPosition = new Position(2907, 3544, 0);
	private final Position lowerDoorPosition = new Position(3061, 4984, 1);
	private final Position ladderPosition = new Position(2892, 9907, 0);
	private final Position guildMiddlePosition = new Position(2897, 3510, 0);
	private final Position guildDoorPosition = new Position(2902, 3511, 0);
	private final Position minePosition = new Position(2937, 9883, 0);
	private final Position bankPosition = new Position(3043, 4972, 0);
	private final Position bankDoorPosition = new Position(3061, 4982, 0);

	private final Area barArea = new Area(2905, 3543, 2915, 3536);

	// Whether to hop worlds in search for a place to mine
	private final boolean searchHop;

	/**
	 * Constructor
	 * 
	 * @param rocks
	 *            the list of rock names to be mined
	 * @param prioritize
	 *            whether or not to prioritize rocks by the order in rocks array
	 * @param s
	 *            reference to the actual script being run
	 * @param customPositions
	 * @param b
	 */
	public HeroesGuildRuniteRocksMiner(Script s, String[] rocks,
			boolean prioritize, Position[] customPositions, boolean b) {
		super(s, rocks, prioritize, null, null, null, null, null, null, null,
				null, null, runitePositions, null, customPositions, b);
		this.searchHop = true;
	}

	/**
	 * onStart
	 * 
	 * Calls the super's onStart and sets up whatever necessary for this
	 * particular miner
	 * 
	 */
	@Override
	public void onStart() {
		super.onStart();
		if (miningArea.isInAreaInclusive(script.myPosition()))
			state = State.MINE;
		else if (bankArea.isInAreaInclusive(script.myPosition()))
			state = State.BANK;
		else if (script.inventory.getEmptySlots() < 14)
			state = State.WALK_TO_BANK;
		else
			state = State.WALK_TO_MINE;
		state = State.WALK_TO_MINE;
		canStart = true;
	}

	/**
	 * 
	 * onLoop
	 * 
	 * Worker for the script
	 * 
	 */
	public int onLoop() {
		try {
			super.onLoop();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (canStart) {
			switch (state) {
			case MINE:
				Object[] keys = incorrectIDs.keySet().toArray();
				for (Object i : keys) {
					long elapsed = System.currentTimeMillis()
							- incorrectIDs.get(i).addedTime;
					if (elapsed > incorrectIDs.get(i).expireTime)
						incorrectIDs.remove(i);
				}
				if (script.inventory.isFull()) {
					state = State.WALK_TO_BANK;
					break;
				}
				// If you're not mining/idle/rock you're mining is dead, then
				// mine a new rock
				if ((rockPosition == null
						|| Library.getRockIDAt(rockPosition, script) != rockID
						|| System.currentTimeMillis() - lastActionTime > 2000 || Library
						.getFacingRock(script) != Library.getRockAt(
						rockPosition, script))
						&& !script.myPlayer().isMoving()) {
					// System.out.println("Getting new rock...");
					RS2Object nearestRock = Library.getRockForIDs(
							rocksPositions, miningArea, prioritize, script,
							incorrectIDs);
					if (nearestRock != null) {
						// System.out.println("New rock position: (" +
						// nearestRock.getX() + "," + nearestRock.getY() + ")");
						rockID = nearestRock.getId();
						rockPosition = nearestRock.getPosition();
						for (int i = 0; i < 5
								&& !Library.interactRock(nearestRock, script); i++) {
							try {
								script.sleep(random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						lastActionTime = System.currentTimeMillis();
					} else {
						if (searchHop) {
							((DreamMiner) script).worldHopping.switchWorlds();
						}
					}
				} else {
				}
				break;
			case WALK_TO_BANK:
				if (script.myPlayer().getY() > 8000) {
					if (Library.distanceTo(
							Library.closestObject("Ladder", "Climb-up", script)
									.getPosition(), script) > 6) {
						Library.walkPath(pathMineToLadder, ladderPosition,
								script);
					} else {
						Library.closestObject("Ladder", "Climb-up", script)
								.interact("Climb-up");
						try {
							script.sleep(MethodProvider.random(400, 800));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				} else if (script.myPlayer().getZ() == 0) {
					if (script.map.canReach(guildMiddlePosition)
							&& script.myPosition().getX() <= 2901) {
						RS2Object door = script.objects.closest("Door");
						if (door != null) {
							door.interact("Open");
							try {
								script.sleep(MethodProvider.random(450, 800));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					} else {
						if (barArea.isInAreaInclusive(script.myPlayer()
								.getPosition())) {
							RS2Object trapdoor = script.objects
									.closest("Trapdoor");
							if (trapdoor != null) {
								trapdoor.interact("Enter");
								try {
									script.sleep(MethodProvider
											.random(450, 750));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						} else if (Library.distanceTo(barDoorPosition, script) < 6) {
							RS2Object door = Library.getObjectAt(
									barDoorPosition, "Door", script);
							if (door == null) {
								RS2Object trapdoor = script.objects
										.closest("Trapdoor");
								if (trapdoor != null) {
									trapdoor.interact("Enter");
									try {
										script.sleep(MethodProvider.random(450,
												750));
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
							} else {
								door.interact("Open");
							}
						} else {
							Library.walkPath(pathGuildToBar, barDoorPosition,
									script);
						}
					}
				} else {
					RS2Object door = Library.getObjectAt(lowerDoorPosition,
							"Door", script);
					if (door != null) {
						door.interact("Open");
					} else {
						state = State.BANK;
					}
				}
				break;
			case BANK:
				if (Library.distanceTo(bankPosition, script) > 6) {
					script.localWalker.walk(bankPosition);
				}
				if (script.inventory.getEmptySlots() > 20) {
					state = State.WALK_TO_MINE;
					break;
				}
				RS2Object door = Library.getObjectAt(lowerDoorPosition, "Door",
						script);

				if (door != null
						&& (script.myPosition().equals(
								new Position(3061, 4984, 1)) || script
								.myPosition().equals(
										new Position(3061, 4985, 1)))) {
					door.interact("Open");
				}
				if (!script.bank.isOpen()) {
					NPC bank = script.npcs.closest("Emerald Benedict");
					if (bank != null) {
						bank.interact("Bank");
					}
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return script.myPlayer().isMoving();
						}
					}, 1000, 100, script);
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return !script.myPlayer().isMoving();
						}
					}, 5000, 100, script);
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return script.bank.isOpen();
						}
					}, 1000, 100, script);
				} else {
					script.bank.depositAll(new PickaxeFilter<Item>());
				}
				break;
			case WALK_TO_MINE:
				if (script.myPlayer().getZ() == 1) {
					if (Library.distanceTo(bankDoorPosition, script) > 6) {
						script.localWalker.walk(bankDoorPosition);
					}
					door = Library.getObjectAt(lowerDoorPosition, "Door",
							script);
					if (door != null
							&& !(script.myPosition().equals(
									new Position(3061, 4984, 1)) || script
									.myPosition().equals(
											new Position(3061, 4985, 1)))) {
						door.interact("Open");
					} else {
						RS2Object passageway = script.objects
								.closest("Passageway");
						if (passageway != null) {
							passageway.interact("Enter");
							try {
								script.sleep(MethodProvider.random(400, 800));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				} else {
					if (script.myPosition().getY() > 8000) {
						if (Library.distanceTo(minePosition, script) < 6) {
							state = State.MINE;
							break;
						} else
							Library.walkPath(pathToMine, minePosition, script);
					} else {
						if (barArea.isInAreaInclusive(script.myPosition())) {
							door = Library.getObjectAt(barDoorPosition, "Door",
									script);
							if (door != null) {
								door.interact("Open");
								try {
									script.sleep(MethodProvider
											.random(400, 800));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							} else {
								if (Library.distanceTo(guildDoorPosition,
										script) < 6
										|| script.map
												.canReach(guildMiddlePosition)) {
									if (script.map
											.canReach(guildMiddlePosition)) {
										RS2Object ladder = Library
												.closestObject("Ladder",
														"Climb-down", script);
										if (ladder != null) {
											ladder.interact("Climb-down");
											try {
												script.sleep(MethodProvider
														.random(400, 800));
											} catch (InterruptedException e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}
										}
									} else {
										door = Library.getObjectAt(
												guildDoorPosition, "Door",
												script);
										if (door != null) {
											door.interact("Open");
											try {
												script.sleep(MethodProvider
														.random(400, 800));
											} catch (Exception e) {

											}
										}
									}
								} else {
									Library.walkPath(pathToGuild,
											guildDoorPosition, script);
								}
							}
						} else {
							if (Library.distanceTo(guildDoorPosition, script) < 6
									|| script.map.canReach(guildMiddlePosition)) {
								if (script.map.canReach(guildMiddlePosition)) {
									RS2Object ladder = Library.closestObject(
											"Ladder", "Climb-down", script);
									if (ladder != null) {
										ladder.interact("Climb-down");
										try {
											script.sleep(MethodProvider.random(
													400, 800));
										} catch (InterruptedException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									}
								} else {
									door = Library.getObjectAt(
											guildDoorPosition, "Door", script);
									if (door != null) {
										door.interact("Open");
										try {
											script.sleep(MethodProvider.random(
													400, 800));
										} catch (Exception e) {

										}
									}
								}
							} else {
								Library.walkPath(pathToGuild,
										guildDoorPosition, script);
							}
						}
					}
				}
				break;
			}
		}
		return 100;
	}

	public void onMessage(Message m) throws InterruptedException {
		super.onMessage(m);
	}

	public void onExit() {
		stopScript = true;
	}
}