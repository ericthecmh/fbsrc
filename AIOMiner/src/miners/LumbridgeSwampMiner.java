package miners;

import java.awt.Point;

import library.Area;
import library.Conditional;
import library.Library;
import library.PickaxeFilter;
import main.DreamMiner;

import org.osbot.rs07.api.filter.Filter;
import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.map.PositionPolygon;
import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.Message;
import org.osbot.rs07.input.mouse.MiniMapTileDestination;
import org.osbot.rs07.script.Script;

/**
 * 
 * Mines in the Lumbridge Swamp (the one with coal, mithril, and adamant)
 * 
 * @author Ericthecmh, Eliot
 * 
 */

public class LumbridgeSwampMiner extends Miner {

	/************************************ CONSTANTS ************************************/

	// Mining area
	private final Area miningArea = new Area(3143, 3154, 3149, 3143);
	private final Area bankArea = new Area(3097, 3240, 3088, 3246);

	// Array of positions of some coal rocks (used to determine IDs)
	private final static Position[] coalPositions = new Position[] {
			new Position(3144, 3148, 0), new Position(3145, 3149, 0),
			new Position(3144, 3150, 0), new Position(3145, 3151, 0),
			new Position(3146, 3151, 0), new Position(3144, 3152, 0),
			new Position(3145, 3153, 0) };

	// Array of positions of some mithril rocks (used to determine IDs)
	private final static Position[] mithrilPositions = new Position[] {
			new Position(3143, 3146, 0), new Position(3144, 3147, 0),
			new Position(3144, 3145, 0), new Position(3148, 3144, 0),
			new Position(3146, 3145, 0) };

	// Array of positions of some adamant rocks (used to determine IDs)
	private final static Position[] adamantPositions = new Position[] {
			new Position(3147, 3146, 0), new Position(3148, 3147, 0) };

	// Path to bank
	private final PositionPolygon pathToBank = new PositionPolygon(new int[][] {
			{ 3146, 3150 }, { 3150, 3157 }, { 3154, 3166 }, { 3156, 3175 },
			{ 3150, 3185 }, { 3147, 3188 }, { 3145, 3193 }, { 3140, 3200 },
			{ 3135, 3209 }, { 3122, 3210 }, { 3113, 3213 }, { 3107, 3217 },
			{ 3102, 3224 }, { 3099, 3232 }, { 3093, 3243 } });

	/**
	 * Constructor
	 * 
	 * @param rocks
	 *            the list of rock names to be mined
	 * @param prioritize
	 *            whether or not to prioritize rocks by the order in rocks array
	 * @param s
	 *            reference to the actual script being run
	 * @param customPositions
	 * @param b
	 */
	public LumbridgeSwampMiner(Script s, String[] rocks, boolean prioritize,
			Position[] customPositions, boolean b) {
		super(s, rocks, prioritize, null, null, null, null, coalPositions,
				null, null, mithrilPositions, adamantPositions, null, null,
				customPositions, b);
	}

	/**
	 * onStart
	 * 
	 * Calls the super's onStart and sets up whatever necessary for this
	 * particular miner
	 * 
	 */
	@Override
	public void onStart() {
		super.onStart();
		if (miningArea.isInAreaInclusive(script.myPosition()))
			state = State.MINE;
		else if (bankArea.isInAreaInclusive(script.myPosition()))
			state = State.BANK;
		else if (script.inventory.getEmptySlots() < 14)
			state = State.WALK_TO_BANK;
		else
			state = State.WALK_TO_MINE;
		canStart = true;
	}

	/**
	 * 
	 * onLoop
	 * 
	 * Worker for the script
	 * 
	 */
	public int onLoop() {
		try {
			super.onLoop();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (canStart) {
			switch (state) {
			case MINE:
				Object[] keys = incorrectIDs.keySet().toArray();
				for (Object i : keys) {
					long elapsed = System.currentTimeMillis()
							- incorrectIDs.get(i).addedTime;
					if (elapsed > incorrectIDs.get(i).expireTime)
						incorrectIDs.remove(i);
				}
				if (script.inventory.isFull()) {
					state = State.WALK_TO_BANK;
					break;
				}
				// If you're not mining/idle/rock you're mining is dead, then
				// mine a new rock
				if ((rockPosition == null
						|| Library.getRockIDAt(rockPosition, script) != rockID
						|| System.currentTimeMillis() - lastActionTime > 2000 || Library.getFacingRock(script) != Library.getRockAt(
						rockPosition, script))
						&& !script.myPlayer().isMoving()) {
					// System.out.println("Getting new rock...");
					RS2Object nearestRock = Library.getRockForIDs(
							rocksPositions, miningArea, prioritize, script,
							incorrectIDs);
					if (nearestRock != null) {
						// System.out.println("New rock position: (" +
						// nearestRock.getX() + "," + nearestRock.getY() + ")");
						rockID = nearestRock.getId();
						rockPosition = nearestRock.getPosition();
						for (int i = 0; i < 5
								&& !Library.interactRock(nearestRock, script); i++) {
							try {
								script.sleep(random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						lastActionTime = System.currentTimeMillis();
					} else {

					}
				} else {
				}
				break;
			case WALK_TO_BANK:
				if (bankArea.isInAreaInclusive(script.myPosition())) {
					keys = incorrectIDs.keySet().toArray();
					for (Object i : keys) {
						incorrectIDs.get(i).value--;
						if (incorrectIDs.get(i).value == 0) {
							incorrectIDs.remove(i);
						} else {
							long elapsed = System.currentTimeMillis()
									- incorrectIDs.get(i).addedTime;
							if (elapsed > incorrectIDs.get(i).expireTime)
								incorrectIDs.remove(i);
						}
					}
					state = State.BANK;
					break;
				}
				walk(false);
				break;
			case BANK:
				if (script.inventory.getEmptySlots() > 20) {
					if (!pickaxeInInventory
							|| script.inventory.contains(pickaxeID)) {
						state = State.WALK_TO_MINE;
						break;
					}
				}
				if (!script.bank.isOpen()) {
					RS2Object bankBooth = Library.closestObject("Bank booth",
							"Bank", script);
					if (bankBooth != null) {
						if (Library.distanceTo(bankBooth.getPosition(), script) > 3
								&& !bankArea.isInAreaInclusive(script
										.myPosition())) {
							final MiniMapTileDestination td = new MiniMapTileDestination(
									script.bot, bankBooth.getPosition());
							// script.mouse.move(td);
							// if (Library.waitFor(new Conditional() {
							//
							// @Override
							// public boolean isSatisfied(Script script) {
							// return td.getBoundingBox().contains(
							// script.mouse.getPosition());
							// }
							//
							// }, 3000, 100, script)) {
							// script.mouse.click(false);
							// }
							script.mouse.click(td);
							Library.waitFor(new Conditional() {

								@Override
								public boolean isSatisfied(Script script) {
									return script.myPlayer().isMoving();
								}
							}, 1000, 100, script);
							Library.waitFor(new Conditional() {

								@Override
								public boolean isSatisfied(Script script) {
									return !script.myPlayer().isMoving();
								}
							}, 5000, 100, script);
							Library.waitFor(new Conditional() {

								@Override
								public boolean isSatisfied(Script script) {
									return script.bank.isOpen();
								}
							}, 1000, 100, script);
							return 0;
						}
						if (DreamMiner.USE_CUSTOM_INTERACT)
							Library.interact(bankBooth, "Bank", script);
						else
							bankBooth.interact("Bank");
					}
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return script.myPlayer().isMoving();
						}
					}, 1000, 100, script);
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return !script.myPlayer().isMoving();
						}
					}, 5000, 100, script);
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return script.bank.isOpen();
						}
					}, 1000, 100, script);
				} else {
					script.bank.depositAll(new PickaxeFilter<Item>());
				}
				break;
			case WALK_TO_MINE:
				if (miningArea.isInAreaInclusive(script.myPosition())) {
					state = State.MINE;
					break;
				}
				walk(true);
				break;
			}
		}
		return 100;
	}

	// Code stolen from Laz <3
	private int walk(boolean reverse) {
		int pi = pathToBank.getClosestIndex(script.myPosition());
		if (pi == -1) {
			return -1;
		}

		if (!reverse && bankArea.isInAreaInclusive(script.myPosition())) {
			return -1;
		} else if (reverse && miningArea.isInAreaInclusive(script.myPosition())) {
			return -1;
		}

		int c = pathToBank.getCount();
		int wi = reverse ? 0 : c - 1;

		if (wi == pi) {
			return -1;
		}

		if (!script.settings.isRunning()
				&& script.settings.getRunEnergy() > random(30, 50)) {
			script.settings.setRunning(true);
			return gRandom(400, 200);
		}

		while (wi >= 0 && wi < c && (reverse ? wi <= pi : wi >= pi)) {
			Point point = pathToBank.get(wi);
			if (script.localWalker.walk(point.x, point.y)) {
				try {
					script.sleep(random(500, 800));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return 0;
			}
			wi += reverse ? 1 : -1;
		}

		return -1;
	}

	public void onMessage(Message m) throws InterruptedException {
		super.onMessage(m);
	}

	public void onExit() {
		stopScript = true;
	}
}
