package library;

import org.osbot.rs07.api.filter.Filter;
import org.osbot.rs07.api.model.Item;

public class PickaxeFilter<T> implements Filter<Item> {

	@Override
	public boolean match(Item item) {
		if (item == null)
			return false;
		for (int i = 0; i < Library.pickIDs.length; i++) {
			if (item.getId() == Library.pickIDs[i]) {
				return false;
			}
		}
		return true;
	}

}
