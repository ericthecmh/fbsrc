package library;

public class TimePair {
	public int value;
	public long addedTime;
	public long expireTime;
	public TimePair(int v, long at, long et){
		this.value = v;
		this.addedTime = at;
		this.expireTime = et;
	}
}
