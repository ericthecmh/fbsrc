package library;

/**
 * @author Erik
 */

import org.osbot.rs07.api.filter.Filter;
import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.script.Script;

public class BestMatch implements Filter<Item> {
	Script script;

	public BestMatch(Script script) {
		this.script = script;
	}

	@Override
	public boolean match(Item arg0) {
		return arg0.getName().contains("pickaxe");
	}
}