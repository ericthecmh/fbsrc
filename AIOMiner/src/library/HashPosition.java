package library;

import org.osbot.rs07.api.map.Position;

public class HashPosition {
	public final int x, y, z;

	public HashPosition(Position position) {
		x = position.getX();
		y = position.getY();
		z = position.getZ();
	}

	public HashPosition(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public int hashCode() {
		return this.x * this.y * this.z;
	}

	public boolean equals(Object o) {
		HashPosition p = (HashPosition) o;
		return p.x == x && p.y == y && p.z == z;
	}

	public String toString() {
		return "(" + x + ", " + y + ")";
	}
}
