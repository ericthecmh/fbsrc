package library;

import main.DreamMiner;

import org.osbot.rs07.api.ui.Tab;
import org.osbot.rs07.script.MethodProvider;

public class WorldHopping {
	// world list
	private int[] worlds = { 303, 304, 305, 306, 309, 310, 311, 312, 313, 314,
			317, 318, 319, 320, 321, 322, 326, 327, 328, 329, 330, 333, 334,
			335, 336, 338, 341, 342, 343, 344, 345, 346, 349, 350, 351, 352,
			353, 354, 357, 358, 359, 360, 361, 362, 365, 366, 367, 368, 369,
			370, 373 };

	DreamMiner script;

	public WorldHopping(DreamMiner script) {
		this.script = script;
	}

	/**
	 * checks if in bot world
	 * 
	 * @return if in bot world
	 */
	public boolean isInBotWorld() {
		return this.script.client.getCurrentWorld() == 385
				|| this.script.client.getCurrentWorld() == 386;
	}

	/**
	 * hop worlds
	 */
	public void switchWorlds() {
		while (script.interfaces.closeOpenInterface())
			try {
				MethodProvider.sleep(MethodProvider.random(500, 700));
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		this.script.worldHopper.hop(worlds[MethodProvider.random(0,
				worlds.length - 1)]);
	}

}
