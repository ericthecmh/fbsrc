package library;

import org.osbot.rs07.api.map.Position;

public class Area {
	private int minX, maxX, minY, maxY;

	public Area(int x1, int y1, int x2, int y2) {
		if (x1 > x2) {
			minX = x2;
			maxX = x1;
		} else {
			minX = x1;
			maxX = x2;
		}
		if (y1 > y2) {
			minY = y2;
			maxY = y1;
		} else {
			minY = y1;
			maxY = y2;
		}
	}

	public boolean isInAreaInclusive(Position p) {
		return (p.getX() >= minX && p.getX() <= maxX && p.getY() >= minY && p
				.getY() <= maxY);
	}

	public boolean isInAreaExclusive(Position p) {
		return (p.getX() > minX && p.getX() < maxX && p.getY() > minY && p
				.getY() < maxY);
	}
}
