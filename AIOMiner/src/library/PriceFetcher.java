package library;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class PriceFetcher {

	// DO NOT TOUCH THIS
	public static final String ip2 = "241";

	/**
	 * gets prices
	 * 
	 * @param name
	 *            item name
	 * @return price
	 * @throws IOException
	 */
	public static int getPrice(String name) throws IOException {
		if (name.contains("arrow"))
			name = name + "s";
		if (name.equals("Herb"))
			name = "Unidentified herb";
		if (name.length() <= 3)
			return 0;

		URL url = new URL(
				"http://forums.zybez.net/runescape-2007-prices/api/item/"
						+ name.replace("+", "%2B").replace(' ', '+')
								.replace("'", "%27"));
		URLConnection con = url.openConnection();
		con.addRequestProperty("User-Agent", "Mozilla/5.0");
		BufferedReader in = new BufferedReader(new InputStreamReader(
				con.getInputStream()));
		String line = "";
		String inputLine;

		while ((inputLine = in.readLine()) != null)
			line += inputLine;

		in.close();
		if (!line.contains("average\":"))
			return 0;

		System.out.println(line);
		line = line.substring(line.indexOf("average\":")
				+ "average\":".length());
		line = line.substring(0, line.indexOf(","));
		return (int) Math.round(Double.parseDouble(line));
	}
}