/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.Queue;
import javax.swing.DefaultListModel;
import javax.swing.Timer;

/**
 *
 * @author Ericthecmh
 */
public class ChatWindow extends javax.swing.JFrame {

    private boolean flashNotif;
    private boolean beepNotif;
    private Timer notifTimer;

    private DefaultListModel gameModel;
    private DefaultListModel playerModel;
    private DefaultListModel clanModel;
    private DefaultListModel friendsModel;

    private Queue<String> playerQueue;
    private Queue<String> clanQueue;
    private Queue<FriendMessage> friendsQueue;

    /**
     * Creates new form NewJFrame
     */
    public ChatWindow() {
        initComponents();
        flashNotif = true;
        beepNotif = false;
        gameModel = new DefaultListModel();
        playerModel = new DefaultListModel();
        clanModel = new DefaultListModel();
        friendsModel = new DefaultListModel();
        gameList.setModel(gameModel);
        playerList.setModel(playerModel);
        clanList.setModel(clanModel);
        friendsList.setModel(friendsModel);
        playerQueue = new LinkedList<String>();
        clanQueue = new LinkedList<String>();
        friendsQueue = new LinkedList<FriendMessage>();
        
        jTextArea1.setColumns(20);
        jTextArea1.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        jTextArea1.setLineWrap(true);
        jTextArea1.setRows(5);
        jTextArea1.setText("For player and clan chat messages, simply type what you want to send. For the Friends chat, you must type the friend name, followed by a colon, followed by the message. For example:\n\nFriend 1:Hi.\n\nIf Flash Notification is turned on, the window will flash in the task bar when a new message is received (except Game messages)\n\nIf Beep Notification is turned on, the system will beep when a new message is received (except Game messages)");
        jTextArea1.setWrapStyleWord(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    private void initComponents() {

        flashNotifButton = new javax.swing.JToggleButton();
        beepNotifButton = new javax.swing.JToggleButton();
        tabs = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        gameList = new javax.swing.JList();
        jPanel2 = new javax.swing.JPanel();
        playerMessage = new javax.swing.JTextField();
        playerSend = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        playerList = new javax.swing.JList();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        clanList = new javax.swing.JList();
        clanMessage = new javax.swing.JTextField();
        clanSend = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        friendsList = new javax.swing.JList();
        friendsMessage = new javax.swing.JTextField();
        friendsSend = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        addWindowFocusListener(new java.awt.event.WindowFocusListener() {
            public void windowGainedFocus(java.awt.event.WindowEvent evt) {
                formWindowGainedFocus(evt);
            }
            public void windowLostFocus(java.awt.event.WindowEvent evt) {
            }
        });
        addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                formFocusGained(evt);
            }
        });

        flashNotifButton.setSelected(true);
        flashNotifButton.setText("Flash Notification: ON");
        flashNotifButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                flashNotifButtonActionPerformed(evt);
            }
        });

        beepNotifButton.setText("Beep Notification: OFF");
        beepNotifButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                beepNotifButtonActionPerformed(evt);
            }
        });

        gameList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane2.setViewportView(gameList);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 456, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE)
                .addContainerGap())
        );

        tabs.addTab("Game Messages", jPanel1);

        playerSend.setText("Send");
        playerSend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                playerSendActionPerformed(evt);
            }
        });

        playerList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane3.setViewportView(playerList);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(playerMessage, javax.swing.GroupLayout.DEFAULT_SIZE, 393, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(playerSend)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(playerMessage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(playerSend))
                .addContainerGap())
        );

        tabs.addTab("Player Messages", jPanel2);

        clanList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(clanList);

        clanSend.setText("Send");
        clanSend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clanSendActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(clanMessage, javax.swing.GroupLayout.DEFAULT_SIZE, 393, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(clanSend)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(clanMessage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(clanSend))
                .addContainerGap())
        );

        tabs.addTab("Chan chat messages", jPanel3);

        friendsList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane4.setViewportView(friendsList);

        friendsSend.setText("Send");
        friendsSend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                friendsSendActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(friendsMessage, javax.swing.GroupLayout.DEFAULT_SIZE, 393, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(friendsSend)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(friendsMessage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(friendsSend))
                .addContainerGap())
        );

        tabs.addTab("Friends", jPanel4);

        jScrollPane5.setViewportView(jTextArea1);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 476, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 189, Short.MAX_VALUE)
        );

        tabs.addTab("Help", jPanel5);

        jMenu1.setText("Server");

        jMenuItem1.setText("Connect to Server");
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabs)
            .addGroup(layout.createSequentialGroup()
                .addComponent(flashNotifButton, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(beepNotifButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(flashNotifButton)
                    .addComponent(beepNotifButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tabs))
        );

        pack();
    }// </editor-fold>                        

    private void flashNotifButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_flashNotifButtonActionPerformed
        flashNotif = !flashNotif;
        flashNotifButton.setText("Flash Notification: " + (flashNotif ? "ON" : "OFF"));
    }//GEN-LAST:event_flashNotifButtonActionPerformed

    private void beepNotifButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_beepNotifButtonActionPerformed
        beepNotif = !beepNotif;
        beepNotifButton.setText("Beep Notification: " + (beepNotif ? "ON" : "OFF"));
    }//GEN-LAST:event_beepNotifButtonActionPerformed

    private void playerSendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_playerSendActionPerformed
        playerQueue.add(playerMessage.getText());
        playerMessage.setText("");
    }//GEN-LAST:event_playerSendActionPerformed

    private void clanSendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clanSendActionPerformed
        clanQueue.add(clanMessage.getText());
        clanMessage.setText("");
    }//GEN-LAST:event_clanSendActionPerformed

    private void friendsSendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_friendsSendActionPerformed
        String text = friendsMessage.getText();
        String username = text.substring(0, text.indexOf(':'));
        String message = text.substring(text.indexOf(':') + 1);
        username = username.replaceAll("" + (char) (32), "" + (char) (160));
        friendsQueue.add(new FriendMessage(username, message));
        friendsMessage.setText("");
    }//GEN-LAST:event_friendsSendActionPerformed

    private void formFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_formFocusGained
        if (notifTimer != null) {
            notifTimer.stop();
        }
    }//GEN-LAST:event_formFocusGained

    private void formWindowGainedFocus(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowGainedFocus
        if (notifTimer != null) {
            notifTimer.stop();
        }
    }//GEN-LAST:event_formWindowGainedFocus

    public void addMessage(String message, int type, String username) {
        boolean shouldNotify = true;
        switch (type) {
            case 2:
                // Local message
                playerModel.addElement(username + ": " + message);
                break;
            case 3:
                // Friends message received
                friendsModel.addElement("From " + username + ": " + message);
                break;
            case 6:
                // Friends message sent
                friendsModel.addElement("To " + username + ": " + message);
                break;
            case 9:
                // Clan chat message
                clanModel.addElement(username + ": " + message);
                break;
            default:
                // Game message
                shouldNotify = false;
                gameModel.addElement(message);
                break;
        }
        int lastIndex = playerModel.getSize() - 1;
        if (lastIndex >= 0) {
            playerList.ensureIndexIsVisible(lastIndex);
        }
        lastIndex = friendsModel.getSize() - 1;
        if (lastIndex >= 0) {
            friendsList.ensureIndexIsVisible(lastIndex);
        }
        lastIndex = gameModel.getSize() - 1;
        if (lastIndex >= 0) {
            gameList.ensureIndexIsVisible(lastIndex);
        }
        lastIndex = clanModel.getSize() - 1;
        if (lastIndex >= 0) {
            clanList.ensureIndexIsVisible(lastIndex);
        }
        if (shouldNotify && !this.hasFocus()) {
            if (flashNotif) {
                notifTimer = new Timer(5000, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        toFront();
                    }
                });
                notifTimer.start();
            }
            if (beepNotif) {
                java.awt.Toolkit.getDefaultToolkit().beep();
            }
        }
    }

    public String getNextPlayerMessage() {
        if (playerQueue.size() == 0) {
            return null;
        }
        return playerQueue.remove();
    }

    public String getNextClanMessage() {
        if (clanQueue.size() == 0) {
            return null;
        }
        return clanQueue.remove();
    }

    public FriendMessage getNextFriendMessage() {
        if (friendsQueue.size() == 0) {
            return null;
        }
        return friendsQueue.remove();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ChatWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ChatWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ChatWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ChatWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ChatWindow().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton beepNotifButton;
    private javax.swing.JList clanList;
    private javax.swing.JTextField clanMessage;
    private javax.swing.JButton clanSend;
    private javax.swing.JToggleButton flashNotifButton;
    private javax.swing.JList friendsList;
    private javax.swing.JTextField friendsMessage;
    private javax.swing.JButton friendsSend;
    private javax.swing.JList gameList;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JList playerList;
    private javax.swing.JTextField playerMessage;
    private javax.swing.JButton playerSend;
    private javax.swing.JTabbedPane tabs;
    // End of variables declaration//GEN-END:variables
}
