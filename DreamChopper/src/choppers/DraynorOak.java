package choppers;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class DraynorOak extends Chopper {

	private final static Area bankArea = new Area(3092, 3240, 3096, 3246);
	private final static Area treeArea = new Area(3094, 3280, 3109, 3293);
	private final static Position[] pathToBank = { new Position(3102, 3285, 0), new Position(3103, 3284, 0),
		 new Position(3103, 3283, 0), new Position(3104, 3282, 0), new Position(3105, 3281, 0),
		 new Position(3105, 3280, 0), new Position(3105, 3279, 0), new Position(3105, 3278, 0),
		 new Position(3105, 3277, 0), new Position(3105, 3276, 0), new Position(3105, 3275, 0),
		 new Position(3105, 3274, 0), new Position(3105, 3272, 0), new Position(3105, 3271, 0),
		 new Position(3105, 3270, 0), new Position(3105, 3269, 0), new Position(3105, 3268, 0),
		 new Position(3105, 3267, 0), new Position(3105, 3266, 0), new Position(3105, 3265, 0),
		 new Position(3104, 3264, 0), new Position(3104, 3263, 0), new Position(3104, 3262, 0),
		 new Position(3104, 3261, 0), new Position(3104, 3260, 0), new Position(3104, 3258, 0),
		 new Position(3104, 3257, 0), new Position(3103, 3256, 0), new Position(3103, 3255, 0),
		 new Position(3102, 3254, 0), new Position(3102, 3253, 0), new Position(3101, 3252, 0),
		 new Position(3101, 3251, 0), new Position(3100, 3251, 0), new Position(3099, 3250, 0),
		 new Position(3098, 3250, 0), new Position(3097, 3249, 0), new Position(3096, 3249, 0),
		 new Position(3096, 3248, 0), new Position(3095, 3247, 0), new Position(3094, 3246, 0),
		 new Position(3093, 3246, 0), new Position(3093, 3245, 0), new Position(3093, 3244, 0) };
	private final static String treeName = "Oak";

	public DraynorOak(Script s, boolean checkForNests) {
		super(s, bankArea, treeArea, pathToBank, treeName, checkForNests);
	}

}
