package choppers;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class VarrockEastOak extends Chopper {
	private final static Area bankArea = new Area(3250, 3419, 3257, 3423);
	private final static Area treeArea = new Area(3274, 3423, 3284, 3439);
	private final static Position[] pathToBank = { new Position(3276, 3432, 0),
			new Position(3273, 3430, 0), new Position(3262, 3428, 0),
			new Position(3254, 3424, 0), new Position(3254, 3420, 0) };
	private final static String treeName = "Oak";

	public VarrockEastOak(Script s, boolean checkForNests) {
		super(s, bankArea, treeArea, pathToBank, treeName, checkForNests);
	}
}