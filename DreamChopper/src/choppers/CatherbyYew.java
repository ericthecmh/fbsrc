package choppers;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class CatherbyYew extends Chopper {
	private final static Area bankArea = new Area(2806, 3438, 2812, 3441);
	private final static Area treeArea = new Area(2752, 3425, 2774, 3440);
	private final static Position[] pathToBank = { new Position(2761, 3430, 0),
			new Position(2772, 3431, 0), new Position(2784, 3432, 0),
			new Position(2790, 3433, 0), new Position(2800, 3433, 0),
			new Position(2809, 3441, 0) };
	private final static String treeName = "Yew";

	public CatherbyYew(Script s, boolean checkForNests) {
		super(s, bankArea, treeArea, pathToBank, treeName, checkForNests);
	}
}