package choppers;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.map.PositionPolygon;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class EdgevilleYew extends Chopper {
	private final static Area bankArea = new Area(3091, 3488, 3098, 3499);
	private final static Area treeArea = new Area(3085, 3468, 3089, 3482);
	private final static Position[] pathToBank = { new Position(3087, 3471, 0),
			new Position(3091, 3470, 0), new Position(3093, 3481, 0),
			new Position(3094, 3491, 0) };
	private final static String treeName = "Yew";

	public EdgevilleYew(Script s, boolean checkForNests) {
		super(s, bankArea, treeArea, pathToBank, treeName, checkForNests);
	}
}
