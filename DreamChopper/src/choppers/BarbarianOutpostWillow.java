package choppers;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class BarbarianOutpostWillow extends Chopper {

	private final static Area bankArea = new Area(2530, 3567, 2538, 3579);
	private final static Area treeArea = new Area(2510, 3575, 2526, 3586);
	private final static Position[] pathToBank = { new Position(2519, 3580, 0),
			new Position(2521, 3571, 0), new Position(2532, 3571, 0),
			new Position(2536, 3573, 0) };
	private final static String treeName = "Willow";

	public BarbarianOutpostWillow(Script s, boolean checkForNests) {
		super(s, bankArea, treeArea, pathToBank, treeName, checkForNests);
	}

}
