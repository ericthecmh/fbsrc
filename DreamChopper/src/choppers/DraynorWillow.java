package choppers;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.map.PositionPolygon;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class DraynorWillow extends Chopper {

	private final static Area bankArea = new Area(3092, 3240, 3096, 3246);
	private final static Area treeArea = new Area(3081, 3224, 3239, 3241);
	private final static Position[] pathToBank = { new Position(3087, 3236, 0),
			new Position(3092, 3245, 0) };
	private final static String treeName = "Willow";

	public DraynorWillow(Script s, boolean checkForNests) {
		super(s, bankArea, treeArea, pathToBank, treeName, checkForNests);
	}

}
