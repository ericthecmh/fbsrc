package choppers;

import library.AntibanAction;
import library.AxeMatch;
import library.Conditional;
import library.Library;
import main.DreamChopper;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.Entity;
import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class PowerChopper {
	// CONSTANTS
	public enum State {
		CHOP, DROP;
	}

	private final Position startLocation;

	// axe ids
	int[] doNotDropsInts = { 1351, 1349, 1353, 1361, 1355, 1357, 1359, 6739 };

	// script reference
	Script script;
	String treeName;
	State state;
	AntibanAction antiBan;

	// construct an instance of Power Chopper
	public PowerChopper(Script s, String treeName, Position sl) {
		this.script = s;
		this.treeName = treeName;
		this.startLocation = sl;
	}

	public void onStart() {
		antiBan = new AntibanAction(script);
		if (script.inventory.isFull())
			state = State.DROP;
		else
			state = State.CHOP;
	}

	public int onLoop() {
		switch (state) {
		case CHOP:
			ab();
			chop();
			break;
		case DROP:
			drop();
			break;
		}
		return 15;
	}

	private void drop() {
		if (script.inventory.isEmpty()
				|| script.inventory.onlyContains(new AxeMatch())) {
			state = State.CHOP;
			return;
		}
		try {
			Library.dropAllExcept(doNotDropsInts, script);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// handles tree interactions
	private void chop() {
		if (script.interfaces.get(177).getChild(2) != null) {
			script.interfaces.interactWithChild(177, 2, "Continue");
			try {
				script.sleep(script.random(300, 500));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (script.inventory.isFull()) {
			state = State.DROP;
		} else {
			Entity closeTree = null;
			NPC ent = script.npcs.closest(this.treeName);
			if (ent != null && script.myPlayer().getInteracting() == ent) {
				closeTree = script.objects.closest(treeName);
				if (closeTree != null
						&& Library.distanceBetween(closeTree.getPosition(),
								startLocation) < 16
						&& closeTree.interact("Chop down")) {
					Library.waitFor(new Conditional() {
						@Override
						public boolean isSatisfied(Script script) {
							return !script.myPlayer().isAnimating();
						}
					}, 3000, 100, script);
					script.log("Ent avoided!");
				} else {
					Area run = new Area(script.myPlayer().getX() - 5, script
							.myPlayer().getY() - 5,
							script.myPlayer().getX() + 5, script.myPlayer()
									.getY() + 5);
					script.localWalker.walk(run.getRandomPosition(0));
				}
			}
			while (!script.myPlayer().isAnimating()
					&& !script.inventory.isFull()) {
				closeTree = script.objects.closest(treeName);
				System.out.println("Chop");
				if (closeTree != null && closeTree.interact("Chop down"))
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return script.myPlayer().isAnimating();
						}
					}, 3000, 100, script);
				System.out.println("Done");
			}
		}
	}

	private void ab() {
		if (script.myPlayer().getAnimation() != -1
				&& Math.random() < DreamChopper.antibanRate) {
			antiBan.execute(script);
		}

	}

	// return the state
	public State getState() {
		return state;
	}

	public void enableAFK(Script s) {
		antiBan.enableAFK(s);
	}
}
