package choppers;

import java.util.ArrayList;

import library.AntibanAction;
import library.AxeMatch;
import library.BestMatch;
import library.Conditional;
import library.DepositBox;
import library.Library;
import library.NotAxeMatch;
import library.Walking;
import main.DreamChopper;

import org.osbot.rs07.api.filter.Filter;
import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.Entity;
import org.osbot.rs07.api.model.GroundItem;
import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.EquipmentSlot;
import org.osbot.rs07.input.mouse.MiniMapTileDestination;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

/**
 * 
 * @author Eliot
 * 
 */
public class Chopper extends Script {
	// CONSTANTS
	public enum State {
		BANK, WALK_TO_TREES, WALK_TO_BANK, CHOP;
	}

	// current state of the script
	protected State state;

	// constructor info
	private final Area bankArea;
	private final Area treeArea;
	private final Position[] pathToBank;
	private final boolean checkForNests;
	public final String treeName;

	// reference to script
	protected final Script script;
	AntibanAction antiBan;

	// construct an instance of Chopper
	public Chopper(Script script, Area bankArea, Area treeArea,
			Position[] pathToBank, String treeName, boolean checkForNests) {
		this.script = script;
		this.bankArea = bankArea;
		this.treeArea = treeArea;
		this.pathToBank = pathToBank;
		this.treeName = treeName;
		this.checkForNests = checkForNests;
	}

	public void enableAFK(Script s) {
		antiBan.enableAFK(s);
	}

	Walking w;
	DepositBox b;

	@Override
	public void onStart() throws InterruptedException {
		w = new Walking(script);
		b = new DepositBox(script);
		antiBan = new AntibanAction(this);
		if (treeArea.contains(script.myPosition()))
			state = State.CHOP;
		else if (bankArea.contains(script.myPosition()))
			state = State.BANK;
		else if (script.inventory.getEmptySlots() < 14)
			state = State.WALK_TO_BANK;
		else
			state = State.WALK_TO_TREES;
	}

	@Override
	public int onLoop() throws InterruptedException {
		Entity closeTree = null;
		NPC ent = script.npcs.closest(this.treeName);
		if (ent != null && script.myPlayer().getInteracting() == ent) {
			closeTree = script.objects.closest(new BestMatch(script, treeName,
					treeArea));
			if (closeTree != null && closeTree.interact("Chop down")) {
				Library.waitFor(new Conditional() {
					@Override
					public boolean isSatisfied(Script script) {
						return !script.myPlayer().isAnimating();
					}
				}, 3000, 100, script);
				script.log("Ent avoided!");
			} else {
				ArrayList<Position> positions = new ArrayList<Position>();
				for (int x = -8; x < 9; x++) {
					for (int y = -8; y < 9; y++) {
						if (x * x + y * y > 64) {
							Position p = new Position(script.myPosition()
									.getX() + x,
									script.myPosition().getY() + y, script
											.myPosition().getZ());
							if (script.map.canReach(p))
								positions.add(p);
						}
					}
				}
				MiniMapTileDestination td = new MiniMapTileDestination(
						script.bot, positions.get(MethodProvider
								.random(positions.size())));
				for (int i = 0; i < MethodProvider.random(2, 4); i++) {
					script.mouse.click(td);
				}
				script.log("Ent avoided");
				return 2000;
			}
		}
		checkRunning();
		if (state == State.CHOP || state == State.BANK) {
			if (script.myPlayer().getPosition().getZ() == 1) {
				// Ladder
				RS2Object ladder = Library.getClosestObjectWithNameAndAction(
						"Ladder", "Climb-down", script);
				RS2Object staircase = Library
						.getClosestObjectWithNameAndAction("Staircase",
								"Climb-down", script);
				RS2Object stair = Library.getClosestObjectWithNameAndAction(
						"Stair", "Climb-down", script);
				double distance = 2000000000.0;
				RS2Object object = null;
				if (ladder != null) {
					if (Library.distanceTo(ladder.getPosition(), script) < distance) {
						object = ladder;
						distance = Library.distanceTo(ladder.getPosition(),
								script);
					}
				}
				if (staircase != null) {
					if (Library.distanceTo(staircase.getPosition(), script) < distance) {
						object = staircase;
						distance = Library.distanceTo(staircase.getPosition(),
								script);
					}
				}
				if (stair != null) {
					if (Library.distanceTo(stair.getPosition(), script) < distance) {
						object = stair;
						distance = Library.distanceTo(stair.getPosition(),
								script);
					}
				}
				if (object != null
						&& script.myPlayer().getPosition().getZ() > 0) {
					object.interact("Climb-down");
					return MethodProvider.random(1200, 1600);
				}
			}
		}
		switch (state) {
		case CHOP:
			ab();
			if (checkForNests)
				if (checkNest())
					break;
			chop();
			break;
		case WALK_TO_BANK:
			walk(this.pathToBank, false);
			break;
		case BANK:
			bank();
			break;
		case WALK_TO_TREES:
			walk(this.pathToBank, true);
			break;
		}
		return 15;
	}

	private boolean checkNest() {
		GroundItem it = script.groundItems.closest(new Filter<GroundItem>() {

			@Override
			public boolean match(GroundItem arg0) {
				return arg0.getName().endsWith("est")
						&& arg0.getName().startsWith("Bird");
			}

		});
		if (it != null && Library.distanceTo(it.getPosition(), script) < 16) {
			it.interact("Take");
			try {
				script.sleep(MethodProvider.random(400, 700));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}

	private void ab() {
		if (script.myPlayer().getAnimation() != -1
				&& Math.random() < DreamChopper.antibanRate) {
			antiBan.execute(script);
		}

	}

	private void checkRunning() {
		if (!script.settings.isRunning()
				&& script.settings.getRunEnergy() > random(30, 50)) {
			while (!script.settings.isRunning()) {
				script.settings.setRunning(true);
				try {
					script.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	// handles banking
	private void bank() {
		if (script.inventory.isEmpty()
				|| Library.onlyContains(script, new AxeMatch())) {
			state = State.WALK_TO_TREES;
			return;
		} else {
			if (script.bank.isOpen()) {
				script.bank.depositAll(new NotAxeMatch());
				try {
					script.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (b.boxIsOpen()) {
				try {
					depositBoxBank();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (script.inventory.isEmpty()
					|| Library.onlyContains(script, new AxeMatch())) {
				state = State.WALK_TO_TREES;
				return;
			}
			RS2Object bankBooth = Library.closestObject("Bank booth", "Bank",
					script);
			if (bankBooth != null && !script.bank.isOpen()) {
				bankBooth.interact("Bank");
				Library.waitFor(new Conditional() {

					@Override
					public boolean isSatisfied(Script script) {
						return script.bank.isOpen() || b.boxIsOpen();
					}
				}, 1000, 100, script);
			} else if (bankBooth == null && !script.bank.isOpen()) {
				bankBooth = Library.closestObject("Bank deposit box",
						"Deposit", script);
				if (bankBooth != null && !script.bank.isOpen()) {
					bankBooth.interact("Deposit");
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return script.bank.isOpen() || b.boxIsOpen();
						}
					}, 1000, 100, script);
				}
			}
		}

	}

	private void depositBoxBank() throws InterruptedException {
		Item[] is = b.getItems();
		ArrayList<Item> items = new ArrayList<Item>();
		ArrayList<Integer> slots = new ArrayList<Integer>();
		do {
			is = b.getItems();
			for (Item i : is) {
				if (i != null && !items.contains(i)
						&& !i.getName().contains("axe")) {
					slots.add(b.getItemSlot(i));
					items.add(i);
				}
			}
			for (int i : slots) {
				b.interact(b.getRectangle(i), "Deposit All");
				script.sleep(200);
			}
			script.sleep(1000);
			if (Library.onlyContains(script, new AxeMatch())
					|| script.inventory.isEmpty()) {
				state = State.WALK_TO_TREES;
				if (b.boxIsOpen()
						&& (Library.onlyContains(script, new AxeMatch()) || script.inventory
								.isEmpty())) {
					b.closeBox();
					script.sleep(200);
				}
				break;
			}
		} while (!script.inventory.isEmpty());
	}

	// handles path walking
	private void walk(Position[] path, boolean reverse)
			throws InterruptedException {
		while (script.interfaces.get(177).getChild(2) != null) {
			script.interfaces.interactWithChild(177, 2, "Continue");
			try {
				script.sleep(script.random(300, 500));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (reverse && treeArea.contains(script.myPosition()))
			state = State.CHOP;
		else if (!reverse && bankArea.contains(script.myPosition()))
			state = State.BANK;
		else if (!reverse) {
			w.walkPathMM(path);
		} else {
			w.walkPathMM(w.reversePath(path));
		}
	}

	// handles tree interactions
	private void chop() {
		if (script.interfaces.get(177).getChild(2) != null) {
			script.interfaces.interactWithChild(177, 2, "Continue");
			try {
				script.sleep(script.random(300, 500));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (script.inventory.isFull()) {
			state = State.WALK_TO_BANK;
		} else {
			Entity closeTree = null;
			NPC ent = script.npcs.closest(this.treeName);
			if (ent != null && script.myPlayer().getInteracting() == ent) {
				closeTree = script.objects.closest(new BestMatch(script,
						treeName, treeArea));
				if (closeTree != null && closeTree.interact("Chop down")) {
					Library.waitFor(new Conditional() {
						@Override
						public boolean isSatisfied(Script script) {
							return !script.myPlayer().isAnimating();
						}
					}, 3000, 100, script);
					script.log("Ent avoided!");
				} else {
					Area run = new Area(myPlayer().getX() + 10, myPlayer()
							.getY() + 10, myPlayer().getX() + 15, myPlayer()
							.getY() + 15);
					script.localWalker.walk(run.getRandomPosition(0));
				}
			}
			while (!script.myPlayer().isAnimating()
					&& !script.inventory.isFull()) {
				closeTree = script.objects.closest(new BestMatch(script,
						treeName, treeArea));
				System.out.println("Calling chop");
				if (closeTree != null && closeTree.interact("Chop down"))
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return script.myPlayer().isAnimating();
						}
					}, 3000, 100, script);
				System.out.println("DONE");
			}
		}
	}

	// return the state
	public State getState() {
		return state;
	}
}
