package choppers;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class SeersVillageYew extends Chopper {
	private final static Area bankArea = new Area(2721, 3490, 2730, 3493);
	private final static Area treeArea = new Area(2703, 3455, 2717, 3468);
	private final static Position[] pathToBank = { new Position(2713, 3462, 0),
			new Position(2722, 3471, 0), new Position(2726, 3478, 0),
			new Position(2726, 3486, 0), new Position(2727, 3493, 0) };
	private final static String treeName = "Yew";

	public SeersVillageYew(Script s, boolean checkForNests) {
		super(s, bankArea, treeArea, pathToBank, treeName, checkForNests);
	}

}