package choppers;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class SeersVillageMaple extends Chopper {
	private final static Area bankArea = new Area(2721, 3490, 2730, 3493);
	private final static Area treeArea = new Area(2719, 3497, 2734, 3505);
	private final static Position[] pathToBank = { new Position(2727, 3500, 0),
			new Position(2727, 3493, 0) };
	private final static String treeName = "Maple tree";

	public SeersVillageMaple(Script s, boolean checkForNests) {
		super(s, bankArea, treeArea, pathToBank, treeName, checkForNests);
	}

}
