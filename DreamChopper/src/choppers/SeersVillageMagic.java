package choppers;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class SeersVillageMagic extends Chopper {
	private final static Area bankArea = new Area(2721, 3490, 2730, 3493);
	private final static Area treeArea = new Area(2689, 3420, 2700, 3432);
	private final static Position[] pathToBank = { new Position(2694, 3425, 0),
			new Position(2703, 3431, 0), new Position(2714, 3439, 0),
			new Position(2722, 3446, 0), new Position(2722, 3454, 0),
			new Position(2723, 3463, 0), new Position(2723, 3473, 0),
			new Position(2724, 3483, 0), new Position(2727, 3493, 0) };
	public final static String treeName = "Magic tree";

	public SeersVillageMagic(Script s, boolean checkForNests) {
		super(s, bankArea, treeArea, pathToBank, treeName, checkForNests);
	}

}