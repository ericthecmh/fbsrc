package choppers;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class VarrockWestOak extends Chopper {
	private final static Area bankArea = new Area(3180, 3433, 3185, 3447);
	private final static Area treeArea = new Area(3159, 3411, 3171, 3430);
	private final static Position[] pathToBank = { new Position(3165, 3418, 0), new Position(3166, 3418, 0),
		 new Position(3167, 3419, 0), new Position(3167, 3420, 0), new Position(3168, 3420, 0),
		 new Position(3169, 3421, 0), new Position(3170, 3422, 0), new Position(3171, 3423, 0),
		 new Position(3172, 3423, 0), new Position(3172, 3424, 0), new Position(3173, 3425, 0),
		 new Position(3174, 3425, 0), new Position(3175, 3426, 0), new Position(3175, 3427, 0),
		 new Position(3176, 3427, 0), new Position(3177, 3428, 0), new Position(3178, 3428, 0),
		 new Position(3179, 3429, 0), new Position(3180, 3430, 0), new Position(3181, 3430, 0),
		 new Position(3182, 3431, 0), new Position(3182, 3432, 0), new Position(3183, 3433, 0),
		 new Position(3183, 3434, 0), new Position(3184, 3435, 0), new Position(3184, 3436, 0),
		 new Position(3185, 3437, 0) };
	private final static String treeName = "Oak";

	public VarrockWestOak(Script s, boolean checkForNests) {
		super(s, bankArea, treeArea, pathToBank, treeName, checkForNests);
	}
}