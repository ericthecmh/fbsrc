package choppers;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class SorcererMagic extends Chopper {
	private final static Area bankArea = new Area(2721, 3490, 2730, 3493);
	private final static Area treeArea = new Area(2698, 3396, 2706, 3401);
	private final static Position[] pathToBank = { new Position(2701, 3397, 0),
			new Position(2716, 3399, 0), new Position(2719, 3411, 0),
			new Position(2722, 3423, 0), new Position(2728, 3434, 0),
			new Position(2730, 3448, 0), new Position(2728, 3457, 0),
			new Position(2728, 3470, 0), new Position(2728, 3479, 0),
			new Position(2726, 3486, 0), new Position(2727, 3493, 0) };
	private final static String treeName = "Magic tree";

	public SorcererMagic(Script s, boolean checkForNests) {
		super(s, bankArea, treeArea, pathToBank, treeName, checkForNests);
	}

}