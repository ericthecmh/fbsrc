package main;

import gui.DreamChopperGUI;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import library.Library;
import library.LootItem;
import library.PriceFetcher;
import library.WorldHopping;

import org.osbot.BotApplication;
import org.osbot.rs07.antiban.AntiBan.BehaviorType;
import org.osbot.rs07.api.Client;
import org.osbot.rs07.api.ui.Message;
import org.osbot.rs07.api.ui.Skill;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.script.ScriptManifest;

import randomstracker.RandomTracker;
import chat.ChatWindow;
import chat.FriendMessage;
import choppers.BarbarianOutpostWillow;
import choppers.CatherbyWillow;
import choppers.CatherbyYew;
import choppers.Chopper;
import choppers.DraynorOak;
import choppers.DraynorWillow;
import choppers.EdgevilleYew;
import choppers.PowerChopper;
import choppers.SeersVillageMagic;
import choppers.SeersVillageMaple;
import choppers.SeersVillageWillow;
import choppers.SeersVillageYew;
import choppers.SorcererMagic;
import choppers.VarrockEastOak;
import choppers.VarrockEastTree;
import choppers.VarrockWestOak;

/**
 * 
 * @author Eliot, Ericthecmh
 * 
 */
@ScriptManifest(author = "Ericthecmh & Eliot", info = "Best AIO woodcutter ever!\nMade by Ericthecmh and Eliot\nVersion 0.1.0", logo = "", name = "DreamChopper v0.1.0", version = 0.10)
public class DreamChopper extends Script {

	// DO NOT TOUCH THIS
	public final static String ip3 = "179";

	// paint stuff
	// paint stuff
	private final Color black = new Color(0, 0, 0, 165);
	private final Color green = new Color(51, 255, 51);
	private final Font paintFont = new Font("Arial", 0, 11);
	private final BasicStroke stroke = new BasicStroke(1.0F);
	private final Color green_outline = new Color(51, 255, 51, 125);
	private int wcExp = 0;
	public LootItem logs;

	private final WorldHopping worldHopping = new WorldHopping(this);

	// The chopper worker
	Chopper chopper;

	int lognum = 0;
	public static double antibanRate = 0.0004;

	//
	PowerChopper powerChop;

	// Should hop worlds
	private boolean hopWorlds;
	private int hopWorldsAmount;

	// Set true if script is stopped
	private boolean stopScript;

	// Set true after onStart runs
	private boolean canStart;

	// Time the script is started
	private long startTime;

	// Whether or not to pick up nests
	private boolean pickUpNests;

	// To track for the server updater
	private int oldLogCount;
	private int oldOakCount;
	private int oldWillowCount;
	private int oldMapleCount;
	private int oldYewCount;
	private int oldMagicCount;
	private int logChopped;
	private int oakChopped;
	private int willowChopped;
	private int mapleChopped;
	private int yewChopped;
	private int magicChopped;
	private int logsChopped;
	private ChatWindow chatWindow;
	private boolean shouldAFK = false;
	private long lastAFKTime;
	private int AFKTimeDelay;

	// Username for the server to track with
	private String username = "Anonymous";

	public void onStart() throws InterruptedException {
		try {
			log("Welcome to DreamChopper!");
			log("Brought to you by the great scripters Eliot and Ericthecmh");
			antiBan.unregisterBehavior(BehaviorType.CAMERA_SHIFT);
			antiBan.unregisterBehavior(BehaviorType.OTHER);
			antiBan.unregisterBehavior(BehaviorType.RANDOM_MOUSE_MOVEMENT);
			antiBan.unregisterBehavior(BehaviorType.SLIGHT_MOUSE_MOVEMENT);
			log("Loading GUI");
			DreamChopperGUI gui = new DreamChopperGUI();
			gui.setVisible(true);
			while (gui.isVisible()) {
				sleep(100);
			}
			if (gui.getName() != null && gui.getName().length() > 0) {
				username = gui.getName();
			}
			hopWorlds = gui.shouldHopWorlds();
			if (hopWorlds)
				hopWorldsAmount = gui.getHopWorldAmount();
			pickUpNests = gui.shouldPickUpNests();
			if (gui.getChatWindow().isSelected()) {
				chatWindow = new ChatWindow();
				chatWindow.setVisible(true);
			}
			if (gui.isPowerChopping()) {
				String treeName = gui.getTreeName();
				powerChop = new PowerChopper(this, treeName, myPlayer()
						.getPosition());
				powerChop.onStart();
			} else {
				switch (gui.getLocationIndex()) {
				case 0:
					chopper = new BarbarianOutpostWillow(this, pickUpNests);
					break;
				case 1:
					switch (gui.getTypeIndex()) {
					case 0:
						chopper = new CatherbyWillow(this, pickUpNests);
						break;
					case 1:
						chopper = new CatherbyYew(this, pickUpNests);
						break;
					}
					break;
				case 2:
					switch (gui.getTypeIndex()) {
					case 0:
						chopper = new DraynorOak(this, pickUpNests);
						break;
					case 1:
						chopper = new DraynorWillow(this, pickUpNests);
						break;
					}
					break;
				case 3:
					chopper = new EdgevilleYew(this, pickUpNests);
					break;
				case 4:
					switch (gui.getTypeIndex()) {
					case 0:
						chopper = new SeersVillageMagic(this, pickUpNests);
						break;
					case 1:
						chopper = new SeersVillageMaple(this, pickUpNests);
						break;
					case 2:
						chopper = new SeersVillageWillow(this, pickUpNests);
						break;
					case 3:
						chopper = new SeersVillageYew(this, pickUpNests);
						break;
					}
					break;
				case 5:
					chopper = new SorcererMagic(this, pickUpNests);
					break;
				case 6:
					switch (gui.getTypeIndex()) {
					case 0:
						chopper = new VarrockEastOak(this, pickUpNests);
						break;
					case 1:
						chopper = new VarrockEastTree(this, pickUpNests);
						break;
					}
					break;
				case 7:
					chopper = new VarrockWestOak(this, pickUpNests);
					break;
				}
			}
			if (chopper != null) {
				int split = chopper.treeName.indexOf(' ');
				final String firstWord = (split != -1) ? chopper.treeName
						.substring(0, chopper.treeName.indexOf(' '))
						: chopper.treeName;
				new Thread(new Runnable() {
					public void run() {
						if (firstWord.equals("Tree"))
							try {
								logs = new LootItem("Logs",
										PriceFetcher.getPrice("Logs"));
								log("Price: " + logs.avgPrice);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						else
							try {
								logs = new LootItem(firstWord + " logs",
										PriceFetcher.getPrice(firstWord
												+ " logs"));
								log("Price: " + logs.avgPrice);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
					}
				}).start();
			}
			wcExp = skills.getExperience(Skill.WOODCUTTING);
			if (chopper != null)
				chopper.onStart();
			if (chopper != null && gui.getEnableAFK().isSelected()) {
				lastAFKTime = System.currentTimeMillis();
				AFKTimeDelay = MethodProvider.random(5 * 60 * 1000,
						10 * 60 * 1000);
				shouldAFK = true;
				log("AFK Enabled");
				log("First AFK: " + AFKTimeDelay / 1000 + " seconds from now");
			} else if (powerChop != null && gui.getEnableAFK().isSelected()) {
				lastAFKTime = System.currentTimeMillis();
				AFKTimeDelay = MethodProvider.random(5 * 60 * 1000,
						10 * 60 * 1000);
				shouldAFK = true;
				log("AFK Enabled");
				log("First AFK: " + AFKTimeDelay / 1000 + " seconds from now");
			}
			new Thread(new ServerUpdater()).start();
			new Thread(new RandomTracker(this)).start();
			startTime = System.currentTimeMillis();
			canStart = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public int onLoop() throws InterruptedException {
		if (chatWindow != null) {
			String playerMessage = chatWindow.getNextPlayerMessage();
			if (playerMessage != null) {
				this.keyboard.typeString(playerMessage);
			}
			String clanMessage = chatWindow.getNextClanMessage();
			if (clanMessage != null) {
				this.keyboard.typeString("/" + clanMessage);
			}
			FriendMessage fm = chatWindow.getNextFriendMessage();
			if (fm != null) {
				log("Send message");
				Library.sendFriendMessage(fm.username, fm.message, this);
			}
		}
		if (client.getLoginState() == Client.LoginState.LOGGED_IN
				&& (hopWorlds
						&& Library.getPlayerCount(this) >= hopWorldsAmount || worldHopping
							.isInBotWorld())) {
			worldHopping.switchWorlds();
		}
		if (shouldAFK
				&& System.currentTimeMillis() - lastAFKTime > AFKTimeDelay) {
			lastAFKTime = System.currentTimeMillis();
			AFKTimeDelay = MethodProvider.random(5 * 60 * 1000, 10 * 60 * 1000);
			int time = MethodProvider.random(5000, 20000);
			log("Going AFK for " + time / 1000 + " seconds");
			mouse.moveOutsideScreen();
			while (System.currentTimeMillis() - lastAFKTime < time) {
				try {
					sleep(MethodProvider.random(100, 200));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			log("Resuming");
			log("Next AFK: " + (AFKTimeDelay - time) / 1000
					+ " seconds from now");
		}
		if (chopper != null) {
			chopper.onLoop();
		} else if (powerChop != null)
			powerChop.onLoop();
		if (logs != null) {
			if (inventory.getAmount(logs.name) > lognum) {
				logs.lootedAmount++;
			}
			if (inventory.getAmount(logs.name) == 0)
				lognum = 0;
		} else {
		}
		int logCount = (int) inventory.getAmount("Logs");
		int oakCount = (int) inventory.getAmount("Oak logs");
		int willowCount = (int) inventory.getAmount("Willow logs");
		int mapleCount = (int) inventory.getAmount("Maple logs");
		int yewCount = (int) inventory.getAmount("Yew logs");
		int magicCount = (int) inventory.getAmount("Magic logs");
		if (logCount > oldLogCount) {
			logChopped++;
			oldLogCount = logCount;
		}
		if (oakCount > oldOakCount) {
			oakChopped++;
			oldOakCount = oakCount;
		}
		if (willowCount > oldWillowCount) {
			willowChopped++;
			oldWillowCount = willowCount;
		}
		if (mapleCount > oldMapleCount) {
			mapleChopped++;
			oldMapleCount = mapleCount;
		}
		if (yewCount > oldYewCount) {
			yewChopped++;
			oldYewCount = yewCount;
		}
		if (magicCount > oldMagicCount) {
			magicChopped++;
			oldMagicCount = magicCount;
		}
		logsChopped = logChopped + oakChopped + willowChopped + mapleChopped
				+ yewChopped + magicChopped;
		if (logs != null)
			lognum = (int) inventory.getAmount(logs.name);
		return 15;
	}

	/**
	 * Draws the paint
	 */
	@Override
	public void onPaint(Graphics2D gr) {
		// paint
		try {
			if (canStart) {
				int totalGP = 0;
				if (logs != null)
					totalGP += logs.avgPrice * logs.lootedAmount;

				int totalExp = skills.getExperience(Skill.WOODCUTTING) - wcExp;
				long runTime = System.currentTimeMillis() - startTime;
				gr.setColor(black);
				gr.fillRoundRect(7, 307, 503, 30, 6, 6);
				gr.setColor(green_outline);
				gr.drawRoundRect(7, 307, 503, 30, 6, 6);
				// text
				gr.setStroke(this.stroke);
				gr.setColor(green);
				gr.setFont(paintFont);
				gr.drawString("Runtime: " + format(runTime), 14, 318);
				if (chopper != null && ((Chopper) chopper).getState() != null)
					gr.drawString("State: "
							+ ((Chopper) chopper).getState().toString(), 14,
							332);
				else if (powerChop != null
						&& ((PowerChopper) powerChop).getState() != null)
					gr.drawString("State: "
							+ ((PowerChopper) powerChop).getState().toString(),
							14, 332);

				gr.drawString("Total EXP: " + totalExp, 182, 318);
				if (logs != null) {
					gr.drawString("Logs [GP]: " + logs.lootedAmount + " ["
							+ totalGP + "]", 342, 318);
				}
				gr.setRenderingHints(new RenderingHints(
						RenderingHints.KEY_ANTIALIASING,
						RenderingHints.VALUE_ANTIALIAS_ON));

				gr.setColor(green);
				gr.setStroke(this.stroke);
				gr.setFont(paintFont);
				gr.drawString("EXP/hour: "
						+ (int) (totalExp * (3600000.0D / runTime)), 182, 332);
				if (logs != null) {
					gr.drawString(
							"Logs/hr [GP/hr]: "
									+ (int) (logs.lootedAmount * (3600000.0D / runTime))
									+ " ["
									+ (int) (totalGP * (3600000.0D / runTime))
									+ "]", 342, 332);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	String format(final long time) {
		final StringBuilder t = new StringBuilder();
		final long total_secs = time / 1000;
		final long total_mins = total_secs / 60;
		final long total_hrs = total_mins / 60;
		final int secs = (int) total_secs % 60;
		final int mins = (int) total_mins % 60;
		final int hrs = (int) total_hrs;
		if (hrs < 10)
			t.append("0");
		t.append(hrs + ":");
		if (mins < 10)
			t.append("0");
		t.append(mins + ":");
		if (secs < 10) {
			t.append("0");
			t.append(secs);
		} else
			t.append(secs);
		return t.toString();
	}

	@Override
	public void onExit() {
		stopScript = true;
	}

	public LootItem getLogs() {
		return logs;
	}

	class ServerUpdater implements Runnable {

		long oldrun = 0;
		int oldxp = skills.getExperience(Skill.WOODCUTTING);
		long oldlogschopped = 0;
		long oldgp = 0;

		public void run() {
			while (!stopScript) {
				if (canStart) {
					long run = (long) ((System.currentTimeMillis() - startTime) / 1000);
					int currentExp = skills.getExperience(Skill.WOODCUTTING);
					String prepString = "" + System.nanoTime();
					prepString = prepString.substring(prepString.length() - 6);
					String runtime = Library.toHex(Library.encrypt(prepString
							+ (run - oldrun), "DSSOSBOT"));
					prepString = "" + System.nanoTime();
					prepString = prepString.substring(prepString.length() - 6);
					String expgained = Library.toHex(Library.encrypt(prepString
							+ (currentExp - oldxp), "DSSOSBOT"));
					prepString = "" + System.nanoTime();
					prepString = prepString.substring(prepString.length() - 6);
					String logschopped = Library.toHex(Library.encrypt(
							prepString + (logsChopped - oldlogschopped),
							"DSSOSBOT"));
					prepString = "" + System.nanoTime();
					prepString = prepString.substring(prepString.length() - 6);
					long totalGP = 151 * logChopped + 46 * oakChopped + 21
							* willowChopped + 50 * mapleChopped + 467
							* yewChopped + 1266 * magicChopped;
					String gpearned = Library.toHex(Library.encrypt(prepString
							+ (totalGP - oldgp), "DSSOSBOT"));
					oldrun = run;
					oldxp = currentExp;
					oldlogschopped = logsChopped;
					oldgp = totalGP;
					String valid = Library.toHex(Library.encrypt("VALID",
							expgained));
					try {
						URL url = new URL(
								"http://dreamscripts.org/updatesig.php?Script=DreamChopper&Username="
										+ BotApplication.getInstance()
												.getOSAccount().username
										+ "&Runtime=" + runtime + "&LogsCut="
										+ logschopped + "&Expgained="
										+ expgained + "&GPEarned=" + gpearned
										+ "&Valid=" + valid);
						BufferedReader br = new BufferedReader(
								new InputStreamReader(url.openStream()));
					} catch (Exception e) {
					}
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}

	public void onMessage(Message m) {
		if (m.getMessage().contains("You do not have an axe which"))
			stop(true);
		if (chatWindow != null)
			try {
				chatWindow.addMessage(m.getMessage(), m.getTypeId(),
						m.getUsername());
			} catch (Exception e) {

			}
	}

}
