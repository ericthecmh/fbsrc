package library;

import org.osbot.rs07.api.filter.Filter;
import org.osbot.rs07.api.model.Item;

public class NotAxeMatch implements Filter<Item> {

	@Override
	public boolean match(Item arg0) {
		return !arg0.getName().contains("axe");
	}

}
