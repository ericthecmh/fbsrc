package library;

import org.osbot.rs07.api.filter.Filter;
import org.osbot.rs07.api.model.Item;

public class AxeMatch implements Filter<Item> {

	@Override
	public boolean match(Item arg0) {
		if(arg0 == null || arg0.getName() == null){
			return false;
		}
		return arg0.getName().contains(" axe");
	}

}
