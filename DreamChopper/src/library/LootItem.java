package library;

/**
 * 
 * @author Erik
 * 
 */
// represents an item we wish to loot
public class LootItem {
	// represent the name of the item
	public String name;

	// represent the avergage price of an item
	public int avgPrice;

	// represent how many have been looted
	public int lootedAmount;
	
	

	// construct an instance of LootItem
	public LootItem(String name, int avgPrice) {
		this.name = name;
		this.avgPrice = avgPrice;
		this.lootedAmount = 0;
	}
}
