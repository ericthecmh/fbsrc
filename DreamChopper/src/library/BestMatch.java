package library;

/**
 * @author Erik
 */

import org.osbot.rs07.api.filter.Filter;
import org.osbot.rs07.api.model.Entity;
import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class BestMatch implements Filter<RS2Object> {
	Script script;
	String treeName;
	Area treeArea;

	public BestMatch(Script script, String treeName, Area treeArea) {
		this.script = script;
		this.treeName = treeName;
		this.treeArea = treeArea;
	}

	@Override
	public boolean match(RS2Object o) {
		if (o instanceof Entity && !(o instanceof NPC)) {
			return ((Entity) o).getName().equals(this.treeName)
					&& treeArea.contains(o) && script.map.canReach((Entity) o);
		}
		return false;
	}
}