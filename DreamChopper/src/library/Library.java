package library;

import static org.osbot.rs07.script.MethodProvider.random;

import java.awt.Point;
import java.awt.Rectangle;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import main.DreamChopper;

import org.osbot.rs07.api.filter.Filter;
import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.map.PositionPolygon;
import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.api.model.Player;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.Option;
import org.osbot.rs07.api.ui.RS2Interface;
import org.osbot.rs07.api.ui.RS2InterfaceChild;
import org.osbot.rs07.api.ui.Tab;
import org.osbot.rs07.input.mouse.InventorySlotDestination;
import org.osbot.rs07.input.mouse.RectangleDestination;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class Library {
	// CONSTANTS
	private final static int MENU_HEADER_HEIGHT = 18;
	private final static int MENU_ITEM_HEIGHT = 15;
	private final static int FRIENDS_CORNER_X = 557;
	private final static int FRIENDS_CORNER_Y = 230;
	private final static int FRIENDS_ITEM_HEIGHT = 15;
	private final static String SERVER_IP = "192.";
	private final static int[] invSlotsBest = { 0, 4, 8, 12, 16, 20, 24, 1, 5,
			9, 13, 17, 21, 25, 2, 6, 10, 14, 18, 22, 26, 3, 7, 11, 15, 19, 23,
			27 };

	// closest object with the specified action
	public static RS2Object closestObject(String name, String action,
			Script script) {
		double distance = 2000000000.0;
		RS2Object ret = null;
		for (RS2Object o : script.objects.getAll()) {
			if (o != null && o.getName().equals(name)
					&& containsInArray(o.getDefinition().getActions(), action)
					&& distanceTo(o.getPosition(), script) < distance) {
				distance = distanceTo(o.getPosition(), script);
				ret = o;
			}
		}
		return ret;
	}

	// distance to
	public static double distanceTo(Position p, Script s) {
		return distanceBetween(p, s.myPosition());
	}

	// distance between
	public static double distanceBetween(Position p, Position p2) {
		return Math.sqrt((p.getX() - p2.getX()) * (p.getX() - p2.getX())
				+ (p.getY() - p2.getY()) * (p.getY() - p2.getY()));
	}

	// does this array contains the wanted value?
	public static boolean containsInArray(String[] hay, String needle) {
		for (String s : hay) {
			if (s != null && s.equals(needle))
				return true;
		}
		return false;
	}

	// wait for the specified event to occur
	public static boolean waitFor(Conditional c, long time, int delta,
			Script script) {
		for (int i = 0; i < (int) (time / (delta + 0.0) + 1); i++) {
			if (c.isSatisfied(script)) {
				return true;
			}
			try {
				script.sleep(delta);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}

	// used for walking
	public static int walk(PositionPolygon path, boolean reverse,
			Script script, Area bankArea, Area treeArea) {
		int pi = path.getClosestIndex(script.myPosition());
		if (pi == -1) {
			return -1;
		}

		if (!reverse && bankArea.contains(script.myPosition())) {
			return -1;
		} else if (reverse && treeArea.contains(script.myPosition())) {
			return -1;
		}

		int c = path.getCount();
		int wi = reverse ? 0 : c - 1;

		if (wi == pi) {
			return -1;
		}

		if (!script.settings.isRunning()
				&& script.settings.getRunEnergy() > script.random(30, 50)) {
			script.settings.setRunning(true);
			return script.gRandom(400, 200);
		}

		while (wi >= 0 && wi < c && (reverse ? wi <= pi : wi >= pi)) {
			Point point = path.get(wi);
			if (script.localWalker.walk(point.x, point.y)) {
				try {
					script.sleep(script.random(500, 800));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return 0;
			}
			wi += reverse ? 1 : -1;
		}

		return -1;
	}

	// drops all except those in which we do not want to drop
	public static void dropAllExcept(int[] list, Script script)
			throws InterruptedException {
		for (int i : invSlotsBest)
			if ((getItemOnSlot(i, script) != null)
					&& (!contains(getItemOnSlot(i, script), list))) {
				String name = getItemOnSlot(i, script).getName().toLowerCase();
				if (!name.contains("pickaxe") && !name.contains("aterski")
						&& !name.contains("axe handle"))
					interact(i, "Drop", script);
			}
	}

	public static boolean contains(Item item, int[] list) {
		if (item != null) {
			for (int key : list) {
				if (item.getId() == key) {
					return true;
				}
			}
		}
		return false;
	}

	// return the item in the given slot
	public static Item getItemOnSlot(int Slot, Script script) {
		if ((Slot > 27) || (Slot < 0))
			return null;
		Item[] items = script.getInventory().getItems();
		return items[Slot];
	}


	// interact with an inventory slot
	private static boolean interact(int Slot, String action, Script script)
			throws InterruptedException {
		while (!script.tabs.open(Tab.INVENTORY)) {
			script.tabs.open(Tab.INVENTORY);
			script.sleep(200);
		}
		InventorySlotDestination isd = new InventorySlotDestination(script.bot,
				Slot);
		while (!isd.getBoundingBox().contains(script.mouse.getPosition())) {
			script.mouse.move(isd);
			script.sleep(5);
		}
		if (script.inventory.isItemSelected()) {
			script.mouse.click(isd, false);
		}
		int i = -1;
		for (int index = 0; index < script.getMenuAPI().getMenu().size(); index++) {
			if ((((Option) script.getMenuAPI().getMenu().get(index)).action
					.toLowerCase().toString().equals(action.toLowerCase()))) {
				i = index;
				break;
			}
		}
		if (i != -1) {
			if (i == 0) {
				// script.log("Click");
				script.mouse.click(isd, false);
				script.log("Interact finished");
			} else {
				// script.log("Right click");
				script.mouse.click(isd, true);
				try {
					script.sleep(MethodProvider.random(300, 600));
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if (script.menu.isOpen()) {
					// int x = script.menu.getX();
					// int y = script.menu.getY();
					// y += MENU_HEADER_HEIGHT + 1;
					// y += i * MENU_ITEM_HEIGHT;
					Point menu = new Point(script.getMenuAPI().getX(), script
							.getMenuAPI().getY());
					// int xOff = random(5, this.client.getMenuWidth() - 4);
					int yOff = 21 + 16 * i + 3;
					RectangleDestination menuItem = new RectangleDestination(
							script.bot, new Rectangle(menu.x + 50, menu.y
									+ yOff, 5, 8));
					// RectangleDestination menuItem = new RectangleDestination(
					// script.bot, x, y + 2,
					// script.menu.getWidth() - 2, MENU_ITEM_HEIGHT);
					while (!menuItem.getBoundingBox().contains(
							script.mouse.getPosition())) {
						script.mouse.move(menuItem);
						script.sleep(5);
					}
					script.mouse.click(menuItem, false);
					return true;
				}
				return false;
			}
		} else {
			return false;
		}
		return false;

	}

	public static boolean doMenu(String action, RectangleDestination e,
			Script script) {
		// script.log("Right click");
		int index = 3;
		script.mouse.click(e, true);
		if (script.menu.isOpen()) {
			try {
				MethodProvider.sleep(MethodProvider.random(400, 700));
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			int x = script.menu.getX();
			int y = script.menu.getY();
			y += MENU_HEADER_HEIGHT + 1;
			y += index * MENU_ITEM_HEIGHT;
			RectangleDestination menuItem = new RectangleDestination(
					script.bot, x, y + 2, script.menu.getWidth() - 2,
					MENU_ITEM_HEIGHT);
			script.mouse.click(menuItem, false);
			return true;
		}
		return false;
	}

	/**
	 * Gets the byte representation of a string (using ascii)
	 * 
	 * @param string
	 * @return byte representation
	 */
	public static byte[] getBytes(String string) {
		byte[] bytes = new byte[string.length()];
		for (int i = 0; i < string.length(); i++) {
			bytes[i] = (byte) string.charAt(i);
		}
		return bytes;
	}

	/**
	 * converts a byte array into a hexadecimal string
	 * 
	 * @param encrypted
	 * @return hex string
	 */
	public static String toHex(byte[] encrypted) {
		byte[] newBytes = new byte[encrypted.length * 2];
		for (int i = 0; i < encrypted.length; i++) {
			newBytes[i * 2 + 1] = getHex(encrypted[i] & 15);
			newBytes[i * 2] = getHex(encrypted[i] >> 4);
		}
		return new String(newBytes);
	}

	/**
	 * converts a hexadecimal string to a byte array
	 * 
	 * @param hex
	 * @return
	 */
	public static byte[] fromHex(String hex) {
		byte[] newBytes = new byte[hex.length() / 2];
		for (int i = 0; i < newBytes.length; i++) {
			newBytes[i] = fromHex(hex.charAt(i * 2));
			newBytes[i] <<= 4;
			newBytes[i] += fromHex(hex.charAt(i * 2 + 1));
		}
		return newBytes;
	}

	/**
	 * Converts a hexademical character into the base 10 value
	 * 
	 * @param a
	 * @return
	 */
	public static byte fromHex(char a) {
		if (a >= 48 && a <= 57)
			return (byte) (a - 48);
		else
			return (byte) (a - 55);
	}

	/**
	 * Converts a base 10 value to the hexadecimal number (ascii)
	 * 
	 * @param i
	 * @return
	 */
	public static byte getHex(int i) {
		if (i >= 0 && i <= 9)
			return (byte) (i + 48);
		else
			return (byte) (i - 10 + 65);
	}

	/**
	 * Flips the byte array
	 * 
	 * @param key
	 * @return
	 */
	public static byte[] flip(byte[] key) {
		byte[] newbytes = new byte[key.length];
		for (int i = 0; i < key.length; i++) {
			newbytes[key.length - i - 1] = key[i];
		}
		return newbytes;
	}

	/**
	 * Encrypts a string based on a given key
	 * 
	 * @param string
	 * @param keyString
	 * @return
	 */
	public static byte[] encrypt(String string, String keyString) {
		byte[] data = getBytes(string);
		byte[] key = flip(getBytes(keyString));
		byte[] newData = new byte[string.length()];
		for (int i = 0; i < string.length(); i++) {
			newData[i] = (byte) (data[i] ^ key[i % key.length]);
		}
		return newData;
	}

	/**
	 * Decrypts an encrypted string based on the key
	 * 
	 * @param string
	 * @param keyString
	 * @return
	 */
	public static byte[] decrypt(String string, String keyString) {
		byte[] key = flip(getBytes(keyString));
		byte[] data = getBytes(string);
		byte[] newData = new byte[string.length()];
		for (int i = 0; i < string.length(); i++) {
			newData[i] = (byte) (data[i] ^ key[i % key.length]);
		}
		return newData;
	}

	/**
	 * Decrypts an encrypted byte array based on the key
	 * 
	 * @param string
	 * @param keyString
	 * @return
	 */
	public static String decrypt(byte[] data, String keyString) {
		byte[] key = flip(getBytes(keyString));
		byte[] newData = new byte[data.length];
		for (int i = 0; i < data.length; i++) {
			newData[i] = (byte) (data[i] ^ key[i % key.length]);
		}
		return new String(newData);
	}

	/**
	 * Checks if the server is valid
	 * 
	 * @return true if valid, false if not
	 */
	public static boolean serverCheck(DreamChopper s) {
		long time = System.nanoTime();
		boolean valid = true;
		// Server check 1, decryption check
		String data;
		String pi;
		try {
			pi = SERVER_IP + DepositBox.ip2 + "." + s.ip3 + Walking.ip4;
			data = toHex(encrypt("" + time, "DreamFighter"));
			URL url = new URL("http://" + pi + "/verify1.php?AES_DATA=" + data
					+ "&AES_KEY=" + s.bot.getUsername());
			BufferedReader br = new BufferedReader(new InputStreamReader(
					url.openStream()));
			long response = Long.parseLong(br.readLine());
			valid = valid && (response == time);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		// Server check 2, encryption check
		try {
			URL url = new URL("http://" + pi + "/verify2.php?AES_DATA=" + data
					+ "&AES_KEY=" + "AOEU");
			BufferedReader br = new BufferedReader(new InputStreamReader(
					url.openStream()));
			String response = br.readLine();
			return valid
					&& response.equals(Library.toHex(Library.encrypt(data,
							"DreamScripts")));
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Checks whether the current system time is valid
	 * 
	 * @return true if valid
	 */
	public static boolean timeCheck() {
		try {
			System.out
					.println(Math.abs(System.currentTimeMillis() - getTime()));
			return (Math.abs(System.currentTimeMillis() - getTime()) < 86400000);
		} catch (Exception e) {

		}
		return false;
	}

	/**
	 * Gets the current time in milliseconds from the server
	 * 
	 * @return time
	 */
	public static long getTime() {
		try {
			String pi = SERVER_IP + DepositBox.ip2 + "." + DreamChopper.ip3
					+ Walking.ip4;
			URL url = new URL("http://" + pi + "/time.php");
			BufferedReader br = new BufferedReader(new InputStreamReader(
					url.openStream()));
			String s;
			while ((s = br.readLine()) != null) {
				return Long.parseLong(s, 10);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return -10000000;
	}

	/**
	 * Returns valid if the script is valid
	 * 
	 * @return
	 */
	public static boolean scriptValidCheck() {
		try {
			String pi = SERVER_IP + DepositBox.ip2 + "." + DreamChopper.ip3
					+ Walking.ip4;
			URL url = new URL("http://" + pi + "/validMiner.php");
			BufferedReader br = new BufferedReader(new InputStreamReader(
					url.openStream()));
			String s;
			while ((s = br.readLine()) != null) {
				return s.equals("True");
			}
			return false;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Sends a message to a friend using the friends list
	 * 
	 * @param username
	 * @param message
	 */
	public static void sendFriendMessage(String username, String message,
			Script script) {
		while (script.tabs.getOpen() != Tab.FRIENDS) {
			script.tabs.open(Tab.FRIENDS);
			try {
				script.sleep(random(400, 700));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		RS2Interface inte = script.interfaces.get(550);
		RS2InterfaceChild child = inte.getChild(1);
		int i = 0;
		for (; i < 1000; i++) {
			RS2InterfaceChild friend = child.getChild(i);
			if (friend != null && friend.isVisible()) {
				if (friend.getMessage().equalsIgnoreCase(username)) {
					break;
				}
			} else {
				i = 1000;
				break;
			}
		}
		script.log(i + "");
		if (i != 1000) {
			// Friend found.
			int y = FRIENDS_CORNER_Y + i * FRIENDS_ITEM_HEIGHT;
			if (y < 410) {
				RectangleDestination rd = new RectangleDestination(script.bot,
						FRIENDS_CORNER_X, y + 1, 70, FRIENDS_ITEM_HEIGHT - 1);
				script.mouse.click(rd);
				if (Library.waitFor(new Conditional() {

					@Override
					public boolean isSatisfied(Script script) {
						RS2Interface inte = script.interfaces.get(548);
						RS2InterfaceChild child = inte.getChild(123);
						return child != null && child.isVisible();
					}

				}, 1000, 100, script)) {
					script.keyboard.typeString(message);
				}
			}
		}
	}

	/**
	 * Determines whether this container only exists out of the items specified
	 * by the filters. This method does not require that all the specified items
	 * exist, it determines whether there are no other items in the container.
	 * Note on the other hand if NONE of the items specified in the filters
	 * exists in the container false will be returned.
	 * 
	 * @param filters
	 *            The filter(s).
	 * @return True if the container only exists out of the specified items.
	 *         False if NONE of the items exist, even when the container is
	 *         empty.
	 */
	public static boolean onlyContains(Script script, Filter<Item>... filters) {
		List<Item> items = filter(script, filters);
		for (Item inv : script.inventory.getItems()) {
			if(inv == null) continue;
			boolean found = false;
			for (Item result : items) {
				if ((inv != null && result != null)
						&& result.getId() == inv.getId()) {
					found = true;
					break;
				}
			}

			if (!found) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Filters the item array with the specified filters.
	 * 
	 * @param filters
	 *            The filters.
	 * @return A list of filtered items.
	 */
	public static List<Item> filter(Script script, Filter<Item>... filters) {
		List<Item> filteredItems = new ArrayList<>();

		iLoop: for (Item item : script.inventory.getItems()) {
			if (item == null)
				continue;

			for (Filter<Item> filter : filters) {
				if (!filter.match(item)) {
					continue iLoop;
				}
			}

			filteredItems.add(item);
		}

		return filteredItems;
	}

	public static RS2Object getClosestObjectWithNameAndAction(String name,
			String action, Script script) {
		RS2Object object = null;
		double distance = 2000000000.0;
		for(RS2Object o : script.objects.getAll()){
			if(o == null || o.getName() == null || o.getActions() == null || o.getActions().length == 0) continue;
			if(o.getName().equals(name) && o.getActions()[0].equals(action) && Library.distanceTo(o.getPosition(), script) < distance){
				distance = Library.distanceTo(o.getPosition(), script);
				object = o;
			}
		}
		return object;
	}
	
	public static int getPlayerCount(Script s){
		int i = 0;
		for(Player p : s.players.getAll()){
			if(p == null) continue;
			if(Library.distanceTo(p.getPosition(), s) < 15){
				i++;
			}
		}
		return i;
	}
	
}
