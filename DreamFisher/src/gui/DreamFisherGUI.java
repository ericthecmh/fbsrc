/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.util.HashMap;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;

/**
 * 
 * @author Ericthecmh
 */
public class DreamFisherGUI extends javax.swing.JFrame {

	// List/Combobox models
	private DefaultComboBoxModel locationModel;
	private DefaultComboBoxModel typeModel;
	private DefaultListModel possibleFishModel;

	private boolean initialized;

	// Methods for certain locations
	private final HashMap<String, Integer> actionsIndicesMap;
	private final String[][] actions = new String[][] {
			// Draynor
			new String[] { "Net", "Bait" },
			// Alkharid
			new String[] { "Net", "Bait" },
			// Barb fishing
			new String[] { "Use-rod" },
			// Barb village
			new String[] { "Lure", "Bait" },
			// Barb outpost
			new String[] { "Net", "Bait" },
			// Baxtorian Falls
			new String[] { "Lure", "Bait" },
			// Catherby
			new String[] { "Net", "Bait", "Big net", "Cage", "Harpoon" },
			// Fishing guild
			new String[] { "Big net", "Cage", "Harpoon" },
			// Gnome stronghold
			new String[] { "Lure", "Bait" },
			// Lumbridge
			new String[] { "Lure", "Bait" },
			// Lumbridge Swamp
			new String[] { "Net", "Bait" },
			// Lower Baxtorian
			new String[] { "Lure", "Bait" },
			// Musa point
			new String[] { "Net", "Bait", "Cage", "Harpoon" },
			// Mud skippers point
			new String[] { "Net", "Bait" },
			// Picatoris
			new String[] { "Net" },
			// Shilo village
			new String[] { "Lure", "Bait" },
			// Sinclair mansion
			new String[] { "Lure", "Bait" },
			// Rellekka
			new String[] { "Net", "Bait", "Cage", "Harpoon" }

	};

	private final HashMap<String, Integer> fishIndicesMap;
	private final String[][] fishNames = new String[][] {
			// Net
			new String[] { "Raw shrimps", "Raw anchovies", "Raw monkfish" },
			// Bait
			new String[] { "Raw herring", "Raw pike" },
			// Heavyrod
			new String[] { "Leaping trout", "Leaping salmon",
					"Leaping sturgeon" },
			// Lure
			new String[] { "Raw trout", "Raw salmon" },
			// Big net
			new String[] { "Raw mackerel", "Raw cod", "Casket", "Oyster",
					"Seaweed" },
			// Cage
			new String[] { "Raw lobster" },
			// Harpoon
			new String[] { "Raw tuna", "Raw bass", "Raw swordfish", "Raw shark" }, };

	/**
	 * Creates new form DreamFisher
	 */
	public DreamFisherGUI() {
		System.out.println("CALLING INITCOMPONENTS");
		initComponents();
		System.out.println("DONE INITCOMPONENTS");
		actionsIndicesMap = new HashMap<String, Integer>();
		actionsIndicesMap.put("Draynor", 0);
		actionsIndicesMap.put("Alkharid", 1);
		actionsIndicesMap.put("Barbarian Fishing", 2);
		actionsIndicesMap.put("Barbarian Village", 3);
		actionsIndicesMap.put("Barbarian Outpost", 4);
		actionsIndicesMap.put("Baxtorias Falls", 5);
		actionsIndicesMap.put("Catherby", 6);
		actionsIndicesMap.put("Fishing Guild", 7);
		actionsIndicesMap.put("Gnome stronghold", 8);
		actionsIndicesMap.put("Lumbridge", 9);
		actionsIndicesMap.put("Lumbridge Swamp", 10);
		actionsIndicesMap.put("Lower Baxtorian", 11);
		actionsIndicesMap.put("Musa point", 12);
		actionsIndicesMap.put("Mud skippers point", 13);
		actionsIndicesMap.put("Piscatoris", 14);
		actionsIndicesMap.put("Shilo Village", 15);
		actionsIndicesMap.put("Sinclair mansion", 16);
		actionsIndicesMap.put("Rellekka", 17);
		locationModel = new DefaultComboBoxModel();
		location.setModel(locationModel);
		for (String s : actionsIndicesMap.keySet()) {
			locationModel.addElement(s);
		}
		fishIndicesMap = new HashMap<String, Integer>();
		fishIndicesMap.put("Net", 0);
		fishIndicesMap.put("Bait", 1);
		fishIndicesMap.put("Use-rod", 2);
		fishIndicesMap.put("Lure", 3);
		fishIndicesMap.put("Big net", 4);
		fishIndicesMap.put("Cage", 5);
		fishIndicesMap.put("Harpoon", 6);
		updateGUIComponents(true, true);
		initialized = true;
		System.out.println("AOEU");
	}

	private void updateGUIComponents(boolean updateActions,
			boolean updatePossibleList) {
		if (!initialized)
			return;
		if (updateActions) {
			typeModel = new DefaultComboBoxModel(
					actions[actionsIndicesMap.get(location.getSelectedItem())]);
			type.setModel(typeModel);
		}
		if (updatePossibleList) {
			possibleFishModel = new DefaultListModel();
			for (String s : fishNames[fishIndicesMap
					.get(type.getSelectedItem())]) {
				possibleFishModel.addElement(s);
			}
			possibleFish.setModel(possibleFishModel);
		}
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
	// <editor-fold defaultstate="collapsed"
	// <editor-fold defaultstate="collapsed"
	// desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		jLabel1 = new javax.swing.JLabel();
		location = new javax.swing.JComboBox();
		jLabel2 = new javax.swing.JLabel();
		type = new javax.swing.JComboBox();
		jLabel3 = new javax.swing.JLabel();
		jScrollPane1 = new javax.swing.JScrollPane();
		possibleFish = new javax.swing.JList();
		jScrollPane2 = new javax.swing.JScrollPane();
		jTextArea1 = new javax.swing.JTextArea();
		powerfish = new javax.swing.JCheckBox();
		jButton1 = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		System.out.println(1);
		jLabel1.setText("Location:");

		location.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(java.awt.event.ItemEvent evt) {
				locationItemStateChanged(evt);
			}
		});
		location.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				locationActionPerformed(evt);
			}
		});
		System.out.println(2);
		jLabel2.setText("Fishing Method:");

		type.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(java.awt.event.ItemEvent evt) {
				typeItemStateChanged(evt);
			}
		});
		type.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				typeActionPerformed(evt);
			}
		});
		System.out.println(3);
		jLabel3.setText("Possible Fish:");

		jScrollPane1.setViewportView(possibleFish);

		jScrollPane2.setViewportView(jTextArea1);

		System.out.println(4);
		powerfish.setText("Powerfish");

		jButton1.setText("Start!");
		jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton1ActionPerformed(evt);
			}
		});

		System.out.println(5);
		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addContainerGap()
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING)
												.addGroup(
														layout.createSequentialGroup()
																.addComponent(
																		jButton1,
																		javax.swing.GroupLayout.DEFAULT_SIZE,
																		javax.swing.GroupLayout.DEFAULT_SIZE,
																		Short.MAX_VALUE)
																.addContainerGap())
												.addGroup(
														layout.createSequentialGroup()
																.addGroup(
																		layout.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.LEADING)
																				.addGroup(
																						layout.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.TRAILING)
																								.addGroup(
																										layout.createSequentialGroup()
																												.addGroup(
																														layout.createParallelGroup(
																																javax.swing.GroupLayout.Alignment.LEADING)
																																.addComponent(
																																		jLabel2)
																																.addComponent(
																																		jLabel1))
																												.addPreferredGap(
																														javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																												.addGroup(
																														layout.createParallelGroup(
																																javax.swing.GroupLayout.Alignment.LEADING)
																																.addComponent(
																																		type,
																																		0,
																																		javax.swing.GroupLayout.DEFAULT_SIZE,
																																		Short.MAX_VALUE)
																																.addComponent(
																																		location,
																																		0,
																																		javax.swing.GroupLayout.DEFAULT_SIZE,
																																		Short.MAX_VALUE)))
																								.addGroup(
																										layout.createSequentialGroup()
																												.addComponent(
																														jScrollPane1,
																														javax.swing.GroupLayout.DEFAULT_SIZE,
																														229,
																														Short.MAX_VALUE)
																												.addPreferredGap(
																														javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																												.addComponent(
																														jScrollPane2,
																														javax.swing.GroupLayout.PREFERRED_SIZE,
																														javax.swing.GroupLayout.DEFAULT_SIZE,
																														javax.swing.GroupLayout.PREFERRED_SIZE)))
																				.addComponent(
																						powerfish)
																				.addComponent(
																						jLabel3))
																.addGap(10, 10,
																		10)))));
		System.out.println(6);
		layout.setVerticalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addContainerGap()
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel1)
												.addComponent(
														location,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel2)
												.addComponent(
														type,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addComponent(jLabel3)
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING,
												false)
												.addComponent(jScrollPane2)
												.addComponent(
														jScrollPane1,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														169, Short.MAX_VALUE))
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addComponent(powerfish)
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(jButton1,
										javax.swing.GroupLayout.PREFERRED_SIZE,
										42,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addContainerGap(
										javax.swing.GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)));
		System.out.println(7);
		pack();
	}// </editor-fold>//GEN-END:initComponents

	private void typeActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_typeActionPerformed
		// TODO add your handling code here:
	}// GEN-LAST:event_typeActionPerformed

	private void locationActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_locationActionPerformed
		// TODO add your handling code here:
	}// GEN-LAST:event_locationActionPerformed

	private void locationItemStateChanged(java.awt.event.ItemEvent evt) {// GEN-FIRST:event_locationItemStateChanged
		updateGUIComponents(true, true);
	}// GEN-LAST:event_locationItemStateChanged

	private void typeItemStateChanged(java.awt.event.ItemEvent evt) {// GEN-FIRST:event_typeItemStateChanged
		updateGUIComponents(false, true);
	}// GEN-LAST:event_typeItemStateChanged

	private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton1ActionPerformed
		this.setVisible(false);
	}// GEN-LAST:event_jButton1ActionPerformed

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JButton jButton1;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JTextArea jTextArea1;
	private javax.swing.JComboBox location;
	private javax.swing.JList possibleFish;
	private javax.swing.JCheckBox powerfish;
	private javax.swing.JComboBox type;

	// End of variables declaration//GEN-END:variables
	public boolean shouldPowerFish() {
		return powerfish.isSelected();
	}

	public int getLocationIndex() {
		return location.getSelectedIndex();
	}

	public List<Object> getSelectedFish() {
		return possibleFish.getSelectedValuesList();
	}

	public String getAction() {
		return (String) type.getSelectedItem();
	}

	public String getLocationItem() {
		return (String) location.getSelectedItem();
	}

}
