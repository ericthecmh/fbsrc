package library;

import java.util.Arrays;

import org.osbot.rs07.api.filter.Filter;
import org.osbot.rs07.api.model.Item;

public class GearMatch implements Filter<Item> {
	int[] fishGear = { 11323, 301, 10129, 995, 303, 305, 307, 309, 311, 312, 313, 314,
			1585, 6209, 17794 };

	@Override
	public boolean match(Item arg0) {
		for (int i : fishGear) {
			if (arg0.getId() == i)
				return true;
		}
		return false;
	}

}
