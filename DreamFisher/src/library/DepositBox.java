package library;

import java.awt.Point;
import java.awt.Rectangle;

import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.api.ui.RS2InterfaceChild;
import org.osbot.rs07.input.mouse.RectangleDestination;
import org.osbot.rs07.script.Script;

/*
 *Original OSBot 1 class created by TheScrub
 *Ported to OSBot 2 and modified by DreamScripts
 */

public class DepositBox {

	// DO NOT TOUCH THIS
	public static final String ip2 = "241";

	private final int START_WIDTH = 135;
	private final int START_HEIGHT = 82;
	// width+ height includes spacing lengths
	private final int WIDTH = 41;
	private final int HEIGHT = 44;
	private Script script;
	private final int PARENTID = 11;
	private final int CHILDID = 61;
	private final int INFOID = 60;

	public DepositBox(Script script) {
		this.script = script;

	}

	public Item[] getItems() {
		if (boxIsOpen()) {
			if (script.interfaces.get(PARENTID) != null) {
				RS2InterfaceChild rc = script.interfaces.get(PARENTID)
						.getChild(CHILDID);
				if (rc != null && rc.isVisible()) {
					return script.interfaces.get(PARENTID).getItems(CHILDID);
				}
			}
		}
		return null;
	}

	public int getItemSlot(Item item) {
		Item[] array = getItems();
		if (array != null && array.length > 0) {
			for (int i = 0; i < array.length; i++) {
				if (array[i] != null) {
					if (array[i].equals(item)) {
						return i;
					}
				}
			}
		}

		return -1;
	}

	public void closeBox() {
		if (script.interfaces.get(11).getChild(63) != null) {
			script.interfaces.interactWithChild(11, 63, "Close");
		}
	}

	public int getItemSlot(String item) {
		Item[] array = getItems();
		if (array != null && array.length > 0) {
			for (int i = 0; i < array.length; i++) {
				if (array[i] != null) {
					if (array[i].getName() != null) {
						if (array[i].getName().equalsIgnoreCase(item)) {
							return i;
						}
					}
				}
			}
		}

		return -1;
	}

	public int getItemSlot(int item) {
		Item[] array = getItems();
		if (array != null && array.length > 0) {
			for (int i = 0; i < array.length; i++) {
				if (array[i] != null) {
					if (array[i].getName() != null) {
						if (array[i].getId() == item) {
							return i;
						}
					}
				}
			}
		}

		return -1;
	}

	public boolean contains(String item) {
		if (boxIsOpen()) {
			Item[] array = getItems();
			if (array.length > 0) {
				for (Item i : array) {
					if (i != null) {
						if (i.getName() != null) {
							if (i.getName().equalsIgnoreCase(item)) {
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}

	public boolean contains(int item) {
		if (boxIsOpen()) {
			Item[] array = getItems();
			if (array.length > 0) {
				for (Item i : array) {
					if (i != null) {
						if (i.getId() == item) {
							return true;
						}

					}
				}
			}
		}
		return false;
	}

	public boolean contains(Item item) {
		if (boxIsOpen()) {
			Item[] array = getItems();
			if (array.length > 0) {
				for (Item i : array) {
					if (i != null) {
						if (i.equals(item)) {
							return true;

						}
					}
				}
			}
		}
		return false;
	}

	private Point getPoint(int slot) {
		int column = slot / 7;
		int row = slot % 7;
		int x = (int) (START_WIDTH + (row * WIDTH));
		int y = START_HEIGHT + (column * HEIGHT);
		return new Point(x, y);
	}

	public Rectangle getRectangle(int slot) {
		Point p = getPoint(slot);
		return new Rectangle(p.x, p.y, 18, 18);
	}

	public boolean boxIsOpen() {
		if (script.interfaces.get(PARENTID) != null) {
			RS2InterfaceChild rc = script.interfaces.get(PARENTID).getChild(
					CHILDID);
			if (rc != null && rc.isVisible()) {
				return true;
			}
		}
		return false;
	}

	public String getBoxInfo() {
		if (boxIsOpen()) {
			if (script.interfaces.get(PARENTID).getChild(INFOID).getMessage() != null) {
				return script.interfaces.get(PARENTID).getChild(INFOID)
						.getMessage();
			}
		}
		return null;
	}

	public boolean interact(Rectangle rec, String action)
			throws InterruptedException {
		if (rec != null && action != null && !action.isEmpty()) {
			return Library.doMenu(action, new RectangleDestination(script.bot,
					rec), script);
		}
		return false;
	}

}