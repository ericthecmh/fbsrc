package library;

import org.osbot.rs07.script.Script;

public interface Conditional {
	public abstract boolean isSatisfied(Script script);
}
