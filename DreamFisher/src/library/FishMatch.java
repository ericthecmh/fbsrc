package library;

import java.util.Arrays;
import java.util.HashSet;

import org.osbot.rs07.api.filter.Filter;
import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.script.Script;

public class FishMatch implements Filter<Item> {
	int[] fishGear = { 11323, 303, 305, 307, 309, 313, 314, 1585, 6209, 17794 };
	HashSet<String> fishToKeep;
	Script script;

	public FishMatch(HashSet<String> fishToKeep, Script script) {
		this.fishToKeep = fishToKeep;
		this.script = script;
	}

	@Override
	public boolean match(Item arg0) {

		script.log(arg0.getName()
				+ "  "
				+ arg0.getId()
				+ "  "
				+ (this.fishToKeep.contains(arg0.getName()) || hasFishGear(arg0)));
		return this.fishToKeep.contains(arg0.getName()) || hasFishGear(arg0);
	}

	private boolean hasFishGear(Item arg0) {
		for (int i : fishGear) {
			if (arg0.getId() == i)
				return true;
		}
		return false;
	}

}
