package library;

import java.util.Arrays;

import org.osbot.rs07.api.filter.Filter;
import org.osbot.rs07.api.model.Item;

public class NotFishingGearMusaPoint implements Filter<Item> {
	int[] fishGear = { 301, 303, 995, 305, 307, 309, 311, 312, 313, 314, 1585,
			6209, 17794, 10129 };

	@Override
	public boolean match(Item arg0) {
		for (int i : fishGear) {
			if (arg0.getId() == i)
				return false;
		}
		return true;
	}

}
