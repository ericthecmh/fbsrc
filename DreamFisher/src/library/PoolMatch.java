package library;


import java.util.Arrays;
import java.util.HashSet;

import org.osbot.rs07.api.filter.Filter;
import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.script.Script;

public class PoolMatch implements Filter<NPC> {
	String action;
	Script script;
	HashSet<String> fishToKeep;

	public PoolMatch(String action, Script script, HashSet<String> fishToKeep) {
		this.action = action;
		this.script = script;
		this.fishToKeep = fishToKeep;
	}

	@Override
	public boolean match(NPC obj) {
		if (obj != null && obj.getDefinition() != null) {
			if (fishToKeep != null && fishToKeep.contains("Raw shark") && fishToKeep.size() == 1) {
				return Arrays.asList(obj.getDefinition().getActions())
						.contains(action)
						&& Arrays.asList(obj.getDefinition().getActions())
								.contains("Net")
						&& obj.getModel().getVerticesCount() < 30;
			} else
				return Arrays.asList(obj.getDefinition().getActions())
						.contains(action)
						&& obj.getModel().getVerticesCount() < 30;
		} else
			return false;
	}

}
