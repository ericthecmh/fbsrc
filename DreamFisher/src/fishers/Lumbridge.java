package fishers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import library.Conditional;
import library.Library;
import library.Walking;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.Entity;
import org.osbot.rs07.api.model.GroundItem;
import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.input.mouse.MiniMapTileDestination;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

import fishers.Fisher.State;

public class Lumbridge extends Fisher {
	static Area bankArea = new Area(new Position[] {
			new Position(3207, 3218, 2), new Position(3210, 3221, 2) });
	private final static Area poolArea = new Area(3239, 3235, 3246, 3259);
	private Position[] swampToStair = { new Position(3240, 3243, 0),
			new Position(3242, 3241, 0), new Position(3244, 3239, 0),
			new Position(3245, 3236, 0), new Position(3247, 3234, 0),
			new Position(3250, 3232, 0), new Position(3252, 3229, 0),
			new Position(3253, 3227, 0), new Position(3250, 3226, 0),
			new Position(3247, 3226, 0), new Position(3244, 3226, 0),
			new Position(3241, 3226, 0), new Position(3238, 3226, 0),
			new Position(3235, 3224, 0), new Position(3234, 3222, 0),
			new Position(3233, 3219, 0), new Position(3230, 3218, 0),
			new Position(3229, 3218, 0), new Position(3226, 3218, 0),
			new Position(3223, 3218, 0), new Position(3220, 3218, 0),
			new Position(3217, 3218, 0), new Position(3215, 3216, 0),
			new Position(3215, 3213, 0), new Position(3215, 3211, 0),
			new Position(3214, 3210, 0), new Position(3211, 3210, 0),
			new Position(3208, 3210, 0), new Position(3206, 3209, 0) };
	Area lowStairArea = new Area(3203, 3206, 3216, 3211);
	Area midStairArea = new Area(3203, 3206, 3216, 3231);
	Area highStairArea = new Area(3203, 3206, 3216, 3231);

	public Lumbridge(Script s, String action, boolean powerFish,
			HashSet<String> fishToKeep) {
		super(s, bankArea, poolArea, null, fishToKeep, action, powerFish);
	}

	@Override
	public int onLoop() throws InterruptedException {
		NPC closePool = null;
		if (script.myPlayer().getInteracting() != null
				&& script.myPlayer().getInteracting().getHeight() > 50) {
			script.log("Whirlpool detected");
			closePool = script.npcs.closest(poolId);
			ArrayList<Position> positions = new ArrayList<Position>();
			for (int x = -8; x < 9; x++) {
				for (int y = -8; y < 9; y++) {
					if (x * x + y * y > 64) {
						Position p = new Position(script.myPosition().getX()
								+ x, script.myPosition().getY() + y, script
								.myPosition().getZ());
						if (script.map.canReach(p))
							positions.add(p);
					}
				}
			}
			MiniMapTileDestination td = new MiniMapTileDestination(script.bot,
					positions.get(MethodProvider.random(positions.size())));
			for (int i = 0; i < MethodProvider.random(2, 4); i++) {
				script.mouse.click(td);
			}
			script.log("Whirlpool avoided");
			return 2000;
		}
		List<GroundItem> groundItems = script.groundItems.getAll();
		for (GroundItem item : groundItems) {
			if (item == null)
				continue;
			if (Library.distanceTo(item.getPosition(), script) < 10
					&& item.getName().equals(fishGear2)
					&& !script.inventory.contains(item.getId())) {
				item.interact("Take");
				script.sleep(MethodProvider.random(500, 600));
				return 15;
			}
		}
		super.checkRunning();
		switch (state) {
		case FISH:
			super.ab();
			super.fish();
			break;
		case WALK_TO_BANK:
			walk(false);
			break;
		case BANK:
			super.bank();
			break;
		case WALK_TO_FISH:
			walk(true);
			break;
		case DROP:
			super.drop();
			break;
		}
		return 15;
	}

	private void walk(boolean reverse) throws InterruptedException {
		Walking w = new Walking(script);
		if (!reverse && !contains(lowStairArea, 0)
				&& !contains(midStairArea, 1) && !contains(highStairArea, 2))
			w.walkPathMM(swampToStair);
		else if (!reverse
				&& (contains(lowStairArea, 0) || contains(midStairArea, 1))) {
			Entity stairs = script.objects.closest("Staircase");
			if (stairs != null) {
				stairs.interact("Climb-up");
				script.sleep(1000);
			}
		} else if (!reverse && contains(highStairArea, 2)) {
			script.localWalker.walk(bankArea.getRandomPosition(0));
		} else if (reverse && !contains(midStairArea, 1)
				&& !contains(highStairArea, 2))
			w.walkPathMM(w.reversePath(swampToStair));
		else if (reverse
				&& (contains(highStairArea, 2) || contains(midStairArea, 1))) {
			Entity stairs = script.objects.closest("Staircase");
			if (stairs != null) {
				stairs.interact("Climb-down");
				script.sleep(1000);
			}
		}
		if (!reverse && contains(bankArea, 2))
			state = State.BANK;
		if (reverse && poolArea.contains(script.myPosition()))
			state = State.FISH;

	}

	boolean contains(Area area, int z) {
		return area.contains(script.myPosition().getX(), script.myPosition()
				.getY())
				&& script.myPosition().getZ() == z;
	}
}
