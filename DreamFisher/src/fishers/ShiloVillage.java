package fishers;

import java.util.HashSet;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class ShiloVillage extends Fisher {
	private final static Area bankArea = new Area(2842, 2950, 2861, 2958);
	private final static Area poolArea = new Area(2850, 2970, 2868, 2980);
	private final static Position[] pathToBank = { new Position(2858, 2972, 0),
			new Position(2856, 2970, 0), new Position(2853, 2970, 0),
			new Position(2851, 2968, 0), new Position(2850, 2966, 0),
			new Position(2850, 2963, 0), new Position(2852, 2960, 0),
			new Position(2852, 2957, 0), new Position(2852, 2954, 0) };

	public ShiloVillage(Script s, String action, boolean powerFish,
			HashSet<String> fishToKeep) {
		super(s, bankArea, poolArea, pathToBank, fishToKeep, action, powerFish);
	}

}
