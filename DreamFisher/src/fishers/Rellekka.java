package fishers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import library.GearMatch;
import library.Library;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.GroundItem;
import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.input.mouse.MiniMapTileDestination;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class Rellekka extends Fisher {
	private final static Area bankArea = new Area(2625, 3658, 2643, 3673);
	private final static Area poolArea = new Area(2629, 3678, 2656, 3716);
	private final static Position[] pathToBank = { new Position(2632, 3693, 0),
			new Position(2632, 3691, 0), new Position(2632, 3689, 0),
			new Position(2631, 3686, 0), new Position(2631, 3684, 0),
			new Position(2632, 3682, 0), new Position(2632, 3680, 0),
			new Position(2632, 3678, 0), new Position(2633, 3676, 0),
			new Position(2634, 3674, 0), new Position(2635, 3672, 0),
			new Position(2635, 3670, 0), new Position(2635, 3668, 0),
			new Position(2635, 3667, 0) };
	int[] fishGear = { 11323, 301, 995, 303, 305, 307, 309, 311, 312, 313, 314,
			1585, 6209, 17794, 10129 };

	public Rellekka(Script s, String action, boolean powerFish,
			HashSet<String> fishToKeep) {
		super(s, bankArea, poolArea, pathToBank, fishToKeep, action, powerFish);
	}

	@Override
	public int onLoop() throws InterruptedException {
		NPC closePool = null;
		if (script.myPlayer().getInteracting() != null
				&& script.myPlayer().getInteracting().getHeight() > 50) {
			script.log("Whirlpool detected");
			closePool = script.npcs.closest(poolId);
			ArrayList<Position> positions = new ArrayList<Position>();
			for (int x = -8; x < 9; x++) {
				for (int y = -8; y < 9; y++) {
					if (x * x + y * y > 64) {
						Position p = new Position(script.myPosition().getX()
								+ x, script.myPosition().getY() + y, script
								.myPosition().getZ());
						if (script.map.canReach(p))
							positions.add(p);
					}
				}
			}
			MiniMapTileDestination td = new MiniMapTileDestination(script.bot,
					positions.get(MethodProvider.random(positions.size())));
			for (int i = 0; i < MethodProvider.random(2, 4); i++) {
				script.mouse.click(td);
			}
			script.log("Whirlpool avoided");
			return 2000;
		}
		List<GroundItem> groundItems = script.groundItems.getAll();
		for (GroundItem item : groundItems) {
			if (item == null)
				continue;
			if (Library.distanceTo(item.getPosition(), script) < 10
					&& item.getName().equals(fishGear2)
					&& !script.inventory.contains(item.getId())) {
				item.interact("Take");
				script.sleep(MethodProvider.random(500, 600));
				return 15;
			}
		}
		super.checkRunning();
		switch (state) {
		case FISH:
			super.ab();
			super.fish();
			break;
		case WALK_TO_BANK:
			super.walk(this.pathToBank, false);
			break;
		case BANK:
			useBank();
			break;
		case WALK_TO_FISH:
			super.walk(this.pathToBank, true);
			break;
		case DROP:
			super.drop();
			break;
		}
		return 15;
	}

	private void useBank() {
		if (!script.dialogues.isPendingContinuation()
				&& script.inventory.contains(new GearMatch())
				&& !script.inventory.isEmpty()) {
			script.inventory.dropForFilter(new GearMatch());
		} else if (!script.dialogues.isPendingContinuation()
				&& !script.inventory.contains(new GearMatch())
				&& !script.inventory.isEmpty()
				&& script.interfaces.get(228).getChild(1) == null) {
			NPC peerTheSeer = script.npcs.closest("Peer the Seer");
			if (peerTheSeer != null)
				peerTheSeer.interact("Talk-to");
			try {
				script.sleep(script.random(500, 1000));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if ((script.dialogues.isPendingContinuation() || script.interfaces
				.get(228).getChild(1) != null)
				&& !script.inventory.contains(new GearMatch())
				&& !script.inventory.isEmpty()) {
			if (script.interfaces.get(228).getChild(1) != null) {
				script.interfaces.interactWithChild(228, 1, "Continue");
			}
			script.dialogues.clickContinue();
			try {
				script.sleep(script.random(500, 1000));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (Library.onlyContains(script, new GearMatch())
				&& !script.inventory.isEmpty()) {
			state = State.WALK_TO_FISH;
		}

	}

	// return true if the invy only has fish and gear we wnat
	private boolean onlyHas2() {
		for (Item i : script.inventory.getItems()) {
			if (i != null && i.getName().contains("Raw"))
				return false;
		}
		return true;
	}

}
