package fishers;

import java.util.HashSet;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class Catherby extends Fisher {
	private final static Area bankArea = new Area(2806, 3438, 2812, 3445);
	private final static Area poolArea = new Area(2829, 3419, 2866, 3438);
	private final static Position[] pathToBank = { new Position(2855, 3427, 0), new Position(2853, 3428, 0), new Position(2852, 3429, 0), new Position(2850, 3430, 0), new Position(2848, 3431, 0), new Position(2846, 3432, 0), new Position(2844, 3433, 0), new Position(2843, 3434, 0), new Position(2841, 3434, 0), new Position(2839, 3435, 0), new Position(2837, 3435, 0), new Position(2834, 3436, 0), new Position(2832, 3436, 0), new Position(2830, 3436, 0), new Position(2828, 3436, 0),
			new Position(2826, 3436, 0), new Position(2824, 3436, 0), new Position(2822, 3436, 0), new Position(2820, 3436, 0), new Position(2818, 3436, 0), new Position(2815, 3436, 0), new Position(2813, 3436, 0), new Position(2811, 3436, 0), new Position(2809, 3436, 0), new Position(2809, 3438, 0), new Position(2809, 3441, 0) };

	public Catherby(Script s, String action, boolean powerFish, HashSet<String> fishToKeep) {
		super(s, bankArea, poolArea, pathToBank, fishToKeep, action, powerFish);
	}

}
