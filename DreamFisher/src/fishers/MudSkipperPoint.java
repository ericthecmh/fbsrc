package fishers;

import java.util.HashSet;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class MudSkipperPoint extends Fisher {
	private final static Area bankArea = new Area(3044, 3232, 3053, 3238);
	private final static Area poolArea = new Area(2985, 3154, 2999, 3180);
	private final static Position[] pathToBank = { new Position(2992, 3169, 0),
			new Position(2991, 3173, 0), new Position(2990, 3178, 0),
			new Position(2991, 3182, 0), new Position(2993, 3186, 0),
			new Position(2994, 3189, 0), new Position(2996, 3192, 0),
			new Position(2998, 3195, 0), new Position(3000, 3198, 0),
			new Position(3002, 3200, 0), new Position(3004, 3203, 0),
			new Position(3006, 3206, 0), new Position(3007, 3210, 0),
			new Position(3010, 3213, 0), new Position(3013, 3216, 0),
			new Position(3017, 3217, 0), new Position(3020, 3218, 0),
			new Position(3026, 3218, 0), new Position(3028, 3221, 0),
			new Position(3028, 3224, 0), new Position(3028, 3228, 0),
			new Position(3028, 3231, 0), new Position(3028, 3234, 0),
			new Position(3030, 3235, 0), new Position(3032, 3235, 0),
			new Position(3035, 3235, 0), new Position(3039, 3235, 0),
			new Position(3042, 3235, 0), new Position(3046, 3235, 0),
			new Position(3047, 3235, 0) };

	public MudSkipperPoint(Script s, String action, boolean powerFish,
			HashSet<String> fishToKeep) {
		super(s, bankArea, poolArea, pathToBank, fishToKeep, action, powerFish);
	}

}
