package fishers;

import java.util.HashSet;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class FishingGuild extends Fisher {
	private final static Area bankArea = new Area(2581, 3415, 2594, 3423);
	private final static Area poolArea = new Area(2591, 3407, 2616, 3428);
	private final static Position[] pathToBank =  { new Position(2605, 3420, 0), new Position(2604, 3420, 0),
		 new Position(2603, 3420, 0), new Position(2602, 3420, 0), new Position(2600, 3420, 0),
		 new Position(2599, 3420, 0), new Position(2598, 3420, 0), new Position(2597, 3420, 0),
		 new Position(2596, 3420, 0), new Position(2595, 3420, 0), new Position(2594, 3419, 0),
		 new Position(2594, 3418, 0), new Position(2594, 3417, 0), new Position(2594, 3416, 0),
		 new Position(2593, 3416, 0), new Position(2592, 3415, 0), new Position(2591, 3415, 0),
		 new Position(2590, 3416, 0), new Position(2590, 3417, 0), new Position(2589, 3418, 0),
		 new Position(2588, 3418, 0), new Position(2588, 3419, 0), new Position(2587, 3419, 0),
		 new Position(2586, 3419, 0), new Position(2585, 3420, 0) };

	public FishingGuild(Script s, String action, boolean powerFish,
			HashSet<String> fishToKeep) {
		super(s, bankArea, poolArea, pathToBank, fishToKeep, action, powerFish);
	}

}