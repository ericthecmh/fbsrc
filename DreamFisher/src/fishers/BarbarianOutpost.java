package fishers;

import java.util.HashSet;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class BarbarianOutpost extends Fisher {
	private final static Area bankArea = new Area(2530, 3567, 2538, 3579);
	private final static Area poolArea = new Area(2496, 3545, 2519, 3578);
	private final static Position[] pathToBank = { new Position(2518, 3569, 0),
			new Position(2521, 3570, 0), new Position(2532, 3572, 0),
			new Position(2536, 3573, 0) };

	public BarbarianOutpost(Script s, String action, boolean powerFish,
			HashSet<String> fishToKeep) {
		super(s, bankArea, poolArea, pathToBank, fishToKeep, action, powerFish);
	}

}