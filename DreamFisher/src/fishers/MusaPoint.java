package fishers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import library.Library;
import library.NotFishingGear;
import library.NotFishingGearMusaPoint;
import library.Walking;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.Entity;
import org.osbot.rs07.api.model.GroundItem;
import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.input.mouse.MiniMapTileDestination;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class MusaPoint extends Fisher {
	static Area bankArea = new Area(3044, 3233, 3053, 3238);
	static Area poolArea = new Area(2917, 3171, 2931, 3182);
	private final Area poolDockArea = new Area(2924, 3175, 2925, 3180);
	private Position[] poolToBoat = { new Position(2924, 3182, 0),
			new Position(2924, 3181, 0), new Position(2924, 3180, 0),
			new Position(2924, 3179, 0), new Position(2924, 3178, 0),
			new Position(2924, 3177, 0), new Position(2924, 3176, 0),
			new Position(2924, 3174, 0), new Position(2924, 3173, 0),
			new Position(2924, 3172, 0), new Position(2923, 3171, 0),
			new Position(2923, 3170, 0), new Position(2922, 3169, 0),
			new Position(2922, 3168, 0), new Position(2921, 3167, 0),
			new Position(2920, 3166, 0), new Position(2920, 3165, 0),
			new Position(2919, 3164, 0), new Position(2919, 3163, 0),
			new Position(2918, 3162, 0), new Position(2918, 3161, 0),
			new Position(2918, 3160, 0), new Position(2917, 3159, 0),
			new Position(2917, 3158, 0), new Position(2916, 3157, 0),
			new Position(2916, 3156, 0), new Position(2916, 3155, 0),
			new Position(2915, 3154, 0), new Position(2915, 3153, 0),
			new Position(2916, 3152, 0), new Position(2917, 3152, 0),
			new Position(2918, 3151, 0), new Position(2919, 3151, 0),
			new Position(2920, 3151, 0), new Position(2921, 3151, 0),
			new Position(2922, 3150, 0), new Position(2923, 3150, 0),
			new Position(2925, 3150, 0), new Position(2926, 3150, 0),
			new Position(2927, 3150, 0), new Position(2928, 3150, 0),
			new Position(2929, 3149, 0), new Position(2930, 3149, 0),
			new Position(2931, 3149, 0), new Position(2932, 3149, 0),
			new Position(2933, 3149, 0), new Position(2934, 3148, 0),
			new Position(2935, 3148, 0), new Position(2936, 3148, 0),
			new Position(2937, 3148, 0), new Position(2938, 3147, 0),
			new Position(2939, 3147, 0), new Position(2940, 3147, 0),
			new Position(2941, 3147, 0), new Position(2943, 3146, 0),
			new Position(2944, 3146, 0), new Position(2945, 3146, 0),
			new Position(2946, 3146, 0), new Position(2947, 3146, 0),
			new Position(2948, 3146, 0), new Position(2949, 3146, 0),
			new Position(2951, 3146, 0), new Position(2953, 3146, 0),
			new Position(2954, 3147, 0) };
	private Position[] boatToBank = { new Position(3028, 3220, 0),
			new Position(3028, 3223, 0), new Position(3028, 3226, 0),
			new Position(3028, 3229, 0), new Position(3028, 3232, 0),
			new Position(3028, 3235, 0), new Position(3032, 3235, 0),
			new Position(3036, 3235, 0), new Position(3041, 3235, 0),
			new Position(3045, 3235, 0), new Position(3049, 3235, 0) };
	private Area fishArea = new Area(2900, 3134, 2955, 3188);
	private Area musaBoatArea = new Area(2945, 3140, 2965, 3163);
	private Area musaBoat = new Area(2966, 3137, 2949, 3143);
	private Area portSBoat = new Area(3032, 3211, 3037, 3225);
	private Area portSArea = new Area(3018, 3202, 3062, 3247);

	public MusaPoint(Script s, String action, boolean powerFish,
			HashSet<String> fishToKeep) {
		super(s, bankArea, poolArea, null, fishToKeep, action, powerFish);
	}

	@Override
	public int onLoop() throws InterruptedException {
		NPC closePool = null;
		if (script.myPlayer().getInteracting() != null
				&& script.myPlayer().getInteracting().getHeight() > 50) {
			script.log("Whirlpool detected");
			closePool = script.npcs.closest(poolId);
			ArrayList<Position> positions = new ArrayList<Position>();
			for (int x = -8; x < 9; x++) {
				for (int y = -8; y < 9; y++) {
					if (x * x + y * y > 64) {
						Position p = new Position(script.myPosition().getX()
								+ x, script.myPosition().getY() + y, script
								.myPosition().getZ());
						if (script.map.canReach(p))
							positions.add(p);
					}
				}
			}
			MiniMapTileDestination td = new MiniMapTileDestination(script.bot,
					positions.get(MethodProvider.random(positions.size())));
			for (int i = 0; i < MethodProvider.random(2, 4); i++) {
				script.mouse.click(td);
			}
			script.log("Whirlpool avoided");
			return 2000;
		}
		List<GroundItem> groundItems = script.groundItems.getAll();
		for (GroundItem item : groundItems) {
			if (item == null)
				continue;
			if (Library.distanceTo(item.getPosition(), script) < 10
					&& item.getName().equals(fishGear2)
					&& !script.inventory.contains(item.getId())) {
				item.interact("Take");
				script.sleep(MethodProvider.random(500, 600));
				return 15;
			}
		}
		super.checkRunning();
		switch (state) {
		case FISH:
			super.ab();
			super.fish();
			break;
		case WALK_TO_BANK:
			if (poolDockArea.contains(script.myPosition())) {
				Position furthest = null;
				for (Position p : poolToBoat) {
					if (Library.distanceTo(p, script) > 14) {
						break;
					}
					furthest = p;
				}
				script.mouse.click(new MiniMapTileDestination(script.bot,
						furthest));
				script.sleep(MethodProvider.random(400, 800));
			} else {
				walk(false);
			}
			break;
		case BANK:
			bank();
			break;
		case WALK_TO_FISH:
			walk(true);
			break;
		case DROP:
			super.drop();
			break;
		}
		return 15;
	}

	// handles banking
	protected void bank() {
		if (script.depositBox.isEmpty() && script.inventory.isEmpty()
				|| onlyHas2()) {
			state = State.WALK_TO_FISH;
			return;
		} else {
			if (script.depositBox.isOpen()) {
				script.depositBox.depositAll(new NotFishingGearMusaPoint());
				try {
					script.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				script.depositBox.open();
			}
			if (script.depositBox.isEmpty() && script.inventory.isEmpty()
					|| onlyHas2()) {
				state = State.WALK_TO_FISH;
				return;
			}
		}
	}

	// executes banking via deposit box
	private void depositBoxBank() throws InterruptedException {
		do {
			script.depositBox.depositAll(new NotFishingGear());
			try {
				script.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (onlyHas2()) {
				break;
			}
		} while (!script.depositBox.isEmpty());
	}

	// return true if the invy only has fish and gear we wnat
	private boolean onlyHas2() {
		for (Item i : script.depositBox.getItems()) {
			if (i != null && i.getName().contains("Raw"))
				return false;
		}
		for (Item i : script.inventory.getItems()) {
			if (i != null && i.getName().contains("Raw"))
				return false;
		}
		return true;
	}

	private void walk(boolean reverse) throws InterruptedException {
		Walking w = new Walking(script);
		if (!reverse && fishArea.contains(script.myPosition())
				&& !musaBoatArea.contains(script.myPosition())) {
			w.walkPathMM(poolToBoat);
		} else if (!reverse && !poolArea.contains(script.myPosition())
				&& musaBoatArea.contains(script.myPosition())) {
			if (script.interfaces.get(241).getChild(3) != null) {
				script.interfaces.interactWithChild(241, 3, "Continue");
				Script.sleep(500);
			} else if (script.interfaces.get(228).getChild(1) != null) {
				script.interfaces.interactWithChild(228, 1, "Continue");
				script.sleep(500);
			} else if (script.interfaces.get(64).getChild(3) != null) {
				script.interfaces.interactWithChild(64, 3, "Continue");
				Script.sleep(500);
			} else if (script.interfaces.get(230).getChild(2) != null) {
				script.interfaces.interactWithChild(230, 2, "Continue");
				Script.sleep(500);
			} else if (script.interfaces.get(242).getChild(4) != null) {
				script.interfaces.interactWithChild(242, 4, "Continue");
				Script.sleep(500);
			} else {
				NPC customsOfficer = script.npcs.closest("Customs officer");
				if (customsOfficer != null)
					customsOfficer.interact("Pay-Fare");
				script.sleep(MethodProvider.random(4000, 6000));
			}
		} else if (!reverse && portSBoat.contains(script.myPosition())) {
			Entity gangplank = script.objects.closest("Gangplank");
			if (gangplank != null)
				gangplank.interact("Cross");
			script.sleep(1000);
		} else if (!reverse && portSArea.contains(script.myPosition())
				&& !portSBoat.contains(script.myPosition()))
			w.walkPathMM(boatToBank);
		if (!reverse && bankArea.contains(script.myPosition()))
			state = State.BANK;
		else {
		}

		if (reverse && portSArea.contains(script.myPosition())
				&& !portSBoat.contains(script.myPosition())) {
			if (script.interfaces.get(241).getChild(3) != null) {
				script.interfaces.interactWithChild(241, 3, "Continue");
				Script.sleep(500);
			} else if (script.interfaces.get(228).getChild(1) != null) {
				script.interfaces.interactWithChild(228, 1, "Continue");
				script.sleep(500);
			} else if (script.interfaces.get(64).getChild(3) != null) {
				script.interfaces.interactWithChild(64, 3, "Continue");
				Script.sleep(500);
			} else if (script.interfaces.get(230).getChild(2) != null) {
				script.interfaces.interactWithChild(230, 2, "Continue");
				Script.sleep(500);
			} else if (script.interfaces.get(242).getChild(4) != null) {
				script.interfaces.interactWithChild(242, 4, "Continue");
				Script.sleep(500);
			} else {
				w.walkPathMM(w.reversePath(boatToBank));
				NPC customsOfficer = script.npcs.closest("Seaman Lorris");
				if (customsOfficer != null)
					customsOfficer.interact("Pay-Fare");
				script.sleep(MethodProvider.random(4000, 6000));
			}
		} else if (reverse && musaBoat.contains(script.myPosition())) {
			Entity gangplank = script.objects.closest("Gangplank");
			if (gangplank != null)
				gangplank.interact("Cross");
			script.sleep(1000);
		} else if (reverse
				&& (fishArea.contains(script.myPosition()) || musaBoatArea
						.contains(script.myPosition())))
			w.walkPathMM(w.reversePath(poolToBoat));
		if (reverse && poolArea.contains(script.myPosition()))
			state = State.FISH;

	}
}
