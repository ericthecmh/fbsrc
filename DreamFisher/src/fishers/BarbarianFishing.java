package fishers;

import java.util.HashSet;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class BarbarianFishing extends Fisher {
	private final static Area bankArea = null;
	
	private final static Area poolArea = new Area(2498, 3492, 2529, 3525);
	private final static Position[] pathToBank = null;

	public BarbarianFishing(Script s, String action, boolean powerFish,
			HashSet<String> fishToKeep) {
		super(s, bankArea, poolArea, pathToBank, fishToKeep, action, powerFish);
	}

}
