package fishers;

import java.util.HashSet;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class Piscatoris extends Fisher {
	private final static Area bankArea = new Area(2325, 3682, 2334, 3694);
	private final static Area poolArea = new Area(2306, 3694, 2316, 3705);
	private final static Position[] pathToBank = { new Position(2310, 3700, 0),
			new Position(2313, 3700, 0), new Position(2316, 3699, 0),
			new Position(2319, 3697, 0), new Position(2321, 3695, 0),
			new Position(2323, 3694, 0), new Position(2326, 3692, 0),
			new Position(2328, 3690, 0), new Position(2331, 3688, 0) };

	public Piscatoris(Script s, String action, boolean powerFish,
			HashSet<String> fishToKeep) {
		super(s, bankArea, poolArea, pathToBank, fishToKeep, action, powerFish);
	}

}
