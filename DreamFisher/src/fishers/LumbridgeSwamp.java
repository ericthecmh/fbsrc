package fishers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import library.Conditional;
import library.Library;
import library.Walking;

import org.osbot.rs07.api.filter.Filter;
import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.Entity;
import org.osbot.rs07.api.model.GroundItem;
import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.input.mouse.MiniMapTileDestination;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

import fishers.Fisher.State;

public class LumbridgeSwamp extends Fisher {

	static Area bankArea = new Area(new Position[] {
			new Position(3207, 3218, 2), new Position(3210, 3221, 2) });
	private final static Area poolArea = new Area(3232, 3140, 3250, 3161);
	private Position[] swampToStair = { new Position(3243, 3150, 0),
			new Position(3241, 3153, 0), new Position(3240, 3155, 0),
			new Position(3239, 3158, 0), new Position(3239, 3161, 0),
			new Position(3239, 3164, 0), new Position(3239, 3167, 0),
			new Position(3239, 3171, 0), new Position(3239, 3174, 0),
			new Position(3239, 3175, 0), new Position(3240, 3178, 0),
			new Position(3241, 3181, 0), new Position(3242, 3184, 0),
			new Position(3244, 3186, 0), new Position(3244, 3189, 0),
			new Position(3243, 3192, 0), new Position(3243, 3195, 0),
			new Position(3241, 3198, 0), new Position(3239, 3200, 0),
			new Position(3236, 3202, 0), new Position(3235, 3205, 0),
			new Position(3234, 3208, 0), new Position(3234, 3211, 0),
			new Position(3233, 3214, 0), new Position(3232, 3217, 0),
			new Position(3229, 3218, 0), new Position(3226, 3218, 0),
			new Position(3223, 3218, 0), new Position(3221, 3218, 0),
			new Position(3218, 3218, 0), new Position(3215, 3218, 0),
			new Position(3215, 3216, 0), new Position(3215, 3214, 0),
			new Position(3215, 3213, 0), new Position(3215, 3211, 0),
			new Position(3214, 3210, 0), new Position(3212, 3210, 0),
			new Position(3209, 3210, 0), new Position(3207, 3210, 0),
			new Position(3206, 3209, 0) };
	Area lowStairArea = new Area(3203, 3206, 3216, 3211);
	Area midStairArea = new Area(3203, 3206, 3216, 3231);
	Area highStairArea = new Area(3203, 3206, 3216, 3231);

	public LumbridgeSwamp(Script s, String action, boolean powerFish,
			HashSet<String> fishToKeep) {
		super(s, bankArea, poolArea, null, fishToKeep, action, powerFish);
	}

	@Override
	public int onLoop() throws InterruptedException {
		NPC closePool = null;
		if (script.myPlayer().getInteracting() != null
				&& script.myPlayer().getInteracting().getHeight() > 50) {
			script.log("Whirlpool detected");
			closePool = getClosestPool(poolId);
			ArrayList<Position> positions = new ArrayList<Position>();
			for (int x = -8; x < 9; x++) {
				for (int y = -8; y < 9; y++) {
					if (x * x + y * y > 64) {
						Position p = new Position(script.myPosition().getX()
								+ x, script.myPosition().getY() + y, script
								.myPosition().getZ());
						if (script.map.canReach(p))
							positions.add(p);
					}
				}
			}
			MiniMapTileDestination td = new MiniMapTileDestination(script.bot,
					positions.get(MethodProvider.random(positions.size())));
			for (int i = 0; i < MethodProvider.random(2, 4); i++) {
				script.mouse.click(td);
			}
			script.log("Whirlpool avoided");
			return 2000;
		}
		List<GroundItem> groundItems = script.groundItems.getAll();
		for (GroundItem item : groundItems) {
			if (item == null)
				continue;
			if (Library.distanceTo(item.getPosition(), script) < 10
					&& item.getName().equals(fishGear2)
					&& !script.inventory.contains(item.getId())) {
				item.interact("Take");
				script.sleep(MethodProvider.random(500, 600));
				return 15;
			}
		}
		super.checkRunning();
		switch (state) {
		case FISH:
			super.ab();
			fish();
			break;
		case WALK_TO_BANK:
			walk(false);
			break;
		case BANK:
			super.bank();
			break;
		case WALK_TO_FISH:
			walk(true);
			break;
		case DROP:
			super.drop();
			break;
		}
		return 15;
	}
	
	private NPC getClosestPool(final int poolId) {
		return script.npcs.closest(new Filter<NPC>(){

			@Override
			public boolean match(NPC arg0) {
				return arg0.getId() == poolId && !(arg0.getX() == 3246 && arg0.getY() == 3157);
			}
			
		});
	}

	protected void fish() {
		if (poolId == 0)
			poolId = super.getPoolId();
		if (poolId != 0) {
			if (script.interfaces.get(164).getChild(2) != null) {
				script.interfaces.interactWithChild(164, 2, "Continue");
				try {
					script.sleep(script.random(300, 500));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (script.interfaces.get(131).getChild(3) != null) {
				script.interfaces.interactWithChild(131, 3, "Continue");
				try {
					script.sleep(script.random(300, 500));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (script.inventory.isFull()) {
				state = State.DROP;
			} else {
				Entity closePool = null;
				if (script.myPlayer().getInteracting() != null && script.myPlayer().getInteracting().getId() != poolId) {
					script.log("Whirlpool detected");
					closePool = getClosestPool(poolId);
					if (closePool != null && closePool.interact(action)) {
						Library.waitFor(new Conditional() {
							@Override
							public boolean isSatisfied(Script script) {
								return !script.myPlayer().isAnimating();
							}
						}, 3000, 100, script);
						script.log("Whirlpool avoided!");
					} else {
						ArrayList<Position> positions = new ArrayList<Position>();
						for (int x = -8; x < 9; x++) {
							for (int y = -8; y < 9; y++) {
								if (x * x + y * y > 64) {
									Position p = new Position(script.myPosition().getX() + x, script.myPosition().getY() + y, script.myPosition().getZ());
									if (script.map.canReach(p))
										positions.add(p);
								}
							}
						}
						MiniMapTileDestination td = new MiniMapTileDestination(script.bot, positions.get(MethodProvider.random(positions.size())));
						for (int i = 0; i < MethodProvider.random(2, 4); i++) {
							script.mouse.click(td);
						}
						script.log("Whirlpool avoided");
						try {
							script.sleep(2000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				if (((!script.myPlayer().isAnimating()) || (script.myPlayer().getInteracting() == null && !script.myPlayer().isMoving())) && !script.inventory.isFull()) {
					closePool = getClosestPool(poolId);
					if (closePool != null && closePool.interact(action))
						Library.waitFor(new Conditional() {

							@Override
							public boolean isSatisfied(Script script) {
								return script.myPlayer().isAnimating();
							}
						}, 3000, 100, script);
				}
			}
		}
	}

	private void walk(boolean reverse) throws InterruptedException {
		Walking w = new Walking(script);
		if (!reverse && !contains(lowStairArea, 0)
				&& !contains(midStairArea, 1) && !contains(highStairArea, 2))
			w.walkPathMM(swampToStair);
		else if (!reverse
				&& (contains(lowStairArea, 0) || contains(midStairArea, 1))) {
			Entity stairs = script.objects.closest("Staircase");
			if (stairs != null) {
				stairs.interact("Climb-up");
				script.sleep(1000);
			}
		} else if (!reverse && contains(highStairArea, 2)) {
			script.localWalker.walk(bankArea.getRandomPosition(0));
		} else if (reverse && !contains(midStairArea, 1)
				&& !contains(highStairArea, 2))
			w.walkPathMM(w.reversePath(swampToStair));
		else if (reverse
				&& (contains(highStairArea, 2) || contains(midStairArea, 1))) {
			Entity stairs = script.objects.closest("Staircase");
			if (stairs != null) {
				stairs.interact("Climb-down");
				script.sleep(1000);
			}
		}
		if (!reverse && contains(bankArea, 2))
			state = State.BANK;
		if (reverse && poolArea.contains(script.myPosition()))
			state = State.FISH;
	}

	boolean contains(Area area, int z) {
		return area.contains(script.myPosition().getX(), script.myPosition()
				.getY())
				&& script.myPosition().getZ() == z;
	}
}
