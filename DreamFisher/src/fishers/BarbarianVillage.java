package fishers;

import java.util.HashSet;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class BarbarianVillage extends Fisher {
	private final static Area bankArea = new Area(3091, 3488, 3098, 3499);
	private final static Area poolArea = new Area(3097, 3423, 3111, 3435);
	private final static Position[] pathToBank = { new Position(3108, 3433, 0),
			new Position(3100, 3435, 0), new Position(3097, 3442, 0),
			new Position(3094, 3454, 0), new Position(3098, 3465, 0),
			new Position(3098, 3473, 0), new Position(3098, 3479, 0),
			new Position(3094, 3486, 0), new Position(3094, 3491, 0) };

	public BarbarianVillage(Script s, String action, boolean powerFish,
			HashSet<String> fishToKeep) {
		super(s, bankArea, poolArea, pathToBank, fishToKeep, action, powerFish);
	}

}