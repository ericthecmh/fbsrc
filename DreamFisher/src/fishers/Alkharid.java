package fishers;

import java.util.HashSet;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class Alkharid extends Fisher {
	private final static Area bankArea = new Area(3269, 3161, 3272, 3173);
	private final static Area poolArea = new Area(3263, 3137, 3282, 3153);
	private final static Position[] pathToBank = { new Position(3269, 3166, 0),
			new Position(3274, 3156, 0), new Position(3272, 3147, 0) };

	public Alkharid(Script s, String action, boolean powerFish,
			HashSet<String> fishToKeep) {
		super(s, bankArea, poolArea, pathToBank, fishToKeep, action, powerFish);
	}

}