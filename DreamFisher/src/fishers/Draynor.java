package fishers;

import java.util.ArrayList;
import java.util.HashSet;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class Draynor extends Fisher {

	private final static Area bankArea = new Area(3092, 3240, 3096, 3246);
	private final static Area poolArea = new Area(3085, 3221, 3093, 3235);
	private final static Position[] pathToBank = { new Position(3087, 3228, 0),
			new Position(3087, 3237, 0), new Position(3092, 3245, 0) };

	public Draynor(Script s, String action, boolean powerFish,
			HashSet<String> fishToKeep) {
		super(s, bankArea, poolArea, pathToBank, fishToKeep, action, powerFish);
	}

}
