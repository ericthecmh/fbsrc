package fishers;

import java.util.HashSet;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class BaxtorianFalls extends Fisher {

	private final static Area bankArea = new Area(2529, 3565, 2539, 3579);
	private final static Area poolArea = new Area(2496, 3507, 2526, 3523);
	private final static Position[] pathToBank = { new Position(2510, 3519, 0),
			new Position(2510, 3522, 0), new Position(2511, 3525, 0),
			new Position(2511, 3528, 0), new Position(2512, 3531, 0),
			new Position(2514, 3534, 0), new Position(2515, 3536, 0),
			new Position(2517, 3539, 0), new Position(2518, 3542, 0),
			new Position(2519, 3545, 0), new Position(2520, 3548, 0),
			new Position(2520, 3551, 0), new Position(2520, 3554, 0),
			new Position(2520, 3557, 0), new Position(2520, 3560, 0),
			new Position(2520, 3563, 0), new Position(2519, 3566, 0),
			new Position(2520, 3569, 0), new Position(2522, 3571, 0),
			new Position(2525, 3571, 0), new Position(2529, 3571, 0),
			new Position(2532, 3571, 0), new Position(2535, 3572, 0),
			new Position(2536, 3572, 0) };

	public BaxtorianFalls(Script s, String action, boolean powerFish,
			HashSet<String> fishToKeep) {
		super(s, bankArea, poolArea, pathToBank, fishToKeep, action, powerFish);
	}

}
