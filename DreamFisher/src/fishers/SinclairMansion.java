package fishers;

import java.util.HashSet;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class SinclairMansion extends Fisher {

	private final static Area bankArea = new Area(2720, 3489, 2730, 3495);
	private final static Area poolArea = new Area(2712, 3519, 2734, 3536);
	private final static Position[] pathToBank = { new Position(2723, 3528, 0),
			new Position(2727, 3528, 0), new Position(2730, 3526, 0),
			new Position(2733, 3523, 0), new Position(2735, 3519, 0),
			new Position(2735, 3515, 0), new Position(2735, 3511, 0),
			new Position(2735, 3509, 0), new Position(2734, 3508, 0),
			new Position(2733, 3505, 0), new Position(2733, 3502, 0),
			new Position(2733, 3498, 0), new Position(2733, 3495, 0),
			new Position(2732, 3492, 0), new Position(2732, 3490, 0),
			new Position(2730, 3488, 0), new Position(2729, 3486, 0),
			new Position(2726, 3486, 0), new Position(2726, 3489, 0),
			new Position(2726, 3492, 0) };

	public SinclairMansion(Script s, String action, boolean powerFish,
			HashSet<String> fishToKeep) {
		super(s, bankArea, poolArea, pathToBank, fishToKeep, action, powerFish);
	}

}
