package fishers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import library.AntibanAction;
import library.Conditional;
import library.DepositBox;
import library.GearMatch;
import library.Library;
import library.NotFishingGear;
import library.PoolMatch;
import library.Walking;
import main.DreamFisher;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.Entity;
import org.osbot.rs07.api.model.GroundItem;
import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.input.mouse.MiniMapTileDestination;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;

public class Fisher extends Script {
	// CONSTANTS
	public enum State {
		BANK, WALK_TO_FISH, WALK_TO_BANK, FISH, DROP;
	}

	// current state of the script
	protected State state;

	// constructor info
	private Area bankArea;
	private final Area poolArea;
	private Position[] pathToBank;
	private HashSet<String> fishToKeep;
	private boolean powerFish;
	protected String action;
	int poolId = 0;
	int[] fishGear = { 11323, 301, 995, 303, 305, 307, 309, 311, 312, 313, 314,
			1585, 6209, 17794, 10129 };
	public String fishGear2;
	private final static int[] invSlotsBest = { 0, 4, 8, 12, 16, 20, 24, 1, 5,
			9, 13, 17, 21, 25, 2, 6, 10, 14, 18, 22, 26, 3, 7, 11, 15, 19, 23,
			27 };

	// reference to script
	protected final Script script;
	AntibanAction antiBan;

	Walking w;
	DepositBox b;

	// construct an instance of Chopper
	public Fisher(Script script, Area bankArea, Area poolArea,
			Position[] pathToBank, HashSet<String> fishToKeep, String action,
			boolean powerFish) {
		this.script = script;
		this.bankArea = bankArea;
		this.poolArea = poolArea;
		this.pathToBank = pathToBank;
		this.fishToKeep = fishToKeep;
		this.action = action;
		this.powerFish = powerFish;
	}

	@Override
	public void onStart() throws InterruptedException {
		w = new Walking(script);
		b = new DepositBox(script);
		antiBan = new AntibanAction(script);
		if (poolArea.contains(script.myPosition()) || bankArea == null)
			state = State.FISH;
		else if (bankArea.contains(script.myPosition()))
			state = State.BANK;
		else if (script.inventory.getEmptySlots() < 14)
			state = State.WALK_TO_BANK;
		else
			state = State.WALK_TO_FISH;
		switch (action) {
		case "Net":
			fishGear2 = "Small fishing net";
			break;
		case "Bait":
			fishGear2 = "Fishing rod";
			break;
		case "Lure":
		case "Use-rod":
			fishGear2 = "Fly fishing rod";
			break;
		case "Big net":
			fishGear2 = "Big fishing net";
			break;
		case "Cage":
			fishGear2 = "Lobster pot";
			break;
		case "Harpoon":
			fishGear2 = "Harpoon";
			break;
		}
		if(script.inventory.contains("Barbarian rod")){
			fishGear2 = "Barbarian rod";
      }
		if(script.equipment.isWieldingWeapon("Barb-tail harpoon")){
			fishGear2 = "Barb-tail harpoon";
		}
		script.log("Setting pickup equipment to " + fishGear2);
		script.log("If this is not correct, please post on OSBot with your settings");
	}

	@Override
	public int onLoop() throws InterruptedException {
		NPC closePool = null;
		if (script.myPlayer().getInteracting() != null
				&& script.myPlayer().getInteracting().getId() != poolId) {
			script.log("Whirlpool detected");
			closePool = script.npcs.closest(poolId);
			if (closePool != null && closePool.interact(action)) {
				Library.waitFor(new Conditional() {
					@Override
					public boolean isSatisfied(Script script) {
						return !script.myPlayer().isAnimating();
					}
				}, 3000, 100, script);
				script.log("Whirlpool avoided!");
			} else {
				ArrayList<Position> positions = new ArrayList<Position>();
				if (poolArea.getMinX() == 3097 && poolArea.getMinY() == 3423) {
					for (int x = -4; x < 0; x++) {
						for (int y = -4; y < 5; y++) {
							if (x * x + y * y > 4) {
								Position p = new Position(script.myPosition()
										.getX() + x, script.myPosition().getY()
										+ y, script.myPosition().getZ());
								if (script.map.canReach(p))
									positions.add(p);
							}
						}
					}
				} else {
					for (int x = -8; x < 9; x++) {
						for (int y = -8; y < 9; y++) {
							if (x * x + y * y > 64) {
								Position p = new Position(script.myPosition()
										.getX() + x, script.myPosition().getY()
										+ y, script.myPosition().getZ());
								if (script.map.canReach(p))
									positions.add(p);
							}
						}
					}
				}
				MiniMapTileDestination td = new MiniMapTileDestination(
						script.bot, positions.get(MethodProvider
								.random(positions.size())));
				for (int i = 0; i < MethodProvider.random(2, 4); i++) {
					script.mouse.click(td);
				}
				script.log("Whirlpool avoided");
				return 2000;
			}
		}
		List<GroundItem> groundItems = script.groundItems.getAll();
		for (GroundItem item : groundItems) {
			if (item == null)
				continue;
			if (Library.distanceTo(item.getPosition(), script) < 10
					&& item.getName().equals(fishGear2)
					&& !script.inventory.contains(item.getId())) {
				item.interact("Take");
				script.sleep(MethodProvider.random(500, 600));
				return 15;
			}
		}
		checkRunning();
		switch (state) {
		case FISH:
			ab();
			fish();
			break;
		case WALK_TO_BANK:
			walk(this.pathToBank, false);
			break;
		case BANK:
			bank();
			break;
		case WALK_TO_FISH:
			walk(this.pathToBank, true);
			break;
		case DROP:
			drop();
			break;
		}
		return 15;
	}

	// dropping logic, for power fishing, bank all, and bank some
	protected void drop() {
		if (powerFish) {
			if (onlyHas()) {
				state = State.FISH;
				return;
			}
			try {
				Library.dropAllExcept(fishGear, script);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			// script.log(onlyHas() + "");
			if (onlyHas() && script.inventory.isFull()) {
				state = State.WALK_TO_BANK;
			} else if (onlyHas() && !script.inventory.isFull()) {
				state = State.FISH;
			} else {
				try {
					Library.dropAllExcept(fishGear, fishToKeep, script);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	// return true if the invy only has fish and gear we wnat
	private boolean onlyHas() {
		for (Item i : script.inventory.getItems()) {
			if (i != null
					&& !(fishToKeep.contains(i.getName()) || hasFishGear(i))) {
				// script.log("Failed at " + i.getName() + " " +
				// hasFishGear(i));
				return false;
			}
		}
		return true;
	}

	// return true if the invy only has fish and gear we wnat
	private boolean onlyHas2() {
		for (Item i : script.inventory.getItems()) {
			if (i != null && i.getName().contains("Raw"))
				return false;
		}
		return true;
	}

	// return true if we have fish gear
	private boolean hasFishGear(Item arg0) {
		for (int i : fishGear) {
			if (arg0.getId() == i)
				return true;
		}
		return false;
	}

	// return the state
	public State getState() {
		return state;
	}

	// handles fishing interactions and whirlpool detection
	protected void fish() {
		if (poolId == 0)
			poolId = getPoolId();
		if (poolId != 0) {
			if (script.interfaces.get(164).getChild(2) != null) {
				script.interfaces.interactWithChild(164, 2, "Continue");
				try {
					script.sleep(script.random(300, 500));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (script.interfaces.get(131).getChild(3) != null) {
				script.interfaces.interactWithChild(131, 3, "Continue");
				try {
					script.sleep(script.random(300, 500));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (script.inventory.isFull()) {
				state = State.DROP;
			} else {
				Entity closePool = null;
				if (script.myPlayer().getInteracting() != null
						&& script.myPlayer().getInteracting().getId() != poolId) {
					script.log("Whirlpool detected");
					closePool = script.npcs.closest(poolId);
					if (closePool != null && closePool.interact(action)) {
						Library.waitFor(new Conditional() {
							@Override
							public boolean isSatisfied(Script script) {
								return !script.myPlayer().isAnimating();
							}
						}, 3000, 100, script);
						script.log("Whirlpool avoided!");
					} else {
						ArrayList<Position> positions = new ArrayList<Position>();
						for (int x = -8; x < 9; x++) {
							for (int y = -8; y < 9; y++) {
								if (x * x + y * y > 64) {
									Position p = new Position(script
											.myPosition().getX() + x, script
											.myPosition().getY() + y, script
											.myPosition().getZ());
									if (script.map.canReach(p))
										positions.add(p);
								}
							}
						}
						MiniMapTileDestination td = new MiniMapTileDestination(
								script.bot, positions.get(MethodProvider
										.random(positions.size())));
						for (int i = 0; i < MethodProvider.random(2, 4); i++) {
							script.mouse.click(td);
						}
						script.log("Whirlpool avoided");
						try {
							script.sleep(2000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				if (((!script.myPlayer().isAnimating()) || (script.myPlayer()
						.getInteracting() == null && !script.myPlayer()
						.isMoving()))
						&& !script.inventory.isFull()) {
					closePool = script.npcs.closest(poolId);
					if (closePool != null && closePool.interact(action))
						Library.waitFor(new Conditional() {

							@Override
							public boolean isSatisfied(Script script) {
								return script.myPlayer().isAnimating();
							}
						}, 3000, 100, script);
				}
			}
		}
	}

	// gets the pool ID to be used for the duration of the script
	protected int getPoolId() {
		NPC pool = script.npcs
				.closest(new PoolMatch(action, script, fishToKeep));
		if (pool != null)
			return pool.getId();
		else
			return 0;
	}

	// handles banking
	protected void bank() {
		if (script.inventory.isEmpty() || onlyHas2()) {
			state = State.WALK_TO_FISH;
			return;
		} else {
			if (script.bank.isOpen()) {
				script.bank.depositAll(new NotFishingGear());
				try {
					script.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (b.boxIsOpen()) {
				try {
					depositBoxBank();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (script.inventory.isEmpty() || onlyHas2()) {
				state = State.WALK_TO_FISH;
				return;
			}
			RS2Object bankBooth = Library.closestObject("Bank booth", "Bank",
					script);
			NPC banker = script.npcs.closest("Banker");
			if (bankBooth != null && !script.bank.isOpen()) {
				bankBooth.interact("Bank");
				Library.waitFor(new Conditional() {

					@Override
					public boolean isSatisfied(Script script) {
						return script.bank.isOpen() || b.boxIsOpen();
					}
				}, 1000, 100, script);
			} else if (bankBooth == null && !script.bank.isOpen()) {
				bankBooth = Library.closestObject("Bank deposit box",
						"Deposit", script);
				if (bankBooth != null && !script.bank.isOpen()) {
					bankBooth.interact("Deposit");
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return script.bank.isOpen() || b.boxIsOpen();
						}
					}, 10000, 100, script);
					script.bank.depositAll(new NotFishingGear());
					try {
						script.sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			if (bankBooth == null && !script.bank.isOpen() && banker != null) {
				banker.interact("Bank");
				Library.waitFor(new Conditional() {

					@Override
					public boolean isSatisfied(Script script) {
						return script.bank.isOpen() || b.boxIsOpen();
					}
				}, 1000, 100, script);
			} else if (bankBooth == null && !script.bank.isOpen()
					&& banker == null) {
				banker = npcs.closest("Arnold Lydspor");
				if (banker != null) {
					banker.interact("Bank");
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return script.bank.isOpen() || b.boxIsOpen();
						}
					}, 1000, 100, script);
				}
			}
		}
	}

	// turns on running if need be
	protected void checkRunning() {
		if (!script.settings.isRunning()
				&& script.settings.getRunEnergy() > random(30, 50)) {
			while (!script.settings.isRunning()) {
				script.settings.setRunning(true);
				try {
					script.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	// executes antiban related actions
	protected void ab() {
		if (script.myPlayer().getAnimation() != -1
				&& Math.random() < DreamFisher.antibanRate) {
			antiBan.execute(script);
		}
	}

	// handles path walking
	protected void walk(Position[] path, boolean reverse)
			throws InterruptedException {
		while (script.interfaces.get(164).getChild(2) != null) {
			script.interfaces.interactWithChild(164, 2, "Continue");
			try {
				script.sleep(script.random(300, 500));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (reverse && poolArea.contains(script.myPosition()))
			state = State.FISH;
		else if (!reverse && bankArea.contains(script.myPosition()))
			state = State.BANK;
		else if (!reverse) {
			w.walkPathMM(path);
		} else {
			w.walkPathMM(w.reversePath(path));
		}
	}

	// executes banking via deposit box
	private void depositBoxBank() throws InterruptedException {
		Item[] is = b.getItems();
		ArrayList<Item> items = new ArrayList<Item>();
		ArrayList<Integer> slots = new ArrayList<Integer>();
		do {
			is = b.getItems();
			for (Item i : is) {
				if (i != null && !items.contains(i)
						&& !Arrays.asList(fishGear).contains(i.getId())
						&& !i.getName().contains("Coins")
						&& !i.getName().contains("pot")
						&& !i.getName().contains("Net")
						&& !i.getName().contains("Harpoon")
						&& !i.getName().contains("rod")) {
					slots.add(b.getItemSlot(i));
					items.add(i);
				}
			}
			for (int i : slots) {
				b.interact(b.getRectangle(i), "Deposit All");
				script.sleep(200);
			}
			script.sleep(1000);
			if (onlyHas2() || script.inventory.isEmpty()) {
				state = State.WALK_TO_FISH;
				if (b.boxIsOpen()
						&& (script.inventory.onlyContains(new GearMatch()) || script.inventory
								.isEmpty())) {
					b.closeBox();
					script.sleep(200);
				}
				break;
			}
		} while (!script.inventory.isEmpty());
	}
}
