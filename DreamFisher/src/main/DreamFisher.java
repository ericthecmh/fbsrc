package main;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;

import javax.swing.JOptionPane;

import library.Conditional;
import library.Library;
import library.LootItem;
import library.PriceFetcher;
import library.WorldHopping;

import org.osbot.BotApplication;
import org.osbot.rs07.antiban.AntiBan.BehaviorType;
import org.osbot.rs07.api.Client.LoginState;
import org.osbot.rs07.api.ui.Skill;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.script.ScriptManifest;

import randomstracker.RandomTracker;
import fishers.Alkharid;
import fishers.BarbarianFishing;
import fishers.BarbarianOutpost;
import fishers.BarbarianVillage;
import fishers.BaxtorianFalls;
import fishers.Catherby;
import fishers.Draynor;
import fishers.Fisher;
import fishers.FishingGuild;
import fishers.LowerBaxtorian;
import fishers.Lumbridge;
import fishers.LumbridgeSwamp;
import fishers.MudSkipperPoint;
import fishers.MusaPoint;
import fishers.Piscatoris;
import fishers.Rellekka;
import fishers.ShiloVillage;
import fishers.SinclairMansion;
import gui.DreamFisherGUI;

@ScriptManifest(author = "Ericthecmh & Eliot", info = "Best AIO fisher ever!\nMade by Ericthecmh and Eliot\nVersion 0.1.8", logo = "", name = "DreamFisher v0.1.8", version = 0.18)
public class DreamFisher extends Script {

	// DO NOT TOUCH THIS
	public final static String ip3 = "179";

	// paint stuff
	private final Color black = new Color(26, 71, 131, 150);
	private final Color green = new Color(202, 232, 240);
	private final Font paintFont = new Font("Arial", 0, 11);
	private final BasicStroke stroke = new BasicStroke(1.0F);
	private int fishExp = 0;
	private Fisher fisher;
	private boolean canStart = false;
	private boolean guiFinished;
	public ArrayList<LootItem> lootList = new ArrayList<LootItem>();
	public ArrayList<String> stringList = new ArrayList<String>();

	// get this from the gui
	private String action = "Net";

	// get this from the gui
	private boolean powerFish = false;

	// get these values from the gui
	private HashSet<String> fishToKeep = new HashSet<String>();

	private final WorldHopping worldHopping = new WorldHopping(this);

	long startTime;
	private boolean window2 = false;
	int fishingExperience = 0;
	public boolean stopScript;

	public static double antibanRate = 0.0005;

	@Override
	public void onStart() throws InterruptedException {
		try {
			log("Welcome to DreamFisher!");
			log("Brought to you by the great scripters Eliot and Ericthecmh");
			antiBan.unregisterBehavior(BehaviorType.CAMERA_SHIFT);
			antiBan.unregisterBehavior(BehaviorType.OTHER);
			antiBan.unregisterBehavior(BehaviorType.RANDOM_MOUSE_MOVEMENT);
			antiBan.unregisterBehavior(BehaviorType.SLIGHT_MOUSE_MOVEMENT);
			System.out.println("INITIALIZING GUI");
			DreamFisherGUI gui = new DreamFisherGUI();
			gui.setVisible(true);
			while (gui.isVisible()) {
				sleep(100);
			}
			// paint
			MouseListener bml = new BotMouseListener();
			bot.addMouseListener(bml);
			fishingExperience = skills.getExperience(Skill.FISHING);
			startTime = System.currentTimeMillis();
			switch (gui.getLocationItem()) {
			case "Draynor":
				log("Action: " + gui.getAction());
				for (Object o : gui.getSelectedFish()) {
					fishToKeep.add((String) o);
					log("Keeping fish " + (String) o);
				}
				log("Powerfishing: " + gui.shouldPowerFish());
				fisher = new Draynor(this, gui.getAction(),
						gui.shouldPowerFish(), fishToKeep);
				break;
			case "Barbarian Outpost":
				log("Action: " + gui.getAction());
				for (Object o : gui.getSelectedFish()) {
					fishToKeep.add((String) o);
					log("Keeping fish " + (String) o);
				}
				log("Powerfishing: " + gui.shouldPowerFish());
				fisher = new BarbarianOutpost(this, gui.getAction(),
						gui.shouldPowerFish(), fishToKeep);
				break;

			case "Alkharid":
				log("Action: " + gui.getAction());
				for (Object o : gui.getSelectedFish()) {
					fishToKeep.add((String) o);
					log("Keeping fish " + (String) o);
				}
				log("Powerfishing: " + gui.shouldPowerFish());
				fisher = new Alkharid(this, gui.getAction(),
						gui.shouldPowerFish(), fishToKeep);
				break;

			case "Barbarian Village":
				log("Action: " + gui.getAction());
				for (Object o : gui.getSelectedFish()) {
					fishToKeep.add((String) o);
					log("Keeping fish " + (String) o);
				}
				log("Powerfishing: " + gui.shouldPowerFish());
				fisher = new BarbarianVillage(this, gui.getAction(),
						gui.shouldPowerFish(), fishToKeep);
				break;

			case "Baxtorian Falls":
				log("Action: " + gui.getAction());
				for (Object o : gui.getSelectedFish()) {
					fishToKeep.add((String) o);
					log("Keeping fish " + (String) o);
				}
				log("Powerfishing: " + gui.shouldPowerFish());
				fisher = new BaxtorianFalls(this, gui.getAction(),
						gui.shouldPowerFish(), fishToKeep);
				break;

			case "Catherby":
				log("Action: " + gui.getAction());
				for (Object o : gui.getSelectedFish()) {
					fishToKeep.add((String) o);
					log("Keeping fish " + (String) o);
				}
				log("Powerfishing: " + gui.shouldPowerFish());
				fisher = new Catherby(this, gui.getAction(),
						gui.shouldPowerFish(), fishToKeep);
				break;

			case "Fishing Guild":
				log("Action: " + gui.getAction());
				for (Object o : gui.getSelectedFish()) {
					fishToKeep.add((String) o);
					log("Keeping fish " + (String) o);
				}
				log("Powerfishing: " + gui.shouldPowerFish());
				fisher = new FishingGuild(this, gui.getAction(),
						gui.shouldPowerFish(), fishToKeep);
				break;

			case "Lower Baxtorian":
				log("Action: " + gui.getAction());
				for (Object o : gui.getSelectedFish()) {
					fishToKeep.add((String) o);
					log("Keeping fish " + (String) o);
				}
				log("Powerfishing: " + gui.shouldPowerFish());
				fisher = new LowerBaxtorian(this, gui.getAction(), true,
						fishToKeep);
				break;

			case "Lumbridge":
				log("Action: " + gui.getAction());
				for (Object o : gui.getSelectedFish()) {
					fishToKeep.add((String) o);
					log("Keeping fish " + (String) o);
				}
				log("Powerfishing: " + gui.shouldPowerFish());
				fisher = new Lumbridge(this, gui.getAction(),
						gui.shouldPowerFish(), fishToKeep);
				break;

			case "Lumbridge Swamp":
				log("Action: " + gui.getAction());
				for (Object o : gui.getSelectedFish()) {
					fishToKeep.add((String) o);
					log("Keeping fish " + (String) o);
				}
				log("Powerfishing: " + gui.shouldPowerFish());
				fisher = new LumbridgeSwamp(this, gui.getAction(),
						gui.shouldPowerFish(), fishToKeep);
				break;

			case "Musa point":
				log("Action: " + gui.getAction());
				for (Object o : gui.getSelectedFish()) {
					fishToKeep.add((String) o);
					log("Keeping fish " + (String) o);
				}
				log("Powerfishing: " + gui.shouldPowerFish());
				fisher = new MusaPoint(this, gui.getAction(),
						gui.shouldPowerFish(), fishToKeep);
				break;

			case "Piscatoris":
				log("Action: " + gui.getAction());
				for (Object o : gui.getSelectedFish()) {
					fishToKeep.add((String) o);
					log("Keeping fish " + (String) o);
				}
				log("Powerfishing: " + gui.shouldPowerFish());
				fisher = new Piscatoris(this, gui.getAction(),
						gui.shouldPowerFish(), fishToKeep);
				break;

			case "Shilo Village":
				log("Action: " + gui.getAction());
				for (Object o : gui.getSelectedFish()) {
					fishToKeep.add((String) o);
					log("Keeping fish " + (String) o);
				}
				log("Powerfishing: " + gui.shouldPowerFish());
				fisher = new ShiloVillage(this, gui.getAction(),
						gui.shouldPowerFish(), fishToKeep);
				break;
			case "Mud skippers point":
				log("Action: " + gui.getAction());
				for (Object o : gui.getSelectedFish()) {
					fishToKeep.add((String) o);
					log("Keeping fish " + (String) o);
				}
				log("Powerfishing: " + gui.shouldPowerFish());
				fisher = new MudSkipperPoint(this, gui.getAction(),
						gui.shouldPowerFish(), fishToKeep);
				break;

			case "Sinclair Mansion":
				log("Action: " + gui.getAction());
				for (Object o : gui.getSelectedFish()) {
					fishToKeep.add((String) o);
					log("Keeping fish " + (String) o);
				}
				log("Powerfishing: " + gui.shouldPowerFish());
				fisher = new SinclairMansion(this, gui.getAction(),
						gui.shouldPowerFish(), fishToKeep);
				break;

			case "Barbarian Fishing":
				log("Action: " + gui.getAction());
				for (Object o : gui.getSelectedFish()) {
					fishToKeep.add((String) o);
					log("Keeping fish " + (String) o);
				}
				log("Powerfishing: " + gui.shouldPowerFish());
				fisher = new BarbarianFishing(this, gui.getAction(),
						gui.shouldPowerFish(), fishToKeep);
				break;

			case "Rellekka":
				log("Action: " + gui.getAction());
				for (Object o : gui.getSelectedFish()) {
					fishToKeep.add((String) o);
					log("Keeping fish " + (String) o);
				}
				log("Powerfishing: " + gui.shouldPowerFish());
				fisher = new Rellekka(this, gui.getAction(),
						gui.shouldPowerFish(), fishToKeep);
				break;

			}

			for (String s : fishToKeep) {
				try {
					lootList.add(new LootItem(s, PriceFetcher.getPrice(s),
							(int) this.inventory.getAmount(s)));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			fisher.onStart();
			canStart = true;
			new Thread(new ServerUpdater()).start();
		} catch (Exception e) {

		}
	}

	@Override
	public int onLoop() throws InterruptedException {
		if (!canStart)
			return 200;
		if (client.getLoginState() == LoginState.LOGGED_IN
				&& worldHopping.isInBotWorld()) {
			worldHopping.switchWorlds();
		}
		// loot tracking
		for (LootItem i : lootList) {
			short amount = (short) inventory.getAmount(i.name);
			if (amount > i.inInventory)
				i.lootedAmount += amount - i.inInventory;
			i.inInventory = amount;
		}
		if (!inventory.contains(fisher.fishGear2)
				&& !equipment.isWieldingWeapon(fisher.fishGear2)
				&& (groundItems.closest(fisher.fishGear2) == null || Library
						.distanceTo(groundItems.closest(fisher.fishGear2)
								.getPosition(), this) >= 10)) {
			Library.waitFor(new Conditional() {

				@Override
				public boolean isSatisfied(Script script) {
					return (script.inventory.contains(fisher.fishGear2)
							|| equipment.isWieldingWeapon(fisher.fishGear2) || (script.groundItems
							.closest(fisher.fishGear2) != null && Library
							.distanceTo(groundItems.closest(fisher.fishGear2)
									.getPosition(), script) < 10));
				}

			}, 5000, 100, this);
			if (!inventory.contains(fisher.fishGear2)
					&& !equipment.isWieldingWeapon(fisher.fishGear2)
					&& (groundItems.closest(fisher.fishGear2) == null || Library
							.distanceTo(groundItems.closest(fisher.fishGear2)
									.getPosition(), this) >= 10)) {
				log("Lost fishing gear?");
				stop();
			}
		}
		// end loot tracking
		return fisher.onLoop();
	}

	/**
	 * Draws the paint
	 */
	@Override
	public void onPaint(Graphics2D g) {
		if (canStart) {
			// paint
			Graphics2D gr = g;
			int totalGP = 0;
			for (LootItem item : lootList) {
				if (item.avgPrice > 0) {
					totalGP += item.avgPrice * item.lootedAmount;
				}
			}
			int totalExp = skills.getExperience(Skill.FISHING)
					- fishingExperience;
			long runTime = System.currentTimeMillis() - startTime;
			gr.setColor(black);
			gr.fillRoundRect(7, 307, 503, 30, 6, 6);
			gr.setColor(green);
			gr.drawRoundRect(7, 307, 503, 30, 6, 6);
			// text
			gr.setStroke(this.stroke);
			gr.setColor(green);
			gr.setFont(paintFont);
			gr.drawString("Runtime: " + format(runTime), 14, 318);
			if (fisher != null && ((Fisher) fisher).getState() != null)
				gr.drawString("State: "
						+ ((Fisher) fisher).getState().toString(), 14, 332);
			gr.drawString("Total EXP: " + totalExp, 182, 318);
			gr.drawString("Total GP: " + totalGP, 362, 318);
			gr.setRenderingHints(new RenderingHints(
					RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_ON));

			gr.setColor(green);
			gr.setStroke(this.stroke);
			gr.setFont(paintFont);
			gr.drawString("EXP/hour: "
					+ (int) (totalExp * (3600000.0D / runTime)), 182, 332);

			if (window2) {
				gr.setColor(black);
				gr.fillRoundRect(355, 339, 155, 135, 6, 6);
				gr.setColor(green);
				gr.drawRoundRect(355, 339, 155, 135, 6, 6);
				gr.setColor(green);
				gr.setStroke(this.stroke);
				gr.setFont(paintFont);
				gr.drawString("GP/hour: "
						+ (int) (totalGP * (3600000.0D / runTime)), 362, 332);
				int y = 355;
				for (LootItem item : lootList) {
					if (item.avgPrice > 0) {
						gr.drawString(item.name + " [GP]: " + item.lootedAmount
								+ " [" + item.avgPrice * item.lootedAmount
								+ "]", 362, y);
						y += 20;
					}
				}
				gr.drawString("Collapse", 412, 471);
			} else {
				gr.setColor(green);
				gr.setStroke(this.stroke);
				gr.setFont(paintFont);
				gr.drawString("Expand", 362, 332);
			}
		}
	}

	String format(final long time) {
		final StringBuilder t = new StringBuilder();
		final long total_secs = time / 1000;
		final long total_mins = total_secs / 60;
		final long total_hrs = total_mins / 60;
		final int secs = (int) total_secs % 60;
		final int mins = (int) total_mins % 60;
		final int hrs = (int) total_hrs;
		if (hrs < 10)
			t.append("0");
		t.append(hrs + ":");
		if (mins < 10)
			t.append("0");
		t.append(mins + ":");
		if (secs < 10) {
			t.append("0");
			t.append(secs);
		} else
			t.append(secs);
		return t.toString();
	}

	public class BotMouseListener implements MouseListener {
		@Override
		public void mouseClicked(MouseEvent e) {
			if (e.getPoint().x >= 353 && e.getPoint().x <= 402
					&& e.getPoint().y >= 319 && e.getPoint().y <= 335
					&& !window2) {
				window2 = true;
			}
			if (e.getPoint().x >= 407 && e.getPoint().x <= 455
					&& e.getPoint().y >= 457 && e.getPoint().y <= 472
					&& window2) {
				window2 = false;
			}
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub

		}

	}

	public void onExit() {
		stopScript = true;
	}

	class ServerUpdater implements Runnable {

		long oldrun = 0;
		int oldxp = skills.getExperience(Skill.FISHING);
		int oldf = 0;
		int oldgp = 0;

		public void run() {
			while (!stopScript) {
				try {
					if (canStart) {
						long run = (long) ((System.currentTimeMillis() - startTime) / 1000);
						int currentExp = skills.getExperience(Skill.FISHING);
						int totalGP = 0;
						for (LootItem item : lootList) {
							if (item.avgPrice > 0) {
								totalGP += item.avgPrice * item.lootedAmount;
							}
						}
						String prepString = "" + System.nanoTime();
						prepString = prepString
								.substring(prepString.length() - 6);
						String runtime = Library.toHex(Library.encrypt(
								prepString + (run - oldrun), "DSSOSBOT"));
						prepString = "" + System.nanoTime();
						prepString = prepString
								.substring(prepString.length() - 6);
						String expgained = Library.toHex(Library.encrypt(
								prepString + (currentExp - oldxp), "DSSOSBOT"));
						prepString = "" + System.nanoTime();
						prepString = prepString
								.substring(prepString.length() - 6);
						int fish = 0;
						for (LootItem l : lootList) {
							fish += l.lootedAmount;
						}
						String fishesFished = Library.toHex(Library.encrypt(
								prepString + (fish - oldf), "DSSOSBOT"));
						prepString = "" + System.nanoTime();
						prepString = prepString
								.substring(prepString.length() - 6);
						String gplooted = Library.toHex(Library.encrypt(
								prepString + (totalGP - oldgp), "DSSOSBOT"));
						oldrun = run;
						oldxp = currentExp;
						oldf = fish;
						oldgp = totalGP;
						String valid = Library.toHex(Library.encrypt("VALID",
								expgained));
						try {
							URL url = new URL(
									"http://dreamscripts.org/updatesig.php?Script=DreamFisher&Username="
											+ BotApplication.getInstance()
													.getOSAccount().username
											+ "&Runtime=" + runtime
											+ "&Fished=" + fishesFished
											+ "&Expgained=" + expgained
											+ "&GPEarned=" + gplooted
											+ "&Valid=" + valid);
							BufferedReader br = new BufferedReader(
									new InputStreamReader(url.openStream()));
							System.out.println(br.readLine());
						} catch (Exception e) {
						}
						try {
							Thread.sleep(10000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

}
