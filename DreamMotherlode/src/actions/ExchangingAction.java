package actions;

import java.awt.Graphics2D;

import library.Conditional;
import library.Library;
import library.Walker;
import main.DreamMotherlode;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.Entity;
import org.osbot.rs07.script.Script;

import filters.OreMatch;
import filters.PickMatch;

public class ExchangingAction implements Action {

	// the script instance
	private DreamMotherlode script;

	// the Walking instance
	Walker w;

	// emptying the sack
	private boolean emptying = false;

	// construct an instance of MiningAction
	public ExchangingAction(DreamMotherlode script) {
		this.script = script;
		w = new Walker(new Position(3749, 5672, 0), script);
	}

	/**
	 * Walks to the center of the map
	 * 
	 * @return if the path was completed successfully.
	 */
	private boolean walk() {
		return w.walk();
	}

	/**
	 * Deposits pay-dirt into the hopper.
	 * 
	 * @return if the pay-dirt was successfully deposited
	 */
	private boolean depositPayDirt() {
		Entity hopper = script.objects.closest("Hopper");
		if (hopper != null && hopper.interact("Deposit")) {
			Library.waitFor(new Conditional() {
				@Override
				public boolean isSatisfied(Script script) {
					return !script.inventory.contains("Pay-dirt");
				}
			}, 3000, 100, script);
		}
		if (!script.inventory.contains("Pay-dirt"))
			script.sack++;
		return !script.inventory.contains("Pay-dirt");
	}

	/**
	 * Checks if the sack needs to be emptied.
	 * 
	 * @return if the sack needs to be emptied.
	 */
	private boolean sackIsFull() {
		return script.sack > 2;
	}

	/**
	 * Empty the sack
	 * 
	 * @return if the sack was emptied
	 */
	private boolean emptySack() {
		Entity sack = script.objects.closest("Sack");
		if (sack != null && sack.interact("Search")) {
			Library.waitFor(new Conditional() {
				@Override
				public boolean isSatisfied(Script script) {
					return script.inventory.contains(new OreMatch());
				}
			}, 3000, 100, script);
		}
		return script.inventory.contains(new OreMatch());
	}

	@Override
	public int execute() {
		System.out.println("Executing ExchangingAction");
		System.out.println("HERE");
		if (needStateChange()) {
			changeState();
			return script.random(70, 120);
		}
		System.out.println("HERE");
		if (sackIsFull()) {
			System.out.println("Sack is full");
			emptying = true;
		} else if (script.sack == 0)
			emptying = false;
		System.out.println("HERE2");
		if (!script.exchangeArea.contains(script.myPosition())) {
			System.out.println("Walking");
			walk();
		} else if ((sackIsFull() || emptying)
				&& !script.inventory.contains(new OreMatch())) {
			System.out.println("Emptying sack");
			emptySack();
		} else if ((sackIsFull() || emptying) && needStateChange())
			changeState();
		else if (script.inventory.contains("Pay-dirt"))
			depositPayDirt();
		return script.random(400, 750);
	}

	/**
	 * Grabs a hammer from the crate.
	 * 
	 * @return if the player acquired a hammer.
	 */
	private boolean retrieveHammer() {
		Entity crate = Library.getObjectAt(new Position(3752, 5664, 0),
				"Crate", script);
		if (script.inventory.isFull()
				&& script.inventory.interact("Drop", "Pay-dirt")) {
			Library.waitFor(new Conditional() {
				@Override
				public boolean isSatisfied(Script script) {
					return !script.inventory.isFull();
				}
			}, 3000, 100, script);
		}
		if (crate != null && crate.interact("Search")) {
			Library.waitFor(new Conditional() {
				@Override
				public boolean isSatisfied(Script script) {
					return script.inventory.contains("Hammer");
				}
			}, 3000, 100, script);
		}
		return script.inventory.contains("Hammer");
	}

	@Override
	public boolean needStateChange() {
		return script.exchangeArea.contains(script.myPosition())
				&& (script.objects.closest(2018) == null && !emptying
						&& script.exchangeArea.contains(script.myPosition())
						|| script.inventory.contains(new OreMatch()) || script.inventory
						.getEmptySlots() > 20);
	}

	@Override
	public void changeState() {
		if (script.objects.closest(2018) == null && !emptying
				&& script.exchangeArea.contains(script.myPosition()))
			script.state = DreamMotherlode.State.REPAIR;
		else if (script.inventory.contains(new OreMatch()))
			script.state = DreamMotherlode.State.BANK;
		else if (script.inventory.getEmptySlots() > 20)
			script.state = DreamMotherlode.State.MINE;
	}

	public void onPaint(Graphics2D g) {
		if (w != null)
			w.onPaint(g);
	}

}
