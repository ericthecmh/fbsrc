package actions;

import java.awt.Graphics2D;

import library.Conditional;
import library.Library;
import main.DreamMotherlode;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.Entity;
import org.osbot.rs07.script.Script;

import filters.NotPickMatch;
import filters.PickMatch;

public class BankingAction implements Action {

	// the script instance
	private DreamMotherlode script;

	// construct an instance of MiningAction
	public BankingAction(DreamMotherlode script) {
		this.script = script;
	}

	@Override
	public int execute() {
		System.out.println("Executing BankAction");
		if (needStateChange()) {
			changeState();
			return script.random(70, 120);
		} else {
			if (script.inventory.getEmptySlots() < 20) {
				depositItems();
			} else {
				retrieveHammer();
			}
		}
		return script.random(350, 750);
	}

	/**
	 * Grabs a hammer from the crate.
	 * 
	 * @return if the player acquired a hammer.
	 */
	private boolean retrieveHammer() {
		Entity crate = Library.getObjectAt(new Position(3752, 5664, 0),
				"Crate", script);
		if (script.inventory.isFull()
				&& script.inventory.interact("Drop", "Pay-dirt")) {
			Library.waitFor(new Conditional() {
				@Override
				public boolean isSatisfied(Script script) {
					return !script.inventory.isFull();
				}
			}, 3000, 100, script);
		}
		if (crate != null && crate.interact("Search")) {
			Library.waitFor(new Conditional() {
				@Override
				public boolean isSatisfied(Script script) {
					return script.inventory.contains("Hammer");
				}
			}, 3000, 100, script);
		}
		return script.inventory.contains("Hammer");
	}

	/**
	 * Deposits all the items.
	 * 
	 * @return if the items were successfully deposited.
	 */
	private boolean depositItems() {
		if (!script.bank.isOpen()) {
			Entity bank = script.objects.closest("Bank chest");
			if (bank != null && bank.interact("Use")) {
				Library.waitFor(new Conditional() {
					@Override
					public boolean isSatisfied(Script script) {
						return script.bank.isOpen();
					}
				}, 3000, 100, script);
			}
		} else {
			script.bank.depositAll(new NotPickMatch());
			Library.waitFor(new Conditional() {
				@Override
				public boolean isSatisfied(Script script) {
					return script.inventory.onlyContains(new PickMatch());
				}
			}, 3000, 100, script);
		}
		return script.inventory.isEmpty()
				|| script.inventory.onlyContains(new PickMatch());
	}

	@Override
	public boolean needStateChange() {
		return script.inventory.isEmpty()
				|| script.inventory.onlyContains(new PickMatch());
	}

	@Override
	public void changeState() {
		script.sack--;
		script.state = DreamMotherlode.State.EXCHANGE;
	}

	public void onPaint(Graphics2D g) {
		// TODO Auto-generated method stub

	}

}
