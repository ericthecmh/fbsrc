package actions;

import org.osbot.rs07.script.Script;

public interface Action {

	/**
	 * Executes all actions related to this state.
	 * 
	 * @return the delay between the next loop.
	 */
	int execute();
	
	/**
	 * Checks if the state needs to be changed.
	 * 
	 * @return if the state needs to change.
	 */
	boolean needStateChange();
	
	/**
	 * Changes the state.
	 */
	void changeState();
}
