package actions;

import java.awt.Graphics2D;

import library.Conditional;
import library.Library;
import main.DreamMotherlode;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.Entity;
import org.osbot.rs07.script.Script;

public class RepairingAction implements Action {

	// the script instance
	private DreamMotherlode script;

	// construct an instance of MiningAction
	public RepairingAction(DreamMotherlode script) {
		this.script = script;
	}

	/**
	 * Grabs a hammer from the crate.
	 * 
	 * @return if the player acquired a hammer.
	 */
	private boolean retrieveHammer() {
		Entity crate = Library.getObjectAt(new Position(3752, 5664, 0),
				"Crate", script);
		if (script.inventory.isFull()
				&& script.inventory.interact("Drop", "Pay-dirt")) {
			Library.waitFor(new Conditional() {
				@Override
				public boolean isSatisfied(Script script) {
					return !script.inventory.isFull();
				}
			}, 3000, 100, script);
		}
		if (crate != null && crate.interact("Search")) {
			Library.waitFor(new Conditional() {
				@Override
				public boolean isSatisfied(Script script) {
					return script.inventory.contains("Hammer");
				}
			}, 3000, 100, script);
		}
		return script.inventory.contains("Hammer");
	}

	/**
	 * Repair a broken strut
	 * 
	 * @return if all the struts were repaired.
	 */
	private boolean repairStrut() {
		Entity brokenStrut = script.objects.closest("Broken strut");
		if (!script.inventory.contains("Hammer")) {
			retrieveHammer();
			return false;
		}
		if (brokenStrut != null && brokenStrut.interact("Hammer")) {
			try {
				script.sleep(script.random(2000, 3000));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return script.objects.closest(2018) != null;
	}

	/**
	 * Drop the hammer.
	 * 
	 * @return if the hammer was successfully dropped.
	 */
	private boolean dropHammer() {
		if (script.inventory.contains("Hammer")
				&& script.inventory.interact("Drop", "Hammer")) {
			Library.waitFor(new Conditional() {
				@Override
				public boolean isSatisfied(Script script) {
					return !script.inventory.contains("Hammer");
				}
			}, 3000, 100, script);
		}
		return !script.inventory.contains("Hammer");
	}

	@Override
	public int execute() {
		Entity brokenStrut = script.objects.closest(2018);
		if (!script.inventory.contains("Hammer")) {
			retrieveHammer();
			return script.random(403,745);
		}
		if (brokenStrut == null && script.inventory.contains("Hammer"))
			repairStrut();
		if (needStateChange())
			changeState();
		return script.random(430, 780);
	}

	@Override
	public boolean needStateChange() {
		return script.objects.closest(2018) != null;
	}

	@Override
	public void changeState() {
		script.state = DreamMotherlode.State.EXCHANGE;
	}

	public void onPaint(Graphics2D g) {
		// TODO Auto-generated method stub

	}

}
