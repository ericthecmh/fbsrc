package actions;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;

import library.Library;
import library.Walker;
import library.path.Path;
import library.path.PathFinder;
import main.DreamMotherlode;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.RS2Object;

public class MiningAction implements Action {

	// HashSet of positions that are blacklisted
	private final HashSet<Position> blacklistedPositions;
	private final PathFinder pathFinder;

	// the script instance
	private DreamMotherlode script;
	// Last time animation != -1
	private long lastAnimationTime;
	// Walker to vein
	private Walker walker;

	// construct an instance of MiningAction
	public MiningAction(DreamMotherlode script) {
		this.script = script;
		blacklistedPositions = new HashSet<Position>();
		blacklistedPositions.add(new Position(3729, 5662, 0));
		blacklistedPositions.add(new Position(3729, 5661, 0));
		blacklistedPositions.add(new Position(3729, 5660, 0));
		blacklistedPositions.add(new Position(3733, 5663, 0));
		blacklistedPositions.add(new Position(3732, 5665, 0));
		blacklistedPositions.add(new Position(3733, 5667, 0));
		pathFinder = new PathFinder(script);
	}

	/**
	 * Interacts with an Ore vein.
	 * 
	 * @return if the interaction was successful
	 */
	private boolean mineOreVein() {
		RS2Object vein = getClosestOreVein();
		if (vein == null)
			return false;
		walker = new Walker(vein.getPosition(), script, 8);
		while (!walker.walk()) {
		}
		walker = null;
		return vein.interact("Mine");
	}

	@Override
	public int execute() {
		if (needStateChange()) {
			changeState();
			return script.random(70, 120);
		}
		if (script.myPlayer().getAnimation() != -1)
			lastAnimationTime = System.currentTimeMillis();
		RS2Object veinObject = Library
				.closestObject("Ore vein", "Mine", script);
		if (Library.distanceTo(veinObject.getPosition(), script) > 1.1
				|| System.currentTimeMillis() - lastAnimationTime > 5000) {
			mineOreVein();
		}
		// TODO Auto-generated method stub
		return script.random(400, 800);
	}

	@Override
	public boolean needStateChange() {
		return script.inventory.isFull();
	}

	@Override
	public void changeState() {
		script.state = DreamMotherlode.State.EXCHANGE;
	}

	private RS2Object getClosestOreVein() {
		ArrayList<RS2Object> veins = new ArrayList<RS2Object>();
		for (RS2Object o : script.objects.getAll()) {
			if (o == null)
				continue;
			if (o.getName().equals("Ore vein")) {
				if (!blacklistedPositions.contains(o.getPosition()))
					veins.add(o);
			}
		}
		if (veins.size() == 0)
			return null;
		Collections.sort(veins, new ObjectComparator());
		RS2Object closest = veins.get(0);
		Path path = pathFinder.generatePath(closest.getPosition());
		if (path == null)
			return null;
		int length = path.getCurrentLength();
		for (RS2Object o : veins) {
			if (Library.distanceTo(o.getPosition(), script) > length)
				break;
			Path tempPath = pathFinder.generatePath(o.getPosition());
			if (tempPath == null)
				continue;
			int lengthToObject = tempPath.getCurrentLength();
			if (lengthToObject < length) {
				length = lengthToObject;
				closest = o;
			}
		}
		return closest;
	}

	private class ObjectComparator implements Comparator<RS2Object> {

		@Override
		public int compare(RS2Object arg0, RS2Object arg1) {
			return (int) ((Library.distanceTo(arg0.getPosition(), script) - Library
					.distanceTo(arg1.getPosition(), script)) * 100000);
		}

	}

	public void onPaint(Graphics2D g) {
		if (walker != null) {
			walker.onPaint(g);
		}
	}

}
