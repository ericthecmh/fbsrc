package library;

import static org.osbot.rs07.script.MethodProvider.random;

import java.awt.Point;
import java.awt.Rectangle;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.map.PositionPolygon;
import org.osbot.rs07.api.model.Entity;
import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.api.model.Player;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.Option;
import org.osbot.rs07.api.ui.RS2Interface;
import org.osbot.rs07.api.ui.RS2InterfaceChild;
import org.osbot.rs07.api.ui.Tab;
import org.osbot.rs07.input.mouse.EntityDestination;
import org.osbot.rs07.input.mouse.InventorySlotDestination;
import org.osbot.rs07.input.mouse.MainScreenTileDestination;
import org.osbot.rs07.input.mouse.MiniMapTileDestination;
import org.osbot.rs07.input.mouse.RectangleDestination;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;

public class Library {

	public final static int[] pickIDs = { 1265, 1267, 1269, 1273, 1271, 1275,
			11920 };
	private final static int[] invSlotsBest = { 0, 4, 8, 12, 16, 20, 24, 1, 5,
			9, 13, 17, 21, 25, 2, 6, 10, 14, 18, 22, 26, 3, 7, 11, 15, 19, 23,
			27 };
	private final static int MENU_HEADER_HEIGHT = 18;
	private final static int MENU_ITEM_HEIGHT = 15;
	private final static int FRIENDS_CORNER_X = 557;
	private final static int FRIENDS_CORNER_Y = 230;
	private final static int FRIENDS_ITEM_HEIGHT = 15;
	private final static int[] blacklistedWorlds = new int[] { 308, 316, 307,
			315, 323, 324, 325, 331, 332, 337, 339, 340, 347, 348, 355, 356,
			363, 364, 371, 372 };
	private final static String SERVER_IP = "192.";

	public static int getRockIDAt(Position p, Script script) {
		for (RS2Object o : script.objects.getAll()) {
			if (o != null && p != null && o.getX() == p.getX()
					&& o.getY() == p.getY() && o.getZ() == p.getZ()
					&& o.getName().equals("Rocks"))
				return o.getId();
		}
		return -1;
	}

	public static RS2Object getRockfallAt(Position p, Script script) {
		for (RS2Object o : script.objects.getAll()) {
			if (o != null && p != null && o.getX() == p.getX()
					&& o.getY() == p.getY() && o.getZ() == p.getZ()
					&& o.getName().equals("Rockfall"))
				return o;
		}
		return null;
	}

	public static String arrayToString(int[] array, Script script) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < array.length - 1; i++) {
			sb.append(array[i]);
			sb.append(',');
			sb.append(' ');
		}
		sb.append(array[array.length - 1]);
		return sb.toString();
	}

	public static double distanceBetween(Position p, Position p2) {
		return Math.sqrt((p.getX() - p2.getX()) * (p.getX() - p2.getX())
				+ (p.getY() - p2.getY()) * (p.getY() - p2.getY()));
	}

	public static double distanceTo(Position p, Script s) {
		return distanceBetween(p, s.myPosition());
	}

	// drops all except those in which we do not want to drop
	public static void dropAllExcept(int[] list, Script script)
			throws InterruptedException {
		for (int i : invSlotsBest)
			if ((getItemOnSlot(i, script) != null)
					&& (!contains(getItemOnSlot(i, script), list))) {
				String name = getItemOnSlot(i, script).getName().toLowerCase();
				if (!name.contains("pickaxe") && !name.contains("aterski")
						&& !name.contains("axe handle"))
					interact(i, "Drop", script);
			}
	}

	// return the item in the given slot
	public static Item getItemOnSlot(int Slot, Script script) {
		if ((Slot > 27) || (Slot < 0))
			return null;
		Item[] items = script.getInventory().getItems();
		return items[Slot];
	}

	// interact with an inventory slot
	private static boolean interact(int Slot, String action, Script script)
			throws InterruptedException {
		while (!script.tabs.open(Tab.INVENTORY)) {
			script.tabs.open(Tab.INVENTORY);
			script.sleep(200);
		}
		InventorySlotDestination isd = new InventorySlotDestination(script.bot,
				Slot);
		while (!isd.getBoundingBox().contains(script.mouse.getPosition())) {
			script.mouse.move(isd);
			script.sleep(5);
		}
		if (script.inventory.isItemSelected()) {
			script.mouse.click(isd, false);
		}
		int i = -1;
		for (int index = 0; index < script.getMenuAPI().getMenu().size(); index++) {
			if ((((Option) script.getMenuAPI().getMenu().get(index)).action
					.toLowerCase().toString().equals(action.toLowerCase()))) {
				i = index;
				break;
			}
		}
		if (i != -1) {
			if (i == 0) {
				// script.log("Click");
				script.mouse.click(isd, false);
				script.log("Interact finished");
			} else {
				// script.log("Right click");
				script.mouse.click(isd, true);
				try {
					script.sleep(MethodProvider.random(300, 600));
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if (script.menu.isOpen()) {
					// int x = script.menu.getX();
					// int y = script.menu.getY();
					// y += MENU_HEADER_HEIGHT + 1;
					// y += i * MENU_ITEM_HEIGHT;
					Point menu = new Point(script.getMenuAPI().getX(), script
							.getMenuAPI().getY());
					// int xOff = random(5, this.client.getMenuWidth() - 4);
					int yOff = 21 + 16 * i + 3;
					RectangleDestination menuItem = new RectangleDestination(
							script.bot, new Rectangle(menu.x + 50, menu.y
									+ yOff, 5, 8));
					// RectangleDestination menuItem = new RectangleDestination(
					// script.bot, x, y + 2,
					// script.menu.getWidth() - 2, MENU_ITEM_HEIGHT);
					while (!menuItem.getBoundingBox().contains(
							script.mouse.getPosition())) {
						script.mouse.move(menuItem);
						script.sleep(5);
					}
					script.mouse.click(menuItem, false);
					return true;
				}
				return false;
			}
		} else {
			return false;
		}
		return false;

	}

	// returns the slot of a menu box
	public int getMenuBoxSlot(Entity e, String interaction, Script script) {
		for (int i = 0; i < script.getMenuAPI().getMenu().size(); i++) {
			Option option = script.getMenuAPI().getMenu().get(i);
			if (option.action.equalsIgnoreCase(interaction)
					&& option.name.contains(e.getName()))
				return i;
		}
		return -1;
	}

	public static boolean doMenu(String action, RectangleDestination e,
			Script script) {
		// script.log("Right click");
		int index = 3;
		script.mouse.click(e, true);
		if (script.menu.isOpen()) {
			try {
				MethodProvider.sleep(MethodProvider.random(400, 700));
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			int x = script.menu.getX();
			int y = script.menu.getY();
			y += MENU_HEADER_HEIGHT + 1;
			y += index * MENU_ITEM_HEIGHT;
			RectangleDestination menuItem = new RectangleDestination(
					script.bot, x, y + 2, script.menu.getWidth() - 2,
					MENU_ITEM_HEIGHT);
			script.mouse.click(menuItem, false);
			return true;
		}
		return false;
	}

	// returns true if it contains the given item
	public static boolean contains(Item item, int[] list) {
		if (item != null) {
			for (int key : list) {
				if (item.getId() == key) {
					return true;
				}
			}
		}
		return false;
	}

	public static boolean waitFor(Conditional c, long time, int delta,
			Script script) {
		for (int i = 0; i < (int) (time / (delta + 0.0) + 1); i++) {
			if (c.isSatisfied(script)) {
				return true;
			}
			try {
				script.sleep(delta);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}

	public static boolean containsInArray(String[] hay, String needle) {
		for (String s : hay) {
			if (s != null && s.equals(needle))
				return true;
		}
		return false;
	}

	public static RS2Object closestObject(String name, String action,
			Script script) {
		double distance = 2000000000.0;
		RS2Object ret = null;
		for (RS2Object o : script.objects.getAll()) {
			if (o != null && o.getName().equals(name)
					&& containsInArray(o.getDefinition().getActions(), action)
					&& distanceTo(o.getPosition(), script) < distance) {
				distance = distanceTo(o.getPosition(), script);
				ret = o;
			}
		}
		return ret;
	}

	/**
	 * Searches a option list for a string
	 * 
	 * @param hay
	 *            option to search through
	 * @param needle
	 *            string to be searched for
	 * @return true if needle is found in haystack
	 */
	public static int getOptionIndex(List<Option> hay, String needle, Entity e) {
		for (int i = 0; i < hay.size(); i++) {
			Option o = hay.get(i);
			if (o != null && o.action != null && o.action.equals(needle)
					&& o.name != null && o.name.contains(e.getName())) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Searches a option list for a string
	 * 
	 * @param hay
	 *            option to search through
	 * @param needle
	 *            string to be searched for
	 * @return true if needle is found in haystack
	 */
	public static int getOptionIndex2(List<Option> hay, String needle, Entity e) {
		for (int i = 0; i < hay.size(); i++) {
			Option o = hay.get(i);
			if (o != null && o.action != null && o.action.equals(needle)
					&& o.name != null && o.name.contains("Rocks")) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Interacts with the rock
	 * 
	 * @param nearestRock
	 * @param script
	 * @return true if clicked, false otherwise
	 */
	public static boolean interactRock(final RS2Object nearestRock,
			Script script) {
		System.out.println("Interact called");
		MainScreenTileDestination td = new MainScreenTileDestination(
				script.bot, nearestRock.getPosition());
		Rectangle rect = td.getBoundingBox();
		// If the rock is fully on screen
		if (rect.getMinX() > 30 && rect.getMinY() > 30 && rect.getMaxX() < 450
				&& rect.getMaxY() < 300) {
			System.out.println("Rock fully on screen");
			RectangleDestination rd = new RectangleDestination(script.bot,
					(int) (rect.getCenterX() - 7),
					(int) (rect.getCenterY() - 4), 15, 9);
			System.out.println("Moving mouse...");
			script.mouse.move(rd);
			Library.waitFor(new Conditional() {

				@Override
				public boolean isSatisfied(Script script) {
					return getOptionIndex2(script.menu.getMenu(), "Mine",
							nearestRock) != -1;
				}

			}, 1000, 100, script);
			int index = getOptionIndex2(script.menu.getMenu(), "Mine",
					nearestRock);
			System.out.println("Index is: " + index);
			if (index != -1) {
				if (index == 0) {
					// script.log("Click");
					script.mouse.click(rd, false);
					Library.waitFor(new Conditional() {

						@Override
						public boolean isSatisfied(Script script) {
							return script.myPlayer().getAnimation() != -1
									|| script.myPlayer().isMoving();
						}

					}, 2000, 100, script);
					return getOptionIndex2(script.menu.getMenu(), "Mine",
							nearestRock) == 0;
				} else {
					// script.log("Right click");
					script.mouse.click(rd, true);
					try {
						script.sleep(MethodProvider.random(300, 600));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (script.menu.isOpen()) {
						try {
							MethodProvider.sleep(MethodProvider
									.random(400, 700));
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						int x = script.menu.getX();
						int y = script.menu.getY();
						y += MENU_HEADER_HEIGHT + 1;
						y += index * MENU_ITEM_HEIGHT;
						RectangleDestination menuItem = new RectangleDestination(
								script.bot, x, y + 2,
								script.menu.getWidth() - 2, MENU_ITEM_HEIGHT);
						script.mouse.click(menuItem, false);
						Library.waitFor(new Conditional() {

							@Override
							public boolean isSatisfied(Script script) {
								return script.myPlayer().getAnimation() != -1
										|| script.myPlayer().isMoving();
							}

						}, 1000, 100, script);
						return true;
					}
					return false;
				}
			} else {
				script.mouse.moveSlightly();
				return false;
			}
		} else {
			if (Library.distanceTo(nearestRock.getPosition(), script) > 14)
				script.localWalker.walk(nearestRock);
			else {
				MiniMapTileDestination td1 = new MiniMapTileDestination(
						script.bot, nearestRock.getPosition());
				script.mouse.click(td1);
			}
			return false;
		}
	}

	/**
	 * Scans the area for a player that is less than 20 tiles away
	 * 
	 * @return true if there is a player
	 */
	public static boolean playerNearBy(Script s) {
		for (Player p : s.players.getAll()) {
			if (p != s.myPlayer()
					&& Library.distanceTo(p.getPosition(), s) <= 20) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks whether a world is valid (worlds that are F2P, or high risk, are
	 * not valid)
	 * 
	 * @param world
	 *            world to check
	 * @return true if not valid
	 */
	public static boolean blacklisted(int world) {
		for (int i = 0; i < blacklistedWorlds.length; i++)
			if (blacklistedWorlds[i] == world)
				return true;
		return false;
	}

	/**
	 * Walks the path defined by the Position Polygon until within 6 tiles of
	 * destination
	 * 
	 * @param path
	 *            path to be walked
	 * @param destination
	 *            should be final tile in the path
	 * @param script
	 *            reference to the script. Used for walking and distance
	 * @param exact
	 * @return true if method did something, false otherwise
	 */
	public static boolean walkPath(PositionPolygon path, Position destination,
			Script script) {
		int pi = path.getClosestIndex(script.myPosition());
		if (pi == 0) {
			Point p = path.get(pi);
			Position pos = new Position(p.x, p.y, script.myPosition().getZ());
			if (Library.distanceTo(pos, script) > 14) {
				try {
					Library.walkClosest(pos, script);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return true;
			}
		}
		if (pi == -1) {
			Point p = path.get(0);
			Position pos = new Position(p.x, p.y, script.myPosition().getZ());
			if (Library.distanceTo(pos, script) > 14) {
				try {
					Library.walkClosest(pos, script);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return true;
			}
			return false;
		}

		if (Library.distanceTo(destination, script) < 6
				&& script.map.canReach(destination)) {
			return false;
		}

		int c = path.getCount();
		int wi = c - 1;

		if (wi == pi) {
			return false;
		}

		if (!script.settings.isRunning()
				&& script.settings.getRunEnergy() > random(30, 50)) {
			script.settings.setRunning(true);
			return true;
		}

		while (wi >= 0 && wi < c && wi >= pi) {
			Point point = path.get(wi);
			if (script.localWalker.walk(point.x, point.y)) {
				return true;
			}
			wi--;
		}

		return false;
	}

	private static void walkClosest(Position goal, Script script)
			throws InterruptedException {
		Position p = getBestReachablePositionInDirectionOf(goal, script);
		if (p != null)
			script.localWalker.walk(p);
	}

	private static void walkClosest(RS2Object object, Script script)
			throws InterruptedException {
		Position p = getBestReachablePositionInDirectionOf(
				object.getPosition(), script);
		if (p != null)
			script.localWalker.walk(p);
	}

	private static Position getBestReachablePositionInDirectionOf(
			Position goal, Script script) {
		int threshold = random(12, 14);
		double distance = 2000000000.0;
		Position best = null;
		for (int i = -14; i <= 14; i++) {
			for (int j = -14; j <= 14; j++) {
				if (i * i + j * j <= threshold * threshold) {
					Position p = new Position(script.myPosition().getX() + i,
							script.myPosition().getY() + j, script.myPosition()
									.getZ());
					if (script.map.canReach(p)
							&& script.map.realDistance(p) <= 14
							&& distanceBetween(p, goal) <= Math.min(
									distanceTo(goal, script), distance)) {
						distance = distanceBetween(p, goal);
						best = p;
					}
				}
			}
		}
		return best;
	}

	public static RS2Object getObjectAt(Position p, String name, Script script) {
		for (RS2Object o : script.objects.getAll()) {
			if (o != null && o.getName().equals(name) && o.getX() == p.getX()
					&& o.getY() == p.getY() && p.getZ() == o.getZ()) {
				return o;
			}
		}
		return null;
	}

	/**
	 * Gets the byte representation of a string (using ascii)
	 * 
	 * @param string
	 * @return byte representation
	 */
	public static byte[] getBytes(String string) {
		byte[] bytes = new byte[string.length()];
		for (int i = 0; i < string.length(); i++) {
			bytes[i] = (byte) string.charAt(i);
		}
		return bytes;
	}

	/**
	 * converts a byte array into a hexadecimal string
	 * 
	 * @param encrypted
	 * @return hex string
	 */
	public static String toHex(byte[] encrypted) {
		byte[] newBytes = new byte[encrypted.length * 2];
		for (int i = 0; i < encrypted.length; i++) {
			newBytes[i * 2 + 1] = getHex(encrypted[i] & 15);
			newBytes[i * 2] = getHex(encrypted[i] >> 4);
		}
		return new String(newBytes);
	}

	/**
	 * converts a hexadecimal string to a byte array
	 * 
	 * @param hex
	 * @return
	 */
	public static byte[] fromHex(String hex) {
		byte[] newBytes = new byte[hex.length() / 2];
		for (int i = 0; i < newBytes.length; i++) {
			newBytes[i] = fromHex(hex.charAt(i * 2));
			newBytes[i] <<= 4;
			newBytes[i] += fromHex(hex.charAt(i * 2 + 1));
		}
		return newBytes;
	}

	/**
	 * Converts a hexademical character into the base 10 value
	 * 
	 * @param a
	 * @return
	 */
	public static byte fromHex(char a) {
		if (a >= 48 && a <= 57)
			return (byte) (a - 48);
		else
			return (byte) (a - 55);
	}

	/**
	 * Converts a base 10 value to the hexadecimal number (ascii)
	 * 
	 * @param i
	 * @return
	 */
	public static byte getHex(int i) {
		if (i >= 0 && i <= 9)
			return (byte) (i + 48);
		else
			return (byte) (i - 10 + 65);
	}

	/**
	 * Flips the byte array
	 * 
	 * @param key
	 * @return
	 */
	public static byte[] flip(byte[] key) {
		byte[] newbytes = new byte[key.length];
		for (int i = 0; i < key.length; i++) {
			newbytes[key.length - i - 1] = key[i];
		}
		return newbytes;
	}

	/**
	 * Encrypts a string based on a given key
	 * 
	 * @param string
	 * @param keyString
	 * @return
	 */
	public static byte[] encrypt(String string, String keyString) {
		byte[] data = getBytes(string);
		byte[] key = flip(getBytes(keyString));
		byte[] newData = new byte[string.length()];
		for (int i = 0; i < string.length(); i++) {
			newData[i] = (byte) (data[i] ^ key[i % key.length]);
		}
		return newData;
	}

	/**
	 * Decrypts an encrypted string based on the key
	 * 
	 * @param string
	 * @param keyString
	 * @return
	 */
	public static byte[] decrypt(String string, String keyString) {
		byte[] key = flip(getBytes(keyString));
		byte[] data = getBytes(string);
		byte[] newData = new byte[string.length()];
		for (int i = 0; i < string.length(); i++) {
			newData[i] = (byte) (data[i] ^ key[i % key.length]);
		}
		return newData;
	}

	/**
	 * Decrypts an encrypted byte array based on the key
	 * 
	 * @param string
	 * @param keyString
	 * @return
	 */
	public static String decrypt(byte[] data, String keyString) {
		byte[] key = flip(getBytes(keyString));
		byte[] newData = new byte[data.length];
		for (int i = 0; i < data.length; i++) {
			newData[i] = (byte) (data[i] ^ key[i % key.length]);
		}
		return new String(newData);
	}

	/**
	 * Performs the selected action on the entity
	 * 
	 * @param e
	 *            entity to interact with
	 * @param action
	 *            action to perform
	 * @param script
	 * @return
	 */
	public static boolean interact(final Entity e, final String action,
			Script script) {
		script.log("Interact called");
		EntityDestination ed2 = new EntityDestination(script.bot, e);
		Rectangle rect = ed2.getBoundingBox();
		// If the rock is fully on screen
		if (rect.getMinX() > 30 && rect.getMinY() > 30 && rect.getMaxX() < 450
				&& rect.getMaxY() < 300) {
			RectangleDestination rd = null;
			if (e.getName().equals("Ladder")) {
				rd = new RectangleDestination(script.bot,
						(int) (rect.getCenterX() - rect.width / 4),
						(int) (rect.getCenterY() + rect.height / 2 - 7),
						rect.width / 2, 6);
			} else {
				rd = new RectangleDestination(script.bot,
						(int) (rect.getCenterX() - 3),
						(int) (rect.getCenterY() - 3), 7, 7);
			}
			System.out.println("Moving mouse...");
			script.mouse.move(rd);
			Library.waitFor(new Conditional() {

				@Override
				public boolean isSatisfied(Script script) {
					return getOptionIndex(script.menu.getMenu(), action, e) != -1;
				}

			}, 1000, 100, script);
			int index = getOptionIndex(script.menu.getMenu(), action, e);
			System.out.println("Index is: " + index);
			if (index != -1) {
				if (index == 0) {
					// script.log("Click");
					script.mouse.click(rd, false);
					return getOptionIndex(script.menu.getMenu(), action, e) == 0;
				} else {
					// script.log("Right click");
					script.mouse.click(rd, true);
					try {
						script.sleep(MethodProvider.random(300, 600));
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					if (script.menu.isOpen()) {
						try {
							MethodProvider.sleep(MethodProvider
									.random(400, 700));
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						int x = script.menu.getX();
						int y = script.menu.getY();
						y += MENU_HEADER_HEIGHT + 1;
						y += index * MENU_ITEM_HEIGHT;
						RectangleDestination menuItem = new RectangleDestination(
								script.bot, x, y + 2,
								script.menu.getWidth() - 2, MENU_ITEM_HEIGHT);
						script.mouse.click(menuItem, false);
						script.log("Interact finished");
						return true;
					}
					script.log("Interact finished");
					return false;
				}
			} else {
				script.log("Interact finished");
				return false;
			}
		} else {
			if (Library.distanceTo(e.getPosition(), script) > 14)
				script.localWalker.walk(e);
			else {
				MiniMapTileDestination td1 = new MiniMapTileDestination(
						script.bot, e.getPosition());
				script.mouse.click(td1);
			}
			return false;
		}
	}

	/**
	 * Sends a message to a friend using the friends list
	 * 
	 * @param username
	 * @param message
	 */
	public static void sendFriendMessage(String username, String message,
			Script script) {
		while (script.tabs.getOpen() != Tab.FRIENDS) {
			script.tabs.open(Tab.FRIENDS);
			try {
				script.sleep(random(400, 700));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		RS2Interface inte = script.interfaces.get(550);
		RS2InterfaceChild child = inte.getChild(1);
		int i = 0;
		for (; i < 1000; i++) {
			RS2InterfaceChild friend = child.getChild(i);
			if (friend != null && friend.isVisible()) {
				if (friend.getMessage().equalsIgnoreCase(username)) {
					break;
				}
			} else {
				i = 1000;
				break;
			}
		}
		script.log(i + "");
		if (i != 1000) {
			// Friend found.
			int y = FRIENDS_CORNER_Y + i * FRIENDS_ITEM_HEIGHT;
			if (y < 410) {
				RectangleDestination rd = new RectangleDestination(script.bot,
						FRIENDS_CORNER_X, y + 1, 70, FRIENDS_ITEM_HEIGHT - 1);
				script.mouse.click(rd);
				if (Library.waitFor(new Conditional() {

					@Override
					public boolean isSatisfied(Script script) {
						RS2Interface inte = script.interfaces.get(548);
						RS2InterfaceChild child = inte.getChild(123);
						return child != null && child.isVisible();
					}

				}, 1000, 100, script)) {
					script.keyboard.typeString(message);
				}
			}
		}
	}
}
