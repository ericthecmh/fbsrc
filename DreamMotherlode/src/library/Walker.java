package library;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;

import library.path.Path;
import library.path.PathFinder;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.input.mouse.MiniMapTileDestination;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;

public class Walker {
	private final Position goal;
	private final Script script;
	private final PathFinder pathFinder;

	private final int threshold;

	private Path currentPath;

	public Walker(Position p, Script s, int t) {
		threshold = t;
		goal = p;
		script = s;
		pathFinder = new PathFinder(s);
	}

	public Walker(Position p, Script s) {
		this(p, s, 6);
	}

	/**
	 * Walks towards the goal position
	 * 
	 * @return true if finished path
	 */
	public boolean walk() {
		currentPath = pathFinder.generatePath(goal);
		if (currentPath != null) {
			for (int i = 0; i < Math.min(17, currentPath.getPath().size()); i++) {
				Position pos = currentPath.getPath().get(i);
				if (pathFinder.rockfallAt(pos)) {
					// Rockfall detected
					if (Library.distanceTo(pos, script) < 6) {
						// If it's nearby
						RS2Object rockfall = Library.getRockfallAt(pos, script);
						if (rockfall == null)
							continue;
						rockfall.interact("Mine");
						try {
							MethodProvider.sleep(MethodProvider.gRandom(800,
									300) + MethodProvider.gRandom(200, 100));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {
						pos = new Position(pos.getX() - 1
								+ MethodProvider.random(0, 2), pos.getY() - 1
								+ MethodProvider.random(0, 2), pos.getZ());
						MiniMapTileDestination td = new MiniMapTileDestination(
								script.bot, pos);
						script.mouse.click(td);
						try {
							MethodProvider
									.sleep((long) ((MethodProvider.gRandom(800,
											300)
											+ MethodProvider.gRandom(400, 100) + MethodProvider
											.gRandom(400, 200)) * (script.settings
											.isRunning() ? 1 : 1.6)));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					return false;
				}
			}
			int walkIndex = Math.min(MethodProvider.random(15, 17), currentPath
					.getPath().size()) - 1;
			if (walkIndex < 6) {
				// At the goal and no rockfalls in the way
				return true;
			} else {
				// Walk
				Position pos = currentPath.getPath().get(walkIndex);
				pos = new Position(
						pos.getX() - 1 + MethodProvider.random(0, 2),
						pos.getY() - 1 + MethodProvider.random(0, 2),
						pos.getZ());
				MiniMapTileDestination td = new MiniMapTileDestination(
						script.bot, pos);
				script.mouse.click(td);
				try {
					MethodProvider
							.sleep((long) ((MethodProvider.gRandom(800, 300)
									+ MethodProvider.gRandom(400, 100) + MethodProvider
									.gRandom(400, 200)) * (script.settings
									.isRunning() ? 1 : 1.6)));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return false;
	}

	public void onPaint(Graphics2D g) {
		g.setColor(new Color(0.0f, 1.0f, 0.0f, 0.2f));
		if (currentPath != null) {
			for (Position p : currentPath.getPath()) {
				Polygon poly = p.getPolygon(script.bot);
				if (poly != null) {
					g.fill(poly);
				}
			}
		}
	}
}
