package library.path;

import java.util.Comparator;

public class PathComparator implements Comparator<Path> {

	@Override
	public int compare(Path arg0, Path arg1) {
		return (int)(arg0.getTotalLength() - arg1.getTotalLength());
	}

}
