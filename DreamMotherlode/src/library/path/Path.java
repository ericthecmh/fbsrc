package library.path;

import java.util.LinkedList;

import org.osbot.rs07.api.map.Position;

public class Path {
	private LinkedList<Position> path;
	private int pathLength;
	private double heuristic;

	/**
	 * Initialized the path
	 * 
	 * @param path
	 *            Current path
	 * @param heuristic
	 *            Heuristic value of the last node in path
	 */
	public Path(LinkedList<Position> path, double heuristic) {
		// Clones the path
		this.path = new LinkedList<Position>();
		for (Position p : path)
			this.path.add(p);
		// Sets the path length
		pathLength = this.path.size();
		// Sets the heuristic
		this.heuristic = heuristic;
	}

	/**
	 * Initializes the path using the given path p
	 * 
	 * @param p
	 *            Path to clone
	 */
	public Path(Path p) {
		this(p.path, p.heuristic);
	}

	/**
	 * Adds a position to the path
	 * 
	 * @param pos
	 *            Position to add
	 * @param heuristic
	 *            Heuristic value of the added position
	 */
	public void addPosition(Position p, double heuristic) {
		path.add(p);
		pathLength++;
		this.heuristic = heuristic;
	}

	public int getCurrentLength() {
		return pathLength;
	}

	public double getHeuristic() {
		return heuristic;
	}

	public double getTotalLength() {
		return pathLength + heuristic;
	}

	public LinkedList<Position> getPath() {
		return path;
	}

	public String toString() {
		String s = "[";
		for (Position p : path) {
			s += "(" + p.getX() + "," + p.getY() + "," + p.getZ() + "), ";
		}
		return s + getTotalLength() + "]";
	}
}
