package library.path;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.script.Script;

public class PathFinder {

	// Reference to the script
	Script script;

	// BASEX and BASEY are the x,y for the lower corner of the map in the
	// motherlode
	private static final int BASEX = 3708;
	private static final int BASEY = 5632;

	// Cache the rockfall locations
	private HashSet<Position> passable;

	public PathFinder(Script script) {
		this.script = script;
		passable = new HashSet<Position>();
	}

	/**
	 * Uses the A* algorithm to generate a path to the given position
	 * 
	 * @param goal
	 *            End (goal) position of path
	 * @return path to position p
	 */
	public Path generatePath(Position goal) {
		updateRockfallPositions();

		// Initialize for A*
		PriorityQueue<Path> heap = new PriorityQueue<Path>(1,
				new PathComparator());
		LinkedList<Position> startPath = new LinkedList<Position>();
		startPath.add(script.myPosition());
		heap.add(new Path(startPath, heuristic(script.myPosition(), goal)));
		boolean[][] expanded = new boolean[70][70];

		// Begin A* logic
		while (!heap.isEmpty()) {
			// Get the path with the least "total" cost
			Path top = heap.poll();

			// Check the current path
			if (top.getPath().getLast().equals(goal)) {
				// Found the shortest path
				return top;
			}

			// Didn't find the shortest path yet, expand the current node
			List<Position> neighbors = getNeighbors(top.getPath().getLast());
			for (Position p : neighbors) {
				if (expanded[p.getX() - BASEX][p.getY() - BASEY]) {
					// If we already expanded neighbor p, then skip it
					continue;
				} else {
					// Add the neighbor p to the queue for expansion
					Path newPath = new Path(top);
					newPath.addPosition(p, heuristic(p, goal));
					heap.add(newPath);
				}
			}

			// Mark current node as expanded
			expanded[top.getPath().getLast().getX() - BASEX][top.getPath()
					.getLast().getY()
					- BASEY] = true;
		}

		// If the heap is empty, we've gone through everything and didn't find a
		// path
		return null;
	}

	/**
	 * Updates the rockfall positions
	 */
	private void updateRockfallPositions() {
		passable.clear();
		for (RS2Object o : script.objects.getAll()) {
			if (o == null || o.getName() == null)
				continue;
			if (o.getName().equals("Rockfall") || o.getName().equals("Ore vein")) {
				passable.add(o.getPosition());
			}
		}
	}

	/**
	 * Heuristic used by A* (in this case, straight line distance
	 * 
	 * @param curPosition
	 *            Current position
	 * @param goalPosition
	 *            Goal position
	 * @return Heuristic value for curPosition
	 */
	public double heuristic(Position curPosition, Position goalPosition) {
		return Math.sqrt((curPosition.getX() - goalPosition.getX())
				* (curPosition.getX() - goalPosition.getX())
				+ (curPosition.getY() - goalPosition.getY())
				* (curPosition.getY() - goalPosition.getY()));
	}

	/**
	 * Checks whether the position is walkable
	 * 
	 * @param x
	 *            x-coord of position
	 * @param y
	 *            y-coord of position
	 * @return true if walkable
	 */
	private boolean walkable(int x, int y) {
		if (passable.contains(new Position(x, y, script.myPosition().getZ())))
			return true;
		int[][] tilesFlags = script.client.accessor.getClippingPlanes()[script
				.myPosition().getZ()].getTileFlags();
		if (x - script.bot.getMethods().getMap().getBaseX() < 0
				|| x - script.bot.getMethods().getMap().getBaseX() >= tilesFlags.length
				|| y - script.bot.getMethods().getMap().getBaseY() < 0
				|| y - script.bot.getMethods().getMap().getBaseY() >= tilesFlags[0].length)
			return true;
		int i = tilesFlags[x - script.bot.getMethods().getMap().getBaseX()][y
				- script.bot.getMethods().getMap().getBaseY()];
		return i < 0x20000 && i != 0x100;
	}

	/**
	 * Gets the walkable neighbors of a position
	 * 
	 * @param pos
	 *            The position
	 * @return list of neighbors
	 */
	public List<Position> getNeighbors(Position pos) {
		List<Position> neighbors = new LinkedList<Position>();
		int x = pos.getX(), y = pos.getY();

		// x - 1, y
		if (walkable(x - 1, y)) {
			neighbors.add(new Position(x - 1, y, 0));
		}

		// x + 1, y
		if (walkable(x + 1, y)) {
			neighbors.add(new Position(x + 1, y, 0));
		}

		// x, y - 1
		if (walkable(x, y - 1)) {
			neighbors.add(new Position(x, y - 1, 0));
		}

		// x, y + 1
		if (walkable(x, y + 1)) {
			neighbors.add(new Position(x, y + 1, 0));
		}
		return neighbors;
	}

	/**
	 * Checks whether there is a rockfall at position
	 * 
	 * @param pos
	 *            The position of interest
	 * @return true if there is a rockfall at pos
	 */
	public boolean rockfallAt(Position pos) {
		return passable.contains(pos);
	}
}
