package main;

import java.awt.Graphics2D;

import org.osbot.rs07.script.Script;
import org.osbot.rs07.script.ScriptManifest;
import org.osbot.rs07.utility.Area;

import actions.BankingAction;
import actions.ExchangingAction;
import actions.MiningAction;
import actions.RepairingAction;

@ScriptManifest(author = "DreamScripts", info = "Motherlode", logo = "", name = "DreamMotherlode", version = 0)
public class DreamMotherlode extends Script {
	public static enum State {
		MINE, BANK, EXCHANGE, REPAIR;
	}

	// the state of the script
	public State state;

	public Area exchangeArea = new Area(3745, 5669, 3751, 5675);

	// the amount of trips without emptying the sack.
	public int sack;

	// the actions the script will execute
	BankingAction bA;
	ExchangingAction eA;
	MiningAction mA;
	RepairingAction rA;

	@Override
	public void onStart() {
		bA = new BankingAction(this);
		eA = new ExchangingAction(this);
		mA = new MiningAction(this);
		rA = new RepairingAction(this);
		sack = 0;
		state = determineInitialState();
	}

	private State determineInitialState() {
		return State.MINE;
	}

	@Override
	public int onLoop() throws InterruptedException {
		log(state.toString());
		switch (state) {
		case MINE:
			return mA.execute();
		case EXCHANGE:
			return eA.execute();
		case BANK:
			return bA.execute();
		case REPAIR:
			return rA.execute();
		default:
			return random(70, 120);
		}
	}

	public void onPaint(Graphics2D g) {
		g.drawString(state.toString(), 50, 50);
		switch (state) {
		case MINE:
			mA.onPaint(g);
		case EXCHANGE:
			eA.onPaint(g);
		case BANK:
			bA.onPaint(g);
		case REPAIR:
			rA.onPaint(g);
		}
	}

}
