package filters;

import org.osbot.rs07.api.filter.Filter;
import org.osbot.rs07.api.model.Item;

public class OreMatch implements Filter<Item> {

	@Override
	public boolean match(Item i) {
		return i.getName().contains("ore");
	}

}
