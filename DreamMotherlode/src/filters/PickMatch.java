package filters;

import org.osbot.rs07.api.filter.Filter;
import org.osbot.rs07.api.model.Item;

public class PickMatch implements Filter<Item> {

	@Override
	public boolean match(Item i) {
		return i.getName().contains("pick") || i.getName().equals("Hammer");
	}

}
