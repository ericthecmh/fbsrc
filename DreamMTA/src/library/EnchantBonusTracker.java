package library;

/*
 * Tracks the time of the bonus shape in the Enchantment Room
 */
public class EnchantBonusTracker {
	private int ID;
	private long updateTime;

	public EnchantBonusTracker() {
		ID = -1;
		updateTime = 0;
	}

	/*
	 * Update the ID if necessary
	 */
	public void update(int id) {
		if (id != ID) {
			ID = id;
			updateTime = System.currentTimeMillis();
		}
	}

	/*
	 * Returns the time left before the next update (in seconds)
	 */
	public double timeLeft() {
		return (25.0 - (System.currentTimeMillis() - updateTime) / 1000.0);
	}
}
