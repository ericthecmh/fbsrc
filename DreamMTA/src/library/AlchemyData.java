package library;

public class AlchemyData implements Comparable<AlchemyData> {

	public String name;
	public int value;

	public AlchemyData(String name, int value) {
		this.name = name;
		this.value = value;
	}

	@Override
	public int compareTo(AlchemyData arg0) {
		return value - arg0.value;
	}

}
