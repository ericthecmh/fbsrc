package library;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.ui.Skill;
import org.osbot.rs07.api.ui.Tab;
import org.osbot.rs07.input.mouse.MouseDestination;
import org.osbot.rs07.input.mouse.RectangleDestination;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;

public class AntibanAction {

	public AntibanAction(Script s) {
	}

	public Skill getCurrentTrainingSkill(Script s) {
		return Skill.MAGIC;
	}

	private boolean doneRunning = false;

	public int execute(final Script script) {
		doneRunning = false;
		new Thread(new Runnable() {
			public void run() {
				int k = MethodProvider.random(0, 14);
				switch (k) {
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
					System.out.println("MOUSE");
					MouseDestination md = new RectangleDestination(script.bot,
							MethodProvider.random(2, 750), MethodProvider
									.random(2, 450), 3, 3);
					script.mouse.move(md);
					break;
				case 6:
				case 7:
					try {
						System.out.println("SKILLS");
						script.skills
								.hoverSkill(getCurrentTrainingSkill(script));
						script.sleep(MethodProvider.random(1000, 2000));
						if (Math.random() < 0.5)
							script.tabs.open(Tab.INVENTORY);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					break;
				case 8:
				case 9:
				case 10:
					System.out.println("TAB");
					int k1 = MethodProvider.random(10);
					switch (k1) {
					case 0:
					case 1:
					case 2:
						script.tabs.open(Tab.FRIENDS);
						break;
					case 3:
					case 4:
						script.tabs.open(Tab.EQUIPMENT);
						break;
					case 5:
						script.tabs.open(Tab.EMOTES);
						break;
					case 6:
						script.tabs.open(Tab.QUEST);
						break;
					case 7:
						script.tabs.open(Tab.SETTINGS);
						break;
					case 8:
						script.tabs.open(Tab.MUSIC);
						break;
					}
					break;
				case 11:
				case 12:
				case 13:
					System.out.println("CAMERA");
					script.camera.moveYaw(MethodProvider.random(360));
				}
				doneRunning = true;
			}
		}).start();
		while (!doneRunning) {
			try {
				script.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// try {
		// s.antiBan.hoverSkill(Skill.HITPOINTS);
		// } catch (InterruptedException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		return 0;
	}
}
