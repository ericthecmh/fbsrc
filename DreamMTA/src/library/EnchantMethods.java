package library;

import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.RS2Interface;
import org.osbot.rs07.api.ui.RS2InterfaceChild;
import org.osbot.rs07.api.ui.Tab;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;

import data.Constants;

public class EnchantMethods {
	public static RS2Object getClosestEnchantable(Script s, boolean follow) {
		RS2Object closestEnchantable = null;
		double distance = 2000000000.0;
		for (RS2Object o : s.objects.getAll()) {
			if (o != null
					&& Constants.enchantableObjects.contains(o.getName())
					&& Library.distanceTo(o.getPosition(), s) < distance
					&& (follow ? o.getDefinition().getModelIds() != null
							&& o.getDefinition().getModelIds().length == 1
							&& o.getDefinition().getModelIds()[0] == getBonusID(s)
							: true)) {
				distance = Library.distanceTo(o.getPosition(), s);
				closestEnchantable = o;
			}
		}
		return closestEnchantable;
	}

	public static int getBonusID(Script s) {
		RS2Interface inte = s.interfaces.get(195);
		if (inte == null)
			return -1;
		for (int i = 8; i < 12; i++) {
			RS2InterfaceChild child = inte.getChild(i);
			if (child == null)
				return -1;
			if (child.isVisible())
				return child.getDisabledMediaId();
		}
		return -1;
	}

	public static boolean inventoryContainsEnchantables(Script s) {
		for (Item i : s.inventory.getItems()) {
			if (i != null) {
				if (i.getName() == null) {
					while (s.tabs.getOpen() != Tab.INVENTORY) {
						s.tabs.open(Tab.INVENTORY);
						try {
							s.sleep(MethodProvider.random(400, 700));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					return inventoryContainsEnchantables(s);
				}
				if (Constants.enchantableNames.contains(i.getName()))
					return true;
			}
		}
		return false;
	}

	public static int getPoints(Script s) {
		RS2Interface inte = s.interfaces.get(195);
		if (inte == null)
			return -1;
		RS2InterfaceChild child = inte.getChild(3);
		if (child == null)
			return -1;
		return Integer.parseInt(child.getMessage());
	}
}
