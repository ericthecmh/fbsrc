package library;

import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.RS2Interface;
import org.osbot.rs07.api.ui.RS2InterfaceChild;
import org.osbot.rs07.api.ui.Tab;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;

import data.Constants;

public class TelekineticMethods {

	public static int getPoints(Script s) {
		RS2Interface inte = s.interfaces.get(198);
		if (inte == null)
			return -1;
		RS2InterfaceChild child = inte.getChild(3);
		if (child == null)
			return -1;
		return Integer.parseInt(child.getMessage());
	}

}
