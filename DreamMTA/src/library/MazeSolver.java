package library;

import java.awt.Graphics;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.osbot.rs07.api.model.RS2Object;

public class MazeSolver {

	private static final boolean GRAPHICS = true;

	private ArrayList<RS2Object> mazeObjects;
	private int[][] maze;

	/*
	 * 0 is nothing
	 * 
	 * 1 is x-1 (to the west)
	 * 
	 * 2 is y+1 (to the north)
	 * 
	 * 4 is x+1 (to the east)
	 * 
	 * 8 is y-1 (to the south)
	 */

	public MazeSolver() {
		mazeObjects = new ArrayList<RS2Object>();
	}

	public void createMaze(List<RS2Object> maze) {
		int minx = 2000000000, miny = 2000000000, maxx = 0, maxy = 0;
		for (RS2Object o : maze) {
			minx = Math.min(minx, o.getX());
			maxx = Math.max(maxx, o.getX());
			miny = Math.min(miny, o.getY());
			maxy = Math.max(maxy, o.getY());
			mazeObjects.add(o);
		}
		this.maze = new int[maxx - minx + 1][maxy - miny + 1];
		for (RS2Object o : mazeObjects) {
			if (o.getOrientation() == 3) {
				this.maze[o.getX() - minx - 1][o.getY() - miny] += 3;
			}
			this.maze[o.getX() - minx][o.getY() - miny] += 1 << (o
					.getOrientation());
		}
		if (GRAPHICS) {
			JFrame frame = new JFrame();
			frame.setSize(this.maze.length * 10, this.maze[0].length * 10);
			MazePanel mp = new MazePanel(this.maze);
			mp.setSize(this.maze.length * 10, this.maze[0].length * 10);
			frame.setContentPane(mp);
			frame.setVisible(true);
			frame.repaint();
		}
		try {
			PrintStream ps = new PrintStream(
					"C:\\Users\\Ericthecmh\\OSBot\\Scripts\\file");
			ps.println(this.maze.length);
			ps.println(this.maze[0].length);
			for (int i = 0; i < this.maze.length; i++) {
				for (int y = 0; y < this.maze[0].length; y++)
					ps.println(this.maze[i][y]);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	class MazePanel extends JPanel {

		private int[][] maze;

		public MazePanel(int[][] maze) {
			this.maze = maze;
		}

		public void paintComponent(Graphics g) {
			int MAXY = maze[0].length * 10;
			for (int x = 0; x < maze.length; x++) {
				for (int y = 0; y < maze[0].length; y++) {
					if ((maze[x][y] & 1) != 0)
						g.drawLine(x * 10, MAXY - (y * 10), x * 10, MAXY
								- (y * 10 + 10));
					if ((maze[x][y] & 2) != 0)
						g.drawLine(x * 10, MAXY - (y * 10 + 10), x * 10 + 10,
								MAXY - (y * 10 + 10));
					if ((maze[x][y] & 4) != 0)
						g.drawLine(x * 10 + 10, MAXY - (y * 10), x * 10 + 10,
								MAXY - (y * 10 + 10));
					if ((maze[x][y] & 8) != 0)
						g.drawLine(x * 10, MAXY - (y * 10), x * 10 + 10, MAXY
								- (y * 10));
				}
			}
		}
	}
}
