package main;

import gui.DreamMTAGUI;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.LinkedList;

import library.AlchemyMethods;
import library.AntibanAction;
import library.EnchantMethods;
import library.GraveyardMethods;
import library.Library;
import library.TelekineticMethods;

import org.osbot.BotApplication;
import org.osbot.rs07.antiban.AntiBan.BehaviorType;
import org.osbot.rs07.api.ui.Skill;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.script.ScriptManifest;

import tasks.Task;
import chat.ChatWindow;
import chat.FriendMessage;

@ScriptManifest(author = "Ericthecmh & Eliot", info = "", logo = "", name = "DreamMTA v0.1", version = 0.1)
public class DreamMTA extends Script {

	private final String currentVersion = "0.1";

	private final double antibanRate = 0.0001;

	private LinkedList<Task> tasks;
	private ChatWindow chatWindow;
	private AntibanAction ab;
	private boolean antibanAllowed = true;
	private boolean shouldAFK = false;
	private long lastAFKTime;
	private boolean canStart;
	private int AFKTimeDelay;
	private boolean stopScript;
	private long startTime;
	private int oldxp;
	private int oldpoints;
	private boolean alchemyInitialized;
	private boolean enchantInitialized;
	private boolean graveyardInitialized;
	private boolean telekineticInitialized;
	private final BasicStroke stroke = new BasicStroke(1.0F);
	private final Font paintFont = new Font("Arial", 0, 11);
	private Script script;

	public void disableAntiban() {
		antibanAllowed = false;
	}

	public void enableAntiban() {
		antibanAllowed = true;
	}

	public void onStart() {
		antiBan.unregisterBehavior(BehaviorType.CAMERA_SHIFT);
		antiBan.unregisterBehavior(BehaviorType.OTHER);
		antiBan.unregisterBehavior(BehaviorType.RANDOM_MOUSE_MOVEMENT);
		antiBan.unregisterBehavior(BehaviorType.SLIGHT_MOUSE_MOVEMENT);
		DreamMTAGUI gui = new DreamMTAGUI();
		gui.setVisible(true);
		while (gui.isVisible()) {
			try {
				sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		tasks = gui.getTasks();
		for (Task t : tasks) {
			t.setScript(this);
		}
		if (gui.enableChatWindow()) {
			chatWindow = new ChatWindow();
			chatWindow.setVisible(true);
		}
		if (gui.enableAFK()) {
			lastAFKTime = System.currentTimeMillis();
			AFKTimeDelay = MethodProvider.random(5 * 60 * 1000, 10 * 60 * 1000);
			shouldAFK = true;
			log("AFK Enabled");
			log("First AFK: " + AFKTimeDelay / 1000 + " seconds from now");
		}
		ab = new AntibanAction(this);
		startTime = System.currentTimeMillis();
		new Thread(new ServerUpdater()).start();
		if (AlchemyMethods.getPoints(this) != -1) {
			alchemyInitialized = true;
			oldpoints += AlchemyMethods.getPoints(this);
		}
		if (TelekineticMethods.getPoints(this) != -1) {
			telekineticInitialized = true;
			oldpoints += TelekineticMethods.getPoints(this);
		}
		if (EnchantMethods.getPoints(this) != -1) {
			enchantInitialized = true;
			oldpoints += EnchantMethods.getPoints(this);
		}
		if (GraveyardMethods.getPoints(this) != -1) {
			graveyardInitialized = true;
			oldpoints += GraveyardMethods.getPoints(this);
		}
		oldxp = skills.getExperience(Skill.MAGIC);
		script = this;
		canStart = true;
	}

	@Override
	public int onLoop() throws InterruptedException {
		if (shouldAFK
				&& System.currentTimeMillis() - lastAFKTime > AFKTimeDelay) {
			lastAFKTime = System.currentTimeMillis();
			AFKTimeDelay = MethodProvider.random(5 * 60 * 1000, 10 * 60 * 1000);
			int time = MethodProvider.random(5000, 20000);
			log("Going AFK for " + time / 1000 + " seconds");
			mouse.moveOutsideScreen();
			sleep(time);
			log("Resuming");
			log("Next AFK: " + (AFKTimeDelay - time) / 1000
					+ " seconds from now");
		}
		if (Math.random() < antibanRate && antibanAllowed) {
			ab.execute(this);
		}
		if (chatWindow != null) {
			String playerMessage = chatWindow.getNextPlayerMessage();
			if (playerMessage != null) {
				this.keyboard.typeString(playerMessage);
			}
			String clanMessage = chatWindow.getNextClanMessage();
			if (clanMessage != null) {
				this.keyboard.typeString("/" + clanMessage);
			}
			FriendMessage fm = chatWindow.getNextFriendMessage();
			if (fm != null) {
				log("Send message");
				Library.sendFriendMessage(fm.username, fm.message, this);
			}
		}
		int ret = tasks.get(0).onLoop();
		if (ret == -1) {
			log("Task (" + tasks.get(0).toString() + ") complete");
			tasks.remove(0);
			if (tasks.size() == 0) {
				log("All tasks completed. Stopping script");
				stop();
			}
			return 0;
		} else {
			return ret;
		}
	}

	String format(final long time) {
		final StringBuilder t = new StringBuilder();
		final long total_secs = time / 1000;
		final long total_mins = total_secs / 60;
		final long total_hrs = total_mins / 60;
		final int secs = (int) total_secs % 60;
		final int mins = (int) total_mins % 60;
		final int hrs = (int) total_hrs;
		if (hrs < 10)
			t.append("0");
		t.append(hrs + ":");
		if (mins < 10)
			t.append("0");
		t.append(mins + ":");
		if (secs < 10) {
			t.append("0");
			t.append(secs);
		} else
			t.append(secs);
		return t.toString();
	}

	@Override
	public void onPaint(Graphics2D g) {
		if (canStart) {
			// paint
			int curpoints = 0;
			if (AlchemyMethods.getPoints(script) != -1) {
				if (!alchemyInitialized) {
					alchemyInitialized = true;
					oldpoints += AlchemyMethods.getPoints(script);
				}
				curpoints += AlchemyMethods.getPoints(script);
			}
			if (TelekineticMethods.getPoints(script) != -1) {
				if (!telekineticInitialized) {
					telekineticInitialized = true;
					oldpoints += TelekineticMethods
							.getPoints(script);
				}
				curpoints += TelekineticMethods.getPoints(script);
			}
			if (EnchantMethods.getPoints(script) != -1) {
				if (!enchantInitialized) {
					enchantInitialized = true;
					oldpoints += EnchantMethods.getPoints(script);
				}
				curpoints += EnchantMethods.getPoints(script);
			}
			if (GraveyardMethods.getPoints(script) != -1) {
				if (!graveyardInitialized) {
					graveyardInitialized = true;
					oldpoints += GraveyardMethods.getPoints(script);
				}
				curpoints += GraveyardMethods.getPoints(script);
			}
			Graphics2D gr = g;
			int totalExp = skills.getExperience(Skill.MAGIC) - oldxp;
			int totalPoints = curpoints - oldpoints;

			long runTime = System.currentTimeMillis() - startTime;
			gr.setColor(Color.BLACK);
			gr.fillRoundRect(7, 307, 503, 30, 6, 6);
			Color background = new Color(102, 51, 153);
			gr.setColor(background);
			gr.drawRoundRect(7, 307, 503, 30, 6, 6);
			// text
			gr.setStroke(this.stroke);
			gr.setColor(Color.MAGENTA);
			gr.setFont(paintFont);
			gr.drawString("Runtime: " + format(runTime), 14, 318);
			if (tasks.size() > 0)
				gr.drawString("DreamMTA Version: " + currentVersion, 14, 332);
			gr.drawString("Total EXP: " + totalExp, 182, 318);
			gr.setRenderingHints(new RenderingHints(
					RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_ON));

			gr.setColor(Color.MAGENTA);
			gr.setStroke(this.stroke);
			gr.setFont(paintFont);
			gr.drawString("EXP/hour: "
					+ (int) (totalExp * (3600000.0D / runTime)), 182, 332);

			gr.drawString("Total Points: " + totalPoints, 362, 318);
			gr.setRenderingHints(new RenderingHints(
					RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_ON));
			gr.setColor(Color.MAGENTA);
			gr.setStroke(this.stroke);
			gr.setFont(paintFont);
			gr.drawString("Points/hour: "
					+ (int) (totalPoints * (3600000.0D / runTime)), 362, 332);
		}
	}

	public void onExit() {
		stopScript = true;
	}

	class ServerUpdater implements Runnable {

		long oldrun = 0;
		int oldxp = skills.getExperience(Skill.MAGIC);

		public void run() {
			while (!stopScript) {
				try {
					if (canStart) {
						long run = (long) ((System.currentTimeMillis() - startTime) / 1000);
						int currentExp = skills.getExperience(Skill.MAGIC);
						String prepString = "" + System.nanoTime();
						prepString = prepString
								.substring(prepString.length() - 6);
						String runtime = Library.toHex(Library.encrypt(
								prepString + (run - oldrun), "DSSOSBOT"));
						prepString = "" + System.nanoTime();
						prepString = prepString
								.substring(prepString.length() - 6);
						String expgained = Library.toHex(Library.encrypt(
								prepString + (currentExp - oldxp), "DSSOSBOT"));
						oldrun = run;
						oldxp = currentExp;
						String valid = Library.toHex(Library.encrypt("VALID",
								expgained));
						try {
							URL url = new URL(
									"http://dreamscripts.no-ip.org/updatesig.php?Script=DreamMTA&Username="
											+ BotApplication.getInstance()
													.getOSAccount().username
											+ "&Runtime=" + runtime
											+ "&Expgained=" + expgained
											+ "&Valid=" + valid);
							System.out
									.println("http://dreamscripts.no-ip.org/updatesig.php?Script=DreamMTA&Username="
											+ BotApplication.getInstance()
													.getOSAccount().username
											+ "&Runtime="
											+ runtime
											+ "&Expgained="
											+ expgained
											+ "&Valid=" + valid);
							BufferedReader br = new BufferedReader(
									new InputStreamReader(url.openStream()));
							System.out.println(br.readLine());
						} catch (Exception e) {
						}
						try {
							Thread.sleep(10000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

}
