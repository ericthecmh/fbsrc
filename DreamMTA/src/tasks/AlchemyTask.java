package tasks;

import java.awt.Graphics2D;

import library.Conditional;
import library.Library;
import library.TelekineticMethods;
import main.DreamMTA;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.Message;
import org.osbot.rs07.api.ui.RS2Interface;
import org.osbot.rs07.api.ui.RS2InterfaceChild;
import org.osbot.rs07.api.ui.Skill;
import org.osbot.rs07.api.ui.Spell;
import org.osbot.rs07.api.ui.Tab;
import org.osbot.rs07.input.mouse.MiniMapTileDestination;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;

import data.Constants;

public class AlchemyTask implements Task {

	private final int coinID = 8890;
	private final boolean shouldStop;
	private final int magicStopAt;
	private final boolean stopMagic;
	private final int pointsStopAt;
	private final boolean stopPoints;

	private DreamMTA script;
	private int itemID;
	private boolean isAlching = true;
	private Spell spell = Spell.LOW_LEVEL_ALCHEMY;
	private int position;

	public AlchemyTask(Spell spell, boolean ss, boolean sm, int msa,
			boolean ps, int spa) {
		this.spell = spell;
		this.shouldStop = ss;
		this.stopMagic = sm;
		this.magicStopAt = msa;
		this.stopPoints = ps;
		this.pointsStopAt = spa;
	}

	@Override
	public void setScript(DreamMTA s) {
		this.script = s;
	}

	@Override
	public int onLoop() {
		script.enableAntiban();
		if (shouldStop) {
			if (stopMagic
					&& script.skills.getStatic(Skill.MAGIC) >= magicStopAt)
				return -1;
			if (stopPoints
					&& TelekineticMethods.getPoints(script) >= pointsStopAt)
				return -1;
		}
		if (script.npcs.closest("Alchemy Guardian") == null
				&& script.objects.closest(10755) == null) {
			// Not in the telekinetic room
			if (Constants.lobbyRoomArea.isInAreaInclusive(script.myPosition())) {
				// if in lobby room, enter the enchantment room
				RS2Object teleport = script.objects
						.closest("Telekinetic Teleport");
				if (teleport != null) {
					teleport.interact("Enter");
					try {
						script.sleep(MethodProvider.random(400, 700));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else {
				// In some other room, exit that room
				RS2Object teleport = script.objects.closest("Exit Teleport");
				if (teleport != null) {
					teleport.interact("Enter");
					try {
						script.sleep(MethodProvider.random(400, 700));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		} else {
			if (!script.inventory.contains(getItemToAlch())) {
				if (script.magic.isSpellSelected()) {
					script.mouse.click(false);
				}
				isAlching = false;
			}
			if (script.inventory.getAmount(coinID) > 1000) {
				RS2Object collector = script.objects.closest("Coin Collector");
				if (collector != null) {
					collector.interact("Deposit");
					try {
						script.sleep(MethodProvider.random(400, 700));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else if (script.inventory.getAmount(getItemToAlch()) > 5
					|| isAlching) {
				isAlching = true;
				script.disableAntiban();
				while (script.myPlayer().getAnimation() == -1
						&& script.tabs.getOpen() != Tab.MAGIC
						&& !script.magic.isSpellSelected()) {
					System.out.println("Opening spellbook");
					// Not doing anything and magic tab isn't open?
					// Open
					// it
					script.tabs.magic.open();
					try {
						script.sleep(MethodProvider.random(400, 700));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (script.tabs.getOpen() == Tab.MAGIC
						&& script.myPlayer().getAnimation() == -1
						&& script.inventory.contains(getItemToAlch())) {
					Library.castSpell(spell, script);
					for (int i = 0; i < 10
							&& script.tabs.getOpen() != Tab.INVENTORY; i++) {
						try {
							script.sleep(MethodProvider.random(100, 200));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				} else if (script.tabs.getOpen() == Tab.INVENTORY
						&& script.magic.isSpellSelected()) {
					int k = 0;
					for (Item i : script.inventory.getItems()) {
						if (i != null
								&& i.getName()
										.equalsIgnoreCase(getItemToAlch())) {
							Library.inventoryInteract(k, script);
							for (int j = 0; j < 10
									&& script.tabs.getOpen() != Tab.MAGIC; j++) {
								try {
									script.sleep(MethodProvider
											.random(100, 200));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch
									// block
									e.printStackTrace();
								}
							}
							if (script.inventory.getAmount(getItemToAlch()) > 1) {
								try {
									script.magic.hoverSpell(spell);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
								Library.waitFor(new Conditional() {

									@Override
									public boolean isSatisfied(Script script) {
										return script.myPlayer().getAnimation() == -1;
									}

								}, 2000, 100, script);
							}
							break;
						}
						k++;
					}
				}
			} else if (script.inventory.isFull()) {
				while (script.inventory.getEmptySlots() < 8) {
					int maxCount = 0;
					int id = 0;
					for (int i = 6893; i < 6898; i++) {
						if (script.inventory.getAmount(i) > maxCount) {
							maxCount = (int) script.inventory.getAmount(i);
							id = i;
						}
					}
					script.inventory.interact("Drop", id);
				}
			} else {
				script.log((new StringBuilder(
						"getting the contents of cupboard at position: "))
						.append(position).toString());
				int boots = (int) script.inventory.getAmount(6893);
				int shields = (int) script.inventory.getAmount(6894);
				int helms = (int) script.inventory.getAmount(6895);
				int emeralds = (int) script.inventory.getAmount(6896);
				int runelongs = (int) script.inventory.getAmount(6897);
				final int sum = boots + shields + helms + emeralds + runelongs;
				RS2Object cup = cupboardAtPos(position);
				if (Library.distanceTo(cup.getPosition(), script) > 6) {
					MiniMapTileDestination td = new MiniMapTileDestination(
							script.bot, cup.getPosition());
					script.mouse.click(td);
					try {
						script.sleep(MethodProvider.random(400, 700));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					if (cup.interact("Search")) {
						if (Library.distanceTo(cup.getPosition(), script) > 1.2) {
							if (Library.waitFor(new Conditional() {

								@Override
								public boolean isSatisfied(Script script) {
									return script.myPlayer().isMoving();
								}

							}, 2000, 100, script)) {
								Library.waitFor(new Conditional() {

									@Override
									public boolean isSatisfied(Script script) {
										return !script.myPlayer().isMoving();
									}

								}, 10000, 100, script);
							}
						}
						Library.waitFor(new Conditional() {

							@Override
							public boolean isSatisfied(Script script) {
								int newsum = 0;
								for (int i = 6893; i < 6898; i++) {
									newsum += (int) script.inventory
											.getAmount(i);
								}
								return newsum > sum;
							}

						}, MethodProvider.random(2000, 4000), 100, script);
						if (script.inventory.getAmount(6893) > (long) boots) {
							getItemToAlch();
							int a = (position + itemID) % 8;
							script.log(a + "");
							position = a;
						} else if (script.inventory.getAmount(6894) > (long) shields) {
							getItemToAlch();
							int a = (position + itemID + 7) % 8;
							script.log(a + "");
							position = a;
						} else if (script.inventory.getAmount(6895) > (long) helms) {
							getItemToAlch();
							int a = (position + itemID + 6) % 8;
							script.log(a + "");
							position = a;
						} else if (script.inventory.getAmount(6896) > (long) emeralds) {
							getItemToAlch();
							int a = (position + itemID + 5) % 8;
							script.log(a + "");
							position = a;
						} else if (script.inventory.getAmount(6897) > (long) runelongs) {
							getItemToAlch();
							int a = (position + itemID + 4) % 8;
							script.log(a + "");
							position = a;
						} else {
							position = (position + 4) % 8;
							script.log("Failed");
						}
					}
					try {
						script.sleep(MethodProvider.random(100, 300));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		return 0;
	}

	private RS2Object cupboardAtPos(int pos) {
		Position cupPos = null;
		if (pos == 0)
			cupPos = new Position(3360, 9632, 2);
		if (pos == 1)
			cupPos = new Position(3360, 9636, 2);
		if (pos == 2)
			cupPos = new Position(3360, 9640, 2);
		if (pos == 3)
			cupPos = new Position(3360, 9644, 2);
		if (pos == 4)
			cupPos = new Position(3369, 9644, 2);
		if (pos == 5)
			cupPos = new Position(3369, 9640, 2);
		if (pos == 6)
			cupPos = new Position(3369, 9636, 2);
		if (pos == 7)
			cupPos = new Position(3369, 9632, 2);

		for (RS2Object obj : script.objects.getAll()) {
			if (obj != null) {
				if (obj.getPosition().equals(cupPos)
						&& obj.getName().equals("Cupboard")) {
					return obj;
				}
			}
		}
		return null;
	}

	public String getItemToAlch() {
		RS2Interface inte = script.interfaces.get(194);
		if (inte == null || !inte.isVisible())
			return "";
		for (int i = 4; i < 9; i++) {
			RS2InterfaceChild child = inte.getChild(i);
			RS2InterfaceChild price = inte.getChild(i + 5);
			if (price != null && child != null) {
				if (price.getMessage().equals("30")) {
					itemID = i - 4;
					if (child.getMessage().startsWith("Adamant Helm"))
						return "Adamant med helm";
					else
						return child.getMessage().substring(0,
								child.getMessage().length() - 1);
				}
			}
		}
		return "";
	}

	@Override
	public void onPaint(Graphics2D g) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMessage(Message m) {

	}

}
