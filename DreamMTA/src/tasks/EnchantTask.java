package tasks;

import java.awt.Graphics2D;

import library.Conditional;
import library.EnchantBonusTracker;
import library.EnchantMethods;
import library.Library;
import main.DreamMTA;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.GroundItem;
import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.Message;
import org.osbot.rs07.api.ui.Skill;
import org.osbot.rs07.api.ui.Spell;
import org.osbot.rs07.api.ui.Tab;
import org.osbot.rs07.input.mouse.MiniMapTileDestination;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;

import data.Constants;

/**
 * Performs Enchanting room stuff
 * 
 * @author Ericthecmh
 */
public class EnchantTask implements Task {
	// Reference to Script
	private DreamMTA script;

	// Spell to use
	private final Spell spell;
	// Whether to follow bonus shape
	private final boolean followShape;
	// Whether or not to stop
	private final boolean shouldStop;
	// Whether to stop for magic level
	private final boolean stopMagic;
	// Magic level to stop at, -1 if don't stop
	private final int magicStopAt;
	// Whether to stop for points count
	private final boolean stopPoints;
	// Points to stop at, -1 if don't stop
	private final int pointsStopAt;
	// Whether to get dragonstones
	private final boolean getDragonstones;
	// Whether to get dragonstones only if they are near
	private final boolean getDragonstonesOnlyNear;

	// Current corner
	private Position currentCorner;
	// Current ID of the bonus object
	private EnchantBonusTracker tracker;
	// Amount to collect (based on the tracker data)
	private int amountToCollect = -1;
	// Name of the bonus object
	private String bonusName;

	public EnchantTask(Spell spell, boolean fs, boolean ss, boolean sm,
			int msa, boolean sp, int psa, boolean gd, boolean gdn) {
		this.spell = spell;
		followShape = fs;
		shouldStop = ss;
		stopMagic = sm;
		if (stopMagic)
			magicStopAt = msa;
		else
			magicStopAt = -1;
		stopPoints = sp;
		if (stopPoints)
			pointsStopAt = psa;
		else
			pointsStopAt = -1;
		getDragonstones = gd;
		getDragonstonesOnlyNear = gdn;
	}

	public void setScript(DreamMTA s) {
		this.script = s;
	}

	public int onLoop() {
		script.enableAntiban();
		if (shouldStop) {
			if (stopMagic
					&& script.skills.getStatic(Skill.MAGIC) >= magicStopAt)
				return -1;
			if (stopPoints && EnchantMethods.getPoints(script) >= pointsStopAt)
				return -1;
		}
		if (tracker == null) {
			tracker = new EnchantBonusTracker();
		}
		tracker.update(EnchantMethods.getBonusID(script));
		if (!Constants.enchantmentRoomArea.isInAreaInclusive(script
				.myPosition())) {
			// Not in the enchantment room
			if (Constants.lobbyRoomArea.isInAreaInclusive(script.myPosition())) {
				// if in lobby room, enter the enchantment room
				RS2Object teleport = script.objects
						.closest("Enchanters Teleport");
				if (teleport != null) {
					teleport.interact("Enter");
					try {
						script.sleep(MethodProvider.random(400, 700));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else {
				// In some other room, exit that room
				RS2Object teleport = script.objects.closest("Exit Teleport");
				if (teleport != null) {
					teleport.interact("Enter");
					try {
						script.sleep(MethodProvider.random(400, 700));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		} else {
			// In the room
			if (getDragonstones && !script.inventory.isFull()) {
				// Dragonstones
				GroundItem ds = script.groundItems.closest("Dragonstone");
				if (ds != null) {
					System.out.println("DRAGONSTONE");
					if (getDragonstonesOnlyNear) {
						if (Library.distanceTo(ds.getPosition(), script) < 5) {
							ds.interact("Take");
							return MethodProvider.random(400, 700);
						}
					} else {
						while (Library.distanceTo(ds.getPosition(), script) > 3) {
							if (Library.distanceTo(ds.getPosition(), script) > 8) {
								script.localWalker.walk(ds.getPosition());
							} else {
								script.mouse.click(new MiniMapTileDestination(
										script.bot, ds.getPosition()));
							}
							try {
								script.sleep(MethodProvider.random(500, 700));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						ds.interact("Take");
						return MethodProvider.random(400, 700);
					}
				}
			}
			if (followShape) {
				if (script.inventory.isFull()) {
					if (!EnchantMethods.inventoryContainsEnchantables(script)) {
						// Check to make sure
						while (script.tabs.getOpen() != Tab.INVENTORY) {
							script.tabs.open(Tab.INVENTORY);
							try {
								script.sleep(MethodProvider.random(400, 700));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						if (!EnchantMethods
								.inventoryContainsEnchantables(script)) {
							amountToCollect = -1;
							System.out.println("Depositing in hole");
							RS2Object hole = script.objects.closest("Hole");
							if (hole != null) {
								hole.interact("Deposit");
								try {
									script.sleep(MethodProvider
											.random(400, 700));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
					} else {
						script.disableAntiban();
						while (script.myPlayer().getAnimation() == -1
								&& script.tabs.getOpen() != Tab.MAGIC
								&& !script.magic.isSpellSelected()) {
							System.out.println("Opening spellbook");
							// Not doing anything and magic tab isn't open?
							// Open
							// it
							script.tabs.magic.open();
							try {
								script.sleep(MethodProvider.random(400, 700));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						if (script.tabs.getOpen() == Tab.MAGIC
								&& script.myPlayer().getAnimation() == -1
								&& EnchantMethods
										.inventoryContainsEnchantables(script)) {
							Library.castSpell(spell, script);
							for (int i = 0; i < 10
									&& script.tabs.getOpen() != Tab.INVENTORY; i++) {
								try {
									script.sleep(MethodProvider
											.random(100, 200));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						} else if (script.tabs.getOpen() == Tab.INVENTORY
								&& script.magic.isSpellSelected()) {
							int k = 0;
							for (Item i : script.inventory.getItems()) {
								if (i != null
										&& Constants.enchantableNames
												.contains(i.getName())) {
									Library.inventoryInteract(k, script);
									for (int j = 0; j < 10
											&& script.tabs.getOpen() != Tab.MAGIC; j++) {
										try {
											script.sleep(MethodProvider.random(
													100, 200));
										} catch (InterruptedException e) {
											// TODO Auto-generated catch
											// block
											e.printStackTrace();
										}
									}
									if (EnchantMethods
											.inventoryContainsEnchantables(script)) {
										try {
											script.magic.hoverSpell(spell);
										} catch (InterruptedException e) {
											e.printStackTrace();
										}
										Library.waitFor(new Conditional() {

											@Override
											public boolean isSatisfied(
													Script script) {
												return script.myPlayer()
														.getAnimation() == -1;
											}

										}, 2000, 100, script);
									}
									break;
								}
								k++;
							}
						}
					}
				} else {
					// Not full, do logic
					RS2Object bonus = EnchantMethods.getClosestEnchantable(
							script, true);
					if (bonus != null) {
						bonusName = bonus.getName().substring(0,
								bonus.getName().indexOf(' '));
						System.out.println("Bonus name: " + bonusName);
						if (Library.distanceTo(bonus.getPosition(), script) < 1.2) {
							// Next to bonus item
							if (amountToCollect == -1
									|| script.inventory.getAmount(bonusName) == 0) {
								// Calculate the amount to gather
								amountToCollect = 3; // (int)
														// (tracker.timeLeft() /
														// (Constants.castSpellTime
														// +
														// Constants.takeShapeTime)
														// + 1);
							}
							if (script.inventory.getAmount(bonusName) < amountToCollect) {
								System.out.println("Getting more");
								// Not enough, get more
								bonus.interact("Take-from");
								return MethodProvider.random(300, 600);
							} else {
								script.disableAntiban();
								// Reset amountToCollect (don't collect anymore)
								amountToCollect = -2;
								// Enough, cast spell
								while (script.myPlayer().getAnimation() == -1
										&& script.tabs.getOpen() != Tab.MAGIC
										&& !script.magic.isSpellSelected()) {
									System.out.println("Opening spellbook");
									// Not doing anything and magic tab isn't
									// open?
									// Open
									// it
									script.tabs.magic.open();
									try {
										script.sleep(MethodProvider.random(400,
												700));
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
								if (script.tabs.getOpen() == Tab.MAGIC
										&& script.myPlayer().getAnimation() == -1
										&& EnchantMethods
												.inventoryContainsEnchantables(script)
										&& !script.magic.isSpellSelected()) {
									Library.castSpell(spell, script);
									for (int i = 0; i < 10
											&& script.tabs.getOpen() != Tab.INVENTORY; i++) {
										try {
											script.sleep(MethodProvider.random(
													100, 200));
										} catch (InterruptedException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									}
								} else if (script.tabs.getOpen() == Tab.INVENTORY
										&& script.magic.isSpellSelected()) {
									for (int k = script.inventory.getItems().length - 1; k >= 0; k--) {
										Item i = script.inventory.getItems()[k];
										if (i != null
												&& Constants.enchantableNames
														.contains(i.getName())) {
											Library.inventoryInteract(k, script);
											for (int j = 0; j < 10
													&& script.tabs.getOpen() != Tab.MAGIC; j++) {
												try {
													script.sleep(MethodProvider
															.random(100, 200));
												} catch (InterruptedException e) {
													// TODO Auto-generated catch
													// block
													e.printStackTrace();
												}
											}
											if (EnchantMethods
													.inventoryContainsEnchantables(script)) {
												try {
													script.magic
															.hoverSpell(spell);
												} catch (InterruptedException e) {
													e.printStackTrace();
												}
												Library.waitFor(
														new Conditional() {

															@Override
															public boolean isSatisfied(
																	Script script) {
																return script
																		.myPlayer()
																		.getAnimation() == -1;
															}

														}, 2000, 100, script);
											}
											break;
										}
									}
								}
								return 0;
							}
						} else {
							System.out.println("Moving closer");
							// Not close, move closer
							bonus.interact("Take-from");
							return MethodProvider.random(600, 1000);
						}
					}
				}
			} else {
				// Not following bonus shape
				if (currentCorner == null) {
					System.out.println("Setting corner");
					// Doesn't have a location, randomly pick one
					currentCorner = Constants.enchantmentRoomCorners[MethodProvider
							.random(0, 3)];
				} else {
					// Has a corner
					if (script.inventory.isFull()
							&& !EnchantMethods
									.inventoryContainsEnchantables(script)) {
						// Check to make sure
						while (script.tabs.getOpen() != Tab.INVENTORY) {
							script.tabs.open(Tab.INVENTORY);
							try {
								script.sleep(MethodProvider.random(400, 700));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						if (!EnchantMethods
								.inventoryContainsEnchantables(script)) {
							currentCorner = null;
							System.out.println("Depositing in hole");
							RS2Object hole = script.objects.closest("Hole");
							if (hole != null) {
								hole.interact("Deposit");
								try {
									script.sleep(MethodProvider
											.random(400, 700));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
					} else {
						// Either get more enchantables or cast the spells
						if (!script.inventory.isFull()) {
							System.out.println("Picking up stuff");
							// Needs to pick up more stuff
							if (Library.distanceTo(currentCorner, script) < 14) {
								// Close to corner
								RS2Object closest = EnchantMethods
										.getClosestEnchantable(script, false);
								if (closest != null) {
									closest.interact("Take-from");
									return MethodProvider.random(300, 600);
								}
							} else {
								System.out.println("Walking to corner");
								script.localWalker.walk(currentCorner);
								try {
									script.sleep(MethodProvider
											.random(300, 600));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						} else {
							System.out.println("Casting spells");
							script.disableAntiban();
							// If inventory full, cast the spells
							while (script.myPlayer().getAnimation() == -1
									&& script.tabs.getOpen() != Tab.MAGIC
									&& !script.magic.isSpellSelected()) {
								System.out.println("Opening spellbook");
								// Not doing anything and magic tab isn't open?
								// Open
								// it
								script.tabs.magic.open();
								try {
									script.sleep(MethodProvider
											.random(400, 700));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							if (script.tabs.getOpen() == Tab.MAGIC
									&& script.myPlayer().getAnimation() == -1
									&& !script.magic.isSpellSelected()
									&& EnchantMethods
											.inventoryContainsEnchantables(script)) {
								Library.castSpell(spell, script);
								for (int i = 0; i < 10
										&& script.tabs.getOpen() != Tab.INVENTORY; i++) {
									try {
										script.sleep(MethodProvider.random(100,
												200));
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
							} else if (script.tabs.getOpen() == Tab.INVENTORY
									&& script.magic.isSpellSelected()) {
								int k = 0;
								for (Item i : script.inventory.getItems()) {
									if (i != null
											&& Constants.enchantableNames
													.contains(i.getName())) {
										Library.inventoryInteract(k, script);
										for (int j = 0; j < 10
												&& script.tabs.getOpen() != Tab.MAGIC; j++) {
											try {
												script.sleep(MethodProvider
														.random(100, 200));
											} catch (InterruptedException e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}
										}
										if (EnchantMethods
												.inventoryContainsEnchantables(script)) {
											try {
												script.magic.hoverSpell(spell);
											} catch (InterruptedException e) {
												e.printStackTrace();
											}
											Library.waitFor(new Conditional() {

												@Override
												public boolean isSatisfied(
														Script script) {
													return script.myPlayer()
															.getAnimation() == -1;
												}

											}, 2000, 100, script);
										}
										break;
									}
									k++;
								}
							}
							return 0;
						}
					}
				}
			}
		}
		return MethodProvider.random(300, 600);
	}

	public String toString() {
		if (shouldStop)
			return "Enchantment Room. Stop at "
					+ (stopMagic ? ("Magic Level = " + magicStopAt)
							: ("Points = " + pointsStopAt));
		else
			return "Enchantment Room";
	}

	@Override
	public void onPaint(Graphics2D g) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onMessage(Message m) {
		// TODO Auto-generated method stub
		
	}
}
