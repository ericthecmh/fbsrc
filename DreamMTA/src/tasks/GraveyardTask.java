package tasks;

import java.awt.Graphics2D;

import library.GraveyardMethods;
import main.DreamMTA;

import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.Message;
import org.osbot.rs07.api.ui.RS2Interface;
import org.osbot.rs07.api.ui.RS2InterfaceChild;
import org.osbot.rs07.api.ui.Skill;
import org.osbot.rs07.api.ui.Spell;
import org.osbot.rs07.api.ui.Tab;
import org.osbot.rs07.script.MethodProvider;

import data.Constants;

public class GraveyardTask implements Task {

	private DreamMTA script;
	private final Spell spell;
	private State state;
	private int[] IDS;
	private final int eatAt;
	private final boolean shouldStop;
	private final boolean stopMagic;
	private final boolean stopPoints;
	private final int magicStopAt;
	private final int pointsStopAt;

	private enum State {
		GRAB, CAST, DEPOSIT;
	}

	public GraveyardTask(Spell spell, boolean ss, boolean sm, int msa,
			boolean ps, int spa, int eatAt) {
		IDS = new int[5];
		this.spell = spell;
		this.eatAt = eatAt;
		state = State.GRAB;
		this.shouldStop = ss;
		this.stopMagic = sm;
		this.magicStopAt = msa;
		this.stopPoints = ps;
		this.pointsStopAt = spa;
	}

	@Override
	public void setScript(DreamMTA s) {
		this.script = s;
	}

	private int getInventoryCount() {
		int total = 0;
		for (Item i : script.inventory.getItems()) {
			if (i == null)
				continue;
			for (int num = 1; num < IDS.length; num++) {
				if (i.getDefinition().getModelId() == IDS[num]) {
					total += num;
					break;
				}
			}
		}
		return total;
	}

	private int getFreeSlots() {
		int count = 0;
		for (Item i : script.inventory.getItems()) {
			if (i == null)
				continue;
			for (int num = 1; num < IDS.length; num++) {
				if (i.getDefinition().getModelId() == IDS[num]) {
					count++;
					break;
				}
			}
		}
		return script.inventory.getEmptySlots() + count;
	}

	private boolean inventoryContainsFood() {
		if (spell == Spell.BONES_TO_BANANAS) {
			return script.inventory.contains("Banana");
		} else
			return script.inventory.contains("Peach");
	}

	@Override
	public int onLoop() {
		script.enableAntiban();
		if (shouldStop) {
			if (stopMagic
					&& script.skills.getStatic(Skill.MAGIC) >= magicStopAt)
				return -1;
			if (stopPoints
					&& GraveyardMethods.getPoints(script) >= pointsStopAt)
				return -1;
		}
		if (script.npcs.closest("Graveyard Guardian") == null
				&& script.objects.closest("Bones") == null) {
			// Not in the telekinetic room
			if (Constants.lobbyRoomArea.isInAreaInclusive(script.myPosition())) {
				// if in lobby room, enter the enchantment room
				RS2Object teleport = script.objects
						.closest("Graveyard Teleport");
				if (teleport != null) {
					teleport.interact("Enter");
					try {
						script.sleep(MethodProvider.random(400, 700));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else {
				// In some other room, exit that room
				RS2Object teleport = script.objects.closest("Exit Teleport");
				if (teleport != null) {
					teleport.interact("Enter");
					try {
						script.sleep(MethodProvider.random(400, 700));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			return MethodProvider.random(400, 700);
		}
		if (IDS[0] == 0) {
			RS2Interface inte = script.interfaces.get(196);
			for (int i = 1; i < IDS.length; i++) {
				RS2InterfaceChild child = inte.getChild(i + 3);
				if (!child.isVisible())
					return 200;
				IDS[i] = child.getDisabledMediaId();
			}
			IDS[0] = 1;
		}
		if (script.skills.getDynamic(Skill.HITPOINTS) < eatAt
				&& inventoryContainsFood()) {
			while (script.skills.getDynamic(Skill.HITPOINTS) < (eatAt + 10)
					&& inventoryContainsFood()) {
				if (spell == Spell.BONES_TO_BANANAS) {
					script.inventory.interact("Eat", "Banana");
				} else {
					script.inventory.interact("Eat", "Peach");
				}
			}
		}
		switch (state) {
		case GRAB:
			if (getInventoryCount() >= getFreeSlots()) {
				state = State.CAST;
				break;
			}
			RS2Object bones = script.objects.closest("Bones");
			if (bones != null) {
				bones.interact("Grab");
				return MethodProvider.random(400, 700);
			}
			break;
		case CAST:
			script.disableAntiban();
			if (script.inventory.isFull()) {
				state = State.DEPOSIT;
				break;
			}
			if (getInventoryCount() > 0) {
				while (script.tabs.getOpen() != Tab.MAGIC) {
					script.tabs.open(Tab.MAGIC);
					try {
						script.sleep(MethodProvider.random(400, 700));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				script.magic.castSpell(spell);
			} else {
				state = State.DEPOSIT;
			}
		case DEPOSIT:
			if (script.inventory.isFull()) {
				RS2Object chute = script.objects.closest("Food chute");
				if (chute.interact("Deposit")) {
					try {
						script.sleep(MethodProvider.random(400, 700));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else {
				state = State.GRAB;
			}
			break;
		}
		return MethodProvider.random(400, 700);
	}

	public void onPaint(Graphics2D g) {
		g.drawString(state.toString(), 50, 50);
	}

	public String toString() {
		if (shouldStop)
			return "Graveyard Room. Stop at "
					+ (stopMagic ? ("Magic Level = " + magicStopAt)
							: ("Points = " + pointsStopAt));
		else
			return "Graveyard Room";
	}

	@Override
	public void onMessage(Message m) {
		// TODO Auto-generated method stub

	}

}
