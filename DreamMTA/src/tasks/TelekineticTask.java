package tasks;

import java.awt.Graphics2D;

import library.Conditional;
import library.Library;
import library.TelekineticMaze;
import library.TelekineticMethods;
import main.DreamMTA;

import org.osbot.rs07.api.filter.Filter;
import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.GroundItem;
import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.Message;
import org.osbot.rs07.api.ui.RS2Interface;
import org.osbot.rs07.api.ui.RS2InterfaceChild;
import org.osbot.rs07.api.ui.Skill;
import org.osbot.rs07.api.ui.Spell;
import org.osbot.rs07.api.ui.Tab;
import org.osbot.rs07.input.mouse.MainScreenTileDestination;
import org.osbot.rs07.input.mouse.MiniMapTileDestination;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;

import data.Constants;

/**
 * Performs Enchanting room stuff
 * 
 * @author Ericthecmh
 */
public class TelekineticTask implements Task {
	// Reference to Script
	private DreamMTA script;

	// Whether or not to stop
	private final boolean shouldStop;
	// Whether to stop for magic level
	private final boolean stopMagic;
	// Magic level to stop at, -1 if don't stop
	private final int magicStopAt;
	// Whether to stop for points count
	private final boolean stopPoints;
	// Points to stop at, -1 if don't stop
	private final int pointsStopAt;
	// Current maze
	private TelekineticMaze maze;

	public TelekineticTask(boolean ss, boolean sm, int msa, boolean sp, int psa) {
		this.shouldStop = ss;
		this.stopMagic = sm;
		this.magicStopAt = msa;
		this.stopPoints = sp;
		this.pointsStopAt = psa;
	}

	public void setScript(DreamMTA s) {
		this.script = s;
	}

	public void resetMaze() {
		maze = getRoom();
	}

	public int onLoop() {
		script.enableAntiban();
		if (shouldStop) {
			if (stopMagic
					&& script.skills.getStatic(Skill.MAGIC) >= magicStopAt)
				return -1;
			if (stopPoints
					&& TelekineticMethods.getPoints(script) >= pointsStopAt)
				return -1;
		}
		if (script.npcs.closest("Telekinetic Guardian") == null
				&& script.objects.closest(10755) == null) {
			// Not in the telekinetic room
			if (Constants.lobbyRoomArea.isInAreaInclusive(script.myPosition())) {
				// if in lobby room, enter the enchantment room
				RS2Object teleport = script.objects
						.closest("Telekinetic Teleport");
				if (teleport != null) {
					teleport.interact("Enter");
					try {
						script.sleep(MethodProvider.random(400, 700));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else {
				// In some other room, exit that room
				RS2Object teleport = script.objects.closest("Exit Teleport");
				if (teleport != null) {
					teleport.interact("Enter");
					try {
						script.sleep(MethodProvider.random(400, 700));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		} else {
			GroundItem guardianStatue = script.groundItems.closest(6888);
			if (guardianStatue == null && script.npcs.closest(5983) == null) {
				script.log("Unable to find statue. Walking to closest 10755");
				RS2Object tile = script.objects.closest(10755);
				script.localWalker.walk(tile);
				return MethodProvider.random(400, 700);
			}
			if (maze == null) {
				resetMaze();
				return 100;
			}
			NPC guardian = script.npcs.closest(5983);
			if (guardian != null) {
				guardian.interact("Talk-to");
				if (Library.waitFor(new Conditional() {

					@Override
					public boolean isSatisfied(Script script) {
						RS2Interface inte = script.interfaces.get(64);
						return inte != null && inte.isValid()
								&& inte.isVisible();
					}

				}, 2000, 100, script)) {
					if (script.dialogues.clickContinue()) {
						if (Library.waitFor(new Conditional() {

							@Override
							public boolean isSatisfied(Script script) {
								RS2Interface inte = script.interfaces.get(242);
								return inte != null && inte.isValid()
										&& inte.isVisible();
							}

						}, 2000, 100, script)) {
							if (script.dialogues.clickContinue()) {
								if (Library.waitFor(new Conditional() {

									@Override
									public boolean isSatisfied(Script script) {
										RS2Interface inte = script.interfaces
												.get(228);
										return inte != null && inte.isValid()
												&& inte.isVisible();
									}

								}, 2000, 100, script)) {
									RS2Interface inte = script.interfaces
											.get(228);
									RS2InterfaceChild child = inte.getChild(1);
									if (child.interact()) {
										if (Library.waitFor(new Conditional() {

											@Override
											public boolean isSatisfied(
													Script script) {
												RS2Interface inte = script.interfaces
														.get(64);
												return inte != null
														&& inte.isValid()
														&& inte.isVisible();
											}

										}, 2000, 100, script)) {
											if (script.dialogues
													.clickContinue()) {
												if (Library.waitFor(
														new Conditional() {

															@Override
															public boolean isSatisfied(
																	Script script) {
																RS2Interface inte = script.interfaces
																		.get(241);
																return inte != null
																		&& inte.isValid()
																		&& inte.isVisible();
															}

														}, 2000, 100, script)) {
													if (script.dialogues
															.clickContinue()) {
														maze = null;
													}
													return MethodProvider
															.random(2000, 3000);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			} else {
				return unhumanSolve();
			}
		}
		return 1000;
	}

	/**
	 * Solves the maze without regard to being humanlike
	 * 
	 * @return time to delay
	 */
	private int unhumanSolve() {
		Position p = nextPosition(maze);
		if (p == null) {
			// No position, wait
			script.log("No position, wait");
			return MethodProvider.random(500, 1000);
		} else {
			boolean onObject = false;
			RS2Object closest = script.objects.closest(10755);
			if (closest.getX() == script.myPlayer().getX()
					&& closest.getY() == script.myPlayer().getY()) {
				onObject = true;
			}
			if ((script.configs.get(629) & (1 << 30)) == 0 && onObject) {
				script.log("Not in observe view");
				// If not in observe view, open observe view
				GroundItem statue = script.groundItems.closest(6888);
				script.camera.toEntity(statue);
				if (Library.interactRock(statue, script, "Observe"))
					return MethodProvider.random(2000, 3000);
				else
					return MethodProvider.random(400, 700);
			} else {
				if (script.myPosition().getX() == p.getX()
						&& script.myPosition().getY() == p.getY()) {
					script.log("In position");
					// In position, cast spell
					while (script.tabs.getOpen() != Tab.MAGIC) {
						script.tabs.open(Tab.MAGIC);
						try {
							script.sleep(MethodProvider.random(400, 700));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					if (!script.magic.isSpellSelected()) {
						script.magic.castSpell(Spell.TELEKINETIC_GRAB);
						return MethodProvider.random(400, 700);
					} else {
						final GroundItem statue = script.groundItems
								.closest(6888);
						Library.interactRock(statue, script, "Cast");
						// Wait for move
						final int oldx = statue.getX();
						final int oldy = statue.getY();
						Library.waitFor(new Conditional() {

							@Override
							public boolean isSatisfied(Script script) {
								return statue.getX() != oldx
										|| statue.getY() != oldy;
							}

						}, 5000, 100, script);
						return MethodProvider.random(400, 700);
					}
				} else if (!script.myPlayer().isMoving()) {
					script.log("Getting in position");
					// Get in position
					final MainScreenTileDestination md = new MainScreenTileDestination(
							script.bot, p);
					if (Library.distanceTo(p, script) < 4 && md.isVisible()) {
						// Walk with main screen
						script.mouse.moveSlightly();
						script.mouse.move(md);
						if (Library.waitFor(new Conditional() {

							@Override
							public boolean isSatisfied(Script script) {
								return md.getBoundingBox().contains(
										script.mouse.getPosition());
							}

						}, 5000, 100, script)) {
							script.mouse.click(false);
						}
						return MethodProvider.random(400, 700);
					} else if (Library.distanceTo(p, script) < 14) {
						// Not visible, walk there using mm
						MiniMapTileDestination td = new MiniMapTileDestination(
								script.bot, p);
						script.mouse.click(td);
						return MethodProvider.random(400, 700);
					} else {
						script.localWalker.walk(p);
						return MethodProvider.random(400, 700);
					}
				}
			}
		}
		return MethodProvider.random(400, 700);
	}

	private TelekineticMaze getRoom() {
		GroundItem statue = script.groundItems.closest(6888);
		RS2Object portal = script.objects.closest("Exit Teleport");
		if (statue != null && portal != null) {
			int x = portal.getX() - statue.getX();
			if (x == 12)
				return TelekineticMaze.MAZE1;
			if (x == 14)
				return TelekineticMaze.MAZE2;
			if (x == 13)
				return TelekineticMaze.MAZE3;
			if (x == 7)
				return TelekineticMaze.MAZE4;
			if (x == 10)
				return TelekineticMaze.MAZE5;
			if (x == 21)
				return TelekineticMaze.MAZE6;
			if (x == 1)
				return TelekineticMaze.MAZE7;
			if (x == 3)
				return TelekineticMaze.MAZE8;
			if (x == -8)
				return TelekineticMaze.MAZE9;
			if (x == 5)
				return TelekineticMaze.MAZE10;
		}
		return null;
	}

	public Position nextPosition(TelekineticMaze maze) {
		GroundItem statue = script.groundItems.closest(6888);
		RS2Object portal = script.objects.closest("Exit Teleport");
		if (statue == null || portal == null)
			return null;
		int dx = portal.getX() - statue.getX();
		int dy = portal.getY() - statue.getY();
		switch (maze) {
		case MAZE1:
			if (dx == 12 && dy == 4)
				return span('N', statue.getPosition());
			if (dx == 12 && dy == 3)
				return span('E', statue.getPosition());
			if (dx == 10 && dy == 3)
				return span('N', statue.getPosition());
			if (dx == 10 && dy == -1)
				return span('W', statue.getPosition());
			if (dx == 14 && dy == -1)
				return span('N', statue.getPosition());
			if (dx == 14 && dy == -3)
				return span('E', statue.getPosition());
			if (dx == 7 && dy == -3)
				return span('N', statue.getPosition());
			if (dx == 7 && dy == -4)
				return span('W', statue.getPosition());
			break;
		case MAZE2:
			if (dx == 14 && dy == -17)
				return span('S', statue.getPosition());
			if (dx == 14 && dy == -12)
				return span('W', statue.getPosition());
			if (dx == 15 && dy == -12)
				return span('S', statue.getPosition());
			if (dx == 15 && dy == -10)
				return span('E', statue.getPosition());
			if (dx == 14 && dy == -10)
				return span('S', statue.getPosition());
			if (dx == 14 && dy == -8)
				return span('E', statue.getPosition());
			if (dx == 11 && dy == -8)
				return span('N', statue.getPosition());
			if (dx == 11 && dy == -16)
				return span('W', statue.getPosition());
			if (dx == 15 && dy == -16)
				return span('N', statue.getPosition());
			break;
		case MAZE3:
			if (dx == 13 && dy == -4)
				return span('S', statue.getPosition());
			if (dx == 13 && dy == 5)
				return span('W', statue.getPosition());
			if (dx == 14 && dy == 5)
				return span('N', statue.getPosition());
			if (dx == 14 && dy == -3)
				return span('W', statue.getPosition());
			if (dx == 16 && dy == -3)
				return span('S', statue.getPosition());
			if (dx == 16 && dy == 3)
				return span('W', statue.getPosition());
			if (dx == 20 && dy == 3)
				return span('S', statue.getPosition());
			if (dx == 20 && dy == 5)
				return span('W', statue.getPosition());
			break;
		case MAZE4:
			if (dx == 7 && dy == 12)
				return span('S', statue.getPosition());
			if (dx == 7 && dy == 21)
				return span('E', statue.getPosition());
			if (dx == 3 && dy == 21)
				return span('N', statue.getPosition());
			if (dx == 3 && dy == 15)
				return span('W', statue.getPosition());
			if (dx == 5 && dy == 15)
				return span('S', statue.getPosition());
			if (dx == 5 && dy == 18)
				return span('E', statue.getPosition());
			if (dx == -2 && dy == 18)
				return span('N', statue.getPosition());
			break;
		case MAZE5:
			if (dx == 10 && dy == 10)
				return span('E', statue.getPosition());
			if (dx == 6 && dy == 10)
				return span('S', statue.getPosition());
			if (dx == 6 && dy == 15)
				return span('W', statue.getPosition());
			if (dx == 11 && dy == 15)
				return span('N', statue.getPosition());
			if (dx == 11 && dy == 10)
				return span('W', statue.getPosition());
			if (dx == 15 && dy == 10)
				return span('N', statue.getPosition());
			if (dx == 15 && dy == 6)
				return span('E', statue.getPosition());
			break;
		case MAZE6:
			if (dx == 21 && dy == 6)
				return span('N', statue.getPosition());
			if (dx == 21 && dy == -3)
				return span('W', statue.getPosition());
			if (dx == 24 && dy == -3)
				return span('S', statue.getPosition());
			if (dx == 24 && dy == 5)
				return span('W', statue.getPosition());
			if (dx == 27 && dy == 5)
				return span('N', statue.getPosition());
			if (dx == 27 && dy == -3)
				return span('W', statue.getPosition());
			if (dx == 28 && dy == -3)
				return span('S', statue.getPosition());
			if (dx == 28 && dy == 6)
				return span('W', statue.getPosition());
			if (dx == 30 && dy == 6)
				return span('N', statue.getPosition());
			break;
		case MAZE7:
			if (dx == 1 && dy == 19)
				return span('N', statue.getPosition());
			if (dx == 1 && dy == 16)
				return span('E', statue.getPosition());
			if (dx == 0 && dy == 16)
				return span('S', statue.getPosition());
			if (dx == 0 && dy == 19)
				return span('E', statue.getPosition());
			if (dx == -3 && dy == 19)
				return span('S', statue.getPosition());
			if (dx == -3 && dy == 21)
				return span('W', statue.getPosition());
			if (dx == 0 && dy == 21)
				return span('N', statue.getPosition());
			if (dx == 0 && dy == 20)
				return span('E', statue.getPosition());
			if (dx == -4 && dy == 20)
				return span('N', statue.getPosition());
			if (dx == -4 && dy == 15)
				return span('W', statue.getPosition());
			break;
		case MAZE8:
			if (dx == 3 && dy == -7)
				return span('N', statue.getPosition());
			if (dx == 3 && dy == -16)
				return span('E', statue.getPosition());
			if (dx == 0 && dy == -16)
				return span('S', statue.getPosition());
			if (dx == 0 && dy == -7)
				return span('E', statue.getPosition());
			if (dx == -3 && dy == -7)
				return span('N', statue.getPosition());
			if (dx == -3 && dy == -16)
				return span('E', statue.getPosition());
			if (dx == -6 && dy == -16)
				return span('S', statue.getPosition());
			break;
		case MAZE9:
			if (dx == -8 && dy == 13)
				return span('N', statue.getPosition());
			if (dx == -8 && dy == 10)
				return span('W', statue.getPosition());
			if (dx == -4 && dy == 10)
				return span('N', statue.getPosition());
			if (dx == -4 && dy == 8)
				return span('E', statue.getPosition());
			if (dx == -7 && dy == 8)
				return span('N', statue.getPosition());
			if (dx == -7 && dy == 6)
				return span('W', statue.getPosition());
			if (dx == -5 && dy == 6)
				return span('S', statue.getPosition());
			if (dx == -5 && dy == 7)
				return span('W', statue.getPosition());
			if (dx == -3 && dy == 7)
				return span('N', statue.getPosition());
			if (dx == -3 && dy == 4)
				return span('E', statue.getPosition());
			break;
		case MAZE10:
			if (dx == 5 && dy == 9)
				return span('N', statue.getPosition());
			if (dx == 5 && dy == 6)
				return span('W', statue.getPosition());
			if (dx == 7 && dy == 6)
				return span('S', statue.getPosition());
			if (dx == 7 && dy == 10)
				return span('E', statue.getPosition());
			if (dx == 3 && dy == 10)
				return span('N', statue.getPosition());
			if (dx == 3 && dy == 5)
				return span('W', statue.getPosition());
			if (dx == 9 && dy == 5)
				return span('S', statue.getPosition());
			if (dx == 9 && dy == 13)
				return span('E', statue.getPosition());
			break;
		}
		return null;
	}

	private Position span(char direction, Position p) {
		int dx = 0;
		int dy = 0;
		switch (direction) {
		case 'N':
			dx = 0;
			dy = 1;
			break;
		case 'W':
			dx = -1;
			dy = 0;
			break;
		case 'S':
			dx = 0;
			dy = -1;
			break;
		case 'E':
			dx = 1;
			dy = 0;
			break;
		default:
			return null;
		}
		int count = 0;
		for (int i = 1; i < 100; i++) {
			final Position newP = new Position(p.getX() + i * dx, p.getY() + i
					* dy, 0);
			RS2Object o = script.objects.closest(new Filter<RS2Object>() {

				@Override
				public boolean match(RS2Object arg0) {
					return arg0.getX() == newP.getX()
							&& arg0.getY() == newP.getY();
				}

			});
			if (o != null && o.getId() == 10755) {
				count = 0;
			} else
				count++;
			if (count == 10) {
				return new Position(newP.getX() - count * dx, newP.getY()
						- count * dy, 0);
			}
		}
		return null;
	}

	public String toString() {
		if (shouldStop)
			return "Telekinetic Room. Stop at "
					+ (stopMagic ? ("Magic Level = " + magicStopAt)
							: ("Points = " + pointsStopAt));
		else
			return "Telekinetic Room";
	}

	@Override
	public void onPaint(Graphics2D g) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMessage(Message m) {
		// TODO Auto-generated method stub

	}
}
