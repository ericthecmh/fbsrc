package tasks;

import java.awt.Graphics2D;

import main.DreamMTA;

import org.osbot.rs07.api.ui.Message;

public interface Task {
	public void setScript(DreamMTA s);

	public int onLoop();

	public String toString();

	public void onPaint(Graphics2D g);

	public void onMessage(Message m);
}
