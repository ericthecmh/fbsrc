package data;

import java.util.HashSet;

import library.Area;

import org.osbot.rs07.api.map.Position;

public class Constants {
	/************************ LOBBY ROOM **************************/
	public final static Area lobbyRoomArea = new Area(3356, 3298, 3370, 3322);

	/********************* ENCHANTMENT ROOM ***********************/
	public final static float takeShapeTime = 0.65384615f;
	public final static float castSpellTime = 2.8f;
	public final static Area enchantmentRoomArea = new Area(3386, 9616, 3341,
			9662);
	public final static Position[] enchantmentRoomCorners = new Position[] {
			new Position(3350, 9653, 0), new Position(3379, 9652, 0),
			new Position(3376, 9626, 0), new Position(3349, 9627, 0) };
	public final static HashSet<String> enchantableObjects = new HashSet<String>();
	public final static HashSet<String> enchantableNames = new HashSet<String>();
	static {
		enchantableObjects.add("Pentamid Pile");
		enchantableObjects.add("Cube Pile");
		enchantableObjects.add("Cylinder Pile");
		enchantableObjects.add("Icosahedron Pile");
		enchantableNames.add("Pentamid");
		enchantableNames.add("Cube");
		enchantableNames.add("Cylinder");
		enchantableNames.add("Icosahedron");
		enchantableNames.add("Dragonstone");
	}

}
