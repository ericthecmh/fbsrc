package data;

import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.api.ui.RS2Interface;
import org.osbot.rs07.api.ui.RS2InterfaceChild;
import org.osbot.rs07.api.ui.Spell;
import org.osbot.rs07.api.ui.Tab;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.script.ScriptManifest;

public class CollectData extends Script {
	public int onLoop() {

		for (int i = 8; i < 12; i++) {
			RS2Interface inte = interfaces.get(195);
			RS2InterfaceChild child = inte.getChild(i);
			log((child == null) + "");
			if (child != null) {
				log(i + ": " + child.isVisible());
			}
		}
		return 1000;
	}

	public int castSpell() {
		if (tabs.getOpen() == Tab.MAGIC && myPlayer().getAnimation() == -1) {
			magic.castSpell(Spell.LVL_1_ENCHANT);
			for (int i = 0; i < 10 && tabs.getOpen() != Tab.INVENTORY; i++) {
				try {
					sleep(random(100, 200));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} else if (tabs.getOpen() == Tab.INVENTORY) {
			for (Item i : inventory.getItems()) {
				if (i != null && i.getName().equals("Pentamid")) {
					i.interact("Cast");
					for (int j = 0; j < 10 && tabs.getOpen() != Tab.MAGIC; j++) {
						try {
							sleep(random(100, 200));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					break;
				}
			}
		}
		return 0;
	}
}
