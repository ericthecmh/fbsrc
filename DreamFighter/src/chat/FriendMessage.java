package chat;

/**
 *
 * @author Ericthecmh
 */
public class FriendMessage {

    public String username;
    public String message;

    public FriendMessage(String u, String m) {
        this.username = u;
        this.message = m;
    }
}
