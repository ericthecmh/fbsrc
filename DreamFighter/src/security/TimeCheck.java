package security;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

public class TimeCheck {
	public static boolean isValid() {
		try {
			return (Math.abs(System.currentTimeMillis() - getTime()) < 86400000);
		} catch (Exception e) {

		}
		return false;
	}

	public static long getTime() {
		try {
			URL url = new URL("http://osbot.org/");
			BufferedReader br = new BufferedReader(new InputStreamReader(
					url.openStream()));
			String s;
			while ((s = br.readLine()) != null) {
				return Long.parseLong(s, 10);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return -10000000;
	}
}
