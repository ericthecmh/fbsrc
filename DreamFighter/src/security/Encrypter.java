package security;

public class Encrypter {
	public static byte[] getBytes(String string) {
		byte[] bytes = new byte[string.length()];
		for (int i = 0; i < string.length(); i++) {
			bytes[i] = (byte) string.charAt(i);
		}
		return bytes;
	}

	public static byte[] flip(byte[] key) {
		byte[] newbytes = new byte[key.length];
		for (int i = 0; i < key.length; i++) {
			newbytes[key.length - i - 1] = key[i];
		}
		return newbytes;
	}

	public static byte[] encrypt(String string, String keyString) {
		byte[] data = getBytes(string);
		byte[] key = flip(getBytes(keyString));
		byte[] newData = new byte[string.length()];
		for (int i = 0; i < string.length(); i++) {
			newData[i] = (byte) (data[i] ^ key[i % key.length]);
			System.out.println(data[i] + "^" + key[i % key.length] + " = "
					+ newData[i]);
		}
		System.out.println();
		return newData;
	}

	public static byte[] decrypt(String string, String keyString) {
		byte[] key = flip(getBytes(keyString));
		byte[] data = getBytes(string);
		byte[] newData = new byte[string.length()];
		for (int i = 0; i < string.length(); i++) {
			newData[i] = (byte) (data[i] ^ key[i % key.length]);
			System.out.println(data[i] + "^" + key[i % key.length] + " = "
					+ newData[i]);
		}
		System.out.println();
		return newData;
	}
}
