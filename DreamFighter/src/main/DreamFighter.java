package main;

import gui.DreamFighterGeneralGUI;
import gui.path.PathElement;
import gui.path.PositionElement;
import gui.path.TeletabElement;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import library.WorldHopping;
import library.BankItem;
import library.FightableNPC;
import library.Library;
import library.LootItem;
import library.PotionItem;

import org.osbot.BotApplication;
import org.osbot.rs07.antiban.AntiBan.BehaviorType;
import org.osbot.rs07.api.Client;
import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.api.ui.Message;
import org.osbot.rs07.api.ui.PrayerButton;
import org.osbot.rs07.api.ui.Skill;
import org.osbot.rs07.api.util.ExperienceTracker;
import org.osbot.rs07.api.util.GraphicUtilities;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.RandomBehaviourHook;
import org.osbot.rs07.script.RandomEvent;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.script.ScriptManifest;

import actions.BankAction;
import actions.FightAction;
import actions.WalkAction;
import chat.ChatWindow;
import chat.FriendMessage;

/**
 * 
 * Main class for DreamFighter
 * 
 * @author Eliot, Ericthecmh
 * 
 */
@ScriptManifest(name = "DreamFighter v0.2.3", author = "Eliot & Ericthecmh", info = "The best and only AIO combat script you will ever need!\nWritten by Ericthecmh and Eliot\nVersion 0.2.3", logo = "", version = 0.23)
public class DreamFighter extends Script {

    /**
     * ************************** CONSANTS *******************************
     */
    // Update if we change the format of the settings file
	public final String settingsVersion = "0";

	// Script version
	public final String scriptVersion = "0.2.3";

	// Probability of antiban activating
	public final double antibanRate = 0.002;

	// Use custom interact method in FightAction
	public final boolean useCustomInteract = false;
	// Use custom bank method in BankAction
	public final boolean useCustomBank = true;
	// Used to hop and check botworld
	private final WorldHopping worldHopping = new WorldHopping(this);

    private int enableRunAt = MethodProvider.random(35, 55);

    // Prayer list
    List<PrayerButton> prayers;

    // DO NOT TOUCH THIS
    public final static String ip3 = "179";

	// enum of states that the script could be in
	public enum State {
		BANK, FIGHT, WALK_TO_BANK, WALK_FROM_BANK;
	}

	/**
	 * ************************** VARIABLES ******************************
	 */
	// Current state of the script
	public State state;

	// References to action classes
	private FightAction fightAction;
	private BankAction bankAction;
	private WalkAction walkBankAction;
	private WalkAction walkFightAction;

	// Keeps track of last movement time
	private long lastPlayerUpdate;

	// Keeps track of whether the script is stopping/should stop
	private boolean stopScript;

	// GUI
	DreamFighterGeneralGUI gui;

	// Set to true when gui is done and script can start
	private boolean guiFinished;

	// Whether onLoop can start executing
	private boolean canStart;

	// Keeps track of NPC locations. Update every 700 milliseconds
	public HashMap<NPC, Position> locationTracker;
	public HashMap<NPC, Position> locationTrackerOld;

	// values being grabbed from the GUI
	public String foodType;
	public int foodAmount;
	public int eatBelowHP;
	public ArrayList<LootItem> lootList = new ArrayList<LootItem>();
	public ArrayList<LootItem> highPriorityLootList = new ArrayList<LootItem>();
	public ArrayList<FightableNPC> attackList = new ArrayList<FightableNPC>();
	public ArrayList<BankItem> bankItems = new ArrayList<BankItem>();
	public ArrayList<PotionItem> potions = new ArrayList<PotionItem>();
	public boolean bonesToPeaches = false;
	public boolean useRange;
	public boolean canReach;
	public boolean useSafespot;
	public Position safespot;
	public int wieldAmount;
	public String ammoName;
	public boolean eatFood;
	public int monstersKilled;
	public boolean useDynamicSig;
	public String username;
	public boolean bankWhenFull;
	public boolean buryBones;

	// Antiban
	private ChatWindow chatWindow;

	// paint stuff
	private final Color black = new Color(0, 0, 0, 165);
	private final Color green = new Color(51, 255, 51);
	private final Font paintFont = new Font("Arial", 0, 11);
	private final BasicStroke stroke = new BasicStroke(1.0F);
	private final Color green_outline = new Color(51, 255, 51, 125);
	private boolean window1 = false;
	private boolean window2 = false;
	long startTime;
	int startSpot = 339;
	ExperienceTracker exp = new ExperienceTracker();

	private boolean shouldAFK = false;
	private long lastAFKTime;
	private int AFKTimeDelay;

	public class BotMouseListener implements MouseListener {
		@Override
		public void mouseClicked(MouseEvent e) {
			if (e.getPoint().x >= 173 && e.getPoint().x <= 222 && e.getPoint().y >= 319 && e.getPoint().y <= 335 && !window1) {
				window1 = true;
			}
			if (e.getPoint().x >= 227 && e.getPoint().x <= 275 && e.getPoint().y >= 457 && e.getPoint().y <= 472 && window1) {
				window1 = false;
			}
			if (e.getPoint().x >= 353 && e.getPoint().x <= 402 && e.getPoint().y >= 319 && e.getPoint().y <= 335 && !window2) {
				window2 = true;
			}
			if (e.getPoint().x >= 407 && e.getPoint().x <= 455 && e.getPoint().y >= 457 && e.getPoint().y <= 472 && window2) {
				window2 = false;
			}
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub

		}

	}

	/**
	 * Initializes the script, required variables, and actions
	 */
	@Override
	public void onStart() {
		log("Welcome to DreamFighter!");
		log("Brought to you by the great scripters Eliot and Ericthecmh");
		MouseListener bml = new BotMouseListener();
		bot.addMouseListener(bml);
		// start experience trackers
		experienceTracker.startAll();
		fightAction = new FightAction(this);
		bankAction = new BankAction();
		walkBankAction = new WalkAction(State.BANK);
		walkFightAction = new WalkAction(State.FIGHT);
		prayers = new LinkedList<PrayerButton>();

		// gui
		gui = new DreamFighterGeneralGUI(this);
		gui.setVisible(true);

		guiFinished = false;
		canStart = true;

		// Disable antiban
		antiBan.unregisterBehavior(BehaviorType.CAMERA_SHIFT);
		antiBan.unregisterBehavior(BehaviorType.OTHER);
		antiBan.unregisterBehavior(BehaviorType.RANDOM_MOUSE_MOVEMENT);
		antiBan.unregisterBehavior(BehaviorType.SLIGHT_MOUSE_MOVEMENT);
	}

	/**
	 * determines the initial state based on proximity to path points
	 * 
	 * @return the starting state
	 */
	private State determineInitialState() {
		if (gui.pathToBank == null || gui.pathFromBank == null) {
			// If paths are not complete, script will only fight
			return State.FIGHT;
		} else {
			// Get first position element in path
			PositionElement e = null;
			for (PathElement p : gui.pathFromBank) {
				if (p instanceof PositionElement) {
					e = (PositionElement) p;
					break;
				}
			}
			if (e == null) {
				// Couldn't find a position element, script will only fight
				return State.FIGHT;
			}
			if (Library.distanceTo(new Position(e.getX(), e.getY(), myPosition().getZ()), this) < 10) {
				// If the first position element is in range, you're at the
				// fight area
				return State.BANK;
			}
		}
		// Couldn't find where you are, script will only fight
		return State.FIGHT;
	}

	/**
	 * Calls the appropriate execute method based on the state
	 */
	@Override
	public int onLoop() {
        try {
            bot.getRandomExecutor().registerHook(new RandomBehaviourHook(RandomEvent.EXP_REWARDS) {
                public boolean shouldActivate() {
                    return solver.shouldActivate() && !myPlayer().isUnderAttack();
                }
            });
        }catch(Exception e){

        }
        try {
            bot.getRandomExecutor().registerHook(new RandomBehaviourHook(RandomEvent.MYSTERIOUS_BOX) {
                public boolean shouldActivate() {
                    return solver.shouldActivate() && !myPlayer().isUnderAttack();
                }
            });
        }catch(Exception e){

        }
		if (settings.getRunEnergy() > enableRunAt && !settings.isRunning()) {
			settings.setRunning(true);
            try {
                sleep(MethodProvider.random(500,800));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            enableRunAt = MethodProvider.random(35, 55);
		}
		mouse.setSpeed(MethodProvider.random(7, 10));
		if (canStart) {
			// If debugging path walking, then wait for path gui to be closed
			if (!guiFinished) {
				if (gui.isVisible()) {
					return 100;
				}
				lastPlayerUpdate = System.currentTimeMillis();
				new Thread(new Runnable() {
					@Override
					public void run() {
						while (!stopScript) {
							try {
								if (myPlayer().isMoving() || myPlayer().getAnimation() != -1) {
									lastPlayerUpdate = System.currentTimeMillis();
								}
								Thread.sleep(500);
							} catch (Exception e) {

							}
						}
					}
				}).start();
				walkBankAction.setPath(gui.pathToBank);
				walkFightAction.setPath(gui.pathFromBank);
				// Determine the initial state
				state = determineInitialState();
				if (gui.chatWindow.isSelected()) {
					chatWindow = new ChatWindow();
					chatWindow.setVisible(true);
				}
				if (gui.enableAFK.isSelected()) {
					lastAFKTime = System.currentTimeMillis();
					AFKTimeDelay = MethodProvider.random(5 * 60 * 1000, 20 * 60 * 1000);
					shouldAFK = true;
					log("AFK Enabled");
					log("First AFK: " + AFKTimeDelay / 1000 + " seconds from now");
				}
				for (int i = 0; i < gui.prayerListModel.size(); i++) {
					prayers.add((PrayerButton) gui.prayerListModel.get(i));
				}
				if (gui.useSpecial.isSelected()) {
					int specialAt = 150;
					try {
						specialAt = Integer.parseInt(gui.specialAt.getText());
					} catch (Exception e) {

					}
					fightAction.enableSpecial(specialAt, gui.switchSpecial.isSelected(), gui.specialName.getText(), gui.regularName.getText());
				}
				buryBones = gui.buryBones.isSelected();
				startTime = System.currentTimeMillis();
				guiFinished = true;
				new Thread(new ServerUpdater()).start();
			}
			if (chatWindow != null) {
				String playerMessage = chatWindow.getNextPlayerMessage();
				if (playerMessage != null) {
					this.keyboard.typeString(playerMessage);
				}
				String clanMessage = chatWindow.getNextClanMessage();
				if (clanMessage != null) {
					this.keyboard.typeString("/" + clanMessage);
				}
				FriendMessage fm = chatWindow.getNextFriendMessage();
				if (fm != null) {
					log("Send message");
					Library.sendFriendMessage(fm.username, fm.message, this);
				}
			}
			if (shouldAFK && System.currentTimeMillis() - lastAFKTime > AFKTimeDelay) {
				lastAFKTime = System.currentTimeMillis();
				AFKTimeDelay = MethodProvider.random(5 * 60 * 1000, 20 * 60 * 1000);
				int time = MethodProvider.random(5000, 20000);
				log("Going AFK for " + time / 1000 + " seconds");
				mouse.moveOutsideScreen();
				while (System.currentTimeMillis() - lastAFKTime < time) {
					try {
						sleep(MethodProvider.random(100, 200));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				log("Resuming");
				log("Next AFK: " + (AFKTimeDelay - time) / 1000 + " seconds from now");
			}
			if (client.getLoginState() == Client.LoginState.LOGGED_IN && worldHopping.isInBotWorld()) {
				worldHopping.switchWorlds();
			}
            if(dialogues.isPendingContinuation()){
                dialogues.clickContinue();
                return MethodProvider.random(500,800);
            }
			switch (state) {
			case FIGHT:
				for (PrayerButton b : prayers) {
					if (!prayer.isActivated(b) && skills.getDynamic(Skill.PRAYER) > 0) {
						prayer.set(b, true);
						try {
							sleep(MethodProvider.random(400, 700));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				fightAction.execute(this);
				break;
			case BANK:
				for (PrayerButton b : prayers) {
					if (prayer.isActivated(b)) {
						prayer.set(b, false);
						try {
							sleep(MethodProvider.random(400, 700));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				bankAction.execute(this);
				break;
			case WALK_TO_BANK:
				for (PrayerButton b : prayers) {
					if (prayer.isActivated(b)) {
						prayer.set(b, false);
						try {
							sleep(MethodProvider.random(400, 700));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				walkBankAction.execute(this);
				break;
			case WALK_FROM_BANK:
				for (PrayerButton b : prayers) {
					if (prayer.isActivated(b)) {
						prayer.set(b, false);
						try {
							sleep(MethodProvider.random(400, 700));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				walkFightAction.execute(this);
				break;
			}
		}
		// ab.switchWorlds();
		return 100;
	}

	/**
	 * Exits the script, closes down resources, and prints a nice message to the
	 * user
	 */
	@Override
	public void onExit() {
		stopScript = true;
		log("Thank you for using DreamFighter! Please post progress reports and suggestions in our thread. Thank you!");
	}

	/**
	 * Gets the last moving time (in milliseconds)
	 * 
	 * @return last moving time
	 */
	public long getLastUpdateTime() {
		return this.lastPlayerUpdate;
	}

	/**
	 * Draws the paint
	 */
	@Override
	public void onPaint(Graphics2D g) {

		if (canStart) {
			// If debugging path and the path selection gui is showing, then
			// draw
			// the path on the minimap
			if (!guiFinished && gui != null && gui.pathPanel != null) {
				try {
					final LinkedList<PathElement> pathElements = gui.pathPanel.getPathElements();
					PositionElement old = null;
					for (PathElement p : pathElements) {
						// Draw the positions in the minimap and connect them.
						if (p.getType() == PathElement.Type.POSITION) {
							g.setColor(Color.GREEN);
							short[] coords = GraphicUtilities.getMinimapScreenCoordinate(bot, ((PositionElement) p).getX(), ((PositionElement) p).getY());
							// If the coords[0] == -1, the position isn't in the
							// MM
							if (coords[0] != -1) {
								g.fillRect(coords[0] - 2, coords[1] - 2, 5, 5);
								if (old != null) {
									short[] oldcoords = GraphicUtilities.getMinimapScreenCoordinate(bot, old.getX(), old.getY());
									if (oldcoords[0] != -1) {
										g.drawLine(coords[0], coords[1], oldcoords[0], oldcoords[1]);
										g.fillRect(oldcoords[0] - 2, oldcoords[1] - 2, 5, 5);
									}
								}
								old = (PositionElement) p;
							}
						} else {
							old = null;
						}
						// Get the selected path element in the GUI's list, if
						// any
						PathElement selected = gui.pathPanel.getSelectedPathElement();
						if (selected != null) {
							if (selected.getType() == PathElement.Type.POSITION) {
								// Draw the selected position as red in the
								// minimap
								g.setColor(Color.RED);
								short[] coords = GraphicUtilities.getMinimapScreenCoordinate(bot, ((PositionElement) selected).getX(), ((PositionElement) selected).getY());
								if (coords[0] != -1) {
									g.fillRect(coords[0] - 2, coords[1] - 2, 5, 5);
								}
								g.setColor(Color.GREEN);
							}
						}
					}
				} catch (Exception e) {

				}
			}
		}

		if (!guiFinished)
			return;
		// paint
		Graphics2D gr = g;
		int totalGP = 0;
		for (LootItem item : lootList) {
			if (item.avgPrice > 0) {
				totalGP += item.avgPrice * item.lootedAmount;
			}
		}
		long runTime = System.currentTimeMillis() - startTime;
		int currentExp = experienceTracker.getGainedXP(Skill.ATTACK) + experienceTracker.getGainedXP(Skill.STRENGTH) + experienceTracker.getGainedXP(Skill.DEFENCE) + experienceTracker.getGainedXP(Skill.HITPOINTS) + experienceTracker.getGainedXP(Skill.RANGED) + experienceTracker.getGainedXP(Skill.MAGIC);
		int currentExpPerHour = experienceTracker.getGainedXPPerHour(Skill.ATTACK) + experienceTracker.getGainedXPPerHour(Skill.STRENGTH) + experienceTracker.getGainedXPPerHour(Skill.DEFENCE) + experienceTracker.getGainedXPPerHour(Skill.HITPOINTS) + experienceTracker.getGainedXPPerHour(Skill.RANGED) + experienceTracker.getGainedXPPerHour(Skill.MAGIC);
		gr.setColor(black);
		gr.fillRoundRect(7, 307, 503, 30, 6, 6);
		gr.setColor(green_outline);
		gr.drawRoundRect(7, 307, 503, 30, 6, 6);
		// text
		gr.setStroke(this.stroke);
		gr.setColor(green);
		gr.setFont(paintFont);
		gr.drawString("Runtime: " + format(runTime), 14, 318);
		if (state != null)
			gr.drawString("State: " + state.toString(), 14, 332);
		gr.drawString("Total EXP [P/H]: " + currentExp + " [" + currentExpPerHour + "]", 182, 318);
		gr.drawString("Total GP: " + totalGP, 362, 318);
		gr.setRenderingHints(new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON));
		if (window1) {
			gr.setColor(black);
			gr.fillRoundRect(175, 339, 165, 135, 6, 6);
			gr.setColor(green_outline);
			gr.drawRoundRect(175, 339, 165, 135, 6, 6);
			gr.setColor(green);
			gr.setStroke(this.stroke);
			gr.setFont(paintFont);
			gr.drawString("Collapse", 232, 471);
			gr.drawString("Attack [P/H]: " + experienceTracker.getGainedXP(Skill.ATTACK) + " [" + experienceTracker.getGainedXPPerHour(Skill.ATTACK) + "]", 182, 355);
			gr.drawString("Strength [P/H]: " + experienceTracker.getGainedXP(Skill.STRENGTH) + " [" + experienceTracker.getGainedXPPerHour(Skill.STRENGTH) + "]", 182, 375);
			gr.drawString("Defence [P/H]: " + experienceTracker.getGainedXP(Skill.DEFENCE) + " [" + experienceTracker.getGainedXPPerHour(Skill.DEFENCE) + "]", 182, 395);
			gr.drawString("Hitpoints [P/H]: " + experienceTracker.getGainedXP(Skill.HITPOINTS) + " [" + experienceTracker.getGainedXPPerHour(Skill.HITPOINTS) + "]", 182, 415);
			gr.drawString("Ranged [P/H]: " + experienceTracker.getGainedXP(Skill.RANGED) + " [" + experienceTracker.getGainedXPPerHour(Skill.RANGED) + "]", 182, 435);
			gr.drawString("Magic [P/H]: " + experienceTracker.getGainedXP(Skill.MAGIC) + " [" + experienceTracker.getGainedXPPerHour(Skill.MAGIC) + "]", 182, 455);

		} else {
			gr.setColor(green);
			gr.setStroke(this.stroke);
			gr.setFont(paintFont);
			gr.drawString("Expand", 182, 332);
		}
		if (window2) {
			gr.setColor(black);
			gr.fillRoundRect(355, 339, 155, 135, 6, 6);
			gr.setColor(green_outline);
			gr.drawRoundRect(355, 339, 155, 135, 6, 6);
			gr.setColor(green);
			gr.setStroke(this.stroke);
			gr.setFont(paintFont);
			gr.drawString("GP/hour: " + (int) (totalGP * (3600000.0D / runTime)), 362, 332);
			int y = 355;
			for (LootItem item : lootList) {
				if (item.avgPrice > 0) {
					gr.drawString(item.name + " [GP]: " + item.lootedAmount + " [" + item.avgPrice * item.lootedAmount + "]", 362, y);
					y += 20;
				}
			}
			gr.drawString("Collapse", 412, 471);
		} else {
			gr.setColor(green);
			gr.setStroke(this.stroke);
			gr.setFont(paintFont);
			gr.drawString("Expand", 362, 332);
		}
	}

	/**
	 * sets the food type
	 * 
	 * @param type
	 */
	public void setFoodType(String type) {
		foodType = type;

	}

	/**
	 * sets potion items
	 * 
	 * @param item
	 */
	public void setPotions(PotionItem item) {
		potions.add(item);
	}

	/**
	 * set food amount
	 * 
	 * @param amount
	 */
	public void setFoodAmount(int amount) {
		foodAmount = amount;

	}

	/**
	 * adds the items to loot with their priority
	 * 
	 * @param lootItem
	 */
	public void addToLootTable(LootItem lootItem) {
		lootList.add(lootItem);
		if (lootItem.highPriority) {
			highPriorityLootList.add(lootItem);
			log(lootItem.name);
		}
	}

	/**
	 * add item to bank items
	 * 
	 * @param bItem
	 */
	public void addToBankTable(BankItem bItem) {
		bankItems.add(bItem);
	}

	/**
	 * sets the eat below hp
	 * 
	 * @param hp
	 */
	public void setEatBelow(int hp) {
		eatBelowHP = hp;

	}

	/**
	 * adds the NPCs to the attackList
	 * 
	 * @param fightableNPC
	 */
	public void addToAttackList(FightableNPC fightableNPC) {
		attackList.add(fightableNPC);

	}

	/**
	 * add the bank items
	 */
	public void createBankItems(LinkedList<PathElement> path1, LinkedList<PathElement> path2) {
		HashMap<String, Integer> teletabMap = new HashMap<String, Integer>();
		if (path1 != null) {
			for (PathElement e : path1) {
				if (e.getType() == PathElement.Type.TELETAB) {
					if (teletabMap.containsKey(((TeletabElement) e).getName())) {
						teletabMap.put(((TeletabElement) e).getName(), teletabMap.get(((TeletabElement) e).getName()) + 1);
					} else {
						teletabMap.put(((TeletabElement) e).getName(), 1);
					}
				}
			}
			for (PathElement e : path2) {
				if (e.getType() == PathElement.Type.TELETAB) {
					if (teletabMap.containsKey(((TeletabElement) e).getName())) {
						teletabMap.put(((TeletabElement) e).getName(), teletabMap.get(((TeletabElement) e).getName()) + 1);
					} else {
						teletabMap.put(((TeletabElement) e).getName(), 1);
					}
				}
			}
		}
		for (String teletab : teletabMap.keySet()) {
			bankItems.add(new BankItem(teletab, teletabMap.get(teletab)));
		}
		if (bonesToPeaches) {
			bankItems.add(new BankItem("Bones to peaches", 100));
		}
		if (foodType != null && !bonesToPeaches) {
			BankItem food = new BankItem(foodType, foodAmount);
			if (bankItems != null) {
				bankItems.add(food);
			}
		}
	}

	String format(final long time) {
		final StringBuilder t = new StringBuilder();
		final long total_secs = time / 1000;
		final long total_mins = total_secs / 60;
		final long total_hrs = total_mins / 60;
		final int secs = (int) total_secs % 60;
		final int mins = (int) total_mins % 60;
		final int hrs = (int) total_hrs;
		if (hrs < 10)
			t.append("0");
		t.append(hrs + ":");
		if (mins < 10)
			t.append("0");
		t.append(mins + ":");
		if (secs < 10) {
			t.append("0");
			t.append(secs);
		} else
			t.append(secs);
		return t.toString();
	}

	class ServerUpdater implements Runnable {

		long oldrun = 0;
		int oldxp = skills.getExperience(Skill.ATTACK) + skills.getExperience(Skill.STRENGTH) + skills.getExperience(Skill.DEFENCE) + skills.getExperience(Skill.RANGED) + skills.getExperience(Skill.MAGIC) + skills.getExperience(Skill.HITPOINTS);
		int oldmk = 0;
		int oldgp = 0;

		public void run() {
			while (!stopScript) {
				if (guiFinished) {
					long run = (long) ((System.currentTimeMillis() - startTime) / 1000);
					int currentExp = skills.getExperience(Skill.ATTACK) + skills.getExperience(Skill.STRENGTH) + skills.getExperience(Skill.DEFENCE) + skills.getExperience(Skill.RANGED) + skills.getExperience(Skill.MAGIC) + skills.getExperience(Skill.HITPOINTS);
					int totalGP = 0;
					for (LootItem item : lootList) {
						if (item.avgPrice > 0) {
							totalGP += item.avgPrice * item.lootedAmount;
						}
					}
					String prepString = "" + System.nanoTime();
					prepString = prepString.substring(prepString.length() - 6);
					String runtime = Library.toHex(Library.encrypt(prepString + (run - oldrun), "DSSOSBOT"));
					prepString = "" + System.nanoTime();
					prepString = prepString.substring(prepString.length() - 6);
					String expgained = Library.toHex(Library.encrypt(prepString + (currentExp - oldxp), "DSSOSBOT"));
					prepString = "" + System.nanoTime();
					prepString = prepString.substring(prepString.length() - 6);
					String monsterskilled = Library.toHex(Library.encrypt(prepString + (monstersKilled - oldmk), "DSSOSBOT"));
					prepString = "" + System.nanoTime();
					prepString = prepString.substring(prepString.length() - 6);
					String gplooted = Library.toHex(Library.encrypt(prepString + (totalGP - oldgp), "DSSOSBOT"));
					oldrun = run;
					oldxp = currentExp;
					oldmk = monstersKilled;
					oldgp = totalGP;
					String valid = Library.toHex(Library.encrypt("VALID", expgained));
					try {
						URL url = new URL("http://dreamscripts.org/updatesig.php?Script=DreamFighter&Username=" + BotApplication.getInstance().getOSAccount().username + "&Runtime=" + runtime + "&MonstersKilled=" + monsterskilled + "&Expgained=" + expgained + "&GPLooted=" + gplooted + "&Valid=" + valid);
						BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
						System.out.println(br.readLine());
					} catch (Exception e) {
					}
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}

	public void updateTime() {
		lastPlayerUpdate = System.currentTimeMillis();
	}

	public void onMessage(Message m) {
		if (chatWindow != null)
			try {
				chatWindow.addMessage(m.getMessage(), m.getTypeId(), m.getUsername());
			} catch (Exception e) {
			}
		if(state == State.FIGHT && canStart && System.currentTimeMillis() - startTime > 1000){
			fightAction.onMessage(m);
		}
	}

}