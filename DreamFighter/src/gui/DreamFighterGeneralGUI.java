/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import gui.path.ItemObjectElement;
import gui.path.ObjectElement;
import gui.path.PathElement;
import gui.path.PositionElement;
import gui.path.SSDoorElement;
import gui.path.TeletabElement;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.Scanner;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import library.BankItem;
import library.FightableNPC;
import library.LootItem;
import library.PotionItem;
import main.DreamFighter;
import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.ui.PrayerButton;

/**
 *
 * @author Eliot,Ericthecmh
 */
public class DreamFighterGeneralGUI extends javax.swing.JFrame {

	// Reference to script
	private DreamFighter script;

	// Table model for the attack table
	private DefaultTableModel attackTableModel;
	// Table model for the loot table
	private DefaultTableModel lootTableModel;
	// Table model for the potions table
	private DefaultTableModel potionsTableModel;
	// List model for the prayer list
	public DefaultListModel prayerListModel;
	// Combobox model for prayer
	private DefaultComboBoxModel prayerBoxModel;

	// Path selecion panel. null when no path selecion gui showing
	public DreamFighterPathPanel pathPanel;

	// List of path elements from bank
	public LinkedList<PathElement> pathFromBank;
	// List of path elements to bank
	public LinkedList<PathElement> pathToBank;
	// Safespot position
	private Position safespot;

	// File chooser to save profiles
	private JFileChooser settingsChooser;

	/**
	 * Creates new form DreamFighterGeneralGUI
	 */
	public DreamFighterGeneralGUI(DreamFighter s) {
		this.script = s;
		initComponents();
		attackTableModel = (DefaultTableModel) attackTable.getModel();
		lootTableModel = (DefaultTableModel) lootTable.getModel();
		potionsTableModel = (DefaultTableModel) potionsTable.getModel();
		prayerListModel = new DefaultListModel();
		prayerList.setModel(prayerListModel);
		buttonGroup1.add(highPriority);
		buttonGroup1.add(lowPriority);
		if (System.getProperty("os.name").contains("indows")) {
			settingsChooser = new JFileChooser(System.getProperty("user.home") + "\\OSBot\\Scripts");
		} else {
			settingsChooser = new JFileChooser(System.getProperty("user.home") + "/OSBot/Scripts");
		}
		buttonGroup2.add(yesEat);
		buttonGroup2.add(noEat);
		prayerBoxModel = (DefaultComboBoxModel) prayerComboBox.getModel();
		prayerBoxModel.addElement(PrayerButton.THICK_SKIN);
		prayerBoxModel.addElement(PrayerButton.BURST_OF_STRENGTH);
		prayerBoxModel.addElement(PrayerButton.CLARITY_OF_THOUGHT);
		prayerBoxModel.addElement(PrayerButton.SHARP_EYE);
		prayerBoxModel.addElement(PrayerButton.MYSTIC_WILL);
		prayerBoxModel.addElement(PrayerButton.ROCK_SKIN);
		prayerBoxModel.addElement(PrayerButton.SUPERHUMAN_STRENGTH);
		prayerBoxModel.addElement(PrayerButton.IMPROVED_REFLEXES);
		prayerBoxModel.addElement(PrayerButton.RAPID_RESTORE);
		prayerBoxModel.addElement(PrayerButton.RAPID_HEAL);
		prayerBoxModel.addElement(PrayerButton.PROTECT_ITEM);
		prayerBoxModel.addElement(PrayerButton.HAWK_EYE);
		prayerBoxModel.addElement(PrayerButton.MYSTIC_LORE);
		prayerBoxModel.addElement(PrayerButton.STEEL_SKIN);
		prayerBoxModel.addElement(PrayerButton.ULTIMATE_STRENGTH);
		prayerBoxModel.addElement(PrayerButton.INCREDIBLE_REFLEXES);
		prayerBoxModel.addElement(PrayerButton.PROTECT_FROM_MAGIC);
		prayerBoxModel.addElement(PrayerButton.PROTECT_FROM_MISSILES);
		prayerBoxModel.addElement(PrayerButton.PROTECT_FROM_MELEE);
		prayerBoxModel.addElement(PrayerButton.EAGLE_EYE);
		prayerBoxModel.addElement(PrayerButton.MYSTIC_MIGHT);
		prayerBoxModel.addElement(PrayerButton.RETRIBUTION);
		prayerBoxModel.addElement(PrayerButton.REDEMPTION);
		prayerBoxModel.addElement(PrayerButton.SMITE);
		prayerBoxModel.addElement(PrayerButton.CHIVALRY);
		prayerBoxModel.addElement(PrayerButton.PIETY);
		jTextArea3.setText("Enter the name of the ammunition in the \"Name of ammunition\" field. Make sure\nthe name is exactly as shown in Runescape (eg. Iron arrow).\n\nThe \"Amount to weild at\" is the amount of ammunition to be in inventory before the\nscript equips it. The script will also equip if your quiver runs out of ammunition.\n\nPressing \"Set current position as safespot\" sets your character's current position\nas the safespot.");

		jTextArea1.setText("To add an NPC to the attack list, click the\nAdd button. A popup will appear prompting\nfor the NPC's info. To remove an NPC entry,\nselect the entry and press Remove.\n\nIf you would like to eat food, select the \"Eat\nFood\" checkbox and enter the food\ninformation. Make sure the food name is\nexactly as shown in Runescape (eg. Lobster,\nnot lobster, or lobbies, etc)");

		jTextArea2.setText("To add a loot item into the loot table\nenter it's name (or ID) into the \n\"Loot Name/ID\" field.\n\nIf a loot item has high priority, it will\nbe picked up as soon as it appears.\nOtherwise, the script will pick it up\nonce it finishes attacking the\ncurrent NPC. If \"Eat of space\" is\n\"Yes\", the script will eat to make\nroom for the loot.");

		jTextArea5.setText("If you want to be able to chat without opening OSBot, select \"Enable chat window.\" The script will open a separate window allowing chat. It will also notify you of a incoming message, if you enabled the notifications features.\n\nIf you want even more antiban, you can enable AFK, which will randomly move your mouse off the screen and AFK for 15-45 seconds.");

	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
	// <editor-fold defaultstate="collapsed"
	// <editor-fold defaultstate="collapsed"
	// <editor-fold defaultstate="collapsed"
	// <editor-fold defaultstate="collapsed"
	// <editor-fold defaultstate="collapsed"
	// <editor-fold defaultstate="collapsed"
	// <editor-fold defaultstate="collapsed"
	// <editor-fold defaultstate="collapsed"
	// <editor-fold defaultstate="collapsed"
	// <editor-fold defaultstate="collapsed"
	// <editor-fold defaultstate="collapsed"
	// <editor-fold defaultstate="collapsed"
	// <editor-fold defaultstate="collapsed"
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        combatPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        attackTable = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        withdrawCount = new javax.swing.JTextField();
        eatAtHP = new javax.swing.JTextField();
        foodName = new javax.swing.JTextField();
        jTextArea1 = new javax.swing.JTextArea();
        eatFood = new javax.swing.JCheckBox();
        bonesToPeaches = new javax.swing.JCheckBox();
        bankWhenFull = new javax.swing.JCheckBox();
        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        lootTable = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        lootName = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        highPriority = new javax.swing.JRadioButton();
        lowPriority = new javax.swing.JRadioButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jTextArea2 = new javax.swing.JTextArea();
        jLabel12 = new javax.swing.JLabel();
        yesEat = new javax.swing.JRadioButton();
        noEat = new javax.swing.JRadioButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jButton6 = new javax.swing.JButton();
        pathToBankStatus = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        pathFromBankStatus = new javax.swing.JLabel();
        jButton7 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        ammoName = new javax.swing.JTextField();
        wieldAmount = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jButton10 = new javax.swing.JButton();
        safespotLabel = new javax.swing.JLabel();
        jTextArea3 = new javax.swing.JTextArea();
        useRange = new javax.swing.JCheckBox();
        useSafespot = new javax.swing.JCheckBox();
        canReach = new javax.swing.JCheckBox();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        potionsTable = new javax.swing.JTable();
        jLabel13 = new javax.swing.JLabel();
        jButton11 = new javax.swing.JButton();
        jButton12 = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        prayerComboBox = new javax.swing.JComboBox();
        jScrollPane4 = new javax.swing.JScrollPane();
        prayerList = new javax.swing.JList();
        jLabel19 = new javax.swing.JLabel();
        jButton13 = new javax.swing.JButton();
        jButton14 = new javax.swing.JButton();
        buryBones = new javax.swing.JCheckBox();
        jPanel7 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTextArea5 = new javax.swing.JTextArea();
        enableAFK = new javax.swing.JCheckBox();
        chatWindow = new javax.swing.JCheckBox();
        jPanel9 = new javax.swing.JPanel();
        useSpecial = new javax.swing.JCheckBox();
        switchSpecial = new javax.swing.JCheckBox();
        specialName = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        specialAt = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        regularName = new javax.swing.JTextField();
        jButton5 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("DreamFighter");
        setResizable(false);

        jLabel1.setText("Attack list:");

        attackTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "NPC Name", "Level"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(attackTable);

        jButton1.setText("Add");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Remove");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel2.setText("Food name:");
        jLabel2.setEnabled(false);

        jLabel3.setText("Eat below HP:");
        jLabel3.setEnabled(false);

        jLabel4.setText("# to withdraw:");
        jLabel4.setEnabled(false);

        withdrawCount.setEnabled(false);

        eatAtHP.setEnabled(false);

        foodName.setEnabled(false);

        jTextArea1.setEditable(false);
        jTextArea1.setBackground(new Color(0.0f,0.0f,0.0f,0.0f));
        jTextArea1.setColumns(10);
        jTextArea1.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        jTextArea1.setRows(5);
        jTextArea1.setText("To add an NPC to the attack list, click the\nAdd button. A popup will appear prompting\nfor the NPC's info. To remove an NPC entry,\nselect the entry and press Remove.\n\nIf you would like to eat food, select the \"Eat\nFood\" checkbox and enter the food\ninformation. Make sure the food name is\nexactly as shown in Runescape (eg. Lobster,\nnot lobster, or lobbies, etc)");
        jTextArea1.setWrapStyleWord(true);

        eatFood.setText("Eat Food");
        eatFood.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eatFoodActionPerformed(evt);
            }
        });

        bonesToPeaches.setText("Use Bones to Peaches");
        bonesToPeaches.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bonesToPeachesActionPerformed(evt);
            }
        });

        bankWhenFull.setText("Bank when inventory full");
        bankWhenFull.setEnabled(false);

        javax.swing.GroupLayout combatPanelLayout = new javax.swing.GroupLayout(combatPanel);
        combatPanel.setLayout(combatPanelLayout);
        combatPanelLayout.setHorizontalGroup(
            combatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(combatPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(combatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(combatPanelLayout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(combatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(combatPanelLayout.createSequentialGroup()
                        .addGroup(combatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextArea1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(combatPanelLayout.createSequentialGroup()
                                .addGroup(combatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(bonesToPeaches)
                                    .addComponent(eatFood))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())
                    .addGroup(combatPanelLayout.createSequentialGroup()
                        .addGroup(combatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(combatPanelLayout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addGroup(combatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel2))
                                .addGap(16, 16, 16))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, combatPanelLayout.createSequentialGroup()
                                .addGroup(combatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(bankWhenFull)
                                    .addComponent(jLabel4))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addGroup(combatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(foodName)
                            .addComponent(withdrawCount)
                            .addComponent(eatAtHP))
                        .addGap(31, 31, 31))))
        );
        combatPanelLayout.setVerticalGroup(
            combatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(combatPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(combatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(combatPanelLayout.createSequentialGroup()
                        .addComponent(bonesToPeaches)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(eatFood)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(combatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(foodName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(combatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(eatAtHP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(combatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(withdrawCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bankWhenFull)
                        .addGap(30, 30, 30)
                        .addComponent(jTextArea1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(combatPanelLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 271, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(combatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton2, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jButton1))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Combat", combatPanel);

        jLabel5.setText("Loot Table:");

        lootTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Loot Name", "Priority", "Eat for Space"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(lootTable);

        jLabel6.setText("Loot Name/ID:");

        lootName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lootNameActionPerformed(evt);
            }
        });

        jLabel7.setText("Priority:");

        highPriority.setText("High");

        lowPriority.setSelected(true);
        lowPriority.setText("Low");

        jButton3.setText("Add");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("Remove");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jTextArea2.setEditable(false);
        jTextArea2.setBackground(new Color(0.0f,0.0f,0.0f,0.0f));
        jTextArea2.setColumns(10);
        jTextArea2.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        jTextArea2.setRows(5);
        jTextArea2.setText("To add a loot item into the loot table\nenter it's name (or ID) into the \n\"Loot Name/ID\" field.\n\nIf a loot item has high priority, it will\nbe picked up as soon as it appears.\nOtherwise, the script will pick it up\nonce it finishes attacking the\ncurrent NPC. If \"Eat of space\" is\n\"Yes\", the script will eat to make\nroom for the loot.");
        jTextArea2.setWrapStyleWord(true);

        jLabel12.setText("Eat for space:");

        yesEat.setText("Yes");

        noEat.setSelected(true);
        noEat.setText("No");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 264, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(24, 24, 24)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(highPriority)
                            .addComponent(lowPriority)
                            .addComponent(jLabel7)
                            .addComponent(jLabel6))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel12)
                                    .addComponent(yesEat)
                                    .addComponent(noEat))
                                .addGap(64, 64, 64))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lootName))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jTextArea2, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(lootName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(jLabel12))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(highPriority)
                            .addComponent(yesEat))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lowPriority)
                            .addComponent(noEat))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextArea2))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 271, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton4)
                        .addGap(0, 17, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Loot", jPanel1);

        jLabel8.setText("Path to Bank:");

        jButton6.setText("Edit");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        pathToBankStatus.setText("Not set");

        jLabel9.setText("Path from Bank:");

        pathFromBankStatus.setText("Not set");

        jButton7.setText("Edit");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(jLabel8))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pathToBankStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pathFromBankStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(297, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jButton6)
                    .addComponent(pathToBankStatus))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jButton7)
                    .addComponent(pathFromBankStatus))
                .addContainerGap(296, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Pathing", jPanel2);

        jLabel10.setText("Name of ammunition:");

        ammoName.setEnabled(false);

        wieldAmount.setEnabled(false);

        jLabel11.setText("Amount to weild at:");

        jButton10.setText("Set current position as safespot");
        jButton10.setEnabled(false);
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        safespotLabel.setText("Current safespot:");

        jTextArea3.setEditable(false);
        jTextArea3.setBackground(new Color(0.0f,0.0f,0.0f,0.0f));
        jTextArea3.setColumns(10);
        jTextArea3.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        jTextArea3.setRows(5);
        jTextArea3.setText("Enter the name of the ammunition in the \"Name of ammunition\" field. Make sure\nthe name is exactly as shown in Runescape (eg. Iron arrow).\n\nThe \"Amount to weild at\" is the amount of ammunition to be in inventory before the\nscript equips it. The script will also equip if your quiver runs out of ammunition.\n\nPressing \"Set current position as safespot\" sets your character's current position\nas the safespot.");
        jTextArea3.setWrapStyleWord(true);

        useRange.setText("Enabled range support");
        useRange.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                useRangeActionPerformed(evt);
            }
        });

        useSafespot.setText("Use safe spot");
        useSafespot.setEnabled(false);
        useSafespot.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                useSafespotActionPerformed(evt);
            }
        });

        canReach.setText("Don't use canReach check (check this box for caged monsters or other similar configurations)");
        canReach.setEnabled(false);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10)
                            .addComponent(jLabel11))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ammoName)
                            .addComponent(wieldAmount)))
                    .addComponent(jTextArea3, javax.swing.GroupLayout.DEFAULT_SIZE, 507, Short.MAX_VALUE)
                    .addComponent(jButton10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(canReach)
                            .addComponent(useRange)
                            .addComponent(useSafespot)
                            .addComponent(safespotLabel))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(useRange)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(ammoName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(wieldAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addComponent(canReach)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(useSafespot)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(safespotLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextArea3, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jTabbedPane1.addTab("Range", jPanel4);

        potionsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "Withdraw Amount", "Drink at # above"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane3.setViewportView(potionsTable);

        jLabel13.setText("Potions:");

        jButton11.setText("Remove");
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });

        jButton12.setText("Add");
        jButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton12ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 507, Short.MAX_VALUE)
                    .addComponent(jButton11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jButton12, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 270, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton11))
        );

        jTabbedPane1.addTab("Potions", jPanel5);

        jLabel18.setText("Prayer to use:");

        jScrollPane4.setViewportView(prayerList);

        jLabel19.setText("Prayer List:");

        jButton13.setText("Add to List");
        jButton13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton13ActionPerformed(evt);
            }
        });

        jButton14.setText("Remove from List");
        jButton14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton14ActionPerformed(evt);
            }
        });

        buryBones.setText("Bury bones (Note: You must have Bones in the loot table. DO NOT ENABLE THIS AND B2P SUPPORT)");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4)
                    .addComponent(jButton13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(prayerComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel19)
                            .addComponent(buryBones))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(prayerComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 205, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(buryBones)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Prayer", jPanel8);

        jLabel17.setText("Default antiban is always enabled");

        jTextArea5.setEditable(false);
        jTextArea5.setColumns(20);
        jTextArea5.setLineWrap(true);
        jTextArea5.setRows(5);
        jTextArea5.setText("If you want to be able to chat without opening OSBot, select \"Enable chat window.\" The script will open a separate window allowing chat. It will also notify you of a incoming message, if you enabled the notifications features.\n\nIf you want even more antiban, you can enable AFK, which will randomly move your mouse off the screen and AFK for 15-45 seconds.");
        jTextArea5.setWrapStyleWord(true);
        jScrollPane5.setViewportView(jTextArea5);

        enableAFK.setText("Enable AFK");

        chatWindow.setText("Enable chat window");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(enableAFK)
                    .addComponent(jLabel17)
                    .addComponent(chatWindow))
                .addContainerGap(356, Short.MAX_VALUE))
            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel7Layout.createSequentialGroup()
                    .addGap(184, 184, 184)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 333, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel17)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(chatWindow)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(enableAFK)
                .addContainerGap(286, Short.MAX_VALUE))
            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel7Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 337, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        jTabbedPane1.addTab("Antiban", jPanel7);

        useSpecial.setText("Use Special Attack?");
        useSpecial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                useSpecialActionPerformed(evt);
            }
        });

        switchSpecial.setText("Switch weapons for special attack?");
        switchSpecial.setEnabled(false);
        switchSpecial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                switchSpecialActionPerformed(evt);
            }
        });

        specialName.setEnabled(false);

        jLabel20.setText("Name of special attack weapon:");

        jButton8.setText("Save Settings");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jButton9.setText("Load Settings");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        jLabel14.setText("Activate at:");

        jLabel15.setText("%");

        jLabel16.setText("Name of normal weapon:");

        regularName.setEnabled(false);

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton8, javax.swing.GroupLayout.DEFAULT_SIZE, 507, Short.MAX_VALUE)
                    .addComponent(jButton9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel9Layout.createSequentialGroup()
                                        .addComponent(useSpecial)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel14))
                                    .addComponent(switchSpecial))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(specialAt, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel15))
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addGap(21, 21, 21)
                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel20)
                                    .addComponent(jLabel16))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(regularName)
                                    .addComponent(specialName, javax.swing.GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(useSpecial)
                    .addComponent(jLabel14)
                    .addComponent(specialAt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(switchSpecial)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(specialName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(regularName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 192, Short.MAX_VALUE)
                .addComponent(jButton8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton9)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Special Attack/Save+Load", jPanel9);

        jButton5.setFont(new java.awt.Font("Pristina", 0, 24)); // NOI18N
        jButton5.setText("Start!");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
            .addComponent(jButton5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void useSpecialActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_useSpecialActionPerformed
		switchSpecial.setEnabled(useSpecial.isSelected());
		specialName.setEnabled(useSpecial.isSelected() && switchSpecial.isSelected());
		regularName.setEnabled(useSpecial.isSelected() && switchSpecial.isSelected());
	}// GEN-LAST:event_useSpecialActionPerformed

	private void switchSpecialActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_switchSpecialActionPerformed
		specialName.setEnabled(useSpecial.isSelected() && switchSpecial.isSelected());
		regularName.setEnabled(useSpecial.isSelected() && switchSpecial.isSelected());
	}// GEN-LAST:event_switchSpecialActionPerformed

	private void jButton14ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton14ActionPerformed

	}// GEN-LAST:event_jButton14ActionPerformed

	private void jButton13ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton13ActionPerformed
		PrayerButton pb = (PrayerButton) prayerComboBox.getSelectedItem();
		if (!prayerListModel.contains(pb)) {
			prayerListModel.addElement(pb);
		}
	}// GEN-LAST:event_jButton13ActionPerformed

	private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton11ActionPerformed
		if (potionsTable.getSelectedRow() != -1) {
			potionsTableModel.removeRow(potionsTable.getSelectedRow());
		}
	}// GEN-LAST:event_jButton11ActionPerformed

	private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton12ActionPerformed
		DreamFighterPotionsPanel panel = new DreamFighterPotionsPanel();
		JOptionPane pane = new JOptionPane(panel, JOptionPane.PLAIN_MESSAGE);
		JDialog dialog = pane.createDialog("Potion Selector");
		dialog.setVisible(true);
		int withdrawAmount = panel.getWithdrawAmount();
		int drinkThreshold = panel.getDrinkThreshold();
		if (withdrawAmount == -1 || drinkThreshold == -1) {
			return;
		}
		potionsTableModel.addRow(new Object[] { panel.getPotionName(), withdrawAmount, drinkThreshold });
	}// GEN-LAST:event_jButton12ActionPerformed

	private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton10ActionPerformed
		safespot = new Position(script.myPosition().getX(), script.myPosition().getY(), script.myPosition().getZ());
		safespotLabel.setText("Current safespot: " + safespot.getX() + " " + safespot.getY());
	}// GEN-LAST:event_jButton10ActionPerformed

	private void useRangeActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_useRangeActionPerformed
		ammoName.setEnabled(useRange.isSelected());
		wieldAmount.setEnabled(useRange.isSelected());
		useSafespot.setEnabled(useRange.isSelected());
		canReach.setEnabled(useRange.isSelected());
		jButton10.setEnabled(useRange.isSelected() && useSafespot.isSelected());
	}// GEN-LAST:event_useRangeActionPerformed

	private void useSafespotActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_useSafespotActionPerformed
		jButton10.setEnabled(useRange.isSelected() && useSafespot.isSelected());
	}// GEN-LAST:event_useSafespotActionPerformed

	private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton8ActionPerformed
		int val = settingsChooser.showSaveDialog(this);
		if (val == JFileChooser.APPROVE_OPTION) {
			saveSettings(settingsChooser.getSelectedFile());
		}
	}// GEN-LAST:event_jButton8ActionPerformed

	private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton9ActionPerformed
		int val = settingsChooser.showOpenDialog(this);
		if (val == JFileChooser.APPROVE_OPTION) {
			loadSettings(settingsChooser.getSelectedFile());
		}
	}// GEN-LAST:event_jButton9ActionPerformed

	private void bonesToPeachesActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_bonesToPeachesActionPerformed
		if (!bonesToPeaches.isSelected()) {
			eatFood.setEnabled(true);
			foodName.setEnabled(eatFood.isSelected());
			eatAtHP.setEnabled(eatFood.isSelected());
			withdrawCount.setEnabled(eatFood.isSelected());
			bankWhenFull.setEnabled(true);
		} else {
			eatFood.setEnabled(false);
			foodName.setEnabled(false);
			eatAtHP.setEnabled(true);
			withdrawCount.setEnabled(false);
			bankWhenFull.setEnabled(false);
		}
	}// GEN-LAST:event_bonesToPeachesActionPerformed

	/**
	 * Prompts user for NPC name and Level
	 *
	 * @param evt
	 */
	private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton1ActionPerformed
		// Panel to hold elements
		final JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(2, 2));
		final JLabel nameIDLabel = new JLabel("NPC Name:");
		// Textfield for name/ID input
		final JTextField nameIDField = new JTextField();
		// Label for interact action text
		final JLabel levelLabel = new JLabel("Level:");
		// Textfield for interact action
		final JTextField levelField = new JTextField();
		panel.add(nameIDLabel);
		panel.add(nameIDField);
		panel.add(levelLabel);
		panel.add(levelField);
		int response = JOptionPane.showConfirmDialog(null, panel, "Please input the NPC information:", JOptionPane.OK_CANCEL_OPTION);
		if (response == JOptionPane.OK_OPTION) {
			int level = -1;
			try {
				level = Integer.parseInt(levelField.getText());
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Unable to parse NPC level");
				return;
			}
			attackTableModel.addRow(new Object[] { nameIDField.getText(), level });
		}
	}// GEN-LAST:event_jButton1ActionPerformed

	private void eatFoodActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_eatFoodActionPerformed
		jLabel2.setEnabled(eatFood.isSelected());
		jLabel3.setEnabled(eatFood.isSelected());
		jLabel4.setEnabled(eatFood.isSelected());
		foodName.setEnabled(eatFood.isSelected());
		eatAtHP.setEnabled(eatFood.isSelected());
		withdrawCount.setEnabled(eatFood.isSelected());
	}// GEN-LAST:event_eatFoodActionPerformed

	private void lootNameActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_lootNameActionPerformed
		// TODO add your handling code here:
	}// GEN-LAST:event_lootNameActionPerformed

	/**
	 * Removes the selected row from the attack table
	 *
	 * @param evt
	 */
	private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton2ActionPerformed
		if (attackTable.getSelectedRow() != -1) {
			attackTableModel.removeRow(attackTable.getSelectedRow());
		}
	}// GEN-LAST:event_jButton2ActionPerformed

	/**
	 * Adds the loot data into the loot table
	 *
	 * @param evt
	 */
	private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton3ActionPerformed
		lootTableModel.addRow(new Object[] { lootName.getText(), highPriority.isSelected() ? "High" : "Low", yesEat.isSelected() });
	}// GEN-LAST:event_jButton3ActionPerformed

	/**
	 * Removes the selected row from the loot table
	 *
	 * @param evt
	 */
	private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton4ActionPerformed
		if (lootTable.getSelectedRow() != -1) {
			lootTableModel.removeRow(lootTable.getSelectedRow());
		}
	}// GEN-LAST:event_jButton4ActionPerformed

	/**
	 * Show path selection GUI using JOptionPane. Sets the path from bank
	 *
	 * @param evt
	 */
	private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton6ActionPerformed
		pathPanel = new DreamFighterPathPanel(this.script);
		JOptionPane pane = new JOptionPane(pathPanel, JOptionPane.PLAIN_MESSAGE);
		pane.setOptions(new Object[] {});
		JDialog dialog = pane.createDialog("Path Selector");
		dialog.setModal(false);
		dialog.setVisible(true);
		if (pathToBank != null) {
			pathPanel.setPathElements(pathToBank);
		}
		dialog.addWindowListener(new WindowListener() {

			@Override
			public void windowOpened(WindowEvent e) {
			}

			@Override
			public void windowClosing(WindowEvent e) {
				if (pathPanel == null) {
					return;
				}
				pathToBank = pathPanel.getPathElements();
				if (pathToBank == null || pathToBank.size() == 0) {
					pathToBank = null;
					pathToBankStatus.setText("Not set");
					return;
				}
				pathToBankStatus.setText("Set");
				pathPanel = null;
			}

			@Override
			public void windowClosed(WindowEvent e) {
			}

			@Override
			public void windowIconified(WindowEvent e) {
			}

			@Override
			public void windowDeiconified(WindowEvent e) {
			}

			@Override
			public void windowActivated(WindowEvent e) {
			}

			@Override
			public void windowDeactivated(WindowEvent e) {
			}

		});
	}// GEN-LAST:event_jButton6ActionPerformed

	/**
	 * Show path selection GUI using JOptionPane. Sets the path from bank
	 *
	 * @param evt
	 */
	private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton7ActionPerformed
		pathPanel = new DreamFighterPathPanel(this.script);
		JOptionPane pane = new JOptionPane(pathPanel, JOptionPane.PLAIN_MESSAGE);
		pane.setOptions(new Object[] {});
		JDialog dialog = pane.createDialog("Path Selector");
		dialog.setModal(false);
		dialog.setVisible(true);
		if (pathFromBank != null) {
			pathPanel.setPathElements(pathFromBank);
		}
		dialog.addWindowListener(new WindowListener() {

			@Override
			public void windowOpened(WindowEvent e) {
			}

			@Override
			public void windowClosing(WindowEvent e) {
				if (pathPanel == null) {
					return;
				}
				pathFromBank = pathPanel.getPathElements();
				if (pathFromBank == null || pathFromBank.size() == 0) {
					pathFromBank = null;
					pathFromBankStatus.setText("Not set");
					return;
				}
				pathFromBankStatus.setText("Set");
				pathPanel = null;
			}

			@Override
			public void windowClosed(WindowEvent e) {
			}

			@Override
			public void windowIconified(WindowEvent e) {
			}

			@Override
			public void windowDeiconified(WindowEvent e) {
			}

			@Override
			public void windowActivated(WindowEvent e) {
			}

			@Override
			public void windowDeactivated(WindowEvent e) {
			}

		});
	}// GEN-LAST:event_jButton7ActionPerformed

	private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton5ActionPerformed
		// Set bones to peaches
		script.bonesToPeaches = this.bonesToPeaches.isSelected();
		// set the food type and the amount of food
		if (eatFood.isSelected() && !script.bonesToPeaches) {
			this.script.eatFood = true;
			this.script.setFoodType(foodName.getText());
			try {
				this.script.setFoodAmount(Integer.parseInt(withdrawCount.getText()));
			} catch (Exception e) {
			}
			try {
				this.script.setEatBelow(Integer.parseInt(eatAtHP.getText()));
			} catch (Exception e) {
			}
		}
		if (script.bonesToPeaches) {
			this.script.setFoodType("Peach");
			this.script.setFoodAmount(0);
			try {
				this.script.setEatBelow(Integer.parseInt(eatAtHP.getText()));
			} catch (Exception e) {
			}
		}
		library.PriceFetcher priceFetcher = new library.PriceFetcher();

		// add potions to bank table
		for (int i = 0; i < potionsTableModel.getRowCount(); i++) {
			this.script.addToBankTable(new BankItem((String) potionsTableModel.getValueAt(i, 0), (int) potionsTable.getValueAt(i, 1)));
			this.script.setPotions(new PotionItem((String) potionsTableModel.getValueAt(i, 0), (int) potionsTable.getValueAt(i, 2)));
		}

		// add elements to loot table
		for (int i = 0; i < lootTable.getRowCount(); i++) {
			int cost = 0;
			try {
				String theItem = (String) lootTable.getValueAt(i, 0);
				cost = (theItem.equals("Coins") ? 1 : priceFetcher.getPrice(theItem));
			} catch (Exception e) {

			}
			this.script.addToLootTable(new LootItem((String) lootTable.getValueAt(i, 0), determinePriority((String) lootTable.getValueAt(i, 1)), (Boolean) lootTable.getValueAt(i, 2), cost));
		}
		if (script.bonesToPeaches) {
			this.script.addToLootTable(new LootItem("Bones", false, false, 0));
		}
		// add elements to npc list
		for (int i = 0; i < attackTable.getRowCount(); i++) {
			this.script.addToAttackList(new FightableNPC((String) attackTable.getValueAt(i, 0), Integer.parseInt(attackTable.getValueAt(i, 1) + "")));
		}
		// Create bank list
		this.script.createBankItems(this.pathFromBank, this.pathToBank);
		// Do range stuff
		this.script.useRange = useRange.isSelected();
		this.script.canReach = canReach.isSelected();
		if (this.script.useRange) {
			this.script.addToLootTable(new LootItem(ammoName.getText(), false, false, 0));
			this.script.ammoName = ammoName.getText();
			this.script.wieldAmount = Integer.parseInt(wieldAmount.getText());
			this.script.useSafespot = useSafespot.isSelected();
			if (useSafespot.isSelected()) {
				this.script.safespot = safespot;
			}
		}
		this.script.bankWhenFull = bankWhenFull.isSelected();
		this.dispose();
	}// GEN-LAST:event_jButton5ActionPerformed

	private void loadSettings(File file) {
		try {
			Scanner scan = new Scanner(file);
			while (attackTableModel.getRowCount() != 0) {
				attackTableModel.removeRow(0);
			}
			while (lootTableModel.getRowCount() != 0) {
				lootTableModel.removeRow(0);
			}
			while (potionsTableModel.getRowCount() != 0) {
				potionsTableModel.removeRow(0);
			}
			String string = null;
			while (!(string = scan.nextLine()).equals("Done")) {
				int level = Integer.parseInt(scan.nextLine());
				attackTableModel.addRow(new Object[] { string, level });
			}
			if (scan.nextInt() == 1) {
				eatFood.setSelected(true);
			} else {
				eatFood.setSelected(false);
			}
			scan.nextLine();
			foodName.setText(scan.nextLine());
			eatAtHP.setText(scan.nextLine());
			withdrawCount.setText(scan.nextLine());
			foodName.setEnabled(eatFood.isSelected());
			eatAtHP.setEnabled(eatFood.isSelected());
			withdrawCount.setEnabled(eatFood.isSelected());
			while (!(string = scan.nextLine()).equals("Done")) {
				String priority = scan.nextLine();
				boolean eat = Boolean.parseBoolean(scan.nextLine());
				lootTableModel.addRow(new Object[] { string, priority, eat });
			}
			pathToBank = new LinkedList<PathElement>();
			while (!(string = scan.nextLine()).equals("Done")) {
                            System.out.println(string);
				if (string.equals("POSITION")) {
					int x = Integer.parseInt(scan.nextLine());
					int y = Integer.parseInt(scan.nextLine());
					int z = Integer.parseInt(scan.nextLine());
					pathToBank.add(new PositionElement(PathElement.Type.POSITION, x, y, z));
				} else if (string.equals("OBJECT")) {
					boolean isName = scan.nextInt() == 1;
					scan.nextLine();
					pathToBank.add(new ObjectElement(PathElement.Type.OBJECT, isName, scan.nextLine(), Integer.parseInt(scan.nextLine()), scan.nextLine()));
				} else if (string.equals("CHARACTER")) {
					boolean isName = scan.nextInt() == 1;
					scan.nextLine();
					pathToBank.add(new ObjectElement(PathElement.Type.CHARACTER, isName, scan.nextLine(), Integer.parseInt(scan.nextLine()), scan.nextLine()));
				} else if (string.equals("TELETAB")) {
					pathToBank.add(new TeletabElement(PathElement.Type.TELETAB, scan.nextLine()));
				} else if (string.equals("SSDOOR")) {
					pathToBank.add(new SSDoorElement(PathElement.Type.SSDOOR, scan.nextLine(), new Position(scan.nextInt(), scan.nextInt(), scan.nextInt())));
					scan.nextLine();
				} else if(string.equals("ITEMOBJECT")){
                                        pathToBank.add(new ItemObjectElement(PathElement.Type.ITEMOBJECT, scan.nextLine(), scan.nextLine(), scan.nextInt()));
                                        scan.nextLine();
                                }
			}
			if (pathToBank.size() == 0) {
				pathToBank = null;
			} else {
				pathToBankStatus.setText("Set");
			}
			pathFromBank = new LinkedList<PathElement>();
			while (!(string = scan.nextLine()).equals("Done")) {
				if (string.equals("POSITION")) {
					pathFromBank.add(new PositionElement(PathElement.Type.POSITION, Integer.parseInt(scan.nextLine()), Integer.parseInt(scan.nextLine()), Integer.parseInt(scan.nextLine())));
				} else if (string.equals("OBJECT")) {
					boolean isName = scan.nextInt() == 1;
					scan.nextLine();
					pathFromBank.add(new ObjectElement(PathElement.Type.OBJECT, isName, scan.nextLine(), Integer.parseInt(scan.nextLine()), scan.nextLine()));
				} else if (string.equals("CHARACTER")) {
					boolean isName = scan.nextInt() == 1;
					scan.nextLine();
					pathFromBank.add(new ObjectElement(PathElement.Type.CHARACTER, isName, scan.nextLine(), Integer.parseInt(scan.nextLine()), scan.nextLine()));
				} else if (string.equals("TELETAB")) {
					pathFromBank.add(new TeletabElement(PathElement.Type.TELETAB, scan.nextLine()));
				} else if (string.equals("SSDOOR")) {
					pathFromBank.add(new SSDoorElement(PathElement.Type.SSDOOR, scan.nextLine(), new Position(scan.nextInt(), scan.nextInt(), scan.nextInt())));
					scan.nextLine();
				} else if(string.equals("ITEMOBJECT")){
                                        pathFromBank.add(new ItemObjectElement(PathElement.Type.ITEMOBJECT, scan.nextLine(), scan.nextLine(), scan.nextInt()));
                                        scan.nextLine();
                                }
			}
			if (pathFromBank.size() == 0) {
				pathFromBank = null;
			} else {
				pathFromBankStatus.setText("Set");
			}
			if (scan.nextInt() == 1) {
				bonesToPeaches.setSelected(true);
			}
			scan.nextLine();
			if (!bonesToPeaches.isSelected()) {
				eatFood.setEnabled(true);
				foodName.setEnabled(eatFood.isSelected());
				eatAtHP.setEnabled(eatFood.isSelected());
				withdrawCount.setEnabled(eatFood.isSelected());
				bankWhenFull.setEnabled(true);
			} else {
				eatFood.setEnabled(false);
				foodName.setEnabled(false);
				eatAtHP.setEnabled(true);
				withdrawCount.setEnabled(false);
				bankWhenFull.setEnabled(false);
			}
			useRange.setSelected(scan.nextInt() == 1);
			scan.nextLine();
			ammoName.setText(scan.nextLine());
			wieldAmount.setText(scan.nextLine());
			useSafespot.setSelected(scan.nextInt() == 1);
			scan.nextLine();
			int x, y, z;
			x = scan.nextInt();
			y = scan.nextInt();
			z = scan.nextInt();
			System.out.println(x + " " + y + " " + z);
			safespot = new Position(x, y, z);
			if (x == 0 && y == 0 && z == 0) {
				safespot = null;
			}
			scan.nextLine();
			ammoName.setEnabled(useRange.isSelected());
			wieldAmount.setEnabled(useRange.isSelected());
			useSafespot.setEnabled(useRange.isSelected());
			canReach.setEnabled(useRange.isSelected());
			jButton10.setEnabled(useRange.isSelected() && useSafespot.isSelected());
			while (!(string = scan.nextLine()).equals("DONE")) {
				int wCount = Integer.parseInt(scan.nextLine());
				int dThresh = Integer.parseInt(scan.nextLine());
				potionsTableModel.addRow(new Object[] { string, wCount, dThresh });
			}
			scan.nextInt();
			scan.nextLine();
			scan.nextLine();
			bankWhenFull.setSelected(scan.nextInt() == 1);
			scan.nextLine();
			chatWindow.setSelected(scan.nextInt() == 1);
			enableAFK.setSelected(scan.nextInt() == 1);
			scan.nextLine();
			useSpecial.setSelected(scan.nextInt() == 1);
			switchSpecial.setSelected(scan.nextInt() == 1);
			scan.nextLine();
			specialName.setText(scan.nextLine());
			specialAt.setText(scan.nextLine());
			regularName.setText(scan.nextLine());
			switchSpecial.setEnabled(useSpecial.isSelected());
			specialName.setEnabled(useSpecial.isSelected() && switchSpecial.isSelected());
			regularName.setEnabled(useSpecial.isSelected() && switchSpecial.isSelected());
			canReach.setSelected(scan.nextInt() == 1);
                        buryBones.setSelected(scan.nextInt() == 1);
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Error occured when loading settings.");
		}
	}

	/**
	 * Saves the GUI into a file
	 *
	 */
	private void saveSettings(File file) {
		try {
			// Save Settings
			PrintStream ps = new PrintStream(file);
			for (int i = 0; i < attackTableModel.getRowCount(); i++) {
				ps.println(attackTableModel.getValueAt(i, 0));
				ps.println(attackTableModel.getValueAt(i, 1));
			}
			ps.println("Done");
			ps.println(eatFood.isSelected() ? 1 : 0);
			ps.println(foodName.getText());
			ps.println(eatAtHP.getText());
			ps.println(withdrawCount.getText());
			for (int i = 0; i < lootTableModel.getRowCount(); i++) {
				ps.println(lootTableModel.getValueAt(i, 0));
				ps.println(lootTableModel.getValueAt(i, 1));
				ps.println(lootTableModel.getValueAt(i, 2));
			}
			ps.println("Done");
			if (pathToBank != null) {
				for (PathElement p : pathToBank) {
					ps.println(p.getType());
					ps.print(p.toNewlineString());
				}
			}
			ps.println("Done");
			if (pathFromBank != null) {
				for (PathElement p : pathFromBank) {
					ps.println(p.getType());
					ps.print(p.toNewlineString());
				}
			}
			ps.println("Done");
			ps.println(bonesToPeaches.isSelected() ? 1 : 0);
			ps.println(useRange.isSelected() ? 1 : 0);
			ps.println(ammoName.getText());
			ps.println(wieldAmount.getText());
			ps.println(useSafespot.isSelected() ? 1 : 0);
			if (safespot != null) {
				ps.println(safespot.getX());
				ps.println(safespot.getY());
				ps.println(safespot.getZ());
			} else {
				ps.println("0");
				ps.println("0");
				ps.println("0");
			}
			for (int r = 0; r < potionsTableModel.getRowCount(); r++) {
				ps.println(potionsTable.getValueAt(r, 0));
				ps.println(potionsTable.getValueAt(r, 1));
				ps.println(potionsTable.getValueAt(r, 2));
			}
			ps.println("DONE");
			ps.println(1);
			ps.println("PENIS");
			ps.println(bankWhenFull.isSelected() ? 1 : 0);
			ps.println(chatWindow.isSelected() ? 1 : 0);
			ps.println(enableAFK.isSelected() ? 1 : 0);
			ps.println(useSpecial.isSelected() ? 1 : 0);
			ps.println(switchSpecial.isSelected() ? 1 : 0);
			ps.println(specialName.getText());
			ps.println(specialAt.getText());
			ps.println(regularName.getText());
			ps.println(canReach.isSelected() ? 1 : 0);
                        ps.println(buryBones.isSelected() ? 1 : 0);
			ps.close();
		} catch (Exception e) {

		}
	}

	// String -> boolean for priority setting only
	private boolean determinePriority(String s) {
		return s.equals("High");
	}

	/**
	 * @param args
	 */
	public static void main(String args[]) {
		/* Set the Nimbus look and feel */
		// <editor-fold defaultstate="collapsed"
		// desc=" Look and feel setting code (optional) ">
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(DreamFighterGeneralGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(DreamFighterGeneralGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(DreamFighterGeneralGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(DreamFighterGeneralGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		// </editor-fold>

		/* Create and display the form */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new DreamFighterGeneralGUI(null).setVisible(true);
			}
		});
	}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JTextField ammoName;
    public javax.swing.JTable attackTable;
    public javax.swing.JCheckBox bankWhenFull;
    public javax.swing.JCheckBox bonesToPeaches;
    public javax.swing.JCheckBox buryBones;
    public javax.swing.ButtonGroup buttonGroup1;
    public javax.swing.ButtonGroup buttonGroup2;
    public javax.swing.JCheckBox canReach;
    public javax.swing.JCheckBox chatWindow;
    public javax.swing.JPanel combatPanel;
    public javax.swing.JTextField eatAtHP;
    public javax.swing.JCheckBox eatFood;
    public javax.swing.JCheckBox enableAFK;
    public javax.swing.JTextField foodName;
    public javax.swing.JRadioButton highPriority;
    public javax.swing.JButton jButton1;
    public javax.swing.JButton jButton10;
    public javax.swing.JButton jButton11;
    public javax.swing.JButton jButton12;
    public javax.swing.JButton jButton13;
    public javax.swing.JButton jButton14;
    public javax.swing.JButton jButton2;
    public javax.swing.JButton jButton3;
    public javax.swing.JButton jButton4;
    public javax.swing.JButton jButton5;
    public javax.swing.JButton jButton6;
    public javax.swing.JButton jButton7;
    public javax.swing.JButton jButton8;
    public javax.swing.JButton jButton9;
    public javax.swing.JLabel jLabel1;
    public javax.swing.JLabel jLabel10;
    public javax.swing.JLabel jLabel11;
    public javax.swing.JLabel jLabel12;
    public javax.swing.JLabel jLabel13;
    public javax.swing.JLabel jLabel14;
    public javax.swing.JLabel jLabel15;
    public javax.swing.JLabel jLabel16;
    public javax.swing.JLabel jLabel17;
    public javax.swing.JLabel jLabel18;
    public javax.swing.JLabel jLabel19;
    public javax.swing.JLabel jLabel2;
    public javax.swing.JLabel jLabel20;
    public javax.swing.JLabel jLabel3;
    public javax.swing.JLabel jLabel4;
    public javax.swing.JLabel jLabel5;
    public javax.swing.JLabel jLabel6;
    public javax.swing.JLabel jLabel7;
    public javax.swing.JLabel jLabel8;
    public javax.swing.JLabel jLabel9;
    public javax.swing.JPanel jPanel1;
    public javax.swing.JPanel jPanel2;
    public javax.swing.JPanel jPanel4;
    public javax.swing.JPanel jPanel5;
    public javax.swing.JPanel jPanel7;
    public javax.swing.JPanel jPanel8;
    public javax.swing.JPanel jPanel9;
    public javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JScrollPane jScrollPane2;
    public javax.swing.JScrollPane jScrollPane3;
    public javax.swing.JScrollPane jScrollPane4;
    public javax.swing.JScrollPane jScrollPane5;
    public javax.swing.JTabbedPane jTabbedPane1;
    public javax.swing.JTextArea jTextArea1;
    public javax.swing.JTextArea jTextArea2;
    public javax.swing.JTextArea jTextArea3;
    public javax.swing.JTextArea jTextArea5;
    public javax.swing.JTextField lootName;
    public javax.swing.JTable lootTable;
    public javax.swing.JRadioButton lowPriority;
    public javax.swing.JRadioButton noEat;
    public javax.swing.JLabel pathFromBankStatus;
    public javax.swing.JLabel pathToBankStatus;
    public javax.swing.JTable potionsTable;
    public javax.swing.JComboBox prayerComboBox;
    public javax.swing.JList prayerList;
    public javax.swing.JTextField regularName;
    public javax.swing.JLabel safespotLabel;
    public javax.swing.JTextField specialAt;
    public javax.swing.JTextField specialName;
    public javax.swing.JCheckBox switchSpecial;
    public javax.swing.JCheckBox useRange;
    public javax.swing.JCheckBox useSafespot;
    public javax.swing.JCheckBox useSpecial;
    public javax.swing.JTextField wieldAmount;
    public javax.swing.JTextField withdrawCount;
    public javax.swing.JRadioButton yesEat;
    // End of variables declaration//GEN-END:variables
}
