package gui.path;

import org.osbot.rs07.api.map.Position;

/**
 * A Stronghold of Security door element
 * 
 * @author Ericthecmh, Eliot
 */
public class SSDoorElement extends PathElement {

	private String name;
	private int myX, myY, myZ;

	public SSDoorElement(PathElement.Type type, String name, Position p) {
		super(type);
		this.name = name;
		this.myX = p.getX();
		this.myY = p.getY();
		this.myZ = p.getZ();
	}

	public String getName() {
		return this.name;
	}

	public int getX() {
		return this.myX;
	}

	public int getY() {
		return this.myY;
	}

	public int getZ() {
		return this.myZ;
	}

	@Override
	public String toNewlineString() {
		return name + "\n" + myX + "\n" + myY + "\n" + myZ + "\n";
	}

}
