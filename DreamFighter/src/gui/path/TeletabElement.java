package gui.path;

/**
 * Stores a teletab path element
 * 
 * @author Ericthecmh
 */
public class TeletabElement extends PathElement {

	private final String name;

	public TeletabElement(Type t, String name) {
		super(t);
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	@Override
	public String toNewlineString() {
		return this.name + "\n";
	}
}
