package gui.path;

/**
 * A path element (position, object, etc)
 * 
 * @author Eliot,Ericthecmh
 */
public abstract class PathElement {

	public enum Type {
		POSITION, OBJECT, CHARACTER, TELETAB, SSDOOR, ITEMOBJECT;
	}

	protected final Type myType;

	protected PathElement(Type t) {
		this.myType = t;
	}

	public Type getType() {
		return this.myType;
	}

	public abstract String toNewlineString();

}
