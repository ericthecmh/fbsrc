package gui.path;

/**
 * A position element. Has x and y
 * 
 * @author Eliot,Ericthecmh
 */
public class PositionElement extends PathElement {

	private final int myX, myY, myZ;

	public PositionElement(Type t, int x, int y, int z) {
		super(t);
		this.myX = x;
		this.myY = y;
		this.myZ = z;
	}

	public int getX() {
		return this.myX;
	}

	public int getY() {
		return this.myY;
	}

	public int getZ() {
		return this.myZ;
	}

	@Override
	public String toNewlineString() {
		return this.myX + "\n" + this.myY + "\n" + this.myZ + "\n";
	}
}
