package gui.path;

/**
 * Stores an object interaction element. Stores name/id of object, interaction
 * string, and the location of player when object was added (for paint only);
 * 
 * @author Eliot,Ericthecmh
 */
public class ItemObjectElement extends PathElement {

	private final String itemName;
	private final String objectName;
        private final int waitTime;

	public ItemObjectElement(Type t, String itemName, String objectName, int waitTime) {
		super(t);
		this.itemName = itemName;
                this.objectName = objectName;
                this.waitTime = waitTime;
	}

	public String getItemName() {
		return this.itemName;
	}
        
        public String getObjectName(){
            return this.objectName;
        }
        
        public int getWaitTime(){
            return this.waitTime;
        }

	@Override
	public String toNewlineString() {
		return this.itemName + "\n" + this.objectName + "\n" + this.waitTime + "\n";
	}
}
