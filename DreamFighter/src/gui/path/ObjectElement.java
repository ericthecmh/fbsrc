package gui.path;

/**
 * Stores an object interaction element. Stores name/id of object, interaction
 * string, and the location of player when object was added (for paint only);
 * 
 * @author Eliot,Ericthecmh
 */
public class ObjectElement extends PathElement {

	// Whether object is stored by name or the ID
	private final boolean isName;
	// Name of the object. Null if stored by ID
	private final String name;
	// ID of object. -1 if stored by name
	private final int ID;
	// Interact action string
	private final String interact;

	public ObjectElement(Type t, boolean isName, String name, int ID,
			String interact) {
		super(t);
		this.isName = isName;
		this.name = name;
		this.ID = ID;
		this.interact = interact;
	}

	public String getName() {
		return this.name;
	}

	public int getID() {
		return this.ID;
	}

	public String getInteract() {
		return this.interact;
	}

	public boolean isName() {
		return this.isName;
	}

	@Override
	public String toNewlineString() {
		return (this.isName ? 1 : 0) + "\n" + this.getName() + "\n"
				+ this.getID() + "\n" + this.getInteract() + "\n";
	}
}
