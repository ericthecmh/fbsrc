package actions;

import static org.osbot.rs07.script.MethodProvider.random;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import library.BankItem;
import library.Condition;
import library.Library;
import main.DreamFighter;
import main.DreamFighter.State;

import org.osbot.rs07.api.model.Entity;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.ConditionalSleep;

/**
 * 
 * Executes actions related to banking.
 * 
 * @author Eliot,Ericthecmh
 * 
 */
public class BankAction implements Action {
	DreamFighter s = null;
	ArrayList<BankItem> items = new ArrayList<BankItem>();
	Map<String, Integer> potions = new HashMap<String, Integer>();

	@Override
	/**
	 * 
	 * executes banking related actions
	 * 
	 * @param script
	 *            reference to calling script. Gives access to osbot 2 api
	 *            methods and script specific variables
	 * @return time before next call of script's onLoop
	 */
	public int execute(DreamFighter script) {
		s = script;
		items = script.bankItems;
		initialize();
		if (openBank() && s.bank.depositAll()) {
			if (withdrawItemsFromBank(items)) {
				script.state = State.WALK_FROM_BANK;
			}
		}
		return 100;
	}

	/**
	 * init names to keys, temporary as API is broken on banking w/ potions
	 */
	private void initialize() {
		potions.put("Super attack(4)", 2436);
		potions.put("Super strength(4)", 2440);
		potions.put("Super defence(4)", 2442);
		potions.put("Prayer potion(4)", 2434);
		potions.put("Ranging potion(4)", 2444);
		potions.put("Attack potion(4)", 2428);
		potions.put("Strength potion(4)", 113);
		potions.put("Defence potion(4)", 2432);
		potions.put("Super attack(3)", 145);
		potions.put("Super strength(3)", 157);
		potions.put("Super defence(3)", 163);
		potions.put("Prayer potion(3)", 139);
		potions.put("Ranging potion(3)", 169);
		potions.put("Attack potion(3)", 121);
		potions.put("Strength potion(3)", 115);
		potions.put("Defence potion(3)", 133);
	}

	/**
	 * opens the bank
	 * 
	 * @return
	 * 
	 * @return if we successfully opened the bank
	 */
	private boolean openBank() {
		if (s.bank.isOpen())
			return true;
		Entity bank = Library.getClosestObjectWithInteract("Bank booth",
				"Bank", s);
		if (bank != null)
			bank.interact("Bank");
		new ConditionalSleep(3000) {
			@Override
			public boolean condition() {
				return s.bank.isOpen();
			}
		};
		if (s.bank.isOpen())
			return true;
		return false;
	}

	/**
	 * retrieve needed items from the bank
	 * 
	 * @return if we successfully retrieved all items from the bank
	 */
	public boolean withdrawItemsFromBank(ArrayList<BankItem> items) {
		for (BankItem i : items) {
			String withdraw = null;
			while (!s.inventory.contains(i.name)
					&& !s.inventory.contains(withdraw)) {
				if (i.name.contains("potion") || i.name.contains("Super")) {
					withdraw = (s.bank.contains(i.name + "(4)")) ? i.name
							+ "(4)" : i.name + "(3)";
					final int take = potions.get(withdraw);
					if (s.bank.contains(withdraw)) {
						if (s.useCustomBank) {
							if (i.amount != 1 && i.amount != 5
									&& i.amount != 10) {
								final long oldAmount = s.inventory
										.getAmount(take);
								s.bank.interact(s.bank.getSlot(take),
										"Withdraw-X");
								if (Library.waitFor(new Condition() {

									@Override
									public boolean satisfied(Script s) {
										return s.inventory.getAmount(take) != oldAmount
												|| (s.interfaces.get(137) != null
														&& s.interfaces
																.get(137)
																.getChild(2) != null && s.interfaces
														.get(137).getChild(2)
														.getPosition().getX() == -1);
									}

								}, 5000, 100, s)) {
									if (s.inventory.getAmount(take) == oldAmount
											&& (s.interfaces.get(137) != null
													&& s.interfaces.get(137)
															.getChild(2) != null && s.interfaces
													.get(137).getChild(2)
													.getPosition().getX() == -1)) {
										// If didn't withdraw, then the
										// "Enter x" screen must be up.
										// Enter amount
										s.keyboard.typeString("" + i.amount);
										try {
											MethodProvider.sleep(random(700,
													1000));
										} catch (InterruptedException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									}
								}
							} else {
								s.bank.interact(s.bank.getSlot(take),
										"Withdraw-" + i.amount);
							}
						} else {
							s.bank.withdraw(withdraw, i.amount);
						}
						try {
							MethodProvider.sleep(MethodProvider.random(800,
									1000));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				} else {
					s.bank.withdraw(i.name, i.amount);
				}
				try {
					MethodProvider.sleep(MethodProvider.random(800, 1000));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		return true;
	}

}
