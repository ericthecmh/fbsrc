package actions;

import main.DreamFighter;

public interface Action {
	/**
	 * 
	 * Executes the action
	 * 
	 * @param script
	 *            reference to calling script. Gives access to osbot 2 api
	 *            methods and script specific variables
	 * @return time before next call of script's onLoop
	 */
	public abstract int execute(DreamFighter script);
}
