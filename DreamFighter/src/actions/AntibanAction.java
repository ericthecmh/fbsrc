package actions;

import java.util.LinkedList;

import library.Library;
import main.DreamFighter;

import org.osbot.rs07.api.ui.Skill;
import org.osbot.rs07.api.ui.Tab;
import org.osbot.rs07.input.mouse.MouseDestination;
import org.osbot.rs07.input.mouse.RectangleDestination;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;

public class AntibanAction implements Action {

	int oldAttack;
	int oldStrength;
	int oldDefence;
	int oldRange;
	int oldMagic;
	private boolean doneRunning;

	public AntibanAction(DreamFighter s) {
		oldAttack = s.skills.getExperience(Skill.ATTACK);
		oldStrength = s.skills.getExperience(Skill.STRENGTH);
		oldDefence = s.skills.getExperience(Skill.DEFENCE);
		oldRange = s.skills.getExperience(Skill.RANGED);
		oldMagic = s.skills.getExperience(Skill.MAGIC);
	}

	public Skill getCurrentTrainingSkill(DreamFighter s) {
		LinkedList<Skill> skills = new LinkedList<Skill>();
		if (s.skills.getExperience(Skill.ATTACK) != oldAttack) {
			oldAttack = s.skills.getExperience(Skill.ATTACK);
			skills.add(Skill.ATTACK);
		}
		if (s.skills.getExperience(Skill.STRENGTH) != oldStrength) {
			oldStrength = s.skills.getExperience(Skill.STRENGTH);
			skills.add(Skill.STRENGTH);
		}
		if (s.skills.getExperience(Skill.DEFENCE) != oldDefence) {
			oldDefence = s.skills.getExperience(Skill.DEFENCE);
			skills.add(Skill.DEFENCE);
		}
		if (s.skills.getExperience(Skill.RANGED) != oldRange) {
			oldRange = s.skills.getExperience(Skill.RANGED);
			skills.add(Skill.RANGED);
		}
		if (s.skills.getExperience(Skill.MAGIC) != oldMagic) {
			oldMagic = s.skills.getExperience(Skill.MAGIC);
			skills.add(Skill.MAGIC);
		}
		return skills.get(MethodProvider.random(skills.size()));
	}

	@Override
	public int execute(final DreamFighter script) {
		doneRunning = false;
		new Thread(new Runnable() {
			public void run() {
				
				int k = MethodProvider.random(0, 14);
				switch (k) {
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
					MouseDestination md = new RectangleDestination(script.bot,
							MethodProvider.random(2, 750), MethodProvider
									.random(2, 450), 3, 3);
					script.mouse.move(md);
					break;
				case 6:
					try {
						script.skills.hoverSkill(Skill.HITPOINTS);
						script.sleep(MethodProvider.random(1000, 3000));
						if (Math.random() < 0.5)
							script.tabs.open(Tab.INVENTORY);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					break;
				case 7:
				case 8:
				case 9:
					try {
						script.skills
								.hoverSkill(getCurrentTrainingSkill(script));
						script.sleep(MethodProvider.random(1000, 3000));
						if (Math.random() < 0.5)
							script.tabs.open(Tab.INVENTORY);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					break;
				case 10:
					int k1 = MethodProvider.random(10);
					switch (k1) {
					case 0:
					case 1:
					case 2:
						script.tabs.open(Tab.FRIENDS);
						break;
					case 3:
					case 4:
						script.tabs.open(Tab.EQUIPMENT);
						break;
					case 5:
						script.tabs.open(Tab.EMOTES);
						break;
					case 6:
						script.tabs.open(Tab.QUEST);
						break;
					case 7:
						script.tabs.open(Tab.SETTINGS);
						break;
					case 8:
						script.tabs.open(Tab.MUSIC);
						break;
					}
					break;
				case 11:
				case 12:
				case 13:
					script.camera.moveYaw(MethodProvider.random(360));
				}
				doneRunning = true;
			}
		}).start();
		while (!doneRunning) {
			if (script.getSkills().getDynamic(Skill.HITPOINTS) <= 5) {
				doneRunning = true;
				break;
			}
			try {
				script.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// try {
		// s.antiBan.hoverSkill(Skill.HITPOINTS);
		// } catch (InterruptedException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		return 0;
	}

	public void setAFK(boolean b, Script script) {
		
	}
}
