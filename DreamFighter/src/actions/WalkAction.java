package actions;

import gui.path.CharacterElement;
import gui.path.ItemObjectElement;
import gui.path.ObjectElement;
import gui.path.PathElement;
import gui.path.PositionElement;
import gui.path.SSDoorElement;
import gui.path.TeletabElement;

import java.util.LinkedList;

import library.Condition;
import library.Library;
import main.DreamFighter;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.map.PositionPolygon;
import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.input.mouse.ClickMouseEvent;
import org.osbot.rs07.input.mouse.EntityDestination;
import org.osbot.rs07.input.mouse.MiniMapTileDestination;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;

/**
 * 
 * Executes actions related to walking.
 * 
 * @author Eliot,Ericthecmh
 * 
 */
public class WalkAction implements Action {

	// Path tot the bank
	private LinkedList<PathElement> path;
	// The element in the path list to be processed next
	private int elementi;
	// Type of thing being proccessed
	private PathElement.Type type;
	// Thing being currently processed
	private Object current;
	// State to set script.state to after walking is done
	private DreamFighter.State state;

	/**
	 * Initializes the action
	 * 
	 * @param state
	 *            set script state to this state once walking is complete
	 */
	public WalkAction(DreamFighter.State state) {
		super();
		elementi = 0;
		type = null;
		current = null;
		this.state = state;
	}

	/**
	 * Sets the path of the action
	 * 
	 * @param path
	 */
	public void setPath(LinkedList<PathElement> path) {
		this.path = path;
	}

	@Override
	/**
	 *
	 * executes walking related actions
	 *
	 * @param script reference to calling script. Gives access to osbot 2 api
	 * methods and script specific variables
	 * @return time before next call of script's onLoop
	 */
	public int execute(DreamFighter script) {
		if (path == null || path.size() == 0) {
			script.log("No path found. Terminating");
			script.stop();
		}
		if (elementi >= path.size() && current == null) {
			script.state = this.state;
			elementi = 0;
			current = null;
			return 100;
		}
		// If idle for 15 seconds, try to figure things out
		if (System.currentTimeMillis() - script.getLastUpdateTime() > 15000) {
			script.log("I've been idle for a long time... trying to find last known non-position element.");
			current = null;
			type = null;
			// Search backward and find the most recent position
			for (int i = elementi - 1; i >= 0; i--) {
				if (path.get(i).getType() != PathElement.Type.POSITION
						|| i == 0) {
					elementi = i;
					break;
				}
			}
			if (path.get(elementi) instanceof ObjectElement)
				script.log("Element is an ObjectElement, with name "
						+ ((ObjectElement) path.get(elementi)).getName());
			else if (path.get(elementi) instanceof CharacterElement)
				script.log("Element is a CharacterElement, with name "
						+ ((CharacterElement) path.get(elementi)).getName());
			else if (path.get(elementi) instanceof TeletabElement)
				script.log("Element is a TeletabElement, with name "
						+ ((TeletabElement) path.get(elementi)).getName());
			script.updateTime();
		}
		if (current == null) {
			// If there's nothing to process
			if (path.get(elementi).getType() == PathElement.Type.POSITION) {
				// Next thing to be processed is a path
				type = PathElement.Type.POSITION;
				// Creates a blank path and sets current to path
				current = new PositionPolygon();
				PositionPolygon ref = (PositionPolygon) current;
				// Constructs the path
				for (; elementi < path.size()
						&& path.get(elementi).getType() == PathElement.Type.POSITION; elementi++) {
					ref.add(new int[][] { {
							((PositionElement) path.get(elementi)).getX(),
							((PositionElement) path.get(elementi)).getY() } });
				}
			} else if (path.get(elementi).getType() == PathElement.Type.OBJECT) {
				// Next thing to be processed is an object interaction
				type = PathElement.Type.OBJECT;
				// Set current to be the object element
				current = path.get(elementi++);
			} else if (path.get(elementi).getType() == PathElement.Type.CHARACTER) {
				// Next thing to be processed is a character interaction
				type = PathElement.Type.CHARACTER;
				// Set current to be the character element
				current = path.get(elementi++);
			} else if (path.get(elementi).getType() == PathElement.Type.TELETAB) {
				type = PathElement.Type.TELETAB;
				// Set current to be the teletab element
				current = path.get(elementi++);
			} else if (path.get(elementi).getType() == PathElement.Type.SSDOOR) {
				type = PathElement.Type.SSDOOR;
				// Set current to be the door element
				current = path.get(elementi++);
			}
			if (type != null) {
				script.log("Next type: " + type.toString());
			}
			return 0;
		} else {
			if (type == PathElement.Type.POSITION) {
				PositionPolygon ref = (PositionPolygon) current;
				// Last position in the path
				Position end = new Position((int) ref.get(ref.getCount() - 1)
						.getX(), (int) ref.get(ref.getCount() - 1).getY(), 0);
				boolean walkExact = true;
				if (walkExact) {
					if (Library.distanceTo(end, script) <= 0.5
							&& !script.myPlayer().isMoving()) {
						// If you're at the end position
						current = null;
						type = null;
						return 0;
					} else if (Library.distanceTo(end, script) < 3
							&& Library.distanceTo(end, script) > 0.5
							&& !script.myPlayer().isMoving()) {
						if (script.myPlayer().isMoving()) {
							Library.waitFor(new Condition() {

								@Override
								public boolean satisfied(Script script) {
									return !script.myPlayer().isMoving();
								}

							}, 10000, 100, script);
							try {
								script.sleep(2000);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						end.interact(script.bot, "Walk here");
						try {
							script.sleep(2000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else if (Library.distanceTo(end, script) < 6
							&& !script.myPlayer().isMoving()) {
						script.mouse.click(new MiniMapTileDestination(
								script.bot, end));
						try {
							script.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {
						Library.walkPath(ref, end, script);
					}
				} else {
					if (Library.distanceTo(end, script) < 6) {
						current = null;
						type = null;
						return 0;
					} else {
						Library.walkPath(ref, end, script);
					}
				}
			} else if (type == PathElement.Type.OBJECT) {
				// Wait for the player to stop moving
				if (script.myPlayer().isMoving()) {
					while (script.myPlayer().isMoving()) {
						Library.waitFor(new Condition() {
							@Override
							public boolean satisfied(Script script) {
								return !script.myPlayer().isMoving();
							}
						}, 10000, 100, script);
					}
					try {
						MethodProvider.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				ObjectElement o = (ObjectElement) current;
				RS2Object object = null;
				// Get the object to interact with
				if (o.isName()) {
					object = Library.getClosestObjectWithInteract(o.getName(),
							o.getInteract(), script);
				} else {
					object = Library.getClosestObjectWithInteract(o.getID(),
							o.getInteract(), script);
				}
				if (object != null
						&& Library.distanceTo(object.getPosition(), script) < 8) {
					// Wait for the player to stop moving
					if (script.myPlayer().isMoving()) {
						while (script.myPlayer().isMoving()) {
							Library.waitFor(new Condition() {
								@Override
								public boolean satisfied(Script script) {
									return !script.myPlayer().isMoving();
								}
							}, 10000, 100, script);
						}
						try {
							MethodProvider.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					if (object.interact(o.getInteract())) {
						// If successfully interacted, then move on to next path
						// element
						current = null;
						type = null;
						try {
							// Sleep a bit for stability
							Library.waitFor(new Condition() {

								@Override
								public boolean satisfied(Script script) {
									return script.myPlayer().isMoving()
											|| script.myPlayer().getAnimation() != -1;
								}

							}, 5000, 100, script);
							Library.waitFor(new Condition() {

								@Override
								public boolean satisfied(Script script) {
									return !script.myPlayer().isMoving()
											&& script.myPlayer().getAnimation() == -1;
								}

							}, 10000, 100, script);
							script.sleep(MethodProvider.random(1000, 2000));
						} catch (InterruptedException ex) {
						}
						return 0;
					}
					// Otherwise, pause and try again
					return MethodProvider.random(400, 700);
				} else {
					// If the object doesn't exist, skip and move on to next
					// path element
					current = null;
					type = null;
					return 0;
				}
			} else if (type == PathElement.Type.CHARACTER) {
				// Wait for the player to stop moving
				if (script.myPlayer().isMoving()) {
					while (script.myPlayer().isMoving()) {
						Library.waitFor(new Condition() {
							@Override
							public boolean satisfied(Script script) {
								return !script.myPlayer().isMoving();
							}
						}, 10000, 100, script);
					}
					try {
						MethodProvider.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				CharacterElement c = (CharacterElement) current;
				NPC npc = null;
				// Get the object to interact with
				if (c.isName()) {
					npc = Library.getClosestNPCWithInteract(c.getName(),
							c.getInteract(), script);
				} else {
					npc = Library.getClosestNPCWithInteract(c.getID(),
							c.getInteract(), script);
				}
				if (npc != null
						&& Library.distanceTo(npc.getPosition(), script) < 8) {
					// Wait for the player to stop moving
					if (script.myPlayer().isMoving()) {
						while (script.myPlayer().isMoving()) {
							Library.waitFor(new Condition() {
								@Override
								public boolean satisfied(Script script) {
									return !script.myPlayer().isMoving();
								}
							}, 10000, 100, script);
						}
						try {
							MethodProvider.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					if (npc.interact(c.getInteract())) {
						// If successfully interacted, then move on to next path
						// element
						current = null;
						type = null;
						try {
							// Sleep a bit for stability
							Library.waitFor(new Condition() {

								@Override
								public boolean satisfied(Script script) {
									return script.myPlayer().isMoving()
											|| script.myPlayer().getAnimation() != -1;
								}

							}, 5000, 100, script);
							Library.waitFor(new Condition() {

								@Override
								public boolean satisfied(Script script) {
									// TODO Auto-generated method stub
									return !script.myPlayer().isMoving()
											&& script.myPlayer().getAnimation() == -1;
								}

							}, 10000, 100, script);
							script.sleep(MethodProvider.random(1000, 2000));
						} catch (InterruptedException ex) {
						}
						return 0;
					}
					// Otherwise, pause and try again
					return MethodProvider.random(400, 700);
				} else {
					// If the object doesn't exist, skip and move on to next
					// path element
					current = null;
					type = null;
					return 0;
				}
			} else if (type == PathElement.Type.TELETAB) {
				TeletabElement t = (TeletabElement) current;
				long count = script.inventory.getAmount(t.getName());
				while (script.inventory.getAmount(t.getName()) == count) {
					script.inventory.interact("Break", t.getName());
					try {
						MethodProvider.sleep(MethodProvider.random(200, 500));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				current = null;
				type = null;
				return 0;
			} else if (type == PathElement.Type.SSDOOR) {
				SSDoorElement s = (SSDoorElement) current;
				Position doorPosition = new Position(s.getX(), s.getY(),
						s.getZ());
				RS2Object door = Library.getObjectAt(doorPosition, s.getName(),
						script);
				if (door == null) {
					current = null;
					type = null;
					return 0;
				} else {
					final int x = script.myPosition().getX();
					final int y = script.myPosition().getY();
					while (Library.distanceTo(doorPosition, script) > 2) {
						script.mouse.click(new MiniMapTileDestination(
								script.bot, doorPosition));
						try {
							script.sleep(2000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Library.waitFor(new Condition() {

							@Override
							public boolean satisfied(Script script) {
								return !script.myPlayer().isMoving();
							}

						}, 10000, 1000, script);
					}
					if (script.myPlayer().isMoving()) {
						Library.waitFor(new Condition() {

							@Override
							public boolean satisfied(Script script) {
								return !script.myPlayer().isMoving();
							}

						}, 10000, 200, script);
						try {
							script.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					for (int i = 0; i < 10 && !door.interact("Open"); i++) {
						try {
							script.sleep(MethodProvider.random(500, 700));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					Library.waitFor(new Condition() {

						@Override
						public boolean satisfied(Script script) {
							return script.myPlayer().isMoving()
									|| script.myPosition().getX() != x
									|| script.myPosition().getY() != y;
						}

					}, 1000, 100, script);
					if (Library.waitFor(new Condition() {

						@Override
						public boolean satisfied(Script script) {
							return !script.myPlayer().isMoving();
						}

					}, 15000, 100, script)) {
						try {
							script.sleep(2000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						Library.doDoorQuestion(script);
						try {
							script.sleep(3000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						while (script.dialogues.isPendingContinuation()) {
							script.dialogues.clickContinue();
							try {
								script.sleep(3000);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						current = null;
						type = null;
						return 0;
					}
				}
			} else if (type == PathElement.Type.ITEMOBJECT) {
				if (script.myPlayer().isMoving()) {
					while (script.myPlayer().isMoving()) {
						Library.waitFor(new Condition() {
							@Override
							public boolean satisfied(Script script) {
								return !script.myPlayer().isMoving();
							}
						}, 10000, 100, script);
					}
					try {
						MethodProvider.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				ItemObjectElement element = (ItemObjectElement) current;
				if (!script.inventory.contains(element.getItemName())) {
					// If item not contained in inventory, skip
					type = null;
					current = null;
					return 0;
				} else {
					RS2Object object = script.objects.closest(element
							.getObjectName());
					if (object == null) {
						// If object does not exist, skip
						type = null;
						current = null;
						return 0;
					}
					script.inventory.interact("Use", element.getItemName());
					try {
						script.sleep(MethodProvider.random(500, 800));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					try {
						new ClickMouseEvent(new EntityDestination(script.bot,
								object)).execute();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (element.getWaitTime() == 0) {
						try {
							// Sleep a bit for stability
							Library.waitFor(new Condition() {

								@Override
								public boolean satisfied(Script script) {
									return script.myPlayer().isMoving()
											|| script.myPlayer().getAnimation() != -1;
								}

							}, 5000, 100, script);
							Library.waitFor(new Condition() {

								@Override
								public boolean satisfied(Script script) {
									// TODO Auto-generated method stub
									return !script.myPlayer().isMoving()
											&& script.myPlayer().getAnimation() == -1;
								}

							}, 10000, 100, script);
							script.sleep(MethodProvider.random(1000, 2000));
						} catch (InterruptedException ex) {
						}
					} else {
						try {
							script.sleep(MethodProvider.random(500, 800)
									+ element.getWaitTime());
						} catch (InterruptedException e) {

						}
					}
				}
			}
		}
		return 100;
	}
}
