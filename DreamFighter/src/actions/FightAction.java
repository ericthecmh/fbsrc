package actions;

import java.util.ArrayList;

import library.Condition;
import library.Library;
import library.LootItem;
import library.PotionItem;
import library.PotionMatch;
import main.DreamFighter;
import main.DreamFighter.State;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.GroundItem;
import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.EquipmentSlot;
import org.osbot.rs07.api.ui.Message;
import org.osbot.rs07.api.ui.Skill;
import org.osbot.rs07.event.InteractionEvent;
import org.osbot.rs07.input.mouse.EntityDestination;
import org.osbot.rs07.input.mouse.MainScreenTileDestination;
import org.osbot.rs07.input.mouse.MiniMapTileDestination;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.Area;
import org.osbot.rs07.utility.ConditionalSleep;

/**
 * Executes actions related to fighting.
 *
 * @author Eliot, Ericthecmh
 */
public class FightAction implements Action {
    DreamFighter s = null;
    public int attemptCount = 0;
    NPC current;
    private AntibanAction aa;

    // rock crab variables
    private int rockCrabFailSafe = 0;
    private Area rockCrabEast = new Area(2657, 3650, 2690, 3736);
    private Position safeEast = new Position(2664, 3670, 0);
    private Position returnEast = new Position(2671, 3715, 0);

    private Area rockCrabWest = new Area(2691, 3650, 2722, 3736);
    private Position safeWest = new Position(2706, 3670, 0);
    private Position returnWest = new Position(2705, 3719, 0);
    public Area matchArea = null;

    private boolean useSpecial;
    private int specialAt;
    private int realSpecialAt;
    private boolean switchSpecial;
    private String specialName;
    private String regularName;
    private boolean doorProblem;

    private Position p = null;

    public FightAction(DreamFighter s) {
        aa = new AntibanAction(s);
    }

    public void enableAFK(Script s) {
        aa.setAFK(true, s);
    }

    public void enableSpecial(int realSpecialAt, boolean switchSpecial,
                              String specialName, String regularName) {
        this.realSpecialAt = realSpecialAt;
        this.specialAt = Math.max(100,
                realSpecialAt + MethodProvider.random(0, 7));
        this.switchSpecial = switchSpecial;
        this.specialName = specialName;
        this.regularName = regularName;
        useSpecial = true;
    }

    @Override
    /**
     *
     * executes fighting related actions
     *
     * @param script
     *            reference to calling script. Gives access to osbot 2 api
     *            methods and script specific variables
     * @return time before next call of script's onLoop
     */
    public int execute(DreamFighter script) {
        if (current != null) {
            if (current.getHealth() == 0) {
                s.monstersKilled++;
            }
            current = null;
        }
        s = script;
        if (doorProblem) {
            System.out.println("Attempting to solve door");
            RS2Object door = Library.getClosestSSDoor(script);
            if (door == null) {
                doorProblem = false;
                return 0;
            } else {
                Position doorPosition = new Position(door.getX(), door.getY(),
                        door.getZ());
                final int x = script.myPosition().getX();
                final int y = script.myPosition().getY();
                while (Library.distanceTo(doorPosition, script) > 2) {
                    script.mouse.click(new MiniMapTileDestination(script.bot,
                            doorPosition));
                    try {
                        script.sleep(2000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    Library.waitFor(new Condition() {

                        @Override
                        public boolean satisfied(Script script) {
                            return !script.myPlayer().isMoving();
                        }

                    }, 10000, 1000, script);
                }
                if (script.myPlayer().isMoving()) {
                    Library.waitFor(new Condition() {

                        @Override
                        public boolean satisfied(Script script) {
                            return !script.myPlayer().isMoving();
                        }

                    }, 10000, 200, script);
                    try {
                        script.sleep(1000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                for (int i = 0; i < 10 && !door.interact("Open"); i++) {
                    try {
                        script.sleep(MethodProvider.random(500, 700));
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                Library.waitFor(new Condition() {

                    @Override
                    public boolean satisfied(Script script) {
                        return script.myPlayer().isMoving()
                                || script.myPosition().getX() != x
                                || script.myPosition().getY() != y;
                    }

                }, 1000, 100, script);
                if (Library.waitFor(new Condition() {

                    @Override
                    public boolean satisfied(Script script) {
                        return !script.myPlayer().isMoving();
                    }

                }, 15000, 100, script)) {
                    try {
                        script.sleep(2000);
                    } catch (InterruptedException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    Library.doDoorQuestion(script);
                    try {
                        script.sleep(3000);
                    } catch (InterruptedException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    while (script.dialogues.isPendingContinuation()) {
                        script.dialogues.clickContinue();
                        try {
                            script.sleep(3000);
                        } catch (InterruptedException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                    doorProblem = false;
                    return MethodProvider.random(500, 800);
                }
            }
        }
        if (useSpecial && shouldDoSpecial()) {
            doSpecial();
        }
        // s.log("Called Fight Action execute");
        if (!inCombat()) {
            // System.out.println("Not in combat");
            // System.out.println("Calling eatfood");
            eatFood(script, script.bonesToPeaches);
            // System.out.println("Eatfood returned");
            // System.out.println("Calling drink potions");
            drinkPotions(script);
            // System.out.println("Drink potions returned");
            // System.out.println("Calling loot");
            lootGroundItems(script.lootList);
            buryBones();
            // System.out.println("Loot returned");
            if (s.useRange && s.myPlayer().getAnimation() != -1) {
                if (doRange()) {
                    // System.out.println("DONE");
                    return 0;
                }
                // System.out.println("DONE");
            }
            if (!inCombat()) {
                // System.out.println("Calling attack");
                current = getTarget();
                attack(current);
                // System.out.println("Attack returned");
            }
        } else {
            // s.log("In combat");
            if (s.camera.getPitchAngle() < 55) {
                // System.out.println("Calling camera rotate");
                s.camera.movePitch(60);
                // System.out.println("Camera rotate returned");
            }
            // System.out.println("Calling eat food");
            eatFood(script, script.bonesToPeaches);
            // System.out.println("Eat food returned");
            // s.log("Calling loot2");
            lootGroundItems(script.highPriorityLootList);
            if (Math.random() < s.antibanRate) {
                aa.execute(s);
            }
            // System.out.println("Loot2 returned");
            if (s.useRange) {
                if (doRange()) {
                    // System.out.println("DONE");
                    return 0;
                }
                // System.out.println("DONE");
            }
        }
        shouldLeave();

        return 100;
    }

    private void buryBones() {
        if (s.inventory.isFull() && s.buryBones) {
            while (s.inventory.contains("Bones")
                    || s.inventory.contains("Big bones")) {
                if (s.inventory.contains("Bones")) {
                    s.inventory.interact("Bury", "Bones");
                    try {
                        s.sleep(MethodProvider.random(500, 800));
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                if (s.inventory.contains("Big bones")) {
                    s.inventory.interact("Bury", "Big bones");
                    try {
                        s.sleep(MethodProvider.random(500, 800));
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * drinks potions
     *
     * @param script
     */
    private void drinkPotions(DreamFighter script) {
        for (PotionItem potion : script.potions) {
            String skillName = potion.name.contains("Super") ? potion.name
                    .substring(6).toLowerCase() : potion.name.substring(0,
                    potion.name.indexOf(' ')).toLowerCase();
            Skill skill = null;
            switch (skillName) {
                case "strength":
                    skill = skill.STRENGTH;
                    break;
                case "attack":
                    skill = skill.ATTACK;
                    break;
                case "defence":
                    skill = skill.DEFENCE;
                    break;
                case "ranging":
                    skill = skill.RANGED;
                    break;
                case "prayer":
                    skill = skill.PRAYER;
                    break;
                case "combat":
                    if (script.skills.getStatic(Skill.ATTACK) < script.skills.getStatic(Skill.STRENGTH)) {
                        skill = Skill.ATTACK;
                    } else {
                        skill = Skill.STRENGTH;
                    }
                    break;
            }
            int bonus = skillName.equalsIgnoreCase("prayer") ? script.skills
                    .getStatic(skill) - potion.drinkAt : script.skills
                    .getStatic(skill) + potion.drinkAt;
            while (script.inventory.contains(new PotionMatch(skillName, s))
                    && script.skills.getDynamic(skill) < bonus) {
                script.inventory.interact("Drink",
                        new PotionMatch(skillName, s));
                try {
                    MethodProvider.sleep(MethodProvider.random(600, 800));
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Checks the ammunition and safespot
     *
     * @return true if out of ammo
     */
    public boolean doRange() {
        if (!s.inventory.contains(s.ammoName)
                && s.equipment.getItemInSlot(EquipmentSlot.ARROWS.slot) == null) {
            // No arrows in inventory or quiver
            s.log("Out of arrows");
            s.stop();
            return true;
        }
        if (s.inventory.getAmount(s.ammoName) > s.wieldAmount
                || s.equipment.getItemInSlot(EquipmentSlot.ARROWS.slot) == null) {
            // No more wielded arrows in quiver or arrows in inventory count
            // exceeded threshold
            s.inventory.interact("Wield", s.ammoName);
            try {
                s.sleep(s.random(400, 700));
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (s.useSafespot
                && !(s.myPosition().getX() == s.safespot.getX() && s
                .myPosition().getY() == s.safespot.getY())) {
            // If using safespot and player isn't there
            while (!(s.myPosition().getX() == s.safespot.getX() && s
                    .myPosition().getY() == s.safespot.getY())) {
                try {
                    // Walk to safespot
                    if (Library.distanceTo(s.safespot, s) < 6) {
                        MainScreenTileDestination td = new MainScreenTileDestination(
                                s.bot, s.safespot);
                        if (td.isVisible()) {
                            s.safespot.interact(s.bot, "Walk here");
                        } else {
                            s.camera.toPosition(s.safespot);
                        }
                    } else {
                        s.map.walk(s.safespot);
                    }
                    try {
                        MethodProvider.sleep(MethodProvider.random(400, 650));
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                }
            }
        }
        return false;
    }

    /**
     * determines if it's time to go back to the bank
     */
    private void shouldLeave() {
        if ((s.eatFood && !s.inventory.contains(s.foodType) && (s.skills
                .getDynamic(Skill.HITPOINTS) <= s.eatBelowHP))) {
            s.state = State.WALK_TO_BANK;
        } else if (s.inventory.isFull()
                && s.bankWhenFull
                && (!s.eatFood || s.eatFood
                && !s.inventory.contains(s.foodType))) {
            s.state = State.WALK_TO_BANK;
        }

    }

    /**
     * checks if the player is in combat
     *
     * @return true if in combat
     */
    private boolean inCombat() {
        return (s.myPlayer().isUnderAttack() || s.myPlayer().getInteracting() != null);
    }

    /**
     * attack the target, if we're not in combat
     *
     * @param kill   NPC to attack
     */
    private void attack(final NPC kill) {
        if (kill != null) {
            // If the NPC is moving, speed up the mouse
            int distance = kill.getPosition().distance(
                    s.myPlayer().getPosition());
            if (distance > 10 || attemptCount > 6) {
                attemptCount = 0;
                s.localWalker.walk(kill);
                return;
            }
            if (kill != null && !new EntityDestination(s.bot, kill).isVisible()
                    && distance <= 10) {
                s.camera.toEntity(kill);
                attemptCount++;
                return;
            }
            /*
             * if (kill.isMoving()) { if (s.locationTracker != null &&
			 * s.locationTracker.containsKey(kill) &&
			 * (s.locationTracker.get(kill).getX() != kill.getX() ||
			 * s.locationTracker .get(kill).getY() != kill.getY())) { int mouseX
			 * = kill.getX() + (int) (2.5 * (s.locationTracker.get(kill).getX()
			 * - kill .getX())); int mouseY = kill.getY() + (int) (2.5 *
			 * (s.locationTracker.get(kill).getY() - kill .getY())); // Hover
			 * over the tile that the NPC is approaching
			 * MainScreenTileDestination td = new MainScreenTileDestination(
			 * s.bot, new Position(mouseX, mouseY, kill.getZ())); Area a =
			 * td.getArea(); mouseX = (int) a.getBounds().getCenterX(); mouseY =
			 * (int) a.getBounds().getCenterY(); RectangleDestination rd = new
			 * RectangleDestination(s.bot, mouseX - 1, mouseY - 7, 3, 3);
			 * s.mouse.move(rd); Library.waitFor(new Condition() {
			 * 
			 * @Override public boolean satisfied(Script script) { return
			 * Library.getOptionIndex(s.menu.getMenu(), "Attack", kill) != -1; }
			 * 
			 * }, 1000, 100, s); } else if (s.locationTrackerOld != null &&
			 * s.locationTrackerOld.containsKey(kill) &&
			 * (s.locationTrackerOld.get(kill).getX() != kill .getX() ||
			 * s.locationTrackerOld.get(kill) .getY() != kill.getY())) { int
			 * mouseX = kill.getX() + (int) (2.5 *
			 * (s.locationTracker.get(kill).getX() - s.locationTrackerOld
			 * .get(kill).getX())); int mouseY = kill.getY() + (int) (2.5 *
			 * (s.locationTracker.get(kill).getY() - s.locationTrackerOld
			 * .get(kill).getY())); // Hover over the tile that the NPC is
			 * approaching MainScreenTileDestination td = new
			 * MainScreenTileDestination( s.bot, new Position(mouseX, mouseY,
			 * kill.getZ())); Area a = td.getArea(); mouseX = (int)
			 * a.getBounds().getCenterX(); mouseY = (int)
			 * a.getBounds().getCenterY(); RectangleDestination rd = new
			 * RectangleDestination(s.bot, mouseX - 1, mouseY - 7, 3, 3);
			 * s.mouse.move(rd); Library.waitFor(new Condition() {
			 * 
			 * @Override public boolean satisfied(Script script) { return
			 * Library.getOptionIndex(s.menu.getMenu(), "Attack", kill) != -1; }
			 * 
			 * }, 1000, 100, s); } else { // Hover over the NPC
			 * EntityDestination ed = new EntityDestination(s.bot, kill);
			 * s.mouse.move(ed); } } else {
			 */

            if (s.useCustomInteract) {
                // Hover over the NPC
                EntityDestination ed = new EntityDestination(s.bot, kill);
                s.mouse.move(ed);
                // }
                // Attempt to click Attack
                if (Library.doMenu("Attack", kill, s)) {
                    // If successful, wait for player to move/be in combat
                    attemptCount = 0;
                    Library.waitFor(new Condition() {

                        @Override
                        public boolean satisfied(Script script) {
                            return script.myPlayer().isMoving() || inCombat();
                        }

                    }, 1000, 100, s);
                    if (s.myPlayer().isMoving() || inCombat()) {
                        // Wait a bit, unless the npc died or click failed
                        Library.waitFor(new Condition() {

                            @Override
                            public boolean satisfied(Script script) {
                                if (!s.myPlayer().isMoving()) {
                                    try {
                                        MethodProvider.sleep(500);
                                    } catch (InterruptedException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }
                                    if (!inCombat())
                                        return true;
                                }
                                return kill.getHealth() == 0;
                            }

                        }, 3000, 100, s);
                    }
                }
            } else {
                if (kill.getName().equals("Rocks")
                        || kill.getName().equals("Rock Crab")) {
                    killRockCrabs(kill);
                } else {
                    s.mouse.setSpeed(5);
                    InteractionEvent event = new InteractionEvent(kill, "Attack");
                    if (s.useSafespot)
                        event.setWalkTo(false);
                    else {
                        if (!kill.isVisible() && Library.distanceTo(kill.getPosition(), s) > MethodProvider.random(6, 8))
                            event.setWalkTo(true);
                        else
                            event.setWalkTo(false);
                    }
                    s.getBot().getEventExecutor().execute(event);
                    if (event.hasFinished() && !event.hasFailed()) {
                        // s.log("Interact successful. Wait for move");
                        // If successful, wait for player to move/be in combat
                        attemptCount = 0;
                        Library.waitFor(new Condition() {

                            @Override
                            public boolean satisfied(Script script) {
                                return script.myPlayer().isMoving()
                                        || inCombat();
                            }

                        }, 1000, 100, s);
                        // s.log("Done waiting. Wait again");
                        if (s.myPlayer().isMoving() || !inCombat()) {
                            // Wait a bit, unless the npc died or click failed
                            Library.waitFor(new Condition() {

                                @Override
                                public boolean satisfied(Script script) {
                                    if (!s.myPlayer().isMoving()) {
                                        try {
                                            MethodProvider.sleep(500);
                                        } catch (InterruptedException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }
                                        return true;
                                    }
                                    return kill.getHealth() == 0;
                                }

                            }, 3000, 100, s);
                            try {
                                s.sleep(MethodProvider.random(1000, 1500));
                            } catch (InterruptedException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                    }
                    s.mouse.setSpeed(MethodProvider.random(7, 13));
                }
            }
        }

    }

    private void killRockCrabs(NPC kill) {
        if (kill != null) {
            if (Library.distanceTo(kill.getPosition(), s) < 6) {
                MainScreenTileDestination td = new MainScreenTileDestination(
                        s.bot, kill.getPosition());
                if (td != null && td.isVisible()) {
                    kill.getPosition().interact(s.bot, "Walk here");
                } else {
                    s.camera.toPosition(kill.getPosition());
                }
            } else {
                s.map.walk(kill.getPosition());
            }
            try {
                MethodProvider.sleep(3000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (!inCombat())
                rockCrabFailSafe++;
            else
                rockCrabFailSafe = 0;
            if (rockCrabFailSafe > 4)
                resetAggresion();
        }
    }

    private void resetAggresion() {
        boolean reset = false;
        if (rockCrabEast.contains(s.myPosition())) {
            while (!reset) {
                if (Library.distanceTo(safeEast, s) < 10) {
                    while (Library.distanceTo(returnEast, s) > 5) {
                        s.localWalker.walk(returnEast);
                    }
                } else {
                    s.localWalker.walk(safeEast);
                }
                if (Library.distanceTo(returnEast, s) < 5)
                    reset = true;
            }
        } else if (rockCrabWest.contains(s.myPosition())) {
            while (!reset) {
                if (Library.distanceTo(safeWest, s) < 10) {
                    while (Library.distanceTo(returnWest, s) > 5) {
                        s.localWalker.walk(returnWest);
                    }
                } else {
                    s.localWalker.walk(safeWest);
                }
                if (Library.distanceTo(returnWest, s) < 5)
                    reset = true;
            }
        }
        rockCrabFailSafe = 0;

    }

    /**
     * loots all items in the loot table
     *
     * @param loot  the items we need to loot
     * @throws InterruptedException
     */
    private void lootGroundItems(ArrayList<LootItem> loot) {
        if (loot != null) {
            for (LootItem item : loot) {
                GroundItem g = null;
                if (Library.toInt(item.name) != -1) {
                    g = s.groundItems.closest(Library.toInt(item.name));
                } else {
                    g = Library.getClosestGroundItem(item.name, s);
                }
                if (g == null || Library.distanceTo(g.getPosition(), s) > 14
                        || !s.map.canReach(g))
                    continue;
                if (g != null && g.exists() && s.inventory.isFull()
                        && s.eatBelowHP != 0
                        && s.inventory.contains(s.foodType)
                        && !g.getDefinition().getName().equals("Bones")) {
                    if (s.inventory.contains("Bones") && s.bonesToPeaches
                            && !item.name.equals("Bones")) {
                        s.inventory.getItem("Bones").interact("Drop");
                    } else if (shouldEat(item)) {
                        s.inventory.getItem(s.foodType).interact("Eat");
                    }
                    try {
                        MethodProvider.sleep(MethodProvider.random(1000, 1200));
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                for (int i = 0; i < 5 && g != null
                        && (!s.inventory.isFull() || (isStackable(g) && s.inventory.contains(g.getId())))
                        && g.exists()
                        && Library.distanceTo(g.getPosition(), s) < 14; i++) {
                    // System.out.println(g.getName());
                    long amount = s.inventory.getAmount(g.getId());
                    s.log("Loot ID: " + g.getId() + " Noted: " + (g.getDefinition().getName() == null || g.getDefinition().getName().equals("null")));
                    if (g.interact("Take")) {
                        if (Library.waitFor(new Condition() {

                            @Override
                            public boolean satisfied(Script script) {
                                return script.myPlayer().isMoving();
                            }

                        }, 1000, 100, s)) {
                            Library.waitFor(new Condition() {

                                @Override
                                public boolean satisfied(Script script) {
                                    return !script.myPlayer().isMoving();
                                }

                            }, 5000, 100, s);
                        }
                        long looted = s.inventory.getAmount(g.getId()) - amount;
                        s.lootList.get(s.lootList.indexOf(item)).lootedAmount += looted;
                        if (Library.toInt(item.name) != -1) {
                            g = s.groundItems.closest(Library.toInt(item.name));
                        } else {
                            g = Library.getClosestGroundItem(item.name, s);
                        }
                        break;
                    }
                    try {
                        MethodProvider.sleep(MethodProvider.random(1500, 2000));
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }
        // System.out.println("Done looting");
    }

    private final String[] RUNES = new String[]{"Air rune", "Water rune",
            "Earth rune", "Fire rune", "Mind rune", "Chaos rune", "Death rune",
            "Body rune", "Nature rune", "Law rune", "Cosmic rune",
            "Blood rune", "Soul rune", "Astral rune", "Dust rune"};

    /**
     * Checks whether item is a stackable item
     *
     * @param item
     * @return true if stackable
     */
    private boolean isStackable(GroundItem item) {
        if (item.getDefinition().getName().endsWith("arrow")
                || item.getDefinition().getName().endsWith("dart")
                || item.getDefinition().getName().endsWith("knife")
                || item.getDefinition().getName().endsWith("bolts")
                || item.getDefinition().getName().equals("Coins")
                || item.getDefinition().getName().endsWith(" seed")) {
            return true;
        }
        for (String rune : RUNES) {
            if (item.getDefinition().getName().equals(rune)) {
                return true;
            }
        }
        if(item.getDefinition().getName() == null || item.getDefinition().getName().equals("null")) // noted item
            return true;
        return false;
    }

    /**
     * Checks whether item is arrow, knife, dart, bolt, or rune, and determines
     * whether to eat
     *
     * @param item item to check
     * @return true if should eat
     */
    private boolean shouldEat(LootItem item) {
        if (item.name.endsWith("arrow") || item.name.endsWith("dart")
                || item.name.endsWith("knife") || item.name.endsWith("bolts")
                || item.name.equals("Coins")) {
            return !s.inventory.contains(item.name);
        }
        for (String rune : RUNES) {
            if (item.name.equals(rune)) {
                return !s.inventory.contains(item.name);
            }
        }
        return item.eatForSpace;
    }

    /**
     * Return the best NPC to attack next by filtering a list of all local NPCs
     *
     * @return the best NPC to attack
     */
    @SuppressWarnings("unchecked")
    private NPC getTarget() {
        NPC closest = s.npcs.closest(new library.BestMatch(s));
        return closest;
    }

    /**
     * eats food when needed
     *
     * @param script         allows us access to the scripts behaviors and properties
     * @param bonesToPeaches
     */
    private void eatFood(DreamFighter script, boolean bonesToPeaches) {
        if (bonesToPeaches) {
            usePeaches(script);
        } else {
            regularEat(script);
        }
    }

    /**
     * eat (bones to peaches method
     *
     * @param script
     */
    private void usePeaches(DreamFighter script) {
        if (script.skills.getDynamic(Skill.HITPOINTS) <= script.eatBelowHP
                && script.inventory.contains("Peach")) {
            regularEat(script);
        } else if (script.inventory.isFull()
                && script.inventory.contains("Bones")
                && !script.inventory.contains("Peach")) {
            script.inventory.getItem("Bones to peaches").interact("Break");
        } else if (script.getSkills().getDynamic(Skill.HITPOINTS) <= script.eatBelowHP
                && (script.inventory.contains("Bones") || script.inventory.contains("Big bones"))
                && !script.inventory.contains("Peach")) {
            script.inventory.getItem("Bones to peaches").interact("Break");
        }
    }

    /**
     * eat food
     *
     * @param script
     */
    private void regularEat(DreamFighter script) {
        if (s.skills.getDynamic(Skill.HITPOINTS) <= script.eatBelowHP
                && s.inventory.contains(script.foodType)) {
            s.inventory.getItem(script.foodType).interact("Eat");
            new ConditionalSleep(2000) {
                @Override
                public boolean condition() {
                    return !s.myPlayer().isAnimating();
                }
            };
        }
    }

    public boolean shouldDoSpecial() {
        if (s.combat.getSpecialPercentage() > specialAt) {
            return true;
        } else {
            if (regularName == null) {
                return s.equipment.getItemInSlot(EquipmentSlot.WEAPON.slot) != null;
            } else {
                return s.equipment.getItemInSlot(EquipmentSlot.WEAPON.slot) == null
                        || !s.equipment
                        .getItemInSlot(EquipmentSlot.WEAPON.slot)
                        .getName().equals(regularName);
            }
        }
    }

    public void doSpecial() {
        if (s.combat.getSpecialPercentage() > specialAt) {
            if (s.equipment.getItemInSlot(EquipmentSlot.WEAPON.slot) == null
                    || !s.equipment.getItemInSlot(EquipmentSlot.WEAPON.slot)
                    .getName().equals(specialName)) {
                // Equip the special weapon
                s.inventory.interact("Wield", specialName);
                try {
                    s.sleep(MethodProvider.random(500, 700));
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else {
                s.combat.setSpecialAttack(true);
                try {
                    s.sleep(MethodProvider.random(500, 700));
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                specialAt = Math.max(100,
                        realSpecialAt + MethodProvider.random(0, 7));
            }
        } else {
            if (regularName == null) {
                if (s.equipment.getItemInSlot(EquipmentSlot.WEAPON.slot) != null) {
                    s.equipment.unequip(EquipmentSlot.WEAPON);
                    try {
                        s.sleep(MethodProvider.random(500, 700));
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            } else if (s.equipment.getItemInSlot(EquipmentSlot.WEAPON.slot) == null
                    || !s.equipment.getItemInSlot(EquipmentSlot.WEAPON.slot)
                    .getName().equals(regularName)) {
                s.inventory.interact("Wield", regularName);
            }
        }
    }

    public void onMessage(Message m) {
        if (m.getMessage().contains("can't reach that")) {
            doorProblem = true;
        }
    }

}
