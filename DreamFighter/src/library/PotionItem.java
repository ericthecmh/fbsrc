package library;

/**
 * 
 * @author Erik
 * 
 */
public class PotionItem {
	public String name;
	public int drinkAt;

	public PotionItem(String name, int drinkAt) {
		this.name = name;
		this.drinkAt = drinkAt;
	}
}
