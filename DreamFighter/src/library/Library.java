package library;

import static org.osbot.rs07.script.MethodProvider.random;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import main.DreamFighter;

import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.map.PositionPolygon;
import org.osbot.rs07.api.model.Entity;
import org.osbot.rs07.api.model.GroundItem;
import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.Option;
import org.osbot.rs07.api.ui.RS2Interface;
import org.osbot.rs07.api.ui.RS2InterfaceChild;
import org.osbot.rs07.api.ui.Tab;
import org.osbot.rs07.input.mouse.EntityDestination;
import org.osbot.rs07.input.mouse.RectangleDestination;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;

import security.Constants;

/**
 * 
 * @author Ericthecmh,Eliot
 */
public class Library {

	private final static int MENU_HEADER_HEIGHT = 18;
	private final static int MENU_ITEM_HEIGHT = 15;
	private final static int FRIENDS_CORNER_X = 557;
	private final static int FRIENDS_CORNER_Y = 230;
	private final static int FRIENDS_ITEM_HEIGHT = 15;
	private final static String SERVER_IP = "192.";

	public static double distanceBetween(Position p1, Position p2) {
		if (p1.getZ() != p2.getZ()) // There's no way p1 can be reached from p2
		{
			return 2000000000.0;
		}
		return Math.sqrt((p1.getX() - p2.getX()) * (p1.getX() - p2.getX())
				+ (p1.getY() - p2.getY()) * (p1.getY() - p2.getY()));
	}

	public static double distanceTo(Position p, DreamFighter s) {
		return distanceBetween(p, s.myPosition());
	}

	public static int toInt(String s) {
		try {
			return Integer.parseInt(s);
		} catch (Exception e) {
			return -1;
		}
	}

	/**
	 * Walks the path defined by the Position Polygon until within 6 tiles of
	 * destination
	 * 
	 * @param path
	 *            path to be walked
	 * @param destination
	 *            should be final tile in the path
	 * @param script
	 *            reference to the script. Used for walking and distance
	 * @param exact
	 * @return true if method did something, false otherwise
	 */
	public static boolean walkPath(PositionPolygon path, Position destination,
			DreamFighter script) {
		int pi = path.getClosestIndex(script.myPosition());
		if (pi == 0) {
			Point p = path.get(pi);
			Position pos = new Position(p.x, p.y, script.myPosition().getZ());
			if (Library.distanceTo(pos, script) > 14) {
				try {
					Library.walkClosest(pos, script);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return true;
			}
		}
		if (pi == -1) {
			Point p = path.get(0);
			Position pos = new Position(p.x, p.y, script.myPosition().getZ());
			if (Library.distanceTo(pos, script) > 14) {
				try {
					Library.walkClosest(pos, script);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return true;
			}
			return false;
		}

		if (Library.distanceTo(destination, script) < 6
				&& script.map.canReach(destination)) {
			return false;
		}

		int c = path.getCount();
		int wi = c - 1;

		if (wi == pi) {
			return false;
		}

		if (!script.settings.isRunning()
				&& script.settings.getRunEnergy() > random(30, 50)) {
			script.settings.setRunning(true);
			return true;
		}

		while (wi >= 0 && wi < c && wi >= pi) {
			Point point = path.get(wi);
			if (script.localWalker.walk(point.x, point.y)) {
				return true;
			}
			wi--;
		}

		return false;
	}

	/**
	 * Waits for condition's satisfied method to return true
	 * 
	 * @param c
	 *            the condition
	 * @param waitTime
	 *            total time to wait before giving up
	 * @param delta
	 *            timestep to wait in
	 * @param script
	 *            reference to script
	 * @return true if the condition was met in time, false otherwise
	 */
	public static boolean waitFor(Condition c, int waitTime, int delta,
			Script script) {
		try {
			for (int i = 0; i < (waitTime / delta); i++) {
				if (c.satisfied(script)) {
					return true;
				}
				MethodProvider.sleep(delta);
			}
		} catch (Exception e) {

		}
		return false;
	}

	/**
	 * Searches a string array for a string
	 * 
	 * @param hay
	 *            array to search through
	 * @param needle
	 *            string to be searched for
	 * @return true if needle is found in haystack
	 */
	public static boolean containsString(String[] hay, String needle) {
		for (String s : hay) {
			if (s != null && s.equals(needle)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Searches a option list for a string
	 * 
	 * @param hay
	 *            option to search through
	 * @param needle
	 *            string to be searched for
	 * @return true if needle is found in haystack
	 */
	public static int getOptionIndex(List<Option> hay, String needle, Entity e) {
		for (int i = 0; i < hay.size(); i++) {
			Option o = hay.get(i);
			if (o != null && o.action != null && o.action.equals(needle)
					&& o.name != null
					&& o.name.contains(">" + e.getName() + "<")) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Gets the closest RS2Object with name and action
	 * 
	 * @param name
	 * @param action
	 * @param script
	 * @return RS2Object if found, null otherwise
	 */
	public static RS2Object getClosestObjectWithInteract(String name,
			String action, DreamFighter script) {
		RS2Object ret = null;
		double distance = 2000000000.0;
		for (RS2Object o : script.objects.getAll()) {
			if (o != null && o.getName().equals(name)
					&& containsString(o.getDefinition().getActions(), action)
					&& distanceTo(o.getPosition(), script) < distance) {
				distance = distanceTo(o.getPosition(), script);
				ret = o;
			}
		}
		return ret;
	}

	/**
	 * Gets the closest RS2Object with id and action
	 * 
	 * @param id
	 * @param action
	 * @param script
	 * @return RS2Object if found, null otherwise
	 */
	public static RS2Object getClosestObjectWithInteract(int id, String action,
			DreamFighter script) {
		RS2Object ret = null;
		double distance = 2000000000.0;
		for (RS2Object o : script.objects.getAll()) {
			if (o != null && o.getId() == id
					&& containsString(o.getDefinition().getActions(), action)
					&& distanceTo(o.getPosition(), script) < distance) {
				distance = distanceTo(o.getPosition(), script);
				ret = o;
			}
		}
		return ret;
	}

	/**
	 * Gets the closest RS2Object with name and action
	 * 
	 * @param name
	 * @param action
	 * @param script
	 * @return RS2Object if found, null otherwise
	 */
	public static NPC getClosestNPCWithInteract(String name, String action,
			DreamFighter script) {
		NPC ret = null;
		double distance = 2000000000.0;
		for (NPC n : script.npcs.getAll()) {
			if (n != null && n.getName().equals(name)
					&& containsString(n.getDefinition().getActions(), action)
					&& distanceTo(n.getPosition(), script) < distance) {
				distance = distanceTo(n.getPosition(), script);
				ret = n;
			}
		}
		return ret;
	}

	/**
	 * Gets the closest RS2Object with id and action
	 * 
	 * @param id
	 * @param action
	 * @param script
	 * @return RS2Object if found, null otherwise
	 */
	public static NPC getClosestNPCWithInteract(int id, String action,
			DreamFighter script) {
		NPC ret = null;
		double distance = 2000000000.0;
		for (NPC n : script.npcs.getAll()) {
			if (n != null && n.getId() == id
					&& containsString(n.getDefinition().getActions(), action)
					&& distanceTo(n.getPosition(), script) < distance) {
				distance = distanceTo(n.getPosition(), script);
				ret = n;
			}
		}
		return ret;
	}

	/**
	 * does the inventory contain every item it requires?
	 * 
	 * @param items
	 *            list of items to check for
	 * @param script
	 * 
	 * @return result of checking if we have all the needed items
	 */
	public static boolean inventoryContainsAll(ArrayList<BankItem> items,
			DreamFighter script) {
		for (BankItem i : items) {
			if (!script.inventory.contains(i.name)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Searches the menu for the specified action and tries to select it
	 * 
	 * @param action
	 *            Action to be performed
	 * @param script
	 * @return true if successfully clicked action
	 */
	public static boolean doMenu(String action, Entity e, DreamFighter script) {
		if (script.menu == null) {
			// script.log("No menu");
			return false;
		}
		if (script.menu.getMenu() == null) {
			// script.log("Menu option null");
			return false;
		}
		int index = getOptionIndex(script.menu.getMenu(), action, e);
		if (index != -1) {
			if (index == 0) {
				// script.log("Click");
				EntityDestination ed = new EntityDestination(script.bot, e);
				script.mouse.click(ed, false);
				return getOptionIndex(script.menu.getMenu(), action, e) == 0;
			} else {
				// script.log("Right click");
				EntityDestination ed = new EntityDestination(script.bot, e);
				script.mouse.click(ed, true);
				if (script.menu.isOpen()) {
					try {
						MethodProvider.sleep(MethodProvider.random(400, 700));
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					int x = script.menu.getX();
					int y = script.menu.getY();
					y += MENU_HEADER_HEIGHT + 1;
					y += index * MENU_ITEM_HEIGHT;
					RectangleDestination menuItem = new RectangleDestination(
							script.bot, x, y + 2, script.menu.getWidth() - 2,
							MENU_ITEM_HEIGHT);
					script.mouse.click(menuItem, false);
					return true;
				}
				return false;
			}
		} else {
			// script.log("Not found");
			return false;
		}
	}

	public static void withdraw(final int name, final int amount,
			final DreamFighter script) {
		script.log("Withdrawing " + amount + " of " + name);
		if (!script.bank.isOpen())
			return;
		if (amount == 1 || amount == 5 || amount == 10) {
			script.bank.withdraw(name, amount);
		} else {
			final long oldAmount = script.inventory.getAmount(name);
			new Thread(new Runnable() {
				@Override
				public void run() {
					script.bank.withdraw(name, amount);
				}
			}).start();
			// Wait for either the withdraw to succeed or for the "Enter x" to
			// appear
			if (Library.waitFor(new Condition() {

				@Override
				public boolean satisfied(Script script) {
					return script.inventory.getAmount(name) != oldAmount
							|| (script.interfaces.get(137) != null
									&& script.interfaces.get(137).getChild(2) != null && script.interfaces
									.get(137).getChild(2).getPosition().getX() == -1);
				}

			}, 5000, 100, script)) {
				if (script.inventory.getAmount(name) == oldAmount
						&& (script.interfaces.get(137) != null
								&& script.interfaces.get(137).getChild(2) != null && script.interfaces
								.get(137).getChild(2).getPosition().getX() == -1)) {
					// If didn't withdraw, then the "Enter x" screen must be up.
					// Enter amount
					script.keyboard.typeString("" + amount);
					try {
						MethodProvider.sleep(random(700, 1000));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}

	public static void withdraw(final String name, final int amount,
			final DreamFighter script) {
		if (!script.bank.isOpen())
			return;
		if (amount == 1 || amount == 5 || amount == 10) {
			script.bank.withdraw(name, amount);
		} else {
			final long oldAmount = script.inventory.getAmount(name);
			new Thread(new Runnable() {
				@Override
				public void run() {
					script.bank.withdraw(name, amount);
				}
			}).start();
			// Wait for either the withdraw to succeed or for the "Enter x" to
			// appear
			if (Library.waitFor(new Condition() {

				@Override
				public boolean satisfied(Script script) {
					return script.inventory.getAmount(name) != oldAmount
							|| (script.interfaces.get(137) != null
									&& script.interfaces.get(137).getChild(2) != null && script.interfaces
									.get(137).getChild(2).getPosition().getX() == -1);
				}

			}, 5000, 100, script)) {
				if (script.inventory.getAmount(name) == oldAmount
						&& (script.interfaces.get(137) != null
								&& script.interfaces.get(137).getChild(2) != null && script.interfaces
								.get(137).getChild(2).getPosition().getX() == -1)) {
					// If didn't withdraw, then the "Enter x" screen must be up.
					// Enter amount
					script.keyboard.typeString("" + amount);
					try {
						MethodProvider.sleep(random(700, 1000));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					Library.waitFor(new Condition() {

						@Override
						public boolean satisfied(Script script) {
							return script.inventory.getAmount(name) == oldAmount;
						}

					}, 2000, 100, script);
				}
			}
		}
	}

	/**
	 * Gets the NPC that is currently facing player
	 * 
	 * @param script
	 * @return the NPC, null if no NPC
	 */
	public static NPC getNPCFacingMe(DreamFighter script) {
		for (NPC n : script.npcs.getAll()) {
			if (n != null && n.getInteracting() == script.myPlayer())
				return n;
		}
		return null;
	}

	private static void walkClosest(Position goal, DreamFighter script)
			throws InterruptedException {
		Position p = getBestReachablePositionInDirectionOf(goal, script);
		if (p != null)
			script.localWalker.walk(p);
	}

	private static void walkClosest(RS2Object object, DreamFighter script)
			throws InterruptedException {
		Position p = getBestReachablePositionInDirectionOf(
				object.getPosition(), script);
		if (p != null)
			script.localWalker.walk(p);
	}

	private static Position getBestReachablePositionInDirectionOf(
			Position goal, DreamFighter script) {
		int threshold = random(12, 14);
		double distance = 2000000000.0;
		Position best = null;
		for (int i = -14; i <= 14; i++) {
			for (int j = -14; j <= 14; j++) {
				if (i * i + j * j <= threshold * threshold) {
					Position p = new Position(script.myPosition().getX() + i,
							script.myPosition().getY() + j, script.myPosition()
									.getZ());
					if (script.map.canReach(p)
							&& script.map.realDistance(p) <= 14
							&& distanceBetween(p, goal) <= Math.min(
									distanceTo(goal, script), distance)) {
						distance = distanceBetween(p, goal);
						best = p;
					}
				}
			}
		}
		return best;
	}

	/**
	 * Answers the question on the stronghold of security.
	 * 
	 * NOTE: After the question is answered, an interface says "Correct!". This
	 * method will not close that interface
	 * 
	 * @param script
	 * @return true if answered
	 */
	public static boolean doDoorQuestion(Script script) {
		if (Library.waitFor(new Condition() {

			@Override
			public boolean satisfied(Script script) {
				return script.dialogues.clickContinue()
						|| script.interfaces.getValid()[230]
						|| script.interfaces.getValid()[228];
			}

		}, 2000, 100, script)) {
			try {
				script.sleep(2000);
				while (script.dialogues.isPendingContinuation()) {
					script.dialogues.clickContinue();
					script.sleep(2000);
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (script.interfaces.getValid()[230]) {
				String[] strings = new String[] { "",
						script.interfaces.get(230).getChild(1).getMessage(),
						script.interfaces.get(230).getChild(2).getMessage(),
						script.interfaces.get(230).getChild(3).getMessage() };
				if (strings[2].contains("No") && strings[2].length() < 4) {
					return script.interfaces.get(230).getChild(2).interact();
				}
				if (strings[1].startsWith("Recover"))
					return script.interfaces.get(230).getChild(1).interact();
				if (strings[1].startsWith("Virus scan"))
					return script.interfaces.get(230).getChild(1).interact();
				if (strings[1].contains("Memorable"))
					return script.interfaces.get(230).getChild(1).interact();
				if (strings[3].contains("Nowhere"))
					return script.interfaces.get(230).getChild(3).interact();
				if (strings[3].contains("Inbox"))
					return script.interfaces.get(230).getChild(3).interact();
				if (strings[1].contains("give them the information"))
					return script.interfaces.get(230).getChild(1).interact();
				if (strings[3].contains("tell them"))
					return script.interfaces.get(230).getChild(3).interact();
				if (strings[1].contains("help me recover"))
					return script.interfaces.get(230).getChild(1).interact();
				if (strings[3].contains("birthday"))
					return script.interfaces.get(230).getChild(3).interact();
				if (strings[2].contains("Don't give"))
					return script.interfaces.get(230).getChild(2).interact();
				if (strings[2].contains("Only on"))
					return script.interfaces.get(230).getChild(2).interact();
				if (strings[2].contains("couple of months"))
					return script.interfaces.get(230).getChild(2).interact();
				if (strings[1].contains("steal my pass"))
					return script.interfaces.get(230).getChild(1).interact();
				if (strings[3].contains("Nobody"))
					return script.interfaces.get(230).getChild(3).interact();
				if (strings[2].contains("recover your"))
					return script.interfaces.get(230).getChild(2).interact();
				if (strings[3].contains("Politely"))
					return script.interfaces.get(230).getChild(3).interact();
				if (strings[3].contains("Recover a Lost"))
					return script.interfaces.get(230).getChild(3).interact();
				return false;
			} else if (script.interfaces.getValid()[228]) {
				if (script.interfaces.get(228).getChild(2).getMessage()
						.contains("No")
						&& script.interfaces.get(228).getChild(2).getMessage()
								.length() < 4)
					return script.interfaces.get(228).getChild(2).interact();
				if (script.interfaces.get(228).getChild(1).getMessage()
						.contains("any banker"))
					return script.interfaces.get(228).getChild(1).interact();
				return false;
			}
			return false;
		}
		return false;
	}

	/**
	 * Gets the byte representation of a string (using ascii)
	 * 
	 * @param string
	 * @return byte representation
	 */
	public static byte[] getBytes(String string) {
		byte[] bytes = new byte[string.length()];
		for (int i = 0; i < string.length(); i++) {
			bytes[i] = (byte) string.charAt(i);
		}
		return bytes;
	}

	/**
	 * converts a byte array into a hexadecimal string
	 * 
	 * @param encrypted
	 * @return hex string
	 */
	public static String toHex(byte[] encrypted) {
		byte[] newBytes = new byte[encrypted.length * 2];
		for (int i = 0; i < encrypted.length; i++) {
			newBytes[i * 2 + 1] = getHex(encrypted[i] & 15);
			newBytes[i * 2] = getHex(encrypted[i] >> 4);
		}
		return new String(newBytes);
	}

	/**
	 * converts a hexadecimal string to a byte array
	 * 
	 * @param hex
	 * @return
	 */
	public static byte[] fromHex(String hex) {
		byte[] newBytes = new byte[hex.length() / 2];
		for (int i = 0; i < newBytes.length; i++) {
			newBytes[i] = fromHex(hex.charAt(i * 2));
			newBytes[i] <<= 4;
			newBytes[i] += fromHex(hex.charAt(i * 2 + 1));
		}
		return newBytes;
	}

	/**
	 * Converts a hexademical character into the base 10 value
	 * 
	 * @param a
	 * @return
	 */
	public static byte fromHex(char a) {
		if (a >= 48 && a <= 57)
			return (byte) (a - 48);
		else
			return (byte) (a - 55);
	}

	/**
	 * Converts a base 10 value to the hexadecimal number (ascii)
	 * 
	 * @param i
	 * @return
	 */
	public static byte getHex(int i) {
		if (i >= 0 && i <= 9)
			return (byte) (i + 48);
		else
			return (byte) (i - 10 + 65);
	}

	/**
	 * Flips the byte array
	 * 
	 * @param key
	 * @return
	 */
	public static byte[] flip(byte[] key) {
		byte[] newbytes = new byte[key.length];
		for (int i = 0; i < key.length; i++) {
			newbytes[key.length - i - 1] = key[i];
		}
		return newbytes;
	}

	/**
	 * Encrypts a string based on a given key
	 * 
	 * @param string
	 * @param keyString
	 * @return
	 */
	public static byte[] encrypt(String string, String keyString) {
		byte[] data = getBytes(string);
		byte[] key = flip(getBytes(keyString));
		byte[] newData = new byte[string.length()];
		for (int i = 0; i < string.length(); i++) {
			newData[i] = (byte) (data[i] ^ key[i % key.length]);
		}
		return newData;
	}

	/**
	 * Decrypts an encrypted string based on the key
	 * 
	 * @param string
	 * @param keyString
	 * @return
	 */
	public static byte[] decrypt(String string, String keyString) {
		byte[] key = flip(getBytes(keyString));
		byte[] data = getBytes(string);
		byte[] newData = new byte[string.length()];
		for (int i = 0; i < string.length(); i++) {
			newData[i] = (byte) (data[i] ^ key[i % key.length]);
		}
		return newData;
	}

	/**
	 * Decrypts an encrypted byte array based on the key
	 *
	 * @param keyString
	 * @return
	 */
	public static String decrypt(byte[] data, String keyString) {
		byte[] key = flip(getBytes(keyString));
		byte[] newData = new byte[data.length];
		for (int i = 0; i < data.length; i++) {
			newData[i] = (byte) (data[i] ^ key[i % key.length]);
		}
		return new String(newData);
	}

	/**
	 * Checks if the server is valid
	 * 
	 * @return true if valid, false if not
	 */
	public static boolean serverCheck(DreamFighter s) {
		long time = System.nanoTime();
		boolean valid = true;
		// Server check 1, decryption check
		String data;
		String pi;
		try {
			pi = SERVER_IP + PriceFetcher.ip2 + "." + s.ip3 + Constants.ip4;
			data = toHex(encrypt("" + time, "DreamFighter"));
			URL url = new URL("http://" + pi + "/verify1.php?AES_DATA=" + data
					+ "&AES_KEY=" + s.bot.getUsername());
			BufferedReader br = new BufferedReader(new InputStreamReader(
					url.openStream()));
			long response = Long.parseLong(br.readLine());
			valid = valid && (response == time);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		// Server check 2, encryption check
		try {
			URL url = new URL("http://" + pi + "/verify2.php?AES_DATA=" + data
					+ "&AES_KEY=" + "AOEU");
			BufferedReader br = new BufferedReader(new InputStreamReader(
					url.openStream()));
			String response = br.readLine();
			return valid
					&& response.equals(Library.toHex(Library.encrypt(data,
							"DreamScripts")));
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Checks whether the current system time is valid
	 * 
	 * @return true if valid
	 */
	public static boolean timeCheck() {
		try {
			System.out
					.println(Math.abs(System.currentTimeMillis() - getTime()));
			return (Math.abs(System.currentTimeMillis() - getTime()) < 86400000);
		} catch (Exception e) {

		}
		return false;
	}

	/**
	 * Gets the current time in milliseconds from the server
	 * 
	 * @return time
	 */
	public static long getTime() {
		try {
			String pi = SERVER_IP + PriceFetcher.ip2 + "." + DreamFighter.ip3
					+ Constants.ip4;
			URL url = new URL("http://" + pi + "/time.php");
			BufferedReader br = new BufferedReader(new InputStreamReader(
					url.openStream()));
			String s;
			while ((s = br.readLine()) != null) {
				return Long.parseLong(s, 10);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return -10000000;
	}

	/**
	 * Returns valid if the script is valid
	 * 
	 * @return
	 */
	public static boolean scriptValidCheck() {
		try {
			String pi = SERVER_IP + PriceFetcher.ip2 + "." + DreamFighter.ip3
					+ Constants.ip4;
			URL url = new URL("http://" + pi + "/valid.php");
			BufferedReader br = new BufferedReader(new InputStreamReader(
					url.openStream()));
			String s;
			while ((s = br.readLine()) != null) {
				return s.equals("True");
			}
			return false;
		} catch (Exception e) {
			return false;
		}
	}

	public static RS2Object getObjectAt(Position doorPosition, String name,
			DreamFighter script) {
		for (RS2Object o : script.objects.getAll()) {
			if (o != null && o.getPosition().getX() == doorPosition.getX()
					&& o.getPosition().getY() == doorPosition.getY()
					&& o.getPosition().getZ() == doorPosition.getZ()
					&& o.getName().equals(name))
				return o;
		}
		return null;
	}

	/**
	 * Gets the closest ground item with the given name. Checks name using
	 * definitions
	 * 
	 * @param name
	 *            name of item
	 * @return closest ground item, null if none
	 */
	public static GroundItem getClosestGroundItem(String name,
			DreamFighter script) {
		double distance = 2000000000;
		GroundItem closest = null;
		for (GroundItem i : script.groundItems.getAll()) {
			if (i != null && i.getName().equals(name)
					&& Library.distanceTo(i.getPosition(), script) < distance) {
				distance = Library.distanceTo(i.getPosition(), script);
				closest = i;
			}
		}
		return closest;
	}

	/**
	 * Sends a message to a friend using the friends list
	 * 
	 * @param username
	 * @param message
	 */
	public static void sendFriendMessage(String username, String message,
			Script script) {
		while (script.tabs.getOpen() != Tab.FRIENDS) {
			script.tabs.open(Tab.FRIENDS);
			try {
				script.sleep(random(400, 700));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		RS2Interface inte = script.interfaces.get(429);
		RS2InterfaceChild child = inte.getChild(3);
		int i = 0;
		for (; i < 1000; i+=3) {
			RS2InterfaceChild friend = child.getChild(i);
			if (friend != null && friend.isVisible()) {
				if (friend.getMessage().equalsIgnoreCase(username)) {
					break;
				}
			} else {
				break;
			}
		}
		script.log(i + "");
        i /= 3;
		if (i != 1000) {
			// Friend found.
			int y = FRIENDS_CORNER_Y + i * FRIENDS_ITEM_HEIGHT;
			if (y < 410) {
				RectangleDestination rd = new RectangleDestination(script.bot,
						FRIENDS_CORNER_X, y + 1, 70, FRIENDS_ITEM_HEIGHT - 1);
				script.mouse.click(rd);
				if (Library.waitFor(new Condition() {

					@Override
					public boolean satisfied(Script script) {
						RS2Interface inte = script.interfaces.get(548);
						RS2InterfaceChild child = inte.getChild(124);
						return child != null && child.isVisible();
					}

				}, 1000, 100, script)) {
					script.keyboard.typeString(message);
				}
			}
		}
	}

	public static RS2Object getClosestSSDoor(DreamFighter script) {
		RS2Object ret = script.objects.closest("Gate of War");
		if(ret != null)
			return ret;
		ret = script.objects.closest("Rickety door");
		if(ret != null)
			return ret;
		ret = script.objects.closest("Oozing barrier");
		if(ret != null)
			return ret;
		ret = script.objects.closest("Portal of Death");
		return ret;
	}
}
