package library;

/**
 * @author Erik
 */
import main.DreamFighter;

import org.osbot.rs07.api.filter.Filter;
import org.osbot.rs07.api.model.NPC;

public class BestMatch implements Filter<NPC> {
	DreamFighter script;

	public BestMatch(DreamFighter script) {
		this.script = script;
	}

	@Override
	/**
	 * check if this NPC is a match with ones on our attackList
	 * @param potential target
	 * @return if we can attack the subject
	 */
	public boolean match(NPC subject) {
		if (script.attackList != null) {
			for (FightableNPC n : script.attackList) {
				if (subject.getName().equals(n.name) && subject.getLevel() == n.level && !subject.isUnderAttack() && subject.getHealth() > 0 && (script.map.canReach(subject) || script.canReach && script.useRange || Library.getClosestSSDoor(script) != null)) {
					return true;
				} else if (subject.getName().equals(n.name) && (n.name.equals("Rocks") || n.name.equals("Rock Crab"))) {
					return true;
				}
			}
		}

		return false;
	}
}
