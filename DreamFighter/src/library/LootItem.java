package library;

/**
 * 
 * @author Erik
 * 
 */
// represents an item we wish to loot
public class LootItem {
	// represent the name of the item
	public String name;

	// represent the priority if an item (true-high, false-low)
	public boolean highPriority;

	// whether or not the script should eat to make room for food
	public boolean eatForSpace;

	// represent the avergage price of an item
	public int avgPrice;

	// represent how many have been looted
	public int lootedAmount;

	// construct an instance of LootItem
	public LootItem(String name, boolean highPriority, boolean eat, int avgPrice) {
		this.name = name;
		this.highPriority = highPriority;
		this.eatForSpace = eat;
		this.avgPrice = avgPrice;
		this.lootedAmount = 0;
	}
}
