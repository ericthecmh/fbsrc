package library;

import org.osbot.rs07.script.Script;

import main.DreamFighter;

/**
 * Used in conjunction with Library's wait methods
 * 
 * @author Eliot,Ericthecmh
 */
public interface Condition {

	public abstract boolean satisfied(Script script);
}
