package library;

/**
 * 
 * @author Erik
 * 
 */
// represents an item we wish to withdraw from the bank
public class BankItem {
	// represent the name of the item
	public String name;

	// represent the amount of the item
	public int amount;

	// construct a Bank Item
	public BankItem(String name, int amount) {
		this.name = name;
		this.amount = amount;
	}
}
