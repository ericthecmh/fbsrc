package library;

import org.osbot.rs07.api.filter.Filter;
import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.api.ui.Tab;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;

public class PotionMatch implements Filter<Item> {
	String name;
	Script s;

	public PotionMatch(String name, Script s) {
		this.name = name;
		this.s = s;
	}

	@Override
	public boolean match(Item arg0) {
		while(s.tabs.getOpen() != Tab.INVENTORY){
			s.tabs.open(Tab.INVENTORY);
			try {
				s.sleep(MethodProvider.random(500,700));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return arg0.getName().toLowerCase().contains(this.name);
	}

}
