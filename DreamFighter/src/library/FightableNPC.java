package library;

/**
 * 
 * @author Erik
 * 
 */
// represents a fightable NPC that we wish to engage
public class FightableNPC {
	// Name of NPC
	String name;
	// level of NPC
	int level;

	// construct a FightableNPC
	public FightableNPC(String name, int level) {
		this.name = name;
		this.level = level;
	}
}
