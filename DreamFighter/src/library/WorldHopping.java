package library;

import org.osbot.rs07.api.ui.Tab;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.script.Script;

public class WorldHopping {
	// world list
	private int[] memWorlds = { 303, 304, 305, 306, 309, 310, 311, 312, 313,
			314, 317, 318, 319, 320, 321, 322, 326, 327, 328, 329, 330, 333,
			334, 335, 336, 338, 341, 342, 343, 344, 345, 346, 349, 350, 351,
			352, 353, 354, 357, 358, 359, 360, 361, 362, 365, 366, 367, 368,
			369, 370, 373, 374, 375, 376, 377, 378 };
	private int[] f2pWorlds = { 381, 382, 383, 384, 393, 394, 308, 316 };

	Script script;

	public WorldHopping(Script script) {
		this.script = script;
	}

	/**
	 * checks if in bot world
	 * 
	 * @return if in bot world
	 */
	public boolean isInBotWorld() {
		return this.script.client.getCurrentWorld() == 385
				|| this.script.client.getCurrentWorld() == 386;
	}

	private boolean isMem(){
		for(int i : f2pWorlds){
			if(i == script.client.getCurrentWorld()){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * hop worlds
	 */
	public void switchWorlds() {
		while (script.interfaces.closeOpenInterface())
			try {
				MethodProvider.sleep(MethodProvider.random(500, 700));
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		this.script.tabs.open(Tab.FRIENDS);
		try {
			MethodProvider.sleep(MethodProvider.random(1000, 2000));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(isMem()){
		this.script.worldHopper.hop(memWorlds[MethodProvider.random(0,
				memWorlds.length - 1)]);
		}else{
			this.script.worldHopper.hop(f2pWorlds[MethodProvider.random(0,
				f2pWorlds.length - 1)]);
		}
	}

}
